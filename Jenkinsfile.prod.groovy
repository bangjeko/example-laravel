node {
    checkout scm
   
    docker.image('agung3wi/alpine-rsync:1.1').inside('-u root') {
        sshagent (credentials: ['kiw-apptas']) {
            sh 'mkdir -p ~/.ssh'
            sh 'ssh-keyscan -H "app.pttas.xyz" > ~/.ssh/known_hosts'
            sh "rsync -rav --delete ./ kiw@app.pttas.xyz:/home/kiw/hrm.kiw.center --exclude=.git --exclude=.env --exclude=storage --exclude=public --exclude=resources/views/home.blade.php"
            sh "ssh kiw@app.pttas.xyz 'cd /home/kiw/hrm.kiw.center && composer install'"
        }
    }
}