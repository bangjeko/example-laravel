<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateMasterPresencePermitCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_presence_permit_categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->boolean('reduce_quota_cuti')->nullable(true)->default(false);
            $table->integer('active')->nullable(true)->default('1');
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->timestampsTz($precision = 0);
        });
        $data = [
            [
                "name" => 'Ijin Sakit',
                "reduce_quota_cuti" => false,
            ],
            [
                "name" => 'Ijin Menjadi Wali Nikah',
                "reduce_quota_cuti" => false,
            ],
            [
                "name" => 'Ijin Menunggui Keluarga Sakit',
                "reduce_quota_cuti" => true,
            ],
            [
                "name" => 'Ijin Periksa Dokter / RS',
                "reduce_quota_cuti" => true,
            ],
            [
                "name" => 'Ijin Keperluan Keluarga',
                "reduce_quota_cuti" => true,
            ],
            [
                "name" => 'Lain - Lain',
                "reduce_quota_cuti" => true,
            ],
            [
                "name" => 'Cuti Tahunan',
                "reduce_quota_cuti" => true,
            ],
            [
                "name" => 'Cuti Melahirkan atau Keguguran Kandungan',
                "reduce_quota_cuti" => false,
            ],
            [
                "name" => 'Cuti Haid',
                "reduce_quota_cuti" => false,
            ],
        ];
        DB::table('master_presence_permit_categories')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_presence_permit_categories');
    }
}
