<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeSanksi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_sanksi', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('employee_id')->constrained('employees');
            $table->date('date');
            $table->foreignId('sanksi_type_id')->constrained('master_sanksi_types');
            $table->text('description')->nullable();
            $table->text('file_document')->nullable();
            $table->boolean('is_reduce_salary')->default(false);
            $table->float('sanksi_value', 65, 2)->nullable()->default(0);
            $table->float('base_payment_value', 65, 2)->nullable()->default(0);
            $table->integer('count_payment')->nullable()->default(0);
            $table->float('total_payment_value', 65, 2)->nullable()->default(0);
            $table->float('remaining_sanksi_value', 65, 2)->nullable()->default(0);
            $table->string('status_payment')->nullable()->default('not_finished_yet');

            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->timestampsTz($precision = 0);
        });

        $data = [
            [
                "id" => 1,
                "employee_id" => 66,
                "date" => "2022-01-01",
                "sanksi_type_id" => 1,
                "description" => null,
                "file_document" => null,
                "is_reduce_salary" => true,
                "sanksi_value" => 1100000,
                "base_payment_value" => 1100000,
                "count_payment" => 1,
                "total_payment_value" => 1100000,
                "remaining_sanksi_value" => 0,
                "status_payment" => "finished",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 14:45:45+00",
                "updated_at" => "2022-04-19 14:46:57+00"
            ],
            [
                "id" => 2,
                "employee_id" => 44,
                "date" => "2022-01-01",
                "sanksi_type_id" => 1,
                "description" => null,
                "file_document" => null,
                "is_reduce_salary" => true,
                "sanksi_value" => 1100000,
                "base_payment_value" => 1100000,
                "count_payment" => 1,
                "total_payment_value" => 1100000,
                "remaining_sanksi_value" => 0,
                "status_payment" => "finished",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 14:46:30+00",
                "updated_at" => "2022-04-19 14:47:02+00"
            ]
        ];

        DB::table('employee_sanksi')->insert($data);
        DB::statement("SELECT setval('employee_sanksi_id_seq', (SELECT MAX(id) FROM employee_sanksi)+1)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_sanksi');
    }
}
