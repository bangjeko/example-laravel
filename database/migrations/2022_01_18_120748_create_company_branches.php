<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyBranches extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_branches', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('company_branch_name');
            $table->string('company_type');
            $table->string('nomor_nib')->nullable();
            $table->string('nomor_npwp')->nullable();
            $table->text('primary_address');
            $table->text('secondary_address')->nullable();
            $table->string('postal_code')->nullable();
            $table->string('primary_telephone');
            $table->string('secondary_telephone')->nullable();
            $table->string('email');
            $table->string('url_website')->nullable();
            $table->text('img_logo');
            $table->integer('active')->nullable(true)->default('1');
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->timestampsTz($precision = 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_branches');
    }
}
