<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateMasterJobPositionCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_job_position_categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->unique();
            $table->foreignId('employment_level_id')->constrained('master_employment_levels');
            $table->integer('order_number')->nullable(true);
            $table->integer('active')->nullable(true)->default('1');
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->timestampsTz($precision = 0);
        });
        $data = [
            [
                "name" => 'Board Of Commisioners',
                "employment_level_id" => 1,
                "order_number" => 1,
            ],
            [
                "name" => 'Sekretaris Dewan Komisaris',
                "employment_level_id" => 1,
                "order_number" => 2,
            ],
            [
                "name" => 'Komite Audit',
                "employment_level_id" => 1,
                "order_number" => 3,
            ],
            [
                "name" => 'Komite Nominasi dan Remunerasi',
                "employment_level_id" => 1,
                "order_number" => 4,
            ],
            [
                "name" => 'Board Of Directors',
                "employment_level_id" => 1,
                "order_number" => 5,
            ],
            [
                "name" => 'Senior Division Head',
                "employment_level_id" => 2,
                "order_number" => 6,
            ],
            [
                "name" => 'Division Head',
                "employment_level_id" => 2,
                "order_number" => 7,
            ],
            [
                "name" => 'Department Head',
                "employment_level_id" => 2,
                "order_number" => 8,
            ],
            [
                "name" => 'Project Manager',
                "employment_level_id" => 2,
                "order_number" => 9,
            ],
            [
                "name" => 'Specialist',
                "employment_level_id" => 2,
                "order_number" => 10,
            ],
            [
                "name" => 'Pelaksana',
                "employment_level_id" => 2,
                "order_number" => 11,
            ],
            [
                "name" => 'Head Of KI Brebes',
                "employment_level_id" => 2,
                "order_number" => 12,
            ],
            [
                "name" => 'Staff KI Brebes',
                "employment_level_id" => 2,
                "order_number" => 13,
            ],
        ];
        DB::table('master_job_position_categories')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_job_position_categories');
    }
}
