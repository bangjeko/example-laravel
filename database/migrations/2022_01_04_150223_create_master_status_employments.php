<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateMasterStatusEmployments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_status_employments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->unique();
            $table->foreignId('employment_level_id')->constrained('master_employment_levels');
            $table->integer('order_number')->nullable(true);
            $table->integer('active')->nullable(true)->default('1');
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->timestampsTz($precision = 0);
        });

        $data = [
            [
                "name" => 'Management',
                "employment_level_id" => 1,
                "order_number" => '1',
            ],
            [
                "name" => 'Tenaga Kerja Tetap',
                "employment_level_id" => 2,
                "order_number" => '2',
            ],
            [
                "name" => 'Tenaga Kerja Kontrak',
                "employment_level_id" => 2,
                "order_number" => '3',
            ],
            [
                "name" => 'Tenaga Kerja Outsourching',
                "employment_level_id" => 2,
                "order_number" => '4',
            ],
            [
                "name" => 'Tenaga Kerja Penugasan',
                "employment_level_id" => 2,
                "order_number" => '5',
            ],

        ];
        DB::table('master_status_employments')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_status_employments');
    }
}
