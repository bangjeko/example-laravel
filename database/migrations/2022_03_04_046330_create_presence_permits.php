<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePresencePermits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('presence_permits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('employee_id')->constrained('employees');
            $table->foreignId('presence_permit_category_id')->constrained('master_presence_permit_categories');
            $table->string('title')->nullable();
            $table->text('reason');
            $table->date('start_date');
            $table->date('end_date');
            $table->integer('duration')->nullable(true)->default(0);
            $table->text('file_filename')->nullable();
            $table->string('status_code')->nullable();
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->timestampsTz($precision = 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('presence_permits');
    }
}
