<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyInventarisAssets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_inventaris_assets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('inventaris_asset_type_id')->nullable()->constrained('master_inventaris_asset_types');
            $table->string('asset_name');
            $table->string('asset_code')->nullable();
            $table->integer('asset_qty')->nullable();
            $table->text('asset_condition')->nullable();
            $table->string('pic')->nullable();
            $table->text('current_location')->nullable();
            $table->date('date_purchased')->nullable();
            $table->string('faktur_number')->nullable();
            $table->string('serial_number')->nullable();
            $table->float('price_purchased')->nullable();
            $table->text('img_photo_asset')->nullable();
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->timestampsTz($precision = 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_inventaris_assets');
    }
}
