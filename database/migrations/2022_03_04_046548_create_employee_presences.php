<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeePresences extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_presences', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('date');
            $table->foreignId('employee_id')->constrained('employees');
            $table->foreignId('working_category_id')->constrained('master_working_categories');
            $table->dateTimeTz('checkin', $precision = 0);
            $table->dateTimeTz('checkout', $precision = 0);
            $table->string('late')->nullable(true);
            $table->string('overtime')->nullable(true);
            $table->text('description')->nullable();
            $table->bigInteger('permit_id')->nullable();
            $table->string('status_presence')->nullable(true);
            $table->boolean('is_lock')->nullable(true)->default(false);
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->timestampsTz($precision = 0);

            $table->unique(array('date','employee_id'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_presences');
    }
}
