<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateConfigControlSheetVerificationOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('config_control_sheet_verification_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('role_id')->constrained('roles');
            $table->integer('verification_order');
            $table->integer('active')->nullable(true)->default('1');
            $table->timestampsTz($precision = 0);
        });

        $data = [
            [
                "role_id" => 200,
                "verification_order" => 1
            ],
            [
                "role_id" => 150,
                "verification_order" => 2
            ],
            [
                "role_id" => 100,
                "verification_order" => 3
            ],
        ];
        DB::table('config_control_sheet_verification_orders')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('config_control_sheet_verification_orders');
    }
}
