<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateCompanyOrganizationStructures extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_organization_structures', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('periode_year');
            $table->text('img_photo')->nullable();
            $table->text('description')->nullable();
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->timestampsTz($precision = 0);
        });
        $data = [
            [
                "periode_year" => '2022-01-01',
                "img_photo" => "/company_organization_structures/WhatsApp Image 2022-01-26 at 18.56.32(1)(4).jpeg",
                "description" => null,
            ],
        ];
        DB::table('company_organization_structures')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_organization_structures');
    }
}
