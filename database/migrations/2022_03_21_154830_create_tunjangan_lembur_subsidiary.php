<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateTunjanganLemburSubsidiary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tunjangan_lembur_subsidiary', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('tunjangan_lembur_detail_id')->constrained('tunjangan_lembur_details')->onDelete('cascade');
            $table->foreignId('salary_component_id')->constrained('master_salary_components');
            $table->float('jumlah_hari', 65, 2)->nullable(true)->default(1);
            $table->float('koefisien', 65, 2)->nullable(true)->default(1);
            $table->float('default_value', 65, 2)->nullable(true)->default(0);
            $table->float('component_value', 65, 2)->nullable(true)->default(0);
            $table->text('description')->nullable();
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->timestampsTz($precision = 0);

            $table->unique(array('tunjangan_lembur_detail_id', 'salary_component_id'));
        });

        $data = [
            [
                "id" => 11,
                "tunjangan_lembur_detail_id" => 6,
                "salary_component_id" => 6,
                "jumlah_hari" => 1,
                "koefisien" => 1,
                "default_value" => 25440,
                "component_value" => 25440,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:09:32+00"
            ],
            [
                "id" => 4,
                "tunjangan_lembur_detail_id" => 2,
                "salary_component_id" => 7,
                "jumlah_hari" => 0,
                "koefisien" => 1,
                "default_value" => 0,
                "component_value" => 0,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 10:45:55+00"
            ],
            [
                "id" => 3,
                "tunjangan_lembur_detail_id" => 2,
                "salary_component_id" => 6,
                "jumlah_hari" => 1,
                "koefisien" => 1,
                "default_value" => 31800,
                "component_value" => 31800,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 10:46:16+00"
            ],
            [
                "id" => 12,
                "tunjangan_lembur_detail_id" => 6,
                "salary_component_id" => 7,
                "jumlah_hari" => 2,
                "koefisien" => 1.5,
                "default_value" => 23000,
                "component_value" => 69000,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:09:32+00"
            ],
            [
                "id" => 27,
                "tunjangan_lembur_detail_id" => 15,
                "salary_component_id" => 6,
                "jumlah_hari" => 2,
                "koefisien" => 1,
                "default_value" => 25440,
                "component_value" => 50880,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:10:27+00"
            ],
            [
                "id" => 29,
                "tunjangan_lembur_detail_id" => 16,
                "salary_component_id" => 6,
                "jumlah_hari" => 2,
                "koefisien" => 1,
                "default_value" => 31800,
                "component_value" => 63600,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:51:35+00",
                "updated_at" => "2022-04-21 10:52:01+00"
            ],
            [
                "id" => 30,
                "tunjangan_lembur_detail_id" => 16,
                "salary_component_id" => 7,
                "jumlah_hari" => 2,
                "koefisien" => 1.5,
                "default_value" => 23000,
                "component_value" => 69000,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:51:35+00",
                "updated_at" => "2022-04-21 10:52:01+00"
            ],
            [
                "id" => 9,
                "tunjangan_lembur_detail_id" => 5,
                "salary_component_id" => 6,
                "jumlah_hari" => 0,
                "koefisien" => 1,
                "default_value" => 0,
                "component_value" => 0,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 10:53:44+00"
            ],
            [
                "id" => 10,
                "tunjangan_lembur_detail_id" => 5,
                "salary_component_id" => 7,
                "jumlah_hari" => 1,
                "koefisien" => 1.5,
                "default_value" => 23000,
                "component_value" => 34500,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 10:53:44+00"
            ],
            [
                "id" => 28,
                "tunjangan_lembur_detail_id" => 15,
                "salary_component_id" => 7,
                "jumlah_hari" => 4,
                "koefisien" => 1.5,
                "default_value" => 23000,
                "component_value" => 138000,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:10:27+00"
            ],
            [
                "id" => 19,
                "tunjangan_lembur_detail_id" => 11,
                "salary_component_id" => 6,
                "jumlah_hari" => 2,
                "koefisien" => 1,
                "default_value" => 25440,
                "component_value" => 50880,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:11:10+00"
            ],
            [
                "id" => 31,
                "tunjangan_lembur_detail_id" => 17,
                "salary_component_id" => 6,
                "jumlah_hari" => 2,
                "koefisien" => 1,
                "default_value" => 31800,
                "component_value" => 63600,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:58:39+00",
                "updated_at" => "2022-04-21 10:59:56+00"
            ],
            [
                "id" => 32,
                "tunjangan_lembur_detail_id" => 17,
                "salary_component_id" => 7,
                "jumlah_hari" => 5,
                "koefisien" => 1.5,
                "default_value" => 23000,
                "component_value" => 172500,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:58:39+00",
                "updated_at" => "2022-04-21 10:59:56+00"
            ],
            [
                "id" => 5,
                "tunjangan_lembur_detail_id" => 3,
                "salary_component_id" => 6,
                "jumlah_hari" => 2,
                "koefisien" => 1,
                "default_value" => 31800,
                "component_value" => 63600,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:01:18+00"
            ],
            [
                "id" => 6,
                "tunjangan_lembur_detail_id" => 3,
                "salary_component_id" => 7,
                "jumlah_hari" => 4,
                "koefisien" => 1.5,
                "default_value" => 23000,
                "component_value" => 138000,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:01:18+00"
            ],
            [
                "id" => 7,
                "tunjangan_lembur_detail_id" => 4,
                "salary_component_id" => 6,
                "jumlah_hari" => 2,
                "koefisien" => 1,
                "default_value" => 31800,
                "component_value" => 63600,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:02:15+00"
            ],
            [
                "id" => 20,
                "tunjangan_lembur_detail_id" => 11,
                "salary_component_id" => 7,
                "jumlah_hari" => 2,
                "koefisien" => 1.5,
                "default_value" => 23000,
                "component_value" => 69000,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:11:10+00"
            ],
            [
                "id" => 8,
                "tunjangan_lembur_detail_id" => 4,
                "salary_component_id" => 7,
                "jumlah_hari" => 3,
                "koefisien" => 1.5,
                "default_value" => 23000,
                "component_value" => 103500,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:04:17+00"
            ],
            [
                "id" => 15,
                "tunjangan_lembur_detail_id" => 9,
                "salary_component_id" => 6,
                "jumlah_hari" => 1,
                "koefisien" => 1,
                "default_value" => 31800,
                "component_value" => 31800,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:05:29+00"
            ],
            [
                "id" => 16,
                "tunjangan_lembur_detail_id" => 9,
                "salary_component_id" => 7,
                "jumlah_hari" => 3,
                "koefisien" => 1.5,
                "default_value" => 23000,
                "component_value" => 103500,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:05:29+00"
            ],
            [
                "id" => 25,
                "tunjangan_lembur_detail_id" => 14,
                "salary_component_id" => 6,
                "jumlah_hari" => 3,
                "koefisien" => 1,
                "default_value" => 31800,
                "component_value" => 95400,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:06:20+00"
            ],
            [
                "id" => 26,
                "tunjangan_lembur_detail_id" => 14,
                "salary_component_id" => 7,
                "jumlah_hari" => 2,
                "koefisien" => 1.5,
                "default_value" => 23000,
                "component_value" => 69000,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:06:20+00"
            ],
            [
                "id" => 23,
                "tunjangan_lembur_detail_id" => 13,
                "salary_component_id" => 6,
                "jumlah_hari" => 2,
                "koefisien" => 1,
                "default_value" => 31800,
                "component_value" => 63600,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:07:01+00"
            ],
            [
                "id" => 24,
                "tunjangan_lembur_detail_id" => 13,
                "salary_component_id" => 7,
                "jumlah_hari" => 2,
                "koefisien" => 1.5,
                "default_value" => 23000,
                "component_value" => 69000,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:07:01+00"
            ],
            [
                "id" => 13,
                "tunjangan_lembur_detail_id" => 8,
                "salary_component_id" => 6,
                "jumlah_hari" => 2,
                "koefisien" => 1,
                "default_value" => 31800,
                "component_value" => 63600,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:07:47+00"
            ],
            [
                "id" => 14,
                "tunjangan_lembur_detail_id" => 8,
                "salary_component_id" => 7,
                "jumlah_hari" => 2,
                "koefisien" => 1.5,
                "default_value" => 23000,
                "component_value" => 69000,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:07:47+00"
            ]
        ];
        DB::table('tunjangan_lembur_subsidiary')->insert($data);
        DB::statement("SELECT setval('tunjangan_lembur_subsidiary_id_seq', (SELECT MAX(id) FROM tunjangan_lembur_subsidiary)+1)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tunjangan_lembur_subsidiary');
    }
}
