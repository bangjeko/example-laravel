<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('role_code', 255)->unique();
            $table->string('role_name', 255);
            $table->foreignId('role_group_id')->nullable()->constrained('role_groups');
            $table->text('description')->nullable();
            $table->integer('active')->nullable(true)->default('1');
            $table->bigInteger('created_by')->nullable();
            $table->bigInteger('updated_by')->nullable();
            $table->timestampsTz($precision = 0);
        });
        $data = [
            [
                "id" => -1,
                "role_code" => "developer",
                "role_name" => "Developer",
                "role_group_id" => 1,
                "description" => "Developer"
            ],
            [
                "id" => 1,
                "role_code" => "super-admin-system",
                "role_name" => "Super Admin System",
                "role_group_id" => 2,
                "description" => "Super Admin System"
            ],
            [
                "id" => 10,
                "role_code" => "commisioner",
                "role_name" => "Komisioner",
                "role_group_id" => 10,
                "description" => null
            ],
            [
                "id" => 20,
                "role_code" => "director",
                "role_name" => "Direksi",
                "role_group_id" => 20,
                "description" => null
            ],
            [
                "id" => 100,
                "role_code" => "division-head-hcga",
                "role_name" => "Division Head HC/GA",
                "role_group_id" => 30,
                "description" => null
            ],
            [
                "id" => 150,
                "role_code" => "department-head-hcga",
                "role_name" => "Department Head HC/GA",
                "role_group_id" => 40,
                "description" => null
            ],
            [
                "id" => 200,
                "role_code" => "supervisor-hcga",
                "role_name" => "Supervisor HC/GA",
                "role_group_id" => 50,
                "description" => null
            ],
            [
                "id" => 250,
                "role_code" => "staff-hcga",
                "role_name" => "Staff HC/GA",
                "role_group_id" => 60,
                "description" => null
            ],
            [
                "id" => 300,
                "role_code" => "staff-koperasi",
                "role_name" => "Staff Koperasi",
                "role_group_id" => 60,
                "description" => null
            ],
            [
                "id" => 500,
                "role_code" => "employee",
                "role_name" => "Employee",
                "role_group_id" => 70,
                "description" => null
            ],
            [
                "id" => 1000,
                "role_code" => "job-applicant",
                "role_name" => "Pelamar Kerja",
                "role_group_id" => 100,
                "description" => null
            ],
        ];
        DB::table('roles')->insert($data);
        DB::statement("SELECT setval('roles_id_seq', (SELECT MAX(id) FROM roles)+1)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
