<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateMappingSalaryAccountProportions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mapping_salary_account_proportions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('mapping_salary_account_id')->constrained('mapping_salary_accounts')->onDelete('cascade');
            $table->foreignId('fina_salary_account_id')->nullable()->constrained('fina_salary_accounts');
            $table->float('proportion_value')->nullable(true)->default(0);
            $table->integer('active')->nullable(true)->default('1');
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->timestampsTz($precision = 0);

            $table->unique(array('mapping_salary_account_id', 'fina_salary_account_id'));
        });

        $data = [
            [
                "id" => 2,
                "mapping_salary_account_id" => 2,
                "fina_salary_account_id" => 1110,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-27 16:56:29+07",
                "updated_at" => "2022-04-27 16:56:29+07"
            ],
            [
                "id" => 3,
                "mapping_salary_account_id" => 3,
                "fina_salary_account_id" => 1176,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-27 16:57:02+07",
                "updated_at" => "2022-04-27 16:57:02+07"
            ],
            [
                "id" => 4,
                "mapping_salary_account_id" => 4,
                "fina_salary_account_id" => 1176,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-27 16:57:36+07",
                "updated_at" => "2022-04-27 16:57:36+07"
            ],
            [
                "id" => 5,
                "mapping_salary_account_id" => 5,
                "fina_salary_account_id" => 1143,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-27 16:58:00+07",
                "updated_at" => "2022-04-27 16:58:00+07"
            ],
            [
                "id" => 6,
                "mapping_salary_account_id" => 6,
                "fina_salary_account_id" => 1209,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-27 16:59:46+07",
                "updated_at" => "2022-04-27 16:59:46+07"
            ],
            [
                "id" => 7,
                "mapping_salary_account_id" => 7,
                "fina_salary_account_id" => 1209,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-27 17:00:26+07",
                "updated_at" => "2022-04-27 17:00:26+07"
            ],
            [
                "id" => 55,
                "mapping_salary_account_id" => 52,
                "fina_salary_account_id" => 1440,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 17:14:47+07",
                "updated_at" => "2022-05-10 17:14:47+07"
            ],
            [
                "id" => 8,
                "mapping_salary_account_id" => 1,
                "fina_salary_account_id" => 440,
                "proportion_value" => 0,
                "active" => 0,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-28 14:34:45+07",
                "updated_at" => "2022-04-28 15:10:45+07"
            ],
            [
                "id" => 1,
                "mapping_salary_account_id" => 1,
                "fina_salary_account_id" => 1077,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-27 16:51:42+07",
                "updated_at" => "2022-04-28 15:10:52+07"
            ],
            [
                "id" => 9,
                "mapping_salary_account_id" => 8,
                "fina_salary_account_id" => 1242,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-28 16:39:20+07",
                "updated_at" => "2022-04-28 16:39:20+07"
            ],
            [
                "id" => 10,
                "mapping_salary_account_id" => 9,
                "fina_salary_account_id" => 1308,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-28 16:40:03+07",
                "updated_at" => "2022-04-28 16:40:03+07"
            ],
            [
                "id" => 12,
                "mapping_salary_account_id" => 11,
                "fina_salary_account_id" => 1341,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-28 16:41:24+07",
                "updated_at" => "2022-04-28 16:41:24+07"
            ],
            [
                "id" => 13,
                "mapping_salary_account_id" => 12,
                "fina_salary_account_id" => 1110,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-28 16:44:51+07",
                "updated_at" => "2022-04-28 16:44:51+07"
            ],
            [
                "id" => 14,
                "mapping_salary_account_id" => 13,
                "fina_salary_account_id" => 2034,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-28 16:49:06+07",
                "updated_at" => "2022-04-28 16:49:06+07"
            ],
            [
                "id" => 15,
                "mapping_salary_account_id" => 14,
                "fina_salary_account_id" => 2034,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-28 16:49:34+07",
                "updated_at" => "2022-04-28 16:49:34+07"
            ],
            [
                "id" => 16,
                "mapping_salary_account_id" => 15,
                "fina_salary_account_id" => 2034,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-28 16:50:02+07",
                "updated_at" => "2022-04-28 16:50:02+07"
            ],
            [
                "id" => 17,
                "mapping_salary_account_id" => 16,
                "fina_salary_account_id" => 2034,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-28 16:50:28+07",
                "updated_at" => "2022-04-28 16:50:28+07"
            ],
            [
                "id" => 18,
                "mapping_salary_account_id" => 17,
                "fina_salary_account_id" => 2034,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-28 16:50:53+07",
                "updated_at" => "2022-04-28 16:50:53+07"
            ],
            [
                "id" => 19,
                "mapping_salary_account_id" => 18,
                "fina_salary_account_id" => 2034,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-28 16:51:10+07",
                "updated_at" => "2022-04-28 16:51:10+07"
            ],
            [
                "id" => 20,
                "mapping_salary_account_id" => 19,
                "fina_salary_account_id" => 5169,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-28 16:53:44+07",
                "updated_at" => "2022-04-28 16:53:44+07"
            ],
            [
                "id" => 21,
                "mapping_salary_account_id" => 20,
                "fina_salary_account_id" => 1407,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-28 16:54:37+07",
                "updated_at" => "2022-04-28 16:54:37+07"
            ],
            [
                "id" => 22,
                "mapping_salary_account_id" => 21,
                "fina_salary_account_id" => 1407,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-28 16:55:03+07",
                "updated_at" => "2022-04-28 16:55:03+07"
            ],
            [
                "id" => 23,
                "mapping_salary_account_id" => 22,
                "fina_salary_account_id" => 5169,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-28 16:55:17+07",
                "updated_at" => "2022-04-28 16:55:17+07"
            ],
            [
                "id" => 24,
                "mapping_salary_account_id" => 23,
                "fina_salary_account_id" => 1539,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-28 16:55:39+07",
                "updated_at" => "2022-04-28 16:55:39+07"
            ],
            [
                "id" => 25,
                "mapping_salary_account_id" => 24,
                "fina_salary_account_id" => 2628,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-28 16:56:35+07",
                "updated_at" => "2022-04-28 16:56:35+07"
            ],
            [
                "id" => 26,
                "mapping_salary_account_id" => 25,
                "fina_salary_account_id" => 1539,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-28 17:16:17+07",
                "updated_at" => "2022-04-28 17:16:17+07"
            ],
            [
                "id" => 27,
                "mapping_salary_account_id" => 26,
                "fina_salary_account_id" => 2562,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-28 17:16:37+07",
                "updated_at" => "2022-04-28 17:16:37+07"
            ],
            [
                "id" => 28,
                "mapping_salary_account_id" => 28,
                "fina_salary_account_id" => 2100,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-28 17:17:29+07",
                "updated_at" => "2022-04-28 17:17:29+07"
            ],
            [
                "id" => 29,
                "mapping_salary_account_id" => 29,
                "fina_salary_account_id" => 1605,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-28 17:18:07+07",
                "updated_at" => "2022-04-28 17:18:07+07"
            ],
            [
                "id" => 30,
                "mapping_salary_account_id" => 30,
                "fina_salary_account_id" => 2463,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-28 17:22:49+07",
                "updated_at" => "2022-04-28 17:22:49+07"
            ],
            [
                "id" => 56,
                "mapping_salary_account_id" => 53,
                "fina_salary_account_id" => 1440,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 17:14:59+07",
                "updated_at" => "2022-05-10 17:14:59+07"
            ],
            [
                "id" => 31,
                "mapping_salary_account_id" => 31,
                "fina_salary_account_id" => 2496,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 14:20:28+07",
                "updated_at" => "2022-05-10 14:20:34+07"
            ],
            [
                "id" => 32,
                "mapping_salary_account_id" => 32,
                "fina_salary_account_id" => 2562,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 14:22:02+07",
                "updated_at" => "2022-05-10 14:22:02+07"
            ],
            [
                "id" => 33,
                "mapping_salary_account_id" => 33,
                "fina_salary_account_id" => 2595,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 14:23:13+07",
                "updated_at" => "2022-05-10 14:23:13+07"
            ],
            [
                "id" => 34,
                "mapping_salary_account_id" => 34,
                "fina_salary_account_id" => 2133,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 14:23:28+07",
                "updated_at" => "2022-05-10 14:23:28+07"
            ],
            [
                "id" => 35,
                "mapping_salary_account_id" => 35,
                "fina_salary_account_id" => 2661,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 14:24:40+07",
                "updated_at" => "2022-05-10 14:24:40+07"
            ],
            [
                "id" => 36,
                "mapping_salary_account_id" => 36,
                "fina_salary_account_id" => 2265,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 14:25:00+07",
                "updated_at" => "2022-05-10 14:25:00+07"
            ],
            [
                "id" => 37,
                "mapping_salary_account_id" => 37,
                "fina_salary_account_id" => 1605,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 14:26:57+07",
                "updated_at" => "2022-05-10 14:26:57+07"
            ],
            [
                "id" => 38,
                "mapping_salary_account_id" => 38,
                "fina_salary_account_id" => 2100,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 14:29:29+07",
                "updated_at" => "2022-05-10 14:29:29+07"
            ],
            [
                "id" => 39,
                "mapping_salary_account_id" => 39,
                "fina_salary_account_id" => 1539,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 14:30:29+07",
                "updated_at" => "2022-05-10 14:30:29+07"
            ],
            [
                "id" => 40,
                "mapping_salary_account_id" => 40,
                "fina_salary_account_id" => 1539,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 14:31:06+07",
                "updated_at" => "2022-05-10 14:31:06+07"
            ],
            [
                "id" => 41,
                "mapping_salary_account_id" => 41,
                "fina_salary_account_id" => 1539,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 14:52:20+07",
                "updated_at" => "2022-05-10 14:52:20+07"
            ],
            [
                "id" => 57,
                "mapping_salary_account_id" => 54,
                "fina_salary_account_id" => 440,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 17:22:43+07",
                "updated_at" => "2022-05-10 17:22:43+07"
            ],
            [
                "id" => 58,
                "mapping_salary_account_id" => 55,
                "fina_salary_account_id" => 440,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 17:23:24+07",
                "updated_at" => "2022-05-10 17:23:24+07"
            ],
            [
                "id" => 59,
                "mapping_salary_account_id" => 56,
                "fina_salary_account_id" => 440,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 17:28:01+07",
                "updated_at" => "2022-05-10 17:28:01+07"
            ],
            [
                "id" => 60,
                "mapping_salary_account_id" => 57,
                "fina_salary_account_id" => 440,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 17:28:11+07",
                "updated_at" => "2022-05-10 17:28:11+07"
            ],
            [
                "id" => 61,
                "mapping_salary_account_id" => 58,
                "fina_salary_account_id" => 440,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 17:28:37+07",
                "updated_at" => "2022-05-10 17:28:37+07"
            ],
            [
                "id" => 62,
                "mapping_salary_account_id" => 59,
                "fina_salary_account_id" => 440,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 17:28:46+07",
                "updated_at" => "2022-05-10 17:28:46+07"
            ],
            [
                "id" => 63,
                "mapping_salary_account_id" => 60,
                "fina_salary_account_id" => 440,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 17:29:05+07",
                "updated_at" => "2022-05-10 17:29:05+07"
            ],
            [
                "id" => 43,
                "mapping_salary_account_id" => 42,
                "fina_salary_account_id" => 817,
                "proportion_value" => 0,
                "active" => 0,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 16:24:07+07",
                "updated_at" => "2022-05-10 16:25:55+07"
            ],
            [
                "id" => 64,
                "mapping_salary_account_id" => 61,
                "fina_salary_account_id" => 440,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 17:29:12+07",
                "updated_at" => "2022-05-10 17:29:12+07"
            ],
            [
                "id" => 65,
                "mapping_salary_account_id" => 62,
                "fina_salary_account_id" => 440,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 17:29:32+07",
                "updated_at" => "2022-05-10 17:29:32+07"
            ],
            [
                "id" => 44,
                "mapping_salary_account_id" => 10,
                "fina_salary_account_id" => 1539,
                "proportion_value" => 0,
                "active" => 0,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 16:28:20+07",
                "updated_at" => "2022-05-10 16:29:32+07"
            ],
            [
                "id" => 11,
                "mapping_salary_account_id" => 10,
                "fina_salary_account_id" => 1275,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-28 16:40:47+07",
                "updated_at" => "2022-05-10 16:29:46+07"
            ],
            [
                "id" => 66,
                "mapping_salary_account_id" => 63,
                "fina_salary_account_id" => 440,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 17:29:39+07",
                "updated_at" => "2022-05-10 17:29:39+07"
            ],
            [
                "id" => 42,
                "mapping_salary_account_id" => 42,
                "fina_salary_account_id" => 1539,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 15:56:00+07",
                "updated_at" => "2022-05-10 16:50:03+07"
            ],
            [
                "id" => 45,
                "mapping_salary_account_id" => 43,
                "fina_salary_account_id" => 439,
                "proportion_value" => 0,
                "active" => 0,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 16:50:53+07",
                "updated_at" => "2022-05-10 16:53:13+07"
            ],
            [
                "id" => 46,
                "mapping_salary_account_id" => 43,
                "fina_salary_account_id" => 1539,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 16:53:20+07",
                "updated_at" => "2022-05-10 16:53:20+07"
            ],
            [
                "id" => 47,
                "mapping_salary_account_id" => 44,
                "fina_salary_account_id" => 1671,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 17:03:49+07",
                "updated_at" => "2022-05-10 17:03:49+07"
            ],
            [
                "id" => 48,
                "mapping_salary_account_id" => 45,
                "fina_salary_account_id" => 1440,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 17:06:07+07",
                "updated_at" => "2022-05-10 17:06:07+07"
            ],
            [
                "id" => 49,
                "mapping_salary_account_id" => 46,
                "fina_salary_account_id" => 1440,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 17:06:40+07",
                "updated_at" => "2022-05-10 17:06:40+07"
            ],
            [
                "id" => 50,
                "mapping_salary_account_id" => 47,
                "fina_salary_account_id" => 1440,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 17:07:13+07",
                "updated_at" => "2022-05-10 17:07:13+07"
            ],
            [
                "id" => 51,
                "mapping_salary_account_id" => 48,
                "fina_salary_account_id" => 1440,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 17:07:37+07",
                "updated_at" => "2022-05-10 17:07:37+07"
            ],
            [
                "id" => 52,
                "mapping_salary_account_id" => 49,
                "fina_salary_account_id" => 1440,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 17:08:04+07",
                "updated_at" => "2022-05-10 17:08:04+07"
            ],
            [
                "id" => 53,
                "mapping_salary_account_id" => 50,
                "fina_salary_account_id" => 1440,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 17:08:20+07",
                "updated_at" => "2022-05-10 17:08:20+07"
            ],
            [
                "id" => 54,
                "mapping_salary_account_id" => 51,
                "fina_salary_account_id" => 1440,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 17:13:35+07",
                "updated_at" => "2022-05-10 17:13:35+07"
            ],
            [
                "id" => 67,
                "mapping_salary_account_id" => 64,
                "fina_salary_account_id" => 440,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 17:30:04+07",
                "updated_at" => "2022-05-10 17:30:04+07"
            ],
            [
                "id" => 68,
                "mapping_salary_account_id" => 65,
                "fina_salary_account_id" => 440,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 17:30:11+07",
                "updated_at" => "2022-05-10 17:30:11+07"
            ],
            [
                "id" => 69,
                "mapping_salary_account_id" => 47,
                "fina_salary_account_id" => 440,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 17:34:27+07",
                "updated_at" => "2022-05-10 17:34:27+07"
            ],
            [
                "id" => 70,
                "mapping_salary_account_id" => 46,
                "fina_salary_account_id" => 440,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 17:35:27+07",
                "updated_at" => "2022-05-10 17:35:27+07"
            ],
            [
                "id" => 71,
                "mapping_salary_account_id" => 48,
                "fina_salary_account_id" => 440,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 17:35:54+07",
                "updated_at" => "2022-05-10 17:35:54+07"
            ],
            [
                "id" => 72,
                "mapping_salary_account_id" => 49,
                "fina_salary_account_id" => 440,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 17:36:09+07",
                "updated_at" => "2022-05-10 17:36:09+07"
            ],
            [
                "id" => 73,
                "mapping_salary_account_id" => 52,
                "fina_salary_account_id" => 440,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 17:36:26+07",
                "updated_at" => "2022-05-10 17:36:26+07"
            ],
            [
                "id" => 74,
                "mapping_salary_account_id" => 53,
                "fina_salary_account_id" => 440,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 17:36:37+07",
                "updated_at" => "2022-05-10 17:36:37+07"
            ],
            [
                "id" => 76,
                "mapping_salary_account_id" => 66,
                "fina_salary_account_id" => 1836,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 17:39:53+07",
                "updated_at" => "2022-05-10 17:39:53+07"
            ],
            [
                "id" => 77,
                "mapping_salary_account_id" => 67,
                "fina_salary_account_id" => 1836,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 17:40:06+07",
                "updated_at" => "2022-05-10 17:40:06+07"
            ],
            [
                "id" => 78,
                "mapping_salary_account_id" => 68,
                "fina_salary_account_id" => 5169,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 17:41:15+07",
                "updated_at" => "2022-05-10 17:41:15+07"
            ],
            [
                "id" => 79,
                "mapping_salary_account_id" => 69,
                "fina_salary_account_id" => 2166,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 17:42:35+07",
                "updated_at" => "2022-05-10 17:42:35+07"
            ],
            [
                "id" => 80,
                "mapping_salary_account_id" => 70,
                "fina_salary_account_id" => 4674,
                "proportion_value" => 0,
                "active" => 0,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 17:44:12+07",
                "updated_at" => "2022-05-10 17:47:39+07"
            ],
            [
                "id" => 75,
                "mapping_salary_account_id" => 51,
                "fina_salary_account_id" => 440,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 17:37:27+07",
                "updated_at" => "2022-05-11 14:44:53+07"
            ],
            [
                "id" => 83,
                "mapping_salary_account_id" => 73,
                "fina_salary_account_id" => 4674,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 17:46:16+07",
                "updated_at" => "2022-05-10 17:46:16+07"
            ],
            [
                "id" => 82,
                "mapping_salary_account_id" => 72,
                "fina_salary_account_id" => 4674,
                "proportion_value" => 0,
                "active" => 0,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 17:45:32+07",
                "updated_at" => "2022-05-10 17:46:28+07"
            ],
            [
                "id" => 84,
                "mapping_salary_account_id" => 74,
                "fina_salary_account_id" => 4674,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 17:46:49+07",
                "updated_at" => "2022-05-10 17:46:49+07"
            ],
            [
                "id" => 81,
                "mapping_salary_account_id" => 71,
                "fina_salary_account_id" => 4674,
                "proportion_value" => 0,
                "active" => 0,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 17:44:30+07",
                "updated_at" => "2022-05-10 17:47:07+07"
            ],
            [
                "id" => 85,
                "mapping_salary_account_id" => 75,
                "fina_salary_account_id" => 4674,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 17:47:28+07",
                "updated_at" => "2022-05-10 17:47:28+07"
            ],
            [
                "id" => 86,
                "mapping_salary_account_id" => 76,
                "fina_salary_account_id" => 4674,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-10 17:47:52+07",
                "updated_at" => "2022-05-10 17:47:52+07"
            ],
            [
                "id" => 87,
                "mapping_salary_account_id" => 77,
                "fina_salary_account_id" => 1473,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 11:39:19+07",
                "updated_at" => "2022-05-11 11:39:19+07"
            ],
            [
                "id" => 88,
                "mapping_salary_account_id" => 78,
                "fina_salary_account_id" => 1473,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 11:39:43+07",
                "updated_at" => "2022-05-11 11:39:43+07"
            ],
            [
                "id" => 89,
                "mapping_salary_account_id" => 79,
                "fina_salary_account_id" => 1473,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 11:40:06+07",
                "updated_at" => "2022-05-11 11:40:06+07"
            ],
            [
                "id" => 90,
                "mapping_salary_account_id" => 80,
                "fina_salary_account_id" => 1473,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 11:40:37+07",
                "updated_at" => "2022-05-11 11:40:37+07"
            ],
            [
                "id" => 91,
                "mapping_salary_account_id" => 81,
                "fina_salary_account_id" => 1473,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 11:40:56+07",
                "updated_at" => "2022-05-11 11:40:56+07"
            ],
            [
                "id" => 92,
                "mapping_salary_account_id" => 82,
                "fina_salary_account_id" => 1473,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 11:41:31+07",
                "updated_at" => "2022-05-11 11:41:31+07"
            ],
            [
                "id" => 134,
                "mapping_salary_account_id" => 118,
                "fina_salary_account_id" => 439,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 15:05:17+07",
                "updated_at" => "2022-05-11 15:05:17+07"
            ],
            [
                "id" => 135,
                "mapping_salary_account_id" => 119,
                "fina_salary_account_id" => 439,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 15:05:34+07",
                "updated_at" => "2022-05-11 15:05:34+07"
            ],
            [
                "id" => 136,
                "mapping_salary_account_id" => 120,
                "fina_salary_account_id" => 439,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 15:05:53+07",
                "updated_at" => "2022-05-11 15:05:53+07"
            ],
            [
                "id" => 137,
                "mapping_salary_account_id" => 121,
                "fina_salary_account_id" => 439,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 15:06:46+07",
                "updated_at" => "2022-05-11 15:06:46+07"
            ],
            [
                "id" => 138,
                "mapping_salary_account_id" => 85,
                "fina_salary_account_id" => 439,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 15:08:47+07",
                "updated_at" => "2022-05-11 15:08:47+07"
            ],
            [
                "id" => 93,
                "mapping_salary_account_id" => 83,
                "fina_salary_account_id" => 1473,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 11:42:08+07",
                "updated_at" => "2022-05-11 11:43:19+07"
            ],
            [
                "id" => 94,
                "mapping_salary_account_id" => 84,
                "fina_salary_account_id" => 1473,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 11:43:42+07",
                "updated_at" => "2022-05-11 11:43:42+07"
            ],
            [
                "id" => 95,
                "mapping_salary_account_id" => 85,
                "fina_salary_account_id" => 1473,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 11:43:57+07",
                "updated_at" => "2022-05-11 11:43:57+07"
            ],
            [
                "id" => 96,
                "mapping_salary_account_id" => 86,
                "fina_salary_account_id" => 1506,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 14:08:18+07",
                "updated_at" => "2022-05-11 14:08:18+07"
            ],
            [
                "id" => 97,
                "mapping_salary_account_id" => 87,
                "fina_salary_account_id" => 1506,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 14:08:46+07",
                "updated_at" => "2022-05-11 14:08:46+07"
            ],
            [
                "id" => 98,
                "mapping_salary_account_id" => 88,
                "fina_salary_account_id" => 1506,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 14:08:56+07",
                "updated_at" => "2022-05-11 14:08:56+07"
            ],
            [
                "id" => 99,
                "mapping_salary_account_id" => 89,
                "fina_salary_account_id" => 1506,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 14:09:13+07",
                "updated_at" => "2022-05-11 14:09:13+07"
            ],
            [
                "id" => 100,
                "mapping_salary_account_id" => 90,
                "fina_salary_account_id" => 1506,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 14:10:44+07",
                "updated_at" => "2022-05-11 14:10:44+07"
            ],
            [
                "id" => 101,
                "mapping_salary_account_id" => 91,
                "fina_salary_account_id" => 1506,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 14:10:52+07",
                "updated_at" => "2022-05-11 14:10:52+07"
            ],
            [
                "id" => 102,
                "mapping_salary_account_id" => 92,
                "fina_salary_account_id" => 1506,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 14:11:08+07",
                "updated_at" => "2022-05-11 14:11:08+07"
            ],
            [
                "id" => 103,
                "mapping_salary_account_id" => 93,
                "fina_salary_account_id" => 1506,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 14:11:15+07",
                "updated_at" => "2022-05-11 14:11:15+07"
            ],
            [
                "id" => 104,
                "mapping_salary_account_id" => 94,
                "fina_salary_account_id" => 1506,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 14:11:30+07",
                "updated_at" => "2022-05-11 14:11:30+07"
            ],
            [
                "id" => 105,
                "mapping_salary_account_id" => 95,
                "fina_salary_account_id" => 1506,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 14:11:37+07",
                "updated_at" => "2022-05-11 14:11:37+07"
            ],
            [
                "id" => 106,
                "mapping_salary_account_id" => 96,
                "fina_salary_account_id" => 1506,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 14:11:54+07",
                "updated_at" => "2022-05-11 14:11:54+07"
            ],
            [
                "id" => 107,
                "mapping_salary_account_id" => 97,
                "fina_salary_account_id" => 1506,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 14:12:00+07",
                "updated_at" => "2022-05-11 14:12:00+07"
            ],
            [
                "id" => 108,
                "mapping_salary_account_id" => 98,
                "fina_salary_account_id" => 437,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 14:21:20+07",
                "updated_at" => "2022-05-11 14:21:20+07"
            ],
            [
                "id" => 109,
                "mapping_salary_account_id" => 99,
                "fina_salary_account_id" => 437,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 14:21:29+07",
                "updated_at" => "2022-05-11 14:21:29+07"
            ],
            [
                "id" => 112,
                "mapping_salary_account_id" => 100,
                "fina_salary_account_id" => 437,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 14:22:17+07",
                "updated_at" => "2022-05-11 14:22:17+07"
            ],
            [
                "id" => 113,
                "mapping_salary_account_id" => 101,
                "fina_salary_account_id" => 437,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 14:22:25+07",
                "updated_at" => "2022-05-11 14:22:25+07"
            ],
            [
                "id" => 115,
                "mapping_salary_account_id" => 102,
                "fina_salary_account_id" => 437,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 14:22:51+07",
                "updated_at" => "2022-05-11 14:22:51+07"
            ],
            [
                "id" => 116,
                "mapping_salary_account_id" => 103,
                "fina_salary_account_id" => 437,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 14:22:56+07",
                "updated_at" => "2022-05-11 14:22:56+07"
            ],
            [
                "id" => 118,
                "mapping_salary_account_id" => 104,
                "fina_salary_account_id" => 437,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 14:23:18+07",
                "updated_at" => "2022-05-11 14:23:18+07"
            ],
            [
                "id" => 119,
                "mapping_salary_account_id" => 105,
                "fina_salary_account_id" => 437,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 14:23:24+07",
                "updated_at" => "2022-05-11 14:23:24+07"
            ],
            [
                "id" => 121,
                "mapping_salary_account_id" => 106,
                "fina_salary_account_id" => 437,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 14:23:48+07",
                "updated_at" => "2022-05-11 14:23:48+07"
            ],
            [
                "id" => 122,
                "mapping_salary_account_id" => 107,
                "fina_salary_account_id" => 437,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 14:23:54+07",
                "updated_at" => "2022-05-11 14:23:54+07"
            ],
            [
                "id" => 124,
                "mapping_salary_account_id" => 108,
                "fina_salary_account_id" => 437,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 14:24:10+07",
                "updated_at" => "2022-05-11 14:24:10+07"
            ],
            [
                "id" => 125,
                "mapping_salary_account_id" => 109,
                "fina_salary_account_id" => 437,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 14:24:15+07",
                "updated_at" => "2022-05-11 14:24:15+07"
            ],
            [
                "id" => 139,
                "mapping_salary_account_id" => 84,
                "fina_salary_account_id" => 439,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 15:09:05+07",
                "updated_at" => "2022-05-11 15:09:05+07"
            ],
            [
                "id" => 140,
                "mapping_salary_account_id" => 83,
                "fina_salary_account_id" => 439,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 15:09:22+07",
                "updated_at" => "2022-05-11 15:09:22+07"
            ],
            [
                "id" => 141,
                "mapping_salary_account_id" => 82,
                "fina_salary_account_id" => 439,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 15:09:40+07",
                "updated_at" => "2022-05-11 15:09:40+07"
            ],
            [
                "id" => 142,
                "mapping_salary_account_id" => 81,
                "fina_salary_account_id" => 439,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 15:09:56+07",
                "updated_at" => "2022-05-11 15:09:56+07"
            ],
            [
                "id" => 143,
                "mapping_salary_account_id" => 80,
                "fina_salary_account_id" => 439,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 15:10:21+07",
                "updated_at" => "2022-05-11 15:10:21+07"
            ],
            [
                "id" => 144,
                "mapping_salary_account_id" => 122,
                "fina_salary_account_id" => 439,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 15:10:41+07",
                "updated_at" => "2022-05-11 15:10:41+07"
            ],
            [
                "id" => 110,
                "mapping_salary_account_id" => 96,
                "fina_salary_account_id" => 437,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 14:21:46+07",
                "updated_at" => "2022-05-11 14:28:25+07"
            ],
            [
                "id" => 111,
                "mapping_salary_account_id" => 94,
                "fina_salary_account_id" => 437,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 14:22:10+07",
                "updated_at" => "2022-05-11 14:28:42+07"
            ],
            [
                "id" => 114,
                "mapping_salary_account_id" => 92,
                "fina_salary_account_id" => 437,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 14:22:43+07",
                "updated_at" => "2022-05-11 14:28:55+07"
            ],
            [
                "id" => 117,
                "mapping_salary_account_id" => 90,
                "fina_salary_account_id" => 437,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 14:23:12+07",
                "updated_at" => "2022-05-11 14:29:08+07"
            ],
            [
                "id" => 120,
                "mapping_salary_account_id" => 87,
                "fina_salary_account_id" => 437,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 14:23:43+07",
                "updated_at" => "2022-05-11 14:29:20+07"
            ],
            [
                "id" => 123,
                "mapping_salary_account_id" => 86,
                "fina_salary_account_id" => 437,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 14:24:05+07",
                "updated_at" => "2022-05-11 14:29:28+07"
            ],
            [
                "id" => 145,
                "mapping_salary_account_id" => 79,
                "fina_salary_account_id" => 439,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 15:10:59+07",
                "updated_at" => "2022-05-11 15:10:59+07"
            ],
            [
                "id" => 126,
                "mapping_salary_account_id" => 110,
                "fina_salary_account_id" => 440,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 14:41:24+07",
                "updated_at" => "2022-05-11 14:54:14+07"
            ],
            [
                "id" => 127,
                "mapping_salary_account_id" => 111,
                "fina_salary_account_id" => 439,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 14:58:08+07",
                "updated_at" => "2022-05-11 14:58:08+07"
            ],
            [
                "id" => 128,
                "mapping_salary_account_id" => 112,
                "fina_salary_account_id" => 439,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 14:58:51+07",
                "updated_at" => "2022-05-11 14:58:51+07"
            ],
            [
                "id" => 129,
                "mapping_salary_account_id" => 113,
                "fina_salary_account_id" => 439,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 14:59:14+07",
                "updated_at" => "2022-05-11 14:59:14+07"
            ],
            [
                "id" => 130,
                "mapping_salary_account_id" => 114,
                "fina_salary_account_id" => 439,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 14:59:46+07",
                "updated_at" => "2022-05-11 14:59:46+07"
            ],
            [
                "id" => 131,
                "mapping_salary_account_id" => 115,
                "fina_salary_account_id" => 439,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 15:00:08+07",
                "updated_at" => "2022-05-11 15:00:08+07"
            ],
            [
                "id" => 132,
                "mapping_salary_account_id" => 116,
                "fina_salary_account_id" => 439,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 15:00:30+07",
                "updated_at" => "2022-05-11 15:00:30+07"
            ],
            [
                "id" => 133,
                "mapping_salary_account_id" => 117,
                "fina_salary_account_id" => 439,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 15:01:19+07",
                "updated_at" => "2022-05-11 15:01:19+07"
            ],
            [
                "id" => 146,
                "mapping_salary_account_id" => 78,
                "fina_salary_account_id" => 439,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 15:11:16+07",
                "updated_at" => "2022-05-11 15:11:16+07"
            ],
            [
                "id" => 147,
                "mapping_salary_account_id" => 77,
                "fina_salary_account_id" => 439,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 15:11:39+07",
                "updated_at" => "2022-05-11 15:11:39+07"
            ],
            [
                "id" => 148,
                "mapping_salary_account_id" => 123,
                "fina_salary_account_id" => 439,
                "proportion_value" => 100,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-05-11 15:11:55+07",
                "updated_at" => "2022-05-11 15:11:55+07"
            ]
        ];
        DB::table('mapping_salary_account_proportions')->insert($data);
        DB::statement("SELECT setval('mapping_salary_account_proportions_id_seq', (SELECT MAX(id) FROM mapping_salary_account_proportions)+1)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mapping_salary_account_proportions');
    }
}
