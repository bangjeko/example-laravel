<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateBpjsKesBillDetailTambahan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bpjs_kes_bill_detail_tambahan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('bpjs_kes_bill_detail_id')->constrained('bpjs_kes_bill_details')->onDelete('cascade');
            $table->foreignId('employee_family_id')->constrained('employee_families');
            $table->float('iuran_value', 65, 2)->nullable(true)->default(0);
            $table->text('description')->nullable();
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->timestampsTz($precision = 0);
            $table->unique(array('bpjs_kes_bill_detail_id', 'employee_family_id'));
        });

        $data = [
            [
                "id" => 19,
                "bpjs_kes_bill_detail_id" => 57,
                "employee_family_id" => 10,
                "iuran_value" => 110194,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 21:20:47+00",
                "updated_at" => "2022-04-19 14:28:03+00"
            ],
            [
                "id" => 7,
                "bpjs_kes_bill_detail_id" => 35,
                "employee_family_id" => 2,
                "iuran_value" => 117532,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 21:20:47+00",
                "updated_at" => "2022-04-19 14:30:02+00"
            ],
            [
                "id" => 8,
                "bpjs_kes_bill_detail_id" => 35,
                "employee_family_id" => 3,
                "iuran_value" => 117532,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 21:20:47+00",
                "updated_at" => "2022-04-19 14:30:02+00"
            ],
            [
                "id" => 13,
                "bpjs_kes_bill_detail_id" => 43,
                "employee_family_id" => 11,
                "iuran_value" => 119014,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 21:20:47+00",
                "updated_at" => "2022-04-21 08:35:05+00"
            ],
            [
                "id" => 1,
                "bpjs_kes_bill_detail_id" => 23,
                "employee_family_id" => 9,
                "iuran_value" => 110937,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 21:20:47+00",
                "updated_at" => "2022-04-21 08:35:46+00"
            ],
            [
                "id" => 17,
                "bpjs_kes_bill_detail_id" => 53,
                "employee_family_id" => 17,
                "iuran_value" => 75677,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 21:20:47+00",
                "updated_at" => "2022-04-21 08:36:40+00"
            ],
            [
                "id" => 18,
                "bpjs_kes_bill_detail_id" => 53,
                "employee_family_id" => 18,
                "iuran_value" => 75677,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 21:20:47+00",
                "updated_at" => "2022-04-21 08:36:40+00"
            ],
            [
                "id" => 9,
                "bpjs_kes_bill_detail_id" => 37,
                "employee_family_id" => 16,
                "iuran_value" => 73779,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 21:20:47+00",
                "updated_at" => "2022-04-21 08:39:36+00"
            ],
            [
                "id" => 2,
                "bpjs_kes_bill_detail_id" => 25,
                "employee_family_id" => 4,
                "iuran_value" => 50406,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 21:20:47+00",
                "updated_at" => "2022-04-21 08:42:26+00"
            ],
            [
                "id" => 3,
                "bpjs_kes_bill_detail_id" => 25,
                "employee_family_id" => 5,
                "iuran_value" => 50406,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 21:20:47+00",
                "updated_at" => "2022-04-21 08:42:26+00"
            ],
            [
                "id" => 4,
                "bpjs_kes_bill_detail_id" => 33,
                "employee_family_id" => 6,
                "iuran_value" => 52821,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 21:20:47+00",
                "updated_at" => "2022-04-21 08:42:39+00"
            ],
            [
                "id" => 5,
                "bpjs_kes_bill_detail_id" => 33,
                "employee_family_id" => 7,
                "iuran_value" => 52821,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 21:20:47+00",
                "updated_at" => "2022-04-21 08:42:39+00"
            ],
            [
                "id" => 6,
                "bpjs_kes_bill_detail_id" => 33,
                "employee_family_id" => 8,
                "iuran_value" => 52821,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 21:20:47+00",
                "updated_at" => "2022-04-21 08:42:39+00"
            ],
            [
                "id" => 14,
                "bpjs_kes_bill_detail_id" => 51,
                "employee_family_id" => 13,
                "iuran_value" => 47564,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 21:20:47+00",
                "updated_at" => "2022-04-21 08:46:59+00"
            ],
            [
                "id" => 15,
                "bpjs_kes_bill_detail_id" => 51,
                "employee_family_id" => 14,
                "iuran_value" => 47564,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 21:20:47+00",
                "updated_at" => "2022-04-21 08:46:59+00"
            ],
            [
                "id" => 16,
                "bpjs_kes_bill_detail_id" => 51,
                "employee_family_id" => 15,
                "iuran_value" => 47564,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 21:20:47+00",
                "updated_at" => "2022-04-21 08:46:59+00"
            ],
            [
                "id" => 10,
                "bpjs_kes_bill_detail_id" => 38,
                "employee_family_id" => 19,
                "iuran_value" => 44754,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 21:20:47+00",
                "updated_at" => "2022-04-21 08:47:13+00"
            ],
            [
                "id" => 11,
                "bpjs_kes_bill_detail_id" => 38,
                "employee_family_id" => 20,
                "iuran_value" => 44754,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 21:20:47+00",
                "updated_at" => "2022-04-21 08:47:13+00"
            ],
            [
                "id" => 20,
                "bpjs_kes_bill_detail_id" => 71,
                "employee_family_id" => 12,
                "iuran_value" => 71652,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 16:12:14+00",
                "updated_at" => "2022-04-21 09:12:59+00"
            ],
            [
                "id" => 21,
                "bpjs_kes_bill_detail_id" => 71,
                "employee_family_id" => 26,
                "iuran_value" => 71652,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 16:12:14+00",
                "updated_at" => "2022-04-21 09:12:59+00"
            ],
            [
                "id" => 22,
                "bpjs_kes_bill_detail_id" => 71,
                "employee_family_id" => 27,
                "iuran_value" => 71652,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 16:12:14+00",
                "updated_at" => "2022-04-21 09:12:59+00"
            ]
        ];
        DB::table('bpjs_kes_bill_detail_tambahan')->insert($data);
        DB::statement("SELECT setval('bpjs_kes_bill_detail_tambahan_id_seq', (SELECT MAX(id) FROM bpjs_kes_bill_detail_tambahan)+1)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bpjs_kes_bill_detail_tambahan');
    }
}
