<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreatePinjamanBankDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pinjaman_bank_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('pinjaman_bank_id')->constrained('pinjaman_bank')->onDelete('cascade');
            $table->foreignId('employee_id')->constrained('employees');
            $table->float('pinjaman_value', 65, 2)->nullable(true)->default(0);
            $table->text('description')->nullable();
            $table->string('status_code')->nullable()->default('not_calculated_yet');
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->timestampsTz($precision = 0);

            $table->unique(array('pinjaman_bank_id', 'employee_id'));
        });

        $data = [
            [
                "id" => 1,
                "pinjaman_bank_id" => 1,
                "employee_id" => 66,
                "pinjaman_value" => 3337000,
                "description" => null,
                "status_code" => "calculated",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 14:40:47+00",
                "updated_at" => "2022-04-19 14:40:57+00"
            ],
            [
                "id" => 2,
                "pinjaman_bank_id" => 1,
                "employee_id" => 27,
                "pinjaman_value" => 3874000,
                "description" => null,
                "status_code" => "calculated",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 10:11:59+00",
                "updated_at" => "2022-04-21 10:12:04+00"
            ],
            [
                "id" => 3,
                "pinjaman_bank_id" => 1,
                "employee_id" => 12,
                "pinjaman_value" => 2113000,
                "description" => null,
                "status_code" => "calculated",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 10:12:18+00",
                "updated_at" => "2022-04-21 10:12:22+00"
            ],
            [
                "id" => 4,
                "pinjaman_bank_id" => 1,
                "employee_id" => 18,
                "pinjaman_value" => 861000,
                "description" => null,
                "status_code" => "calculated",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 10:12:33+00",
                "updated_at" => "2022-04-21 10:12:37+00"
            ],
            [
                "id" => 5,
                "pinjaman_bank_id" => 1,
                "employee_id" => 76,
                "pinjaman_value" => 1496000,
                "description" => null,
                "status_code" => "calculated",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 10:12:48+00",
                "updated_at" => "2022-04-21 10:12:52+00"
            ],
            [
                "id" => 6,
                "pinjaman_bank_id" => 1,
                "employee_id" => 36,
                "pinjaman_value" => 2275500,
                "description" => null,
                "status_code" => "calculated",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 10:13:03+00",
                "updated_at" => "2022-04-21 10:13:07+00"
            ],
            [
                "id" => 7,
                "pinjaman_bank_id" => 1,
                "employee_id" => 28,
                "pinjaman_value" => 1406000,
                "description" => null,
                "status_code" => "calculated",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 10:13:20+00",
                "updated_at" => "2022-04-21 10:13:24+00"
            ],
            [
                "id" => 8,
                "pinjaman_bank_id" => 1,
                "employee_id" => 38,
                "pinjaman_value" => 1988000,
                "description" => null,
                "status_code" => "calculated",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 10:13:34+00",
                "updated_at" => "2022-04-21 10:13:38+00"
            ],
            [
                "id" => 9,
                "pinjaman_bank_id" => 1,
                "employee_id" => 33,
                "pinjaman_value" => 2712000,
                "description" => null,
                "status_code" => "calculated",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 10:13:49+00",
                "updated_at" => "2022-04-21 10:13:53+00"
            ],
            [
                "id" => 10,
                "pinjaman_bank_id" => 1,
                "employee_id" => 25,
                "pinjaman_value" => 1667000,
                "description" => null,
                "status_code" => "calculated",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 10:14:07+00",
                "updated_at" => "2022-04-21 10:14:12+00"
            ],
            [
                "id" => 11,
                "pinjaman_bank_id" => 1,
                "employee_id" => 29,
                "pinjaman_value" => 1125000,
                "description" => null,
                "status_code" => "calculated",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 10:14:24+00",
                "updated_at" => "2022-04-21 10:14:28+00"
            ],
            [
                "id" => 12,
                "pinjaman_bank_id" => 1,
                "employee_id" => 52,
                "pinjaman_value" => 1406000,
                "description" => null,
                "status_code" => "calculated",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 10:14:40+00",
                "updated_at" => "2022-04-21 10:14:44+00"
            ],
            [
                "id" => 13,
                "pinjaman_bank_id" => 1,
                "employee_id" => 51,
                "pinjaman_value" => 1476500,
                "description" => null,
                "status_code" => "calculated",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 10:15:00+00",
                "updated_at" => "2022-04-21 10:15:04+00"
            ],
            [
                "id" => 14,
                "pinjaman_bank_id" => 1,
                "employee_id" => 42,
                "pinjaman_value" => 840500,
                "description" => null,
                "status_code" => "calculated",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 10:15:16+00",
                "updated_at" => "2022-04-21 10:15:20+00"
            ],
            [
                "id" => 15,
                "pinjaman_bank_id" => 1,
                "employee_id" => 34,
                "pinjaman_value" => 1194500,
                "description" => null,
                "status_code" => "calculated",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 10:15:31+00",
                "updated_at" => "2022-04-21 10:15:35+00"
            ],
            [
                "id" => 16,
                "pinjaman_bank_id" => 1,
                "employee_id" => 32,
                "pinjaman_value" => 2390500,
                "description" => null,
                "status_code" => "calculated",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 10:15:45+00",
                "updated_at" => "2022-04-21 10:15:49+00"
            ]
        ];
        DB::table('pinjaman_bank_details')->insert($data);
        DB::statement("SELECT setval('pinjaman_bank_details_id_seq', (SELECT MAX(id) FROM pinjaman_bank_details)+1)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pinjaman_bank_details');
    }
}
