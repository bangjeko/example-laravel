<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateTunjanganBeasiswaDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tunjangan_beasiswa_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('tunjangan_beasiswa_id')->constrained('tunjangan_beasiswa')->onDelete('cascade');
            $table->foreignId('employee_id')->constrained('employees');
            $table->float('penerimaan_value', 65, 2)->nullable(true)->default(0);
            $table->text('description')->nullable();
            $table->string('status_code')->nullable()->default('not_calculated_yet');
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->timestampsTz($precision = 0);

            $table->unique(array('tunjangan_beasiswa_id', 'employee_id'));
        });

        $data = [
            [
                "id" => 2,
                "tunjangan_beasiswa_id" => 1,
                "employee_id" => 31,
                "penerimaan_value" => 200000,
                "description" => "NULL",
                "status_code" => "calculated",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 14:36:31+00",
                "updated_at" => "2022-04-19 14:38:51+00"
            ],
            [
                "id" => 4,
                "tunjangan_beasiswa_id" => 1,
                "employee_id" => 16,
                "penerimaan_value" => 225000,
                "description" => "NULL",
                "status_code" => "calculated",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 14:36:47+00",
                "updated_at" => "2022-04-19 14:39:09+00"
            ],
            [
                "id" => 1,
                "tunjangan_beasiswa_id" => 1,
                "employee_id" => 41,
                "penerimaan_value" => 275000,
                "description" => "NULL",
                "status_code" => "calculated",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 14:36:22+00",
                "updated_at" => "2022-04-19 14:39:34+00"
            ],
            [
                "id" => 3,
                "tunjangan_beasiswa_id" => 1,
                "employee_id" => 19,
                "penerimaan_value" => 175000,
                "description" => "NULL",
                "status_code" => "calculated",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 14:36:41+00",
                "updated_at" => "2022-04-19 14:39:49+00"
            ]
        ];
        DB::table('tunjangan_beasiswa_details')->insert($data);
        DB::statement("SELECT setval('tunjangan_beasiswa_details_id_seq', (SELECT MAX(id) FROM tunjangan_beasiswa_details)+1)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tunjangan_beasiswa_details');
    }
}
