<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateMasterSanksiTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_sanksi_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->unique();
            $table->foreignId('salary_component_id')->nullable()->constrained('master_salary_components');
            $table->integer('active')->nullable(true)->default('1');
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->timestampsTz($precision = 0);
        });

        $data = [
            [
                "name" => "Sanksi Darmawisata",
                "salary_component_id" => 37
            ]
        ];
        DB::table('master_sanksi_types')->insert($data);
        DB::statement("SELECT setval('master_sanksi_types_id_seq', (SELECT MAX(id) FROM master_sanksi_types)+1)");
   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_sanksi_types');
    }
}
