<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateConfigProsenBpjsKesehatan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('config_prosen_bpjs_kesehatan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('periode_month')->unique();
            $table->float('prosen_premi_company')->nullable(true)->default(0);
            $table->float('prosen_premi_employee')->nullable(true)->default(0);
            $table->float('upah_min_value')->nullable(true)->default(0);
            $table->float('upah_max_value')->nullable(true)->default(0);
            $table->text('description')->nullable(true);
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->timestampsTz($precision = 0);
        });
        $data = [
            [
                "periode_month" => '2022-01-01',
                "prosen_premi_company" => 4,
                "prosen_premi_employee" => 1,
                "upah_min_value" =>  2715000,
                "upah_max_value" => 12000000,
            ],
            [
                "periode_month" => '2022-02-01',
                "prosen_premi_company" => 4,
                "prosen_premi_employee" => 1,
                "upah_min_value" =>  2715000,
                "upah_max_value" => 12000000,
            ],
        ];
        DB::table('config_prosen_bpjs_kesehatan')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('config_prosen_bpjs_kesehatan');
    }
}
