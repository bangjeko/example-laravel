<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateMasterCompPinjamanBank extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_comp_pinjaman_bank', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->unique();
            $table->foreignId('salary_component_id')->nullable()->constrained('master_salary_components');
            $table->integer('active')->nullable(true)->default('1');
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->timestampsTz($precision = 0);
        });
        $data = [
            [
                "name" => 'Pinjaman Bank Jateng',
                "salary_component_id" => 36,
            ],
        ];
        DB::table('master_comp_pinjaman_bank')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_comp_pinjaman_bank');
    }
}
