<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreatePayrollPeriodes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payroll_periodes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('periode_month')->unique();
            $table->date('start_date');
            $table->date('end_date');
            $table->integer('count_day_of_work')->nullable(true)->default(21);
            $table->text('description')->nullable();
            $table->boolean('is_lock')->nullable()->default(false);
            $table->foreignId('submitted_by')->nullable()->constrained('users');
            $table->timestampTz('submitted_at')->nullable();
            $table->foreignId('unlocked_by')->nullable()->constrained('users');
            $table->timestampTz('unlocked_at')->nullable();
            $table->string('status_code')->nullable();
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->timestampsTz($precision = 0);
        });
 
        $data = [
            [
                "id" => 1,
                "periode_month" => "2022-01-01",
                "start_date" => "2021-01-20",
                "end_date" => "2022-01-20",
                "count_day_of_work" => "21",
                "description" => "",
                "is_lock" => true
            ],
            [
                "id" => 2,
                "periode_month" => "2022-02-01",
                "start_date" => "2022-01-21",
                "end_date" => "2022-02-20",
                "count_day_of_work" => "21",
                "description" => "",
                "is_lock" => false
            ]
        ];

        DB::table('payroll_periodes')->insert($data);
        DB::statement("SELECT setval('payroll_periodes_id_seq', (SELECT MAX(id) FROM payroll_periodes)+1)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payroll_periodes');
    }
}
