<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateTunjanganLemburHariLibur extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tunjangan_lembur_hari_libur', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('tunjangan_lembur_detail_id')->constrained('tunjangan_lembur_details')->onDelete('cascade');
            $table->date('date');
            $table->float('jam_lembur_count', 65, 2)->nullable(true)->default(0);
            $table->float('lembur_jam_kedelapan_value', 65, 2)->nullable(true)->default(0);
            $table->float('lembur_jam_kesembilan_value', 65, 2)->nullable(true)->default(0);
            $table->float('lembur_jam_selanjutnya_value', 65, 2)->nullable(true)->default(0);
            $table->float('penerimaan_value', 65, 2)->nullable(true)->default(0);
            $table->text('description')->nullable();
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->timestampsTz($precision = 0);

            $table->unique(array('tunjangan_lembur_detail_id', 'date'));
        });

        $data = [
            [
                "id" => 7,
                "tunjangan_lembur_detail_id" => 6,
                "date" => "2022-01-01",
                "jam_lembur_count" => 7,
                "lembur_jam_kedelapan_value" => 271628,
                "lembur_jam_kesembilan_value" => 0,
                "lembur_jam_selanjutnya_value" => 0,
                "penerimaan_value" => 271628,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:09:32+00"
            ],
            [
                "id" => 22,
                "tunjangan_lembur_detail_id" => 15,
                "date" => "2021-12-25",
                "jam_lembur_count" => 7,
                "lembur_jam_kedelapan_value" => 271628,
                "lembur_jam_kesembilan_value" => 0,
                "lembur_jam_selanjutnya_value" => 0,
                "penerimaan_value" => 271628,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:10:27+00"
            ],
            [
                "id" => 23,
                "tunjangan_lembur_detail_id" => 15,
                "date" => "2022-01-01",
                "jam_lembur_count" => 7,
                "lembur_jam_kedelapan_value" => 271628,
                "lembur_jam_kesembilan_value" => 0,
                "lembur_jam_selanjutnya_value" => 0,
                "penerimaan_value" => 271628,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:10:27+00"
            ],
            [
                "id" => 12,
                "tunjangan_lembur_detail_id" => 11,
                "date" => "2021-12-25",
                "jam_lembur_count" => 7,
                "lembur_jam_kedelapan_value" => 271628,
                "lembur_jam_kesembilan_value" => 0,
                "lembur_jam_selanjutnya_value" => 0,
                "penerimaan_value" => 271628,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:11:11+00"
            ],
            [
                "id" => 2,
                "tunjangan_lembur_detail_id" => 2,
                "date" => "2022-01-08",
                "jam_lembur_count" => 3.5,
                "lembur_jam_kedelapan_value" => 211337,
                "lembur_jam_kesembilan_value" => 0,
                "lembur_jam_selanjutnya_value" => 0,
                "penerimaan_value" => 211337,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 10:46:16+00"
            ],
            [
                "id" => 13,
                "tunjangan_lembur_detail_id" => 11,
                "date" => "2022-01-01",
                "jam_lembur_count" => 7,
                "lembur_jam_kedelapan_value" => 271628,
                "lembur_jam_kesembilan_value" => 0,
                "lembur_jam_selanjutnya_value" => 0,
                "penerimaan_value" => 271628,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:11:11+00"
            ],
            [
                "id" => 24,
                "tunjangan_lembur_detail_id" => 16,
                "date" => "2022-01-08",
                "jam_lembur_count" => 4,
                "lembur_jam_kedelapan_value" => 281824,
                "lembur_jam_kesembilan_value" => 0,
                "lembur_jam_selanjutnya_value" => 0,
                "penerimaan_value" => 281824,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:51:35+00",
                "updated_at" => "2022-04-21 10:52:01+00"
            ],
            [
                "id" => 25,
                "tunjangan_lembur_detail_id" => 16,
                "date" => "2022-01-15",
                "jam_lembur_count" => 4,
                "lembur_jam_kedelapan_value" => 281824,
                "lembur_jam_kesembilan_value" => 0,
                "lembur_jam_selanjutnya_value" => 0,
                "penerimaan_value" => 281824,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:51:35+00",
                "updated_at" => "2022-04-21 10:52:01+00"
            ],
            [
                "id" => 26,
                "tunjangan_lembur_detail_id" => 17,
                "date" => "2021-12-25",
                "jam_lembur_count" => 7,
                "lembur_jam_kedelapan_value" => 288680,
                "lembur_jam_kesembilan_value" => 0,
                "lembur_jam_selanjutnya_value" => 0,
                "penerimaan_value" => 288680,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:58:39+00",
                "updated_at" => "2022-04-21 11:00:17+00"
            ],
            [
                "id" => 27,
                "tunjangan_lembur_detail_id" => 17,
                "date" => "2022-01-01",
                "jam_lembur_count" => 7,
                "lembur_jam_kedelapan_value" => 288680,
                "lembur_jam_kesembilan_value" => 0,
                "lembur_jam_selanjutnya_value" => 0,
                "penerimaan_value" => 288680,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:58:39+00",
                "updated_at" => "2022-04-21 11:00:17+00"
            ],
            [
                "id" => 3,
                "tunjangan_lembur_detail_id" => 3,
                "date" => "2021-12-25",
                "jam_lembur_count" => 7,
                "lembur_jam_kedelapan_value" => 305774,
                "lembur_jam_kesembilan_value" => 0,
                "lembur_jam_selanjutnya_value" => 0,
                "penerimaan_value" => 305774,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:01:18+00"
            ],
            [
                "id" => 4,
                "tunjangan_lembur_detail_id" => 3,
                "date" => "2022-01-07",
                "jam_lembur_count" => 7,
                "lembur_jam_kedelapan_value" => 305774,
                "lembur_jam_kesembilan_value" => 0,
                "lembur_jam_selanjutnya_value" => 0,
                "penerimaan_value" => 305774,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:01:18+00"
            ],
            [
                "id" => 5,
                "tunjangan_lembur_detail_id" => 4,
                "date" => "2021-12-25",
                "jam_lembur_count" => 7,
                "lembur_jam_kedelapan_value" => 305928,
                "lembur_jam_kesembilan_value" => 0,
                "lembur_jam_selanjutnya_value" => 0,
                "penerimaan_value" => 305928,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:04:17+00"
            ],
            [
                "id" => 6,
                "tunjangan_lembur_detail_id" => 4,
                "date" => "2022-01-01",
                "jam_lembur_count" => 7,
                "lembur_jam_kedelapan_value" => 305928,
                "lembur_jam_kesembilan_value" => 0,
                "lembur_jam_selanjutnya_value" => 0,
                "penerimaan_value" => 305928,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:04:17+00"
            ],
            [
                "id" => 10,
                "tunjangan_lembur_detail_id" => 9,
                "date" => "2022-01-01",
                "jam_lembur_count" => 7,
                "lembur_jam_kedelapan_value" => 304024,
                "lembur_jam_kesembilan_value" => 0,
                "lembur_jam_selanjutnya_value" => 0,
                "penerimaan_value" => 304024,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:05:29+00"
            ],
            [
                "id" => 19,
                "tunjangan_lembur_detail_id" => 14,
                "date" => "2022-01-01",
                "jam_lembur_count" => 3.5,
                "lembur_jam_kedelapan_value" => 144340,
                "lembur_jam_kesembilan_value" => 0,
                "lembur_jam_selanjutnya_value" => 0,
                "penerimaan_value" => 144340,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:06:20+00"
            ],
            [
                "id" => 20,
                "tunjangan_lembur_detail_id" => 14,
                "date" => "2022-01-08",
                "jam_lembur_count" => 4,
                "lembur_jam_kedelapan_value" => 164960,
                "lembur_jam_kesembilan_value" => 0,
                "lembur_jam_selanjutnya_value" => 0,
                "penerimaan_value" => 164960,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:06:20+00"
            ],
            [
                "id" => 21,
                "tunjangan_lembur_detail_id" => 14,
                "date" => "2022-01-15",
                "jam_lembur_count" => 4,
                "lembur_jam_kedelapan_value" => 164960,
                "lembur_jam_kesembilan_value" => 0,
                "lembur_jam_selanjutnya_value" => 0,
                "penerimaan_value" => 164960,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:06:20+00"
            ],
            [
                "id" => 17,
                "tunjangan_lembur_detail_id" => 13,
                "date" => "2011-12-25",
                "jam_lembur_count" => 7,
                "lembur_jam_kedelapan_value" => 508144,
                "lembur_jam_kesembilan_value" => 0,
                "lembur_jam_selanjutnya_value" => 0,
                "penerimaan_value" => 508144,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:07:01+00"
            ],
            [
                "id" => 18,
                "tunjangan_lembur_detail_id" => 13,
                "date" => "2022-01-01",
                "jam_lembur_count" => 7,
                "lembur_jam_kedelapan_value" => 508144,
                "lembur_jam_kesembilan_value" => 0,
                "lembur_jam_selanjutnya_value" => 0,
                "penerimaan_value" => 508144,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:07:01+00"
            ],
            [
                "id" => 8,
                "tunjangan_lembur_detail_id" => 8,
                "date" => "2021-12-25",
                "jam_lembur_count" => 7,
                "lembur_jam_kedelapan_value" => 305928,
                "lembur_jam_kesembilan_value" => 0,
                "lembur_jam_selanjutnya_value" => 0,
                "penerimaan_value" => 305928,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:07:47+00"
            ],
            [
                "id" => 9,
                "tunjangan_lembur_detail_id" => 8,
                "date" => "2022-01-01",
                "jam_lembur_count" => 7,
                "lembur_jam_kedelapan_value" => 305928,
                "lembur_jam_kesembilan_value" => 0,
                "lembur_jam_selanjutnya_value" => 0,
                "penerimaan_value" => 305928,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:07:47+00"
            ]
        ];
        DB::table('tunjangan_lembur_hari_libur')->insert($data);
        DB::statement("SELECT setval('tunjangan_lembur_hari_libur_id_seq', (SELECT MAX(id) FROM tunjangan_lembur_hari_libur)+1)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tunjangan_lembur_hari_libur');
    }
}
