<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFinaOperationalAccounts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fina_operational_accounts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('account_operational_id');
            $table->string('account_code')->unique();
            $table->string('account_name');
            $table->integer('active')->nullable(true)->default('1');
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->timestampsTz($precision = 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fina_operational_accounts');
    }
}
