<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateCompanies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('company_name');
            $table->string('company_type');
            $table->string('nomor_nib')->nullable();
            $table->string('nomor_npwp')->nullable();
            $table->text('primary_address');
            $table->text('secondary_address')->nullable();
            $table->string('postal_code')->nullable();
            $table->string('primary_telephone');
            $table->string('secondary_telephone')->nullable();
            $table->string('email');
            $table->string('url_website')->nullable();
            $table->text('img_logo');
            $table->string('taptap_secret_key')->nullable();
            $table->string('taptap_client_id')->nullable();
            $table->string('status_code')->nullable();
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->timestampsTz($precision = 0);
        });
        $data = [
            [
                "id" => 1,
                "company_name" => "PT. Kawasan Industri Wijayakusuma",
                "company_type" => "persero",
                "nomor_nib" => "1122334455",
                "nomor_npwp" => "5544332211",
                "primary_address" => "Jl. Raya Semarang – Kendal KM 12",
                "secondary_address" => "Semarang – Indonesia",
                "postal_code" => '50153',
                "primary_telephone" => '+62 812-1111-8022',
                "secondary_telephone" => "(024) 8662156",
                "email" => "pemasaran@kiw.co.id",
                "url_website" => 'https://kiw.co.id/',
                "img_logo" => '/companies/logo_353488691b40edff35c7eb70d27fe3c1_1x(6)(8).png',
                "taptap_secret_key" => 'prod-57c501c146b10f2129ee31822d0dd68599a9f501d6',
                "taptap_client_id" => '57047f50a5-d8a9-41cc-b0e6-7ddab0d46f3c',
            ]
        ];
        DB::table('companies')->insert($data);
        DB::statement("SELECT setval('companies_id_seq', (SELECT MAX(id) FROM companies)+1)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
