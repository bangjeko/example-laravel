<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreatePotonganSanksiDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('potongan_sanksi_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('potongan_sanksi_id')->constrained('potongan_sanksi')->onDelete('cascade');
            $table->foreignId('employee_id')->constrained('employees');
            $table->float('potongan_value', 65, 2)->nullable(true)->default(0);
            $table->text('description')->nullable();
            $table->string('status_code')->nullable()->default('not_calculated_yet');
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->timestampsTz($precision = 0);

            $table->unique(array('potongan_sanksi_id', 'employee_id'));
        });

        $data = [
            [
                "id" => 2,
                "potongan_sanksi_id" => 1,
                "employee_id" => 66,
                "potongan_value" => 1100000,
                "description" => null,
                "status_code" => "calculated",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 21:46:40+00",
                "updated_at" => "2022-04-19 14:46:57+00"
            ],
            [
                "id" => 1,
                "potongan_sanksi_id" => 1,
                "employee_id" => 44,
                "potongan_value" => 1100000,
                "description" => null,
                "status_code" => "calculated",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 21:46:40+00",
                "updated_at" => "2022-04-19 14:47:02+00"
            ]
        ];

        DB::table('potongan_sanksi_details')->insert($data);
        DB::statement("SELECT setval('potongan_sanksi_details_id_seq', (SELECT MAX(id) FROM potongan_sanksi_details)+1)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('potongan_sanksi_details');
    }
}
