<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateTunjanganBeasiswaComponents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tunjangan_beasiswa_components', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('tunjangan_beasiswa_detail_id')->constrained('tunjangan_beasiswa_details')->onDelete('cascade');
            $table->foreignId('employee_family_id')->constrained('employee_families');
            $table->foreignId('education_id')->constrained('master_educations');
            $table->integer('rank')->nullable(true);
            $table->float('component_value', 65, 2)->nullable(true)->default(0);
            $table->text('description')->nullable();
            $table->integer('active')->nullable(true)->default('1');
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->timestampsTz($precision = 0);

            $table->unique(array('tunjangan_beasiswa_detail_id', 'employee_family_id'));
        });

        $data = [
            [
                "id" => 1,
                "tunjangan_beasiswa_detail_id" => 2,
                "employee_family_id" => 23,
                "education_id" => 3,
                "rank" => 2,
                "component_value" => 200000,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 14:38:51+00",
                "updated_at" => "2022-04-19 14:38:51+00"
            ],
            [
                "id" => 2,
                "tunjangan_beasiswa_detail_id" => 4,
                "employee_family_id" => 22,
                "education_id" => 4,
                "rank" => 5,
                "component_value" => 225000,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 14:39:09+00",
                "updated_at" => "2022-04-19 14:39:09+00"
            ],
            [
                "id" => 3,
                "tunjangan_beasiswa_detail_id" => 1,
                "employee_family_id" => 24,
                "education_id" => 3,
                "rank" => 1,
                "component_value" => 275000,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 14:39:34+00",
                "updated_at" => "2022-04-19 14:39:34+00"
            ],
            [
                "id" => 4,
                "tunjangan_beasiswa_detail_id" => 3,
                "employee_family_id" => 25,
                "education_id" => 3,
                "rank" => 5,
                "component_value" => 175000,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 14:39:49+00",
                "updated_at" => "2022-04-19 14:39:49+00"
            ]
        ];
        DB::table('tunjangan_beasiswa_components')->insert($data);
        DB::statement("SELECT setval('tunjangan_beasiswa_components_id_seq', (SELECT MAX(id) FROM tunjangan_beasiswa_components)+1)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tunjangan_beasiswa_components');
    }
}
