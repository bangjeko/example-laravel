<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateControlSheetVerifications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('control_sheet_verifications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('payroll_periode_id')->constrained('payroll_periodes')->onDelete('cascade');
            $table->foreignId('privillege_role_id')->constrained('roles');
            $table->integer('verification_order');
            $table->foreignId('verified_by')->nullable()->constrained('users');
            $table->dateTimeTz('verified_at', $precision = 0)->nullable();
            $table->foreignId('job_position_id')->nullable()->constrained('job_positions');
            $table->string('status_code')->nullable();
            $table->text('description')->nullable();
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->timestampsTz($precision = 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('control_sheet_verifications');
    }
}
