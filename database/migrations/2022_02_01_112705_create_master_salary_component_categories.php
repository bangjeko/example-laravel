<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateMasterSalaryComponentCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_salary_component_categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->unique();
            $table->integer('order_number')->nullable(true);
            $table->integer('active')->nullable(true)->default('1');
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->timestampsTz($precision = 0);
        });
        $data = [
            [
                "name" => 'GAJI POKOK',
            ],
            [
                "name" => 'TUNJANGAN TUNJANGAN',
            ],
            [
                "name" => 'POTONGAN BPJS',
            ],
            [
                "name" => 'POTONGAN KOPERASI',
            ],
            [
                "name" => 'POTONGAN LAIN LAIN',
            ],
            [
                "name" => 'RAPEL',
            ],
        ];
        DB::table('master_salary_component_categories')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_salary_component_categories');
    }
}
