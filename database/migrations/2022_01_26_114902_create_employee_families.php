<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeFamilies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_families', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('employee_id')->constrained('employees');
            $table->foreignId('family_relationship_id')->constrained('master_family_relationships');
            $table->string('fullname');
            $table->string('nik')->nullable()->unique();
            $table->string('place_of_birth')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->string('gender');
            $table->string('address')->nullable();;
            $table->string('telephone')->nullable();
            $table->foreignId('education_id')->nullable()->constrained('master_educations');
            $table->text('work_description')->nullable();
            $table->text('description')->nullable();
            $table->boolean('is_bpjs_tambahan')->nullable()->default(false);
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->timestampsTz($precision = 0);
        });

        $data = [
            [
                "id" => 1,
                "employee_id" => 88,
                "family_relationship_id" => 6,
                "fullname" => "Suprapti",
                "nik" => null,
                "place_of_birth" => null,
                "date_of_birth" => null,
                "gender" => "female",
                "address" => null,
                "telephone" => null,
                "education_id" => null,
                "work_description" => null,
                "description" => null,
                "is_bpjs_tambahan" => "True",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-11 06:09:51+00",
                "updated_at" => "2022-04-11 06:09:51+00"
            ],
            [
                "id" => 2,
                "employee_id" => 40,
                "family_relationship_id" => 3,
                "fullname" => "Kurik Miyanto",
                "nik" => null,
                "place_of_birth" => null,
                "date_of_birth" => null,
                "gender" => "male",
                "address" => null,
                "telephone" => null,
                "education_id" => null,
                "work_description" => null,
                "description" => null,
                "is_bpjs_tambahan" => "True",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-11 06:10:25+00",
                "updated_at" => "2022-04-11 06:10:25+00"
            ],
            [
                "id" => 3,
                "employee_id" => 40,
                "family_relationship_id" => 6,
                "fullname" => "MF Purwati",
                "nik" => null,
                "place_of_birth" => null,
                "date_of_birth" => null,
                "gender" => "female",
                "address" => null,
                "telephone" => null,
                "education_id" => null,
                "work_description" => null,
                "description" => null,
                "is_bpjs_tambahan" => "True",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-11 06:10:48+00",
                "updated_at" => "2022-04-11 06:10:48+00"
            ],
            [
                "id" => 4,
                "employee_id" => 51,
                "family_relationship_id" => 6,
                "fullname" => "Satinem",
                "nik" => null,
                "place_of_birth" => null,
                "date_of_birth" => null,
                "gender" => "female",
                "address" => null,
                "telephone" => null,
                "education_id" => null,
                "work_description" => null,
                "description" => null,
                "is_bpjs_tambahan" => "True",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-11 06:11:35+00",
                "updated_at" => "2022-04-11 06:11:35+00"
            ],
            [
                "id" => 5,
                "employee_id" => 51,
                "family_relationship_id" => 3,
                "fullname" => "Paing",
                "nik" => null,
                "place_of_birth" => null,
                "date_of_birth" => null,
                "gender" => "male",
                "address" => null,
                "telephone" => null,
                "education_id" => null,
                "work_description" => null,
                "description" => null,
                "is_bpjs_tambahan" => "True",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-11 06:11:59+00",
                "updated_at" => "2022-04-11 06:11:59+00"
            ],
            [
                "id" => 6,
                "employee_id" => 42,
                "family_relationship_id" => 2,
                "fullname" => "Rumini",
                "nik" => null,
                "place_of_birth" => null,
                "date_of_birth" => null,
                "gender" => "female",
                "address" => null,
                "telephone" => null,
                "education_id" => null,
                "work_description" => null,
                "description" => null,
                "is_bpjs_tambahan" => "True",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-11 06:12:55+00",
                "updated_at" => "2022-04-11 06:12:55+00"
            ],
            [
                "id" => 7,
                "employee_id" => 42,
                "family_relationship_id" => 3,
                "fullname" => "Mulyono",
                "nik" => null,
                "place_of_birth" => null,
                "date_of_birth" => null,
                "gender" => "male",
                "address" => null,
                "telephone" => null,
                "education_id" => null,
                "work_description" => null,
                "description" => null,
                "is_bpjs_tambahan" => "True",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-11 06:13:15+00",
                "updated_at" => "2022-04-11 06:13:15+00"
            ],
            [
                "id" => 8,
                "employee_id" => 42,
                "family_relationship_id" => 6,
                "fullname" => "Suryati",
                "nik" => null,
                "place_of_birth" => null,
                "date_of_birth" => null,
                "gender" => "female",
                "address" => null,
                "telephone" => null,
                "education_id" => null,
                "work_description" => null,
                "description" => null,
                "is_bpjs_tambahan" => "True",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-11 06:13:32+00",
                "updated_at" => "2022-04-11 06:13:32+00"
            ],
            [
                "id" => 9,
                "employee_id" => 53,
                "family_relationship_id" => 6,
                "fullname" => "Wiwik Lestari",
                "nik" => null,
                "place_of_birth" => null,
                "date_of_birth" => null,
                "gender" => "male",
                "address" => null,
                "telephone" => null,
                "education_id" => null,
                "work_description" => null,
                "description" => null,
                "is_bpjs_tambahan" => "True",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-11 06:14:11+00",
                "updated_at" => "2022-04-11 06:14:11+00"
            ],
            [
                "id" => 10,
                "employee_id" => 12,
                "family_relationship_id" => 6,
                "fullname" => "Sariyem",
                "nik" => null,
                "place_of_birth" => null,
                "date_of_birth" => null,
                "gender" => "female",
                "address" => null,
                "telephone" => null,
                "education_id" => null,
                "work_description" => null,
                "description" => null,
                "is_bpjs_tambahan" => "True",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-11 06:14:52+00",
                "updated_at" => "2022-04-11 06:14:52+00"
            ],
            [
                "id" => 11,
                "employee_id" => 30,
                "family_relationship_id" => 6,
                "fullname" => "Jatmiah",
                "nik" => null,
                "place_of_birth" => null,
                "date_of_birth" => null,
                "gender" => "female",
                "address" => null,
                "telephone" => null,
                "education_id" => null,
                "work_description" => null,
                "description" => null,
                "is_bpjs_tambahan" => "True",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-11 06:15:20+00",
                "updated_at" => "2022-04-11 06:15:20+00"
            ],
            [
                "id" => 12,
                "employee_id" => 31,
                "family_relationship_id" => 6,
                "fullname" => "Suparti",
                "nik" => null,
                "place_of_birth" => null,
                "date_of_birth" => null,
                "gender" => "female",
                "address" => null,
                "telephone" => null,
                "education_id" => null,
                "work_description" => null,
                "description" => null,
                "is_bpjs_tambahan" => "True",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-11 06:16:11+00",
                "updated_at" => "2022-04-11 06:16:11+00"
            ],
            [
                "id" => 13,
                "employee_id" => 19,
                "family_relationship_id" => 3,
                "fullname" => "Raminto",
                "nik" => null,
                "place_of_birth" => null,
                "date_of_birth" => null,
                "gender" => "male",
                "address" => null,
                "telephone" => null,
                "education_id" => null,
                "work_description" => null,
                "description" => null,
                "is_bpjs_tambahan" => "True",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-11 06:16:39+00",
                "updated_at" => "2022-04-11 06:16:39+00"
            ],
            [
                "id" => 14,
                "employee_id" => 19,
                "family_relationship_id" => 6,
                "fullname" => "Suratmin",
                "nik" => null,
                "place_of_birth" => null,
                "date_of_birth" => null,
                "gender" => "female",
                "address" => null,
                "telephone" => null,
                "education_id" => null,
                "work_description" => null,
                "description" => null,
                "is_bpjs_tambahan" => "True",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-11 06:16:58+00",
                "updated_at" => "2022-04-11 06:16:58+00"
            ],
            [
                "id" => 15,
                "employee_id" => 19,
                "family_relationship_id" => 10,
                "fullname" => "Samiyem",
                "nik" => null,
                "place_of_birth" => null,
                "date_of_birth" => null,
                "gender" => "female",
                "address" => null,
                "telephone" => null,
                "education_id" => null,
                "work_description" => null,
                "description" => null,
                "is_bpjs_tambahan" => "True",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-11 06:17:17+00",
                "updated_at" => "2022-04-11 06:17:17+00"
            ],
            [
                "id" => 16,
                "employee_id" => 36,
                "family_relationship_id" => 3,
                "fullname" => "Sukirno",
                "nik" => null,
                "place_of_birth" => null,
                "date_of_birth" => null,
                "gender" => "male",
                "address" => null,
                "telephone" => null,
                "education_id" => null,
                "work_description" => null,
                "description" => null,
                "is_bpjs_tambahan" => "True",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-11 06:17:43+00",
                "updated_at" => "2022-04-11 06:17:43+00"
            ],
            [
                "id" => 17,
                "employee_id" => 16,
                "family_relationship_id" => 3,
                "fullname" => "Suhardi BS",
                "nik" => null,
                "place_of_birth" => null,
                "date_of_birth" => null,
                "gender" => "male",
                "address" => null,
                "telephone" => null,
                "education_id" => null,
                "work_description" => null,
                "description" => null,
                "is_bpjs_tambahan" => "True",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-11 06:19:28+00",
                "updated_at" => "2022-04-11 06:19:28+00"
            ],
            [
                "id" => 18,
                "employee_id" => 16,
                "family_relationship_id" => 6,
                "fullname" => "Sumikem",
                "nik" => null,
                "place_of_birth" => null,
                "date_of_birth" => null,
                "gender" => "female",
                "address" => null,
                "telephone" => null,
                "education_id" => null,
                "work_description" => null,
                "description" => null,
                "is_bpjs_tambahan" => "True",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-11 06:19:52+00",
                "updated_at" => "2022-04-11 06:19:52+00"
            ],
            [
                "id" => 19,
                "employee_id" => 35,
                "family_relationship_id" => 3,
                "fullname" => "Abdul Gani",
                "nik" => null,
                "place_of_birth" => null,
                "date_of_birth" => null,
                "gender" => "male",
                "address" => null,
                "telephone" => null,
                "education_id" => null,
                "work_description" => null,
                "description" => null,
                "is_bpjs_tambahan" => "True",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-11 06:20:30+00",
                "updated_at" => "2022-04-11 06:20:30+00"
            ],
            [
                "id" => 20,
                "employee_id" => 35,
                "family_relationship_id" => 6,
                "fullname" => "Surasmi",
                "nik" => null,
                "place_of_birth" => null,
                "date_of_birth" => null,
                "gender" => "female",
                "address" => null,
                "telephone" => null,
                "education_id" => null,
                "work_description" => null,
                "description" => null,
                "is_bpjs_tambahan" => "True",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-11 06:20:54+00",
                "updated_at" => "2022-04-11 06:20:54+00"
            ],
            [
                "id" => 22,
                "employee_id" => 16,
                "family_relationship_id" => 11,
                "fullname" => "Maulana Alif Ibrahim Sugiharto",
                "nik" => null,
                "place_of_birth" => null,
                "date_of_birth" => null,
                "gender" => "male",
                "address" => null,
                "telephone" => null,
                "education_id" => null,
                "work_description" => null,
                "description" => null,
                "is_bpjs_tambahan" => false,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 14:37:30+00",
                "updated_at" => "2022-04-19 14:37:30+00"
            ],
            [
                "id" => 23,
                "employee_id" => 31,
                "family_relationship_id" => 11,
                "fullname" => "Azzam Alif Afandi",
                "nik" => null,
                "place_of_birth" => null,
                "date_of_birth" => null,
                "gender" => "male",
                "address" => null,
                "telephone" => null,
                "education_id" => null,
                "work_description" => null,
                "description" => null,
                "is_bpjs_tambahan" => false,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 14:37:48+00",
                "updated_at" => "2022-04-19 14:37:48+00"
            ],
            [
                "id" => 24,
                "employee_id" => 41,
                "family_relationship_id" => 11,
                "fullname" => "Rafa Dwi Aditia",
                "nik" => null,
                "place_of_birth" => null,
                "date_of_birth" => null,
                "gender" => "male",
                "address" => null,
                "telephone" => null,
                "education_id" => null,
                "work_description" => null,
                "description" => null,
                "is_bpjs_tambahan" => false,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 14:38:08+00",
                "updated_at" => "2022-04-19 14:38:08+00"
            ],
            [
                "id" => 25,
                "employee_id" => 19,
                "family_relationship_id" => 11,
                "fullname" => "Nasyafah Safila Wijaya",
                "nik" => null,
                "place_of_birth" => null,
                "date_of_birth" => null,
                "gender" => "male",
                "address" => null,
                "telephone" => null,
                "education_id" => null,
                "work_description" => null,
                "description" => null,
                "is_bpjs_tambahan" => false,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 14:38:25+00",
                "updated_at" => "2022-04-19 14:38:25+00"
            ],
            [
                "id" => 26,
                "employee_id" => 31,
                "family_relationship_id" => 3,
                "fullname" => "Ayahnya Ali",
                "nik" => null,
                "place_of_birth" => null,
                "date_of_birth" => null,
                "gender" => "male",
                "address" => null,
                "telephone" => null,
                "education_id" => null,
                "work_description" => null,
                "description" => null,
                "is_bpjs_tambahan" => "True",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 09:10:44+00",
                "updated_at" => "2022-04-21 09:11:38+00"
            ],
            [
                "id" => 27,
                "employee_id" => 31,
                "family_relationship_id" => 5,
                "fullname" => "Ayah Mertuanya Ali",
                "nik" => null,
                "place_of_birth" => null,
                "date_of_birth" => null,
                "gender" => "male",
                "address" => null,
                "telephone" => null,
                "education_id" => null,
                "work_description" => null,
                "description" => null,
                "is_bpjs_tambahan" => "True",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 09:11:26+00",
                "updated_at" => "2022-04-21 09:11:39+00"
            ]
        ];
        DB::table('employee_families')->insert($data);
        DB::statement("SELECT setval('employee_families_id_seq', (SELECT MAX(id) FROM employee_families)+1)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_families');
    }
}
