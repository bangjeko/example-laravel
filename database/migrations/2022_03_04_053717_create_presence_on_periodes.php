<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreatePresenceOnPeriodes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('presence_on_periodes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('periode_month')->unique();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->text('description')->nullable();
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->timestampsTz($precision = 0);
        });

        $data = [
            [
                "periode_month" => "2022-01-01",
                "start_date" => "2021-12-21",
                "end_date" => "2022-01-20",
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-08 20:22:14+07",
                "updated_at" => "2022-04-08 20:23:10+07"
            ]
        ];
        DB::table('presence_on_periodes')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('presence_on_periodes');
    }
}
