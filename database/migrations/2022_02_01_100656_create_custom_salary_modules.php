<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateCustomSalaryModules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_salary_modules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('module_code');
            $table->string('name');
            $table->string('function_name');
            $table->integer('active')->nullable(true)->default('1');
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->timestampsTz($precision = 0);
        });
        $data = [
            [
                "id" => 1,
                "module_code" => 'koperasi_simpan_pinjam',
                "name" => 'Iuran Simpanan Koperasi',
                "function_name" => 'customSalaryModuleSimpananKoperasi',
                "active" => 1,
            ],
            [
                "id" => 2,
                "module_code" => 'koperasi_simpan_pinjam',
                "name" => 'Angsuran Pinjaman Koperasi',
                "function_name" => 'customSalaryModuleAngsuranPinjamanKoperasi',
                "active" => 1,
            ],
            [
                "id" => 3,
                "module_code" => 'bpjs_kes_bills',
                "name" => 'Tunjangan BPJS Kesehatan',
                "function_name" => 'customSalaryModuleTunjanganBpjsKes',
                "active" => 1,
            ],
            [
                "id" => 4,
                "module_code" => 'bpjs_kes_bills',
                "name" => 'Potongan BPJS Kesehatan',
                "function_name" => 'customSalaryModulePotonganBpjsKes',
                "active" => 1,
            ],
            [
                "id" => 5,
                "module_code" => 'bpjs_kes_bills',
                "name" => 'Potongan BPJS Kesehatan Tambahan',
                "function_name" => 'customSalaryModulePotonganBpjsKesTambahan',
                "active" => 1,
            ],
            [
                "id" => 6,
                "module_code" => 'bpjs_tk_bills',
                "name" => 'Tunjangan BPJS Tenaga Kerja',
                "function_name" => 'customSalaryModuleTunjanganBpjsTk',
                "active" => 1,
            ],
            [
                "id" => 7,
                "module_code" => 'bpjs_tk_bills',
                "name" => 'Potongan BPJS Tenaga Kerja',
                "function_name" => 'customSalaryModulePotonganBpjsTk',
                "active" => 1,
            ],
            [
                "id" => 8,
                "module_code" => 'tunjangan_transports',
                "name" => 'Tunjangan Transports',
                "function_name" => 'customSalaryModuleTunjanganTransports',
                "active" => 0,
            ],
            [
                "id" => 9,
                "module_code" => 'tunjangan_makan',
                "name" => 'Tunjangan Makan',
                "function_name" => 'customSalaryModuleTunjanganMakan',
                "active" => 0,
            ],
            [
                "id" => 10,
                "module_code" => 'tunjangan_beasiswa',
                "name" => 'Tunjangan Beasiswa',
                "function_name" => 'customSalaryModuleTunjanganBeasiswa',
                "active" => 1,
            ],
            [
                "id" => 11,
                "module_code" => 'tunjangan_lembur',
                "name" => 'Tunjangan Lembur',
                "function_name" => 'customSalaryModuleTunjanganLembur',
                "active" => 1,
            ],
            [
                "id" => 12,
                "module_code" => 'pinjaman_bank',
                "name" => 'Potongan Pinjaman Bank',
                "function_name" => 'customSalaryModulePotonganPinjamanBank',
                "active" => 1,
            ],
            [
                "id" => 13,
                "module_code" => 'potongan_sanksi',
                "name" => 'Potongan Sanksi',
                "function_name" => 'customSalaryModulePotonganSanksi',
                "active" => 1,
            ],
            [
                "id" => 14,
                "module_code" => 'tunjangan_kehadiran',
                "name" => 'Tunjangan Kehadiran',
                "function_name" => 'customSalaryModuleTunjanganKehadiran',
                "active" => 1,
            ],
            
        ];
        DB::table('custom_salary_modules')->insert($data);
        DB::statement("SELECT setval('custom_salary_modules_id_seq', (SELECT MAX(id) FROM custom_salary_modules)+1)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_salary_modules');
    }
}
