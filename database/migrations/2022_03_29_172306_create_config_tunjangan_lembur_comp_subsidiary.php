<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateConfigTunjanganLemburCompSubsidiary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('config_tunjangan_lembur_comp_subsidiary', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('salary_component_id')->constrained('master_salary_components')->unique();
            $table->float('koefisien', 65, 2)->nullable(true)->default(1);
            $table->text('description')->nullable();
            $table->integer('active')->nullable(true)->default('1');
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->timestampsTz($precision = 0);
        });
        $data = [
            [
                "salary_component_id" => 6,
                "koefisien" => 1
            ],
            [
                "salary_component_id" => 7,
                "koefisien" => 1
            ],
        ];
        DB::table('config_tunjangan_lembur_comp_subsidiary')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('config_tunjangan_lembur_comp_subsidiary');
    }
}
