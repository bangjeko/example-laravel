<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateTunjanganLemburDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tunjangan_lembur_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('tunjangan_lembur_id')->constrained('tunjangan_lembur')->onDelete('cascade');
            $table->foreignId('employee_id')->constrained('employees');
            $table->float('upah_value', 65, 2)->nullable(true)->default(0);
            $table->float('pokok_tunjangan_tetap_value', 65, 2)->nullable(true)->default(0);
            $table->float('three_quarter_upah_value', 65, 2)->nullable(true)->default(0);
            $table->float('upah_lembur_perjam_value', 65, 2)->nullable(true)->default(0);
            $table->float('penerimaan_value', 65, 2)->nullable(true)->default(0);
            $table->float('pembulatan_penerimaan_value', 65, 2)->nullable(true)->default(0);
            $table->text('description')->nullable();
            $table->string('status_code')->nullable()->default('not_calculated_yet');
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->timestampsTz($precision = 0);

            $table->unique(array('tunjangan_lembur_id', 'employee_id'));
        });

        $data = [
            [
                "id" => 17,
                "tunjangan_lembur_id" => 1,
                "employee_id" => 19,
                "upah_value" => 4756400,
                "pokok_tunjangan_tetap_value" => 3496000,
                "three_quarter_upah_value" => 3567300,
                "upah_lembur_perjam_value" => 20620,
                "penerimaan_value" => 1648570,
                "pembulatan_penerimaan_value" => 1648600,
                "description" => null,
                "status_code" => "calculated",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 10:58:39+00",
                "updated_at" => "2022-04-21 10:59:56+00"
            ],
            [
                "id" => 3,
                "tunjangan_lembur_id" => 1,
                "employee_id" => 34,
                "upah_value" => 5037940,
                "pokok_tunjangan_tetap_value" => 3777540,
                "three_quarter_upah_value" => 3778455,
                "upah_lembur_perjam_value" => 21841,
                "penerimaan_value" => 1402856,
                "pembulatan_penerimaan_value" => 1402900,
                "description" => null,
                "status_code" => "calculated",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:01:18+00"
            ],
            [
                "id" => 4,
                "tunjangan_lembur_id" => 1,
                "employee_id" => 51,
                "upah_value" => 5040585,
                "pokok_tunjangan_tetap_value" => 3780185,
                "three_quarter_upah_value" => 3780439,
                "upah_lembur_perjam_value" => 21852,
                "penerimaan_value" => 1073958,
                "pembulatan_penerimaan_value" => 1074000,
                "description" => null,
                "status_code" => "calculated",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:04:17+00"
            ],
            [
                "id" => 2,
                "tunjangan_lembur_id" => 1,
                "employee_id" => 29,
                "upah_value" => 5223085,
                "pokok_tunjangan_tetap_value" => 3962685,
                "three_quarter_upah_value" => 3917314,
                "upah_lembur_perjam_value" => 30191,
                "penerimaan_value" => 243137,
                "pembulatan_penerimaan_value" => 243100,
                "description" => null,
                "status_code" => "calculated",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 10:46:16+00"
            ],
            [
                "id" => 9,
                "tunjangan_lembur_id" => 1,
                "employee_id" => 32,
                "upah_value" => 5009240,
                "pokok_tunjangan_tetap_value" => 3748840,
                "three_quarter_upah_value" => 3756930,
                "upah_lembur_perjam_value" => 21716,
                "penerimaan_value" => 1025656,
                "pembulatan_penerimaan_value" => 1025700,
                "description" => null,
                "status_code" => "calculated",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:05:29+00"
            ],
            [
                "id" => 16,
                "tunjangan_lembur_id" => 1,
                "employee_id" => 28,
                "upah_value" => 6094385,
                "pokok_tunjangan_tetap_value" => 4833985,
                "three_quarter_upah_value" => 4570789,
                "upah_lembur_perjam_value" => 35228,
                "penerimaan_value" => 696248,
                "pembulatan_penerimaan_value" => 696200,
                "description" => null,
                "status_code" => "calculated",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 10:51:35+00",
                "updated_at" => "2022-04-21 10:52:01+00"
            ],
            [
                "id" => 14,
                "tunjangan_lembur_id" => 1,
                "employee_id" => 56,
                "upah_value" => 4756400,
                "pokok_tunjangan_tetap_value" => 3496000,
                "three_quarter_upah_value" => 3567300,
                "upah_lembur_perjam_value" => 20620,
                "penerimaan_value" => 638660,
                "pembulatan_penerimaan_value" => 638700,
                "description" => null,
                "status_code" => "calculated",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:06:20+00"
            ],
            [
                "id" => 5,
                "tunjangan_lembur_id" => 1,
                "employee_id" => 52,
                "upah_value" => 5011885,
                "pokok_tunjangan_tetap_value" => 3751485,
                "three_quarter_upah_value" => 3758914,
                "upah_lembur_perjam_value" => 21728,
                "penerimaan_value" => 219188,
                "pembulatan_penerimaan_value" => 219200,
                "description" => null,
                "status_code" => "calculated",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 10:53:44+00"
            ],
            [
                "id" => 13,
                "tunjangan_lembur_id" => 1,
                "employee_id" => 33,
                "upah_value" => 6279185,
                "pokok_tunjangan_tetap_value" => 5018785,
                "three_quarter_upah_value" => 4709389,
                "upah_lembur_perjam_value" => 36296,
                "penerimaan_value" => 1257776,
                "pembulatan_penerimaan_value" => 1257800,
                "description" => null,
                "status_code" => "calculated",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:07:01+00"
            ],
            [
                "id" => 8,
                "tunjangan_lembur_id" => 1,
                "employee_id" => 41,
                "upah_value" => 5040585,
                "pokok_tunjangan_tetap_value" => 3780185,
                "three_quarter_upah_value" => 3780439,
                "upah_lembur_perjam_value" => 21852,
                "penerimaan_value" => 744456,
                "pembulatan_penerimaan_value" => 744500,
                "description" => null,
                "status_code" => "calculated",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:07:47+00"
            ],
            [
                "id" => 6,
                "tunjangan_lembur_id" => 1,
                "employee_id" => 35,
                "upah_value" => 4475425,
                "pokok_tunjangan_tetap_value" => 3215025,
                "three_quarter_upah_value" => 3356569,
                "upah_lembur_perjam_value" => 19402,
                "penerimaan_value" => 511583,
                "pembulatan_penerimaan_value" => 511600,
                "description" => null,
                "status_code" => "calculated",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:09:32+00"
            ],
            [
                "id" => 15,
                "tunjangan_lembur_id" => 1,
                "employee_id" => 58,
                "upah_value" => 4475425,
                "pokok_tunjangan_tetap_value" => 3215025,
                "three_quarter_upah_value" => 3356569,
                "upah_lembur_perjam_value" => 19402,
                "penerimaan_value" => 1255990,
                "pembulatan_penerimaan_value" => 1256000,
                "description" => null,
                "status_code" => "calculated",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:10:27+00"
            ],
            [
                "id" => 11,
                "tunjangan_lembur_id" => 1,
                "employee_id" => 20,
                "upah_value" => 4475425,
                "pokok_tunjangan_tetap_value" => 3215025,
                "three_quarter_upah_value" => 3356569,
                "upah_lembur_perjam_value" => 19402,
                "penerimaan_value" => 663136,
                "pembulatan_penerimaan_value" => 663100,
                "description" => null,
                "status_code" => "calculated",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:11:11+00"
            ],
            [
                "id" => 7,
                "tunjangan_lembur_id" => 1,
                "employee_id" => 11,
                "upah_value" => 3000000,
                "pokok_tunjangan_tetap_value" => 3000000,
                "three_quarter_upah_value" => 2250000,
                "upah_lembur_perjam_value" => 17341,
                "penerimaan_value" => 407516,
                "pembulatan_penerimaan_value" => 407500,
                "description" => null,
                "status_code" => "calculated",
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:11:38+00"
            ]
        ];
        DB::table('tunjangan_lembur_details')->insert($data);
        DB::statement("SELECT setval('tunjangan_lembur_details_id_seq', (SELECT MAX(id) FROM tunjangan_lembur_details)+1)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tunjangan_lembur_details');
    }
}
