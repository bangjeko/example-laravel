<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateKoperasiMembers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('koperasi_members', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('employee_id')->unique()->constrained('employees');
            $table->text('description')->nullable();
            $table->integer('active')->nullable(true)->default('1');
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->timestampsTz($precision = 0);
        });
        $data = [
            [
                "id" => 2,
                "employee_id" => 7,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-03-30 08:43:42+00",
                "updated_at" => "2022-03-30 08:43:42+00"
            ],
            [
                "id" => 3,
                "employee_id" => 77,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-03-30 08:44:03+00",
                "updated_at" => "2022-03-30 08:44:03+00"
            ],
            [
                "id" => 4,
                "employee_id" => 66,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-03-30 08:44:20+00",
                "updated_at" => "2022-03-30 08:44:20+00"
            ],
            [
                "id" => 5,
                "employee_id" => 16,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-03-30 08:44:33+00",
                "updated_at" => "2022-03-30 08:44:33+00"
            ],
            [
                "id" => 6,
                "employee_id" => 46,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-03-30 08:44:47+00",
                "updated_at" => "2022-03-30 08:44:47+00"
            ],
            [
                "id" => 7,
                "employee_id" => 59,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-03-30 08:45:07+00",
                "updated_at" => "2022-03-30 08:45:07+00"
            ],
            [
                "id" => 8,
                "employee_id" => 34,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-03-30 08:45:18+00",
                "updated_at" => "2022-03-30 08:45:18+00"
            ],
            [
                "id" => 9,
                "employee_id" => 20,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-03-30 08:47:24+00",
                "updated_at" => "2022-03-30 08:47:24+00"
            ],
            [
                "id" => 10,
                "employee_id" => 17,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-03-30 08:47:34+00",
                "updated_at" => "2022-03-30 08:47:34+00"
            ],
            [
                "id" => 12,
                "employee_id" => 69,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2021-07-22 22:34:23+00",
                "updated_at" => "2021-07-22 22:34:23+00"
            ],
            [
                "id" => 13,
                "employee_id" => 27,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2021-07-22 22:34:37+00",
                "updated_at" => "2021-07-22 22:34:37+00"
            ],
            [
                "id" => 15,
                "employee_id" => 70,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2021-07-22 22:35:03+00",
                "updated_at" => "2021-07-22 22:35:03+00"
            ],
            [
                "id" => 16,
                "employee_id" => 64,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2021-07-22 22:35:14+00",
                "updated_at" => "2021-07-22 22:35:14+00"
            ],
            [
                "id" => 17,
                "employee_id" => 45,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2021-07-22 22:35:26+00",
                "updated_at" => "2021-07-22 22:35:26+00"
            ],
            [
                "id" => 18,
                "employee_id" => 44,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2021-07-22 22:35:39+00",
                "updated_at" => "2021-07-22 22:35:39+00"
            ],
            [
                "id" => 19,
                "employee_id" => 10,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2021-07-22 22:35:49+00",
                "updated_at" => "2021-07-22 22:35:49+00"
            ],
            [
                "id" => 20,
                "employee_id" => 30,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2021-07-22 22:36:05+00",
                "updated_at" => "2021-07-22 22:36:05+00"
            ],
            [
                "id" => 21,
                "employee_id" => 40,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2021-07-22 22:36:18+00",
                "updated_at" => "2021-07-22 22:36:18+00"
            ],
            [
                "id" => 22,
                "employee_id" => 12,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2021-07-22 22:36:32+00",
                "updated_at" => "2021-07-22 22:36:32+00"
            ],
            [
                "id" => 23,
                "employee_id" => 53,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2021-07-22 22:36:45+00",
                "updated_at" => "2021-07-22 22:36:45+00"
            ],
            [
                "id" => 24,
                "employee_id" => 80,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2021-07-22 22:37:01+00",
                "updated_at" => "2021-07-22 22:37:01+00"
            ],
            [
                "id" => 26,
                "employee_id" => 18,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2021-07-22 22:37:31+00",
                "updated_at" => "2021-07-22 22:37:31+00"
            ],
            [
                "id" => 27,
                "employee_id" => 76,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2021-07-22 22:37:48+00",
                "updated_at" => "2021-07-22 22:37:48+00"
            ],
            [
                "id" => 28,
                "employee_id" => 36,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2021-07-22 22:38:12+00",
                "updated_at" => "2021-07-22 22:38:12+00"
            ],
            [
                "id" => 30,
                "employee_id" => 28,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2021-07-22 22:38:54+00",
                "updated_at" => "2021-07-22 22:38:54+00"
            ],
            [
                "id" => 31,
                "employee_id" => 38,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2021-07-22 22:39:08+00",
                "updated_at" => "2021-07-22 22:39:08+00"
            ],
            [
                "id" => 32,
                "employee_id" => 33,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2021-07-22 22:39:18+00",
                "updated_at" => "2021-07-22 22:39:18+00"
            ],
            [
                "id" => 33,
                "employee_id" => 25,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2021-07-22 22:39:27+00",
                "updated_at" => "2021-07-22 22:39:27+00"
            ],
            [
                "id" => 34,
                "employee_id" => 41,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2021-07-22 22:39:39+00",
                "updated_at" => "2021-07-22 22:39:39+00"
            ],
            [
                "id" => 35,
                "employee_id" => 29,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2021-07-22 22:39:48+00",
                "updated_at" => "2021-07-22 22:39:48+00"
            ],
            [
                "id" => 36,
                "employee_id" => 52,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2021-07-22 22:39:56+00",
                "updated_at" => "2021-07-22 22:39:56+00"
            ],
            [
                "id" => 37,
                "employee_id" => 51,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2021-07-22 22:40:06+00",
                "updated_at" => "2021-07-22 22:40:06+00"
            ],
            [
                "id" => 38,
                "employee_id" => 42,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2021-07-22 22:40:15+00",
                "updated_at" => "2021-07-22 22:40:15+00"
            ],
            [
                "id" => 39,
                "employee_id" => 31,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2021-07-22 22:40:29+00",
                "updated_at" => "2021-07-22 22:40:29+00"
            ],
            [
                "id" => 40,
                "employee_id" => 32,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2021-07-22 22:41:00+00",
                "updated_at" => "2021-07-22 22:41:00+00"
            ],
            [
                "id" => 41,
                "employee_id" => 74,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2021-07-22 22:41:11+00",
                "updated_at" => "2021-07-22 22:41:11+00"
            ],
            [
                "id" => 42,
                "employee_id" => 14,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2021-07-22 22:41:19+00",
                "updated_at" => "2021-07-22 22:41:19+00"
            ],
            [
                "id" => 43,
                "employee_id" => 13,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2021-07-22 22:41:28+00",
                "updated_at" => "2021-07-22 22:41:28+00"
            ],
            [
                "id" => 44,
                "employee_id" => 23,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2021-07-22 22:41:40+00",
                "updated_at" => "2021-07-22 22:41:40+00"
            ],
            [
                "id" => 45,
                "employee_id" => 15,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2021-07-22 22:41:52+00",
                "updated_at" => "2021-07-22 22:41:52+00"
            ],
            [
                "id" => 47,
                "employee_id" => 35,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2021-07-22 22:42:21+00",
                "updated_at" => "2021-07-22 22:42:21+00"
            ],
            [
                "id" => 48,
                "employee_id" => 19,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2021-07-22 22:42:36+00",
                "updated_at" => "2021-07-22 22:42:36+00"
            ],
            [
                "id" => 49,
                "employee_id" => 58,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2021-07-22 22:42:46+00",
                "updated_at" => "2021-07-22 22:42:46+00"
            ],
            [
                "id" => 50,
                "employee_id" => 56,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2021-07-22 22:43:04+00",
                "updated_at" => "2021-07-22 22:43:04+00"
            ],
            [
                "id" => 52,
                "employee_id" => 2,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 13:42:07+00",
                "updated_at" => "2022-04-19 13:42:07+00"
            ],
            [
                "id" => 53,
                "employee_id" => 94,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 13:42:42+00",
                "updated_at" => "2022-04-19 13:42:42+00"
            ],
            [
                "id" => 46,
                "employee_id" => 90,
                "description" => null,
                "active" => 0,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2021-07-22 22:42:05+00",
                "updated_at" => "2022-04-19 13:43:00+00"
            ],
            [
                "id" => 54,
                "employee_id" => 24,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 13:43:17+00",
                "updated_at" => "2022-04-19 13:43:17+00"
            ],
            [
                "id" => 55,
                "employee_id" => 71,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 13:43:32+00",
                "updated_at" => "2022-04-19 13:43:32+00"
            ],
            [
                "id" => 56,
                "employee_id" => 68,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 13:43:53+00",
                "updated_at" => "2022-04-19 13:43:53+00"
            ],
            [
                "id" => 57,
                "employee_id" => 65,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 13:44:16+00",
                "updated_at" => "2022-04-19 13:44:16+00"
            ],
            [
                "id" => 58,
                "employee_id" => 11,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 13:44:40+00",
                "updated_at" => "2022-04-19 13:44:40+00"
            ],
            [
                "id" => 59,
                "employee_id" => 9,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 13:44:52+00",
                "updated_at" => "2022-04-19 13:44:52+00"
            ],
            [
                "id" => 60,
                "employee_id" => 43,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 13:45:00+00",
                "updated_at" => "2022-04-19 13:45:00+00"
            ],
            [
                "id" => 61,
                "employee_id" => 47,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 13:45:08+00",
                "updated_at" => "2022-04-19 13:45:08+00"
            ],
            [
                "id" => 62,
                "employee_id" => 78,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 13:45:17+00",
                "updated_at" => "2022-04-19 13:45:17+00"
            ],
            [
                "id" => 63,
                "employee_id" => 75,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 13:45:24+00",
                "updated_at" => "2022-04-19 13:45:24+00"
            ],
            [
                "id" => 64,
                "employee_id" => 57,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 13:45:34+00",
                "updated_at" => "2022-04-19 13:45:34+00"
            ],
            [
                "id" => 65,
                "employee_id" => 50,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 13:45:42+00",
                "updated_at" => "2022-04-19 13:45:42+00"
            ],
            [
                "id" => 25,
                "employee_id" => 49,
                "description" => null,
                "active" => 0,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2021-07-22 22:37:19+00",
                "updated_at" => "2022-04-19 14:17:43+00"
            ],
            [
                "id" => 14,
                "employee_id" => 88,
                "description" => null,
                "active" => 0,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2021-07-22 22:34:51+00",
                "updated_at" => "2022-04-19 14:17:49+00"
            ],
            [
                "id" => 1,
                "employee_id" => 6,
                "description" => null,
                "active" => 0,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-03-30 08:42:25+00",
                "updated_at" => "2022-04-19 14:17:55+00"
            ],
            [
                "id" => 29,
                "employee_id" => 89,
                "description" => null,
                "active" => 0,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2021-07-22 22:38:42+00",
                "updated_at" => "2022-04-19 14:18:02+00"
            ]
        ];
        DB::table('koperasi_members')->insert($data);
        DB::statement("SELECT setval('koperasi_members_id_seq', (SELECT MAX(id) FROM koperasi_members)+1)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('koperasi_members');
    }
}
