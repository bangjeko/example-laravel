<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateBpjsTkBills extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bpjs_tk_bills', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('periode_month')->unique();
            $table->float('total_iuran_jkk_value', 65, 2)->nullable(true)->default(0);
            $table->float('total_iuran_jkm_value', 65, 2)->nullable(true)->default(0);
            $table->float('total_iuran_jp_bb_company_value', 65, 2)->nullable(true)->default(0);
            $table->float('total_iuran_jp_bb_employee_value', 65, 2)->nullable(true)->default(0);
            $table->float('total_iuran_jht_company_value', 65, 2)->nullable(true)->default(0);
            $table->float('total_iuran_jht_employee_value', 65, 2)->nullable(true)->default(0);
            $table->float('total_kekurangan_jp_value', 65, 2)->nullable(true)->default(0);
            $table->float('total_iuran_bpjs_company_value', 65, 2)->nullable(true)->default(0);
            $table->float('total_iuran_bpjs_employee_value', 65, 2)->nullable(true)->default(0);
            $table->float('total_iuran_value', 65, 2)->nullable(true)->default(0);
            $table->text('description')->nullable();
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->timestampsTz($precision = 0);
        });

        $data = [
            [
                "id" => 1,
                "periode_month" => "2022-01-01",
                "total_iuran_jkk_value" => 4096306,
                "total_iuran_jkm_value" => 2275723,
                "total_iuran_jp_bb_company_value" => 7578933,
                "total_iuran_jp_bb_employee_value" => 3789464,
                "total_iuran_jht_company_value" => 28067263,
                "total_iuran_jht_employee_value" => 15171497,
                "total_kekurangan_jp_value" => 0,
                "total_iuran_bpjs_company_value" => 27110229,
                "total_iuran_bpjs_employee_value" => 33868957,
                "total_iuran_value" => 60979186,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 14:30:34+00",
                "updated_at" => "2022-04-21 08:23:06+00"
            ]
        ];
        DB::table('bpjs_tk_bills')->insert($data);
        DB::statement("SELECT setval('bpjs_tk_bills_id_seq', (SELECT MAX(id) FROM bpjs_tk_bills)+1)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bpjs_tk_bills');
    }
}
