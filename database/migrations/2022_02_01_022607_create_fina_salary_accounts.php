<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateFinaSalaryAccounts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fina_salary_accounts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('account_code')->unique();
            $table->string('account_name');
            $table->string('debit_kredit');
            $table->integer('active')->nullable(true)->default('1');
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->timestampsTz($precision = 0);
        });

        $data = [
            [
                "id" => 437,
                "account_code" => "213-12",
                "account_name" => "Dana Pensiun",
                "debit_kredit" => "kredit",
                "active" => 1,
                "created_by" => null,
                "updated_by" => null,
                "created_at" => "2022-05-11 16:35:28+07",
                "updated_at" => "2022-05-11 16:35:28+07"
            ],
            [
                "id" => 439,
                "account_code" => "213-14",
                "account_name" => "Iuran BPJS Tenaga Kerja",
                "debit_kredit" => "kredit",
                "active" => 1,
                "created_by" => null,
                "updated_by" => null,
                "created_at" => "2022-05-11 16:35:28+07",
                "updated_at" => "2022-05-11 16:35:28+07"
            ],
            [
                "id" => 440,
                "account_code" => "213-15",
                "account_name" => "Iuran BPJS Kesehatan",
                "debit_kredit" => "kredit",
                "active" => 1,
                "created_by" => null,
                "updated_by" => null,
                "created_at" => "2022-05-11 16:35:28+07",
                "updated_at" => "2022-05-11 16:35:28+07"
            ],
            [
                "id" => 817,
                "account_code" => "810-35",
                "account_name" => "Pendapatan Lainnya",
                "debit_kredit" => "debit",
                "active" => 1,
                "created_by" => null,
                "updated_by" => null,
                "created_at" => "2022-05-11 16:35:28+07",
                "updated_at" => "2022-05-11 16:35:28+07"
            ],
            [
                "id" => 1077,
                "account_code" => "611-10.1.1.1",
                "account_name" => "Honorarium Dewan Komisaris-Tanah",
                "debit_kredit" => "debit",
                "active" => 1,
                "created_by" => null,
                "updated_by" => null,
                "created_at" => "2022-05-11 16:35:28+07",
                "updated_at" => "2022-05-11 16:35:28+07"
            ],
            [
                "id" => 1110,
                "account_code" => "611-20.1.1.1",
                "account_name" => "Honorarium Sekretaris Dewan-Tanah",
                "debit_kredit" => "debit",
                "active" => 1,
                "created_by" => null,
                "updated_by" => null,
                "created_at" => "2022-05-11 16:35:28+07",
                "updated_at" => "2022-05-11 16:35:28+07"
            ],
            [
                "id" => 1143,
                "account_code" => "611-30.1.1.1",
                "account_name" => "Gaji Direksi-Tanah",
                "debit_kredit" => "debit",
                "active" => 1,
                "created_by" => null,
                "updated_by" => null,
                "created_at" => "2022-05-11 16:35:28+07",
                "updated_at" => "2022-05-11 16:35:28+07"
            ],
            [
                "id" => 1176,
                "account_code" => "611-40.1.1.1",
                "account_name" => "Honor Komite Audit-Tanah",
                "debit_kredit" => "debit",
                "active" => 1,
                "created_by" => null,
                "updated_by" => null,
                "created_at" => "2022-05-11 16:35:28+07",
                "updated_at" => "2022-05-11 16:35:28+07"
            ],
            [
                "id" => 1209,
                "account_code" => "611-51.1.1.1",
                "account_name" => "Gaji Manajer-Tanah",
                "debit_kredit" => "debit",
                "active" => 1,
                "created_by" => null,
                "updated_by" => null,
                "created_at" => "2022-05-11 16:35:28+07",
                "updated_at" => "2022-05-11 16:35:28+07"
            ],
            [
                "id" => 1242,
                "account_code" => "611-52.1.1.1",
                "account_name" => "Gaji Asisten Manajer-Tanah",
                "debit_kredit" => "debit",
                "active" => 1,
                "created_by" => null,
                "updated_by" => null,
                "created_at" => "2022-05-11 16:35:28+07",
                "updated_at" => "2022-05-11 16:35:28+07"
            ],
            [
                "id" => 1275,
                "account_code" => "611-53.1.1.1",
                "account_name" => "Gaji Pelaksana-Tanah",
                "debit_kredit" => "debit",
                "active" => 1,
                "created_by" => null,
                "updated_by" => null,
                "created_at" => "2022-05-11 16:35:28+07",
                "updated_at" => "2022-05-11 16:35:28+07"
            ],
            [
                "id" => 1308,
                "account_code" => "611-54.1.1.1",
                "account_name" => "Gaji Supervisor-Tanah",
                "debit_kredit" => "debit",
                "active" => 1,
                "created_by" => null,
                "updated_by" => null,
                "created_at" => "2022-05-11 16:35:28+07",
                "updated_at" => "2022-05-11 16:35:28+07"
            ],
            [
                "id" => 1341,
                "account_code" => "611-55.1.1.1",
                "account_name" => "Gaji Pegawai Kontrak-Tanah",
                "debit_kredit" => "debit",
                "active" => 1,
                "created_by" => null,
                "updated_by" => null,
                "created_at" => "2022-05-11 16:35:28+07",
                "updated_at" => "2022-05-11 16:35:28+07"
            ],
            [
                "id" => 1407,
                "account_code" => "611-60.1.1.1",
                "account_name" => "Upah Lembur-Tanah",
                "debit_kredit" => "debit",
                "active" => 1,
                "created_by" => null,
                "updated_by" => null,
                "created_at" => "2022-05-11 16:35:28+07",
                "updated_at" => "2022-05-11 16:35:28+07"
            ],
            [
                "id" => 1440,
                "account_code" => "612-20.1.1.1",
                "account_name" => "Tunjangan Kesehatan-Tanah",
                "debit_kredit" => "debit",
                "active" => 1,
                "created_by" => null,
                "updated_by" => null,
                "created_at" => "2022-05-11 16:35:28+07",
                "updated_at" => "2022-05-11 16:35:28+07"
            ],
            [
                "id" => 1473,
                "account_code" => "612-21.1.1.1",
                "account_name" => "Tunjangan Astek-Tanah",
                "debit_kredit" => "debit",
                "active" => 1,
                "created_by" => null,
                "updated_by" => null,
                "created_at" => "2022-05-11 16:35:28+07",
                "updated_at" => "2022-05-11 16:35:28+07"
            ],
            [
                "id" => 1506,
                "account_code" => "612-22.1.1.1",
                "account_name" => "Tunjangan Dana Pensiun-Tanah",
                "debit_kredit" => "debit",
                "active" => 1,
                "created_by" => null,
                "updated_by" => null,
                "created_at" => "2022-05-11 16:35:28+07",
                "updated_at" => "2022-05-11 16:35:28+07"
            ],
            [
                "id" => 1539,
                "account_code" => "612-23.1.1.1",
                "account_name" => "Tunjangan Jabatan-Tanah",
                "debit_kredit" => "debit",
                "active" => 1,
                "created_by" => null,
                "updated_by" => null,
                "created_at" => "2022-05-11 16:35:28+07",
                "updated_at" => "2022-05-11 16:35:28+07"
            ],
            [
                "id" => 1605,
                "account_code" => "612-27.1.1.1",
                "account_name" => "Kompensasi Rumah Dinas-Tanah",
                "debit_kredit" => "debit",
                "active" => 1,
                "created_by" => null,
                "updated_by" => null,
                "created_at" => "2022-05-11 16:35:28+07",
                "updated_at" => "2022-05-11 16:35:28+07"
            ],
            [
                "id" => 1671,
                "account_code" => "612-29.1.1.1",
                "account_name" => "Tunjangan Komunikasi-Tanah",
                "debit_kredit" => "debit",
                "active" => 1,
                "created_by" => null,
                "updated_by" => null,
                "created_at" => "2022-05-11 16:35:28+07",
                "updated_at" => "2022-05-11 16:35:28+07"
            ],
            [
                "id" => 1836,
                "account_code" => "612-34.1.1.1",
                "account_name" => "Tunjangan Khusus-Tanah",
                "debit_kredit" => "debit",
                "active" => 1,
                "created_by" => null,
                "updated_by" => null,
                "created_at" => "2022-05-11 16:35:28+07",
                "updated_at" => "2022-05-11 16:35:28+07"
            ],
            [
                "id" => 2034,
                "account_code" => "612-40.1.1.1",
                "account_name" => "Tunjangan KPI-Tanah",
                "debit_kredit" => "debit",
                "active" => 1,
                "created_by" => null,
                "updated_by" => null,
                "created_at" => "2022-05-11 16:35:28+07",
                "updated_at" => "2022-05-11 16:35:28+07"
            ],
            [
                "id" => 2100,
                "account_code" => "613-20.1.1.1",
                "account_name" => "Makan Siang Manajer-Tanah",
                "debit_kredit" => "debit",
                "active" => 1,
                "created_by" => null,
                "updated_by" => null,
                "created_at" => "2022-05-11 16:35:28+07",
                "updated_at" => "2022-05-11 16:35:28+07"
            ],
            [
                "id" => 2133,
                "account_code" => "613-30.1.1.1",
                "account_name" => "Makan Siang Asisten Manajer-Tanah",
                "debit_kredit" => "debit",
                "active" => 1,
                "created_by" => null,
                "updated_by" => null,
                "created_at" => "2022-05-11 16:35:28+07",
                "updated_at" => "2022-05-11 16:35:28+07"
            ],
            [
                "id" => 2166,
                "account_code" => "613-40.1.1.1",
                "account_name" => "Makan Siang Pelaksana-Tanah",
                "debit_kredit" => "debit",
                "active" => 1,
                "created_by" => null,
                "updated_by" => null,
                "created_at" => "2022-05-11 16:35:28+07",
                "updated_at" => "2022-05-11 16:35:28+07"
            ],
            [
                "id" => 2199,
                "account_code" => "613-50.1.1.1",
                "account_name" => "Makan Siang Pegawai Kontrak-Tanah",
                "debit_kredit" => "debit",
                "active" => 1,
                "created_by" => null,
                "updated_by" => null,
                "created_at" => "2022-05-11 16:35:28+07",
                "updated_at" => "2022-05-11 16:35:28+07"
            ],
            [
                "id" => 2265,
                "account_code" => "613-70.1.1.1",
                "account_name" => "Makan Siang Supervisor-Tanah",
                "debit_kredit" => "debit",
                "active" => 1,
                "created_by" => null,
                "updated_by" => null,
                "created_at" => "2022-05-11 16:35:28+07",
                "updated_at" => "2022-05-11 16:35:28+07"
            ],
            [
                "id" => 2463,
                "account_code" => "615-01.1.1.1",
                "account_name" => "Transport Dewan Komisaris-Tanah",
                "debit_kredit" => "debit",
                "active" => 1,
                "created_by" => null,
                "updated_by" => null,
                "created_at" => "2022-05-11 16:35:28+07",
                "updated_at" => "2022-05-11 16:35:28+07"
            ],
            [
                "id" => 2496,
                "account_code" => "615-02.1.1.1",
                "account_name" => "Transport  Sekretaris Dewan Komisaris-Tanah",
                "debit_kredit" => "debit",
                "active" => 1,
                "created_by" => null,
                "updated_by" => null,
                "created_at" => "2022-05-11 16:35:28+07",
                "updated_at" => "2022-05-11 16:35:28+07"
            ],
            [
                "id" => 2562,
                "account_code" => "615-20.1.1.1",
                "account_name" => "Transport Manajer-Tanah",
                "debit_kredit" => "debit",
                "active" => 1,
                "created_by" => null,
                "updated_by" => null,
                "created_at" => "2022-05-11 16:35:28+07",
                "updated_at" => "2022-05-11 16:35:28+07"
            ],
            [
                "id" => 2595,
                "account_code" => "615-30.1.1.1",
                "account_name" => "Transport Asisten Manajer-Tanah",
                "debit_kredit" => "debit",
                "active" => 1,
                "created_by" => null,
                "updated_by" => null,
                "created_at" => "2022-05-11 16:35:28+07",
                "updated_at" => "2022-05-11 16:35:28+07"
            ],
            [
                "id" => 2628,
                "account_code" => "615-40.1.1.1",
                "account_name" => "Transport Pelaksana-Tanah",
                "debit_kredit" => "debit",
                "active" => 1,
                "created_by" => null,
                "updated_by" => null,
                "created_at" => "2022-05-11 16:35:28+07",
                "updated_at" => "2022-05-11 16:35:28+07"
            ],
            [
                "id" => 2661,
                "account_code" => "615-50.1.1.1",
                "account_name" => "Transport Supervisor-Tanah",
                "debit_kredit" => "debit",
                "active" => 1,
                "created_by" => null,
                "updated_by" => null,
                "created_at" => "2022-05-11 16:35:28+07",
                "updated_at" => "2022-05-11 16:35:28+07"
            ],
            [
                "id" => 4674,
                "account_code" => "632-20.1.1.1",
                "account_name" => "Biaya Telepon-Tanah",
                "debit_kredit" => "debit",
                "active" => 1,
                "created_by" => null,
                "updated_by" => null,
                "created_at" => "2022-05-11 16:35:28+07",
                "updated_at" => "2022-05-11 16:35:28+07"
            ],
            [
                "id" => 5169,
                "account_code" => "635-42.1.1.1",
                "account_name" => "Beasiswa Anak Pegawai Berprestasi-Tanah",
                "debit_kredit" => "debit",
                "active" => 1,
                "created_by" => null,
                "updated_by" => null,
                "created_at" => "2022-05-11 16:35:28+07",
                "updated_at" => "2022-05-11 16:35:28+07"
            ],
            [
                "id" => 7213,
                "account_code" => "611-61",
                "account_name" => "Gaji Project Manager KI - Brebes",
                "debit_kredit" => "debit",
                "active" => 1,
                "created_by" => null,
                "updated_by" => null,
                "created_at" => "2022-05-11 16:35:28+07",
                "updated_at" => "2022-05-11 16:35:28+07"
            ],
            [
                "id" => 7214,
                "account_code" => "611-62",
                "account_name" => "Gaji Head Of KI - Brebes",
                "debit_kredit" => "debit",
                "active" => 1,
                "created_by" => null,
                "updated_by" => null,
                "created_at" => "2022-05-11 16:35:28+07",
                "updated_at" => "2022-05-11 16:35:28+07"
            ],
            [
                "id" => 7215,
                "account_code" => "611-63",
                "account_name" => "Gaji Staf KI - Brebes",
                "debit_kredit" => "debit",
                "active" => 1,
                "created_by" => null,
                "updated_by" => null,
                "created_at" => "2022-05-11 16:35:28+07",
                "updated_at" => "2022-05-11 16:35:28+07"
            ],
            [
                "id" => 7216,
                "account_code" => "613-80",
                "account_name" => "Makan Siang Head Of KI - Brebes",
                "debit_kredit" => "debit",
                "active" => 1,
                "created_by" => null,
                "updated_by" => null,
                "created_at" => "2022-05-11 16:35:28+07",
                "updated_at" => "2022-05-11 16:35:28+07"
            ],
            [
                "id" => 7217,
                "account_code" => "613-90",
                "account_name" => "Makan Siang Staf KI - Brebes",
                "debit_kredit" => "debit",
                "active" => 1,
                "created_by" => null,
                "updated_by" => null,
                "created_at" => "2022-05-11 16:35:28+07",
                "updated_at" => "2022-05-11 16:35:28+07"
            ],
            [
                "id" => 7218,
                "account_code" => "615-60",
                "account_name" => "Transport Head Of KI - Brebes",
                "debit_kredit" => "debit",
                "active" => 1,
                "created_by" => null,
                "updated_by" => null,
                "created_at" => "2022-05-11 16:35:28+07",
                "updated_at" => "2022-05-11 16:35:28+07"
            ],
            [
                "id" => 7219,
                "account_code" => "615-70",
                "account_name" => "Transport Staf KI - Brebes",
                "debit_kredit" => "debit",
                "active" => 1,
                "created_by" => null,
                "updated_by" => null,
                "created_at" => "2022-05-11 16:35:28+07",
                "updated_at" => "2022-05-11 16:35:28+07"
            ]
        ];
        DB::table('fina_salary_accounts')->insert($data);
        DB::statement("SELECT setval('fina_salary_accounts_id_seq', (SELECT MAX(id) FROM fina_salary_accounts)+1)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fina_salary_accounts');
    }
}
