<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateMasterFamilyRelationships extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_family_relationships', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->unique();
            $table->integer('order_number')->nullable(true);
            $table->integer('active')->nullable(true)->default('1');
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->timestampsTz($precision = 0);
        });

        $data = [
            [
                "name" => 'Kakek',
                "order_number" => '1',
            ],
            [
                "name" => 'Nenek',
                "order_number" => '2',
            ],
            [
                "name" => 'Ayah Kandung',
                "order_number" => '3',
            ],
            [
                "name" => 'Ayah Angkat',
                "order_number" => '4',
            ],
            [
                "name" => 'Ayah Mertua',
                "order_number" => '5',
            ],
            [
                "name" => 'Ibu Kandung',
                "order_number" => '6',
            ],
            [
                "name" => 'Ibu Angkat',
                "order_number" => '7',
            ],
            [
                "name" => 'Ibu Mertua',
                "order_number" => '8',
            ],
            [
                "name" => 'Suami',
                "order_number" => '9',
            ],
            [
                "name" => 'Istri',
                "order_number" => '10',
            ],
            [
                "name" => 'Anak Kandung',
                "order_number" => '11',
            ],
            [
                "name" => 'Anak Angkat',
                "order_number" => '12',
            ],
            [
                "name" => 'Saudara',
                "order_number" => '13',
            ],
        ];
        DB::table('master_family_relationships')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_family_relationships');
    }
}
