<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateMasterEducations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_educations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->unique();
            $table->integer('order_number')->nullable(true);
            $table->integer('active')->nullable(true)->default('1');
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->timestampsTz($precision = 0);
        });

        $data = [
            [
                "name" => 'Tidak Sekolah / Belum Sekolah',
                "order_number" => 1,
            ],
            [
                "name" => 'TK',
                "order_number" => 2,
            ],
            [
                "name" => 'SD',
                "order_number" => 3,
            ],
            [
                "name" => 'SLTP',
                "order_number" => 4,
            ],
            [
                "name" => 'SLTA',
                "order_number" => 5,
            ],
            [
                "name" => 'D-I',
                "order_number" => 6,
            ],
            [
                "name" => 'D-II',
                "order_number" => 7,
            ],
            [
                "name" => 'D-III',
                "order_number" => 8,
            ],
            [
                "name" => 'D-IV',
                "order_number" => 9,
            ],
            [
                "name" => 'S-1',
                "order_number" => 10,
            ],
            [
                "name" => 'S-2',
                "order_number" => 11,
            ],
            [
                "name" => 'S-3',
                "order_number" => 12,
            ],
        ];
        DB::table('master_educations')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_educations');
    }
}
