<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateConfigProsenBpjsTenagaKerja extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('config_prosen_bpjs_tenaga_kerja', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('periode_month')->unique();
            $table->integer('max_age')->default(0);
            $table->float('upah_max_jp_value')->nullable(true)->default(0);
            $table->float('prosen_jkk')->nullable(true)->default(0);
            $table->float('prosen_jkm')->nullable(true)->default(0);
            $table->float('prosen_jp_bb_company')->nullable(true)->default(0);
            $table->float('prosen_jp_bb_employee')->nullable(true)->default(0);
            $table->float('prosen_jht_company')->nullable(true)->default(0);
            $table->float('prosen_jht_employee')->nullable(true)->default(0);
            $table->text('description')->nullable(true);
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->timestampsTz($precision = 0);
        });
        $data = [
            [
                "periode_month" => '2022-01-01',
                "max_age" => 56,
                "upah_max_jp_value" => 8754600,
                "prosen_jkk" => 0.54,
                "prosen_jkm" =>  0.3,
                "prosen_jp_bb_company" => 2,
                "prosen_jp_bb_employee" => 1,
                "prosen_jht_company" => 3.7,
                "prosen_jht_employee" => 2,
            ],
            [
                "periode_month" => '2022-02-01',
                "max_age" => 56,
                "upah_max_jp_value" => 8754600,
                "prosen_jkk" => 0.54,
                "prosen_jkm" =>  0.3,
                "prosen_jp_bb_company" => 2,
                "prosen_jp_bb_employee" => 1,
                "prosen_jht_company" => 3.7,
                "prosen_jht_employee" => 2,
            ],
        ];
        DB::table('config_prosen_bpjs_tenaga_kerja')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('config_prosen_bpjs_tenaga_kerja');
    }
}
