<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateBpjsKesBills extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bpjs_kes_bills', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('periode_month')->unique();
            $table->float('total_iuran_company_value', 65, 2)->nullable(true)->default(0);
            $table->float('total_iuran_employee_value', 65, 2)->nullable(true)->default(0);
            $table->float('total_iuran_tambahan_value', 65, 2)->nullable(true)->default(0);
            $table->float('total_iuran_value', 65, 2)->nullable(true)->default(0);
            $table->text('description')->nullable();
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->timestampsTz($precision = 0);
        });

        $data = [
            [
                "periode_month" => "2022-01-01",
                "total_iuran_company_value" => 16969824,
                "total_iuran_employee_value" => 4242457,
                "total_iuran_tambahan_value" => 1506773,
                "total_iuran_value" => 0,
                "description" => "",
            ]
        ];
        DB::table('bpjs_kes_bills')->insert($data);
        DB::statement("SELECT setval('bpjs_kes_bills_id_seq', (SELECT MAX(id) FROM bpjs_kes_bills)+1)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bpjs_kes_bills');
    }
}
