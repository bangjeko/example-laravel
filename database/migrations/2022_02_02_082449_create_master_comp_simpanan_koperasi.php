<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateMasterCompSimpananKoperasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_comp_simpanan_koperasi', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->unique();
            $table->foreignId('salary_component_id')->nullable()->constrained('master_salary_components');
            $table->integer('active')->nullable(true)->default('1');
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->timestampsTz($precision = 0);
        });
        $data = [
            [
                "name" => 'Simpanan Pokok',
                "salary_component_id" => 22,
            ],
            [
                "name" => 'Simpanan Wajib',
                "salary_component_id" => 23,
            ],
            [
                "name" => 'Simpanan Dana Sosial',
                "salary_component_id" => 24,
            ],
            [
                "name" => 'Simpanan Khusus',
                "salary_component_id" => 27,
            ],
            [
                "name" => 'Simpanan Hari Raya (SHR)',
                "salary_component_id" => 26,
            ],
        ];
        DB::table('master_comp_simpanan_koperasi')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_comp_simpanan_koperasi');
    }
}
