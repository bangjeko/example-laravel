<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateKoperasiSimpanPinjam extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('koperasi_simpan_pinjam', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('periode_month')->unique();
            $table->float('total_simpanan_value', 65, 2)->nullable(true)->default(0);
            $table->float('total_pinjaman_value', 65, 2)->nullable(true)->default(0);
            $table->float('total_value', 65, 2)->nullable(true)->default(0);
            $table->text('description')->nullable();
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->timestampsTz($precision = 0);
        });

        $data = [
            [
                "id" => 1,
                "periode_month" => "2022-01-01",
                "total_simpanan_value" => 28250667,
                "total_pinjaman_value" => 16453333.3333333,
                "total_value" => 44704000.333333,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2021-07-23 05:46:13+07",
                "updated_at" => "2021-07-23 06:19:40+07"
            ]
        ];
        DB::table('koperasi_simpan_pinjam')->insert($data);
        DB::statement("SELECT setval('koperasi_simpan_pinjam_id_seq', (SELECT MAX(id) FROM koperasi_simpan_pinjam)+1)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('koperasi_simpan_pinjam');
    }
}
