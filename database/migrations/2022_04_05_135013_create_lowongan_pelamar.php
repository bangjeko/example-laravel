<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLowonganPelamar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lowongan_pelamar', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('lowongan_id')->nullable()->constrained('lowongan')->onDelete('cascade');
            $table->string('fullname');
            $table->string('nik');
            $table->string('place_of_birth')->nullable();
            $table->date('date_of_birth');
            $table->string('gender')->nullable();
            $table->string('marital_status')->nullable();
            $table->text('address')->nullable();
            $table->foreignId('religion_id')->nullable()->constrained('master_religions');
            $table->string('email');
            $table->string('telephone');
            $table->text('img_photo')->nullable();
            $table->foreignId('education_id')->nullable()->constrained('master_educations');
            $table->string('program_study')->nullable();
            $table->string('institution_name')->nullable();
            $table->string('nationality')->nullable();
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->timestampsTz($precision = 0);

            $table->unique(array('lowongan_id','nik'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lowongan_pelamar');
    }
}
