<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableLemburOnPeriodeRecapsv1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('lembur_on_periode_recaps', function ($table) {
            $table->dropUnique(array('lembur_on_periode_id', 'employee_id', 'date'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('lembur_on_periode_recaps', function ($table) {
            $table->unique(array('lembur_on_periode_id', 'employee_id', 'date'));
        });
    }
}
