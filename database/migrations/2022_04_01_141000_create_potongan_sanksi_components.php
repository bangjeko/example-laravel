<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreatePotonganSanksiComponents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('potongan_sanksi_components', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('potongan_sanksi_detail_id')->constrained('potongan_sanksi_details')->onDelete('cascade');
            $table->foreignId('employee_sanksi_id')->constrained('employee_sanksi');
            $table->float('component_value', 65, 2)->nullable(true)->default(0);
            $table->text('description')->nullable();
            $table->integer('active')->nullable(true)->default('1');
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->timestampsTz($precision = 0);

            $table->unique(array('potongan_sanksi_detail_id', 'employee_sanksi_id'));
        });


        $data = [
            [
                "id" => 1,
                "potongan_sanksi_detail_id" => 2,
                "employee_sanksi_id" => 1,
                "component_value" => 1100000,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 14:46:57+00",
                "updated_at" => "2022-04-19 14:46:57+00"
            ],
            [
                "id" => 2,
                "potongan_sanksi_detail_id" => 1,
                "employee_sanksi_id" => 2,
                "component_value" => 1100000,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-19 14:47:02+00",
                "updated_at" => "2022-04-19 14:47:02+00"
            ]
        ];
        DB::table('potongan_sanksi_components')->insert($data);
        DB::statement("SELECT setval('potongan_sanksi_components_id_seq', (SELECT MAX(id) FROM potongan_sanksi_components)+1)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('potongan_sanksi_components');
    }
}
