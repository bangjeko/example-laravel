<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalaryComponentAccountProportions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salary_component_account_proportions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('salary_component_on_periode_id')->constrained('salary_component_on_periodes')->onDelete('cascade');
            $table->foreignId('fina_salary_account_id')->nullable()->constrained('fina_salary_accounts');
            $table->float('proportion_value')->nullable(true)->default(0);
            $table->integer('active')->nullable(true)->default('1');
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->timestampsTz($precision = 0);

            // $table->unique(array('salary_components_on_periode_id','fina_salary_account_id'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salary_component_account_proportions');
    }
}
