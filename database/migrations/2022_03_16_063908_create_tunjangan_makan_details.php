<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTunjanganMakanDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tunjangan_makan_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('tunjangan_makan_id')->constrained('tunjangan_makan')->onDelete('cascade');
            $table->foreignId('employee_id')->constrained('employees');
            $table->boolean('is_use_presence_data')->nullable(true)->default(true);
            $table->float('base_value', 65, 2)->nullable(true)->default(0);
            $table->float('penerimaan_value', 65, 2)->nullable(true)->default(0);
            $table->text('description')->nullable();
            $table->string('status_code')->nullable()->default('not_calculated_yet');
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->timestampsTz($precision = 0);

            $table->unique(array('tunjangan_makan_id','employee_id'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tunjangan_makan_details');
    }
}
