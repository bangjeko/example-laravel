<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateTunjanganLemburUpahComponents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tunjangan_lembur_upah_components', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('tunjangan_lembur_detail_id')->constrained('tunjangan_lembur_details')->onDelete('cascade');
            $table->foreignId('salary_component_id')->constrained('master_salary_components');
            $table->float('koefisien', 65, 2)->nullable(true)->default(1);
            $table->float('default_value', 65, 2)->nullable(true)->default(0);
            $table->float('component_value', 65, 2)->nullable(true)->default(0);
            $table->text('description')->nullable();
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->timestampsTz($precision = 0);

            $table->unique(array('tunjangan_lembur_detail_id', 'salary_component_id'));
        });

        $data = [
            [
                "id" => 5,
                "tunjangan_lembur_detail_id" => 2,
                "salary_component_id" => 1,
                "koefisien" => 1,
                "default_value" => 3371500,
                "component_value" => 3371500,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 17:43:51+00"
            ],
            [
                "id" => 6,
                "tunjangan_lembur_detail_id" => 2,
                "salary_component_id" => 4,
                "koefisien" => 1,
                "default_value" => 591185,
                "component_value" => 591185,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 17:43:51+00"
            ],
            [
                "id" => 9,
                "tunjangan_lembur_detail_id" => 3,
                "salary_component_id" => 1,
                "koefisien" => 1,
                "default_value" => 3161400,
                "component_value" => 3161400,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 17:43:51+00"
            ],
            [
                "id" => 10,
                "tunjangan_lembur_detail_id" => 3,
                "salary_component_id" => 4,
                "koefisien" => 1,
                "default_value" => 616140,
                "component_value" => 616140,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 17:43:51+00"
            ],
            [
                "id" => 13,
                "tunjangan_lembur_detail_id" => 4,
                "salary_component_id" => 1,
                "koefisien" => 1,
                "default_value" => 3161400,
                "component_value" => 3161400,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 17:43:51+00"
            ],
            [
                "id" => 14,
                "tunjangan_lembur_detail_id" => 4,
                "salary_component_id" => 4,
                "koefisien" => 1,
                "default_value" => 618785,
                "component_value" => 618785,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 17:43:51+00"
            ],
            [
                "id" => 17,
                "tunjangan_lembur_detail_id" => 5,
                "salary_component_id" => 1,
                "koefisien" => 1,
                "default_value" => 3160300,
                "component_value" => 3160300,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 17:43:51+00"
            ],
            [
                "id" => 18,
                "tunjangan_lembur_detail_id" => 5,
                "salary_component_id" => 4,
                "koefisien" => 1,
                "default_value" => 591185,
                "component_value" => 591185,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 17:43:51+00"
            ],
            [
                "id" => 21,
                "tunjangan_lembur_detail_id" => 6,
                "salary_component_id" => 1,
                "koefisien" => 1,
                "default_value" => 2810025,
                "component_value" => 2810025,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 17:43:51+00"
            ],
            [
                "id" => 22,
                "tunjangan_lembur_detail_id" => 6,
                "salary_component_id" => 4,
                "koefisien" => 1,
                "default_value" => 405000,
                "component_value" => 405000,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 17:43:51+00"
            ],
            [
                "id" => 25,
                "tunjangan_lembur_detail_id" => 7,
                "salary_component_id" => 1,
                "koefisien" => 1,
                "default_value" => 3000000,
                "component_value" => 3000000,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 17:43:51+00"
            ],
            [
                "id" => 26,
                "tunjangan_lembur_detail_id" => 8,
                "salary_component_id" => 1,
                "koefisien" => 1,
                "default_value" => 3161400,
                "component_value" => 3161400,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 17:43:51+00"
            ],
            [
                "id" => 27,
                "tunjangan_lembur_detail_id" => 8,
                "salary_component_id" => 4,
                "koefisien" => 1,
                "default_value" => 618785,
                "component_value" => 618785,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 17:43:51+00"
            ],
            [
                "id" => 30,
                "tunjangan_lembur_detail_id" => 9,
                "salary_component_id" => 1,
                "koefisien" => 1,
                "default_value" => 3160300,
                "component_value" => 3160300,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 17:43:51+00"
            ],
            [
                "id" => 31,
                "tunjangan_lembur_detail_id" => 9,
                "salary_component_id" => 4,
                "koefisien" => 1,
                "default_value" => 588540,
                "component_value" => 588540,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 17:43:51+00"
            ],
            [
                "id" => 38,
                "tunjangan_lembur_detail_id" => 11,
                "salary_component_id" => 1,
                "koefisien" => 1,
                "default_value" => 2810025,
                "component_value" => 2810025,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 17:43:51+00"
            ],
            [
                "id" => 39,
                "tunjangan_lembur_detail_id" => 11,
                "salary_component_id" => 4,
                "koefisien" => 1,
                "default_value" => 405000,
                "component_value" => 405000,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 17:43:51+00"
            ],
            [
                "id" => 46,
                "tunjangan_lembur_detail_id" => 13,
                "salary_component_id" => 1,
                "koefisien" => 1,
                "default_value" => 4400000,
                "component_value" => 4400000,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 17:43:51+00"
            ],
            [
                "id" => 47,
                "tunjangan_lembur_detail_id" => 13,
                "salary_component_id" => 4,
                "koefisien" => 1,
                "default_value" => 618785,
                "component_value" => 618785,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 17:43:51+00"
            ],
            [
                "id" => 50,
                "tunjangan_lembur_detail_id" => 14,
                "salary_component_id" => 1,
                "koefisien" => 1,
                "default_value" => 3091000,
                "component_value" => 3091000,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 17:43:51+00"
            ],
            [
                "id" => 51,
                "tunjangan_lembur_detail_id" => 14,
                "salary_component_id" => 4,
                "koefisien" => 1,
                "default_value" => 405000,
                "component_value" => 405000,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 17:43:51+00"
            ],
            [
                "id" => 54,
                "tunjangan_lembur_detail_id" => 15,
                "salary_component_id" => 1,
                "koefisien" => 1,
                "default_value" => 2810025,
                "component_value" => 2810025,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 17:43:51+00"
            ],
            [
                "id" => 55,
                "tunjangan_lembur_detail_id" => 15,
                "salary_component_id" => 4,
                "koefisien" => 1,
                "default_value" => 405000,
                "component_value" => 405000,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 17:43:51+00"
            ],
            [
                "id" => 7,
                "tunjangan_lembur_detail_id" => 2,
                "salary_component_id" => 6,
                "koefisien" => 23,
                "default_value" => 31800,
                "component_value" => 731400,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 10:44:55+00"
            ],
            [
                "id" => 8,
                "tunjangan_lembur_detail_id" => 2,
                "salary_component_id" => 7,
                "koefisien" => 23,
                "default_value" => 23000,
                "component_value" => 529000,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 10:44:55+00"
            ],
            [
                "id" => 60,
                "tunjangan_lembur_detail_id" => 16,
                "salary_component_id" => 6,
                "koefisien" => 23,
                "default_value" => 31800,
                "component_value" => 731400,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:51:35+00",
                "updated_at" => "2022-04-21 10:52:01+00"
            ],
            [
                "id" => 61,
                "tunjangan_lembur_detail_id" => 16,
                "salary_component_id" => 7,
                "koefisien" => 23,
                "default_value" => 23000,
                "component_value" => 529000,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:51:35+00",
                "updated_at" => "2022-04-21 10:52:01+00"
            ],
            [
                "id" => 58,
                "tunjangan_lembur_detail_id" => 16,
                "salary_component_id" => 1,
                "koefisien" => 1,
                "default_value" => 4215200,
                "component_value" => 4215200,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:51:35+00",
                "updated_at" => "2022-04-21 17:51:35+00"
            ],
            [
                "id" => 59,
                "tunjangan_lembur_detail_id" => 16,
                "salary_component_id" => 4,
                "koefisien" => 1,
                "default_value" => 618785,
                "component_value" => 618785,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:51:35+00",
                "updated_at" => "2022-04-21 17:51:35+00"
            ],
            [
                "id" => 19,
                "tunjangan_lembur_detail_id" => 5,
                "salary_component_id" => 6,
                "koefisien" => 23,
                "default_value" => 31800,
                "component_value" => 731400,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 10:53:44+00"
            ],
            [
                "id" => 20,
                "tunjangan_lembur_detail_id" => 5,
                "salary_component_id" => 7,
                "koefisien" => 23,
                "default_value" => 23000,
                "component_value" => 529000,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 10:53:44+00"
            ],
            [
                "id" => 64,
                "tunjangan_lembur_detail_id" => 17,
                "salary_component_id" => 6,
                "koefisien" => 23,
                "default_value" => 31800,
                "component_value" => 731400,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:58:39+00",
                "updated_at" => "2022-04-21 10:59:56+00"
            ],
            [
                "id" => 65,
                "tunjangan_lembur_detail_id" => 17,
                "salary_component_id" => 7,
                "koefisien" => 23,
                "default_value" => 23000,
                "component_value" => 529000,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:58:39+00",
                "updated_at" => "2022-04-21 10:59:56+00"
            ],
            [
                "id" => 62,
                "tunjangan_lembur_detail_id" => 17,
                "salary_component_id" => 1,
                "koefisien" => 1,
                "default_value" => 3091000,
                "component_value" => 3091000,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:58:39+00",
                "updated_at" => "2022-04-21 17:58:39+00"
            ],
            [
                "id" => 63,
                "tunjangan_lembur_detail_id" => 17,
                "salary_component_id" => 4,
                "koefisien" => 1,
                "default_value" => 405000,
                "component_value" => 405000,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:58:39+00",
                "updated_at" => "2022-04-21 17:58:39+00"
            ],
            [
                "id" => 11,
                "tunjangan_lembur_detail_id" => 3,
                "salary_component_id" => 6,
                "koefisien" => 23,
                "default_value" => 31800,
                "component_value" => 731400,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:01:18+00"
            ],
            [
                "id" => 12,
                "tunjangan_lembur_detail_id" => 3,
                "salary_component_id" => 7,
                "koefisien" => 23,
                "default_value" => 23000,
                "component_value" => 529000,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:01:18+00"
            ],
            [
                "id" => 15,
                "tunjangan_lembur_detail_id" => 4,
                "salary_component_id" => 6,
                "koefisien" => 23,
                "default_value" => 31800,
                "component_value" => 731400,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:02:15+00"
            ],
            [
                "id" => 16,
                "tunjangan_lembur_detail_id" => 4,
                "salary_component_id" => 7,
                "koefisien" => 23,
                "default_value" => 23000,
                "component_value" => 529000,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:02:15+00"
            ],
            [
                "id" => 32,
                "tunjangan_lembur_detail_id" => 9,
                "salary_component_id" => 6,
                "koefisien" => 23,
                "default_value" => 31800,
                "component_value" => 731400,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:05:03+00"
            ],
            [
                "id" => 33,
                "tunjangan_lembur_detail_id" => 9,
                "salary_component_id" => 7,
                "koefisien" => 23,
                "default_value" => 23000,
                "component_value" => 529000,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:05:03+00"
            ],
            [
                "id" => 52,
                "tunjangan_lembur_detail_id" => 14,
                "salary_component_id" => 6,
                "koefisien" => 23,
                "default_value" => 31800,
                "component_value" => 731400,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:06:20+00"
            ],
            [
                "id" => 53,
                "tunjangan_lembur_detail_id" => 14,
                "salary_component_id" => 7,
                "koefisien" => 23,
                "default_value" => 23000,
                "component_value" => 529000,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:06:20+00"
            ],
            [
                "id" => 48,
                "tunjangan_lembur_detail_id" => 13,
                "salary_component_id" => 6,
                "koefisien" => 23,
                "default_value" => 31800,
                "component_value" => 731400,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:07:01+00"
            ],
            [
                "id" => 49,
                "tunjangan_lembur_detail_id" => 13,
                "salary_component_id" => 7,
                "koefisien" => 23,
                "default_value" => 23000,
                "component_value" => 529000,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:07:01+00"
            ],
            [
                "id" => 28,
                "tunjangan_lembur_detail_id" => 8,
                "salary_component_id" => 6,
                "koefisien" => 23,
                "default_value" => 31800,
                "component_value" => 731400,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:07:47+00"
            ],
            [
                "id" => 29,
                "tunjangan_lembur_detail_id" => 8,
                "salary_component_id" => 7,
                "koefisien" => 23,
                "default_value" => 23000,
                "component_value" => 529000,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:07:47+00"
            ],
            [
                "id" => 23,
                "tunjangan_lembur_detail_id" => 6,
                "salary_component_id" => 6,
                "koefisien" => 23,
                "default_value" => 31800,
                "component_value" => 731400,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:09:32+00"
            ],
            [
                "id" => 24,
                "tunjangan_lembur_detail_id" => 6,
                "salary_component_id" => 7,
                "koefisien" => 23,
                "default_value" => 23000,
                "component_value" => 529000,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:09:32+00"
            ],
            [
                "id" => 56,
                "tunjangan_lembur_detail_id" => 15,
                "salary_component_id" => 6,
                "koefisien" => 23,
                "default_value" => 31800,
                "component_value" => 731400,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:10:27+00"
            ],
            [
                "id" => 57,
                "tunjangan_lembur_detail_id" => 15,
                "salary_component_id" => 7,
                "koefisien" => 23,
                "default_value" => 23000,
                "component_value" => 529000,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:10:27+00"
            ],
            [
                "id" => 40,
                "tunjangan_lembur_detail_id" => 11,
                "salary_component_id" => 6,
                "koefisien" => 23,
                "default_value" => 31800,
                "component_value" => 731400,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:11:10+00"
            ],
            [
                "id" => 41,
                "tunjangan_lembur_detail_id" => 11,
                "salary_component_id" => 7,
                "koefisien" => 23,
                "default_value" => 23000,
                "component_value" => 529000,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:11:10+00"
            ]
        ];
        DB::table('tunjangan_lembur_upah_components')->insert($data);
        DB::statement("SELECT setval('tunjangan_lembur_upah_components_id_seq', (SELECT MAX(id) FROM tunjangan_lembur_upah_components)+1)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tunjangan_lembur_upah_components');
    }
}
