<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateTunjanganLemburHariKerja extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tunjangan_lembur_hari_kerja', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('tunjangan_lembur_detail_id')->constrained('tunjangan_lembur_details')->onDelete('cascade');
            $table->date('date');
            $table->float('jam_lembur_count', 65, 2)->nullable(true)->default(0);
            $table->float('lembur_jam_pertama_value', 65, 2)->nullable(true)->default(0);
            $table->float('lembur_jam_selanjutnya_value', 65, 2)->nullable(true)->default(0);
            $table->float('penerimaan_value', 65, 2)->nullable(true)->default(0);
            $table->text('description')->nullable();
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->timestampsTz($precision = 0);

            $table->unique(array('tunjangan_lembur_detail_id', 'date'));
        });

        $data = [
            [
                "id" => 4,
                "tunjangan_lembur_detail_id" => 5,
                "date" => "2021-12-16",
                "jam_lembur_count" => 4.5,
                "lembur_jam_pertama_value" => 32592,
                "lembur_jam_selanjutnya_value" => 152096,
                "penerimaan_value" => 184688,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 10:53:44+00"
            ],
            [
                "id" => 20,
                "tunjangan_lembur_detail_id" => 17,
                "date" => "2021-12-27",
                "jam_lembur_count" => 7,
                "lembur_jam_pertama_value" => 30930,
                "lembur_jam_selanjutnya_value" => 247440,
                "penerimaan_value" => 278370,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:58:39+00",
                "updated_at" => "2022-04-21 11:00:17+00"
            ],
            [
                "id" => 21,
                "tunjangan_lembur_detail_id" => 17,
                "date" => "2021-12-28",
                "jam_lembur_count" => 7,
                "lembur_jam_pertama_value" => 30930,
                "lembur_jam_selanjutnya_value" => 247440,
                "penerimaan_value" => 278370,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:58:39+00",
                "updated_at" => "2022-04-21 11:00:17+00"
            ],
            [
                "id" => 22,
                "tunjangan_lembur_detail_id" => 17,
                "date" => "2022-01-02",
                "jam_lembur_count" => 7,
                "lembur_jam_pertama_value" => 30930,
                "lembur_jam_selanjutnya_value" => 247440,
                "penerimaan_value" => 278370,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:58:39+00",
                "updated_at" => "2022-04-21 11:00:17+00"
            ],
            [
                "id" => 1,
                "tunjangan_lembur_detail_id" => 3,
                "date" => "2021-12-24",
                "jam_lembur_count" => 7,
                "lembur_jam_pertama_value" => 32762,
                "lembur_jam_selanjutnya_value" => 262092,
                "penerimaan_value" => 294854,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:01:18+00"
            ],
            [
                "id" => 2,
                "tunjangan_lembur_detail_id" => 3,
                "date" => "2021-12-26",
                "jam_lembur_count" => 7,
                "lembur_jam_pertama_value" => 32762,
                "lembur_jam_selanjutnya_value" => 262092,
                "penerimaan_value" => 294854,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:01:18+00"
            ],
            [
                "id" => 3,
                "tunjangan_lembur_detail_id" => 4,
                "date" => "2021-12-31",
                "jam_lembur_count" => 7,
                "lembur_jam_pertama_value" => 32778,
                "lembur_jam_selanjutnya_value" => 262224,
                "penerimaan_value" => 295002,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:04:17+00"
            ],
            [
                "id" => 11,
                "tunjangan_lembur_detail_id" => 9,
                "date" => "2021-12-24",
                "jam_lembur_count" => 7,
                "lembur_jam_pertama_value" => 32574,
                "lembur_jam_selanjutnya_value" => 260592,
                "penerimaan_value" => 293166,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:05:29+00"
            ],
            [
                "id" => 12,
                "tunjangan_lembur_detail_id" => 9,
                "date" => "2021-12-28",
                "jam_lembur_count" => 7,
                "lembur_jam_pertama_value" => 32574,
                "lembur_jam_selanjutnya_value" => 260592,
                "penerimaan_value" => 293166,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:05:29+00"
            ],
            [
                "id" => 16,
                "tunjangan_lembur_detail_id" => 13,
                "date" => "2022-01-13",
                "jam_lembur_count" => 1,
                "lembur_jam_pertama_value" => 54444,
                "lembur_jam_selanjutnya_value" => 0,
                "penerimaan_value" => 54444,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:07:01+00"
            ],
            [
                "id" => 17,
                "tunjangan_lembur_detail_id" => 13,
                "date" => "2022-01-20",
                "jam_lembur_count" => 1,
                "lembur_jam_pertama_value" => 54444,
                "lembur_jam_selanjutnya_value" => 0,
                "penerimaan_value" => 54444,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:07:01+00"
            ],
            [
                "id" => 5,
                "tunjangan_lembur_detail_id" => 6,
                "date" => "2021-12-13",
                "jam_lembur_count" => 4,
                "lembur_jam_pertama_value" => 29103,
                "lembur_jam_selanjutnya_value" => 116412,
                "penerimaan_value" => 145515,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:09:32+00"
            ],
            [
                "id" => 18,
                "tunjangan_lembur_detail_id" => 15,
                "date" => "2021-12-26",
                "jam_lembur_count" => 7,
                "lembur_jam_pertama_value" => 29103,
                "lembur_jam_selanjutnya_value" => 232824,
                "penerimaan_value" => 261927,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:10:27+00"
            ],
            [
                "id" => 19,
                "tunjangan_lembur_detail_id" => 15,
                "date" => "2022-01-02",
                "jam_lembur_count" => 7,
                "lembur_jam_pertama_value" => 29103,
                "lembur_jam_selanjutnya_value" => 232824,
                "penerimaan_value" => 261927,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 11:10:27+00"
            ],
            [
                "id" => 6,
                "tunjangan_lembur_detail_id" => 7,
                "date" => "2021-12-20",
                "jam_lembur_count" => 3,
                "lembur_jam_pertama_value" => 26012,
                "lembur_jam_selanjutnya_value" => 69364,
                "penerimaan_value" => 95376,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 12:50:34+00"
            ],
            [
                "id" => 7,
                "tunjangan_lembur_detail_id" => 7,
                "date" => "2021-12-30",
                "jam_lembur_count" => 2.5,
                "lembur_jam_pertama_value" => 26012,
                "lembur_jam_selanjutnya_value" => 52023,
                "penerimaan_value" => 78035,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 12:50:34+00"
            ],
            [
                "id" => 8,
                "tunjangan_lembur_detail_id" => 7,
                "date" => "2022-01-03",
                "jam_lembur_count" => 3.5,
                "lembur_jam_pertama_value" => 26012,
                "lembur_jam_selanjutnya_value" => 86705,
                "penerimaan_value" => 112717,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 12:50:34+00"
            ],
            [
                "id" => 9,
                "tunjangan_lembur_detail_id" => 7,
                "date" => "2022-01-04",
                "jam_lembur_count" => 2,
                "lembur_jam_pertama_value" => 26012,
                "lembur_jam_selanjutnya_value" => 34682,
                "penerimaan_value" => 60694,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 12:50:34+00"
            ],
            [
                "id" => 10,
                "tunjangan_lembur_detail_id" => 7,
                "date" => "2022-01-07",
                "jam_lembur_count" => 2,
                "lembur_jam_pertama_value" => 26012,
                "lembur_jam_selanjutnya_value" => 34682,
                "penerimaan_value" => 60694,
                "description" => null,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-04-21 17:43:51+00",
                "updated_at" => "2022-04-21 12:50:34+00"
            ]
        ];

        DB::table('tunjangan_lembur_hari_kerja')->insert($data);
        DB::statement("SELECT setval('tunjangan_lembur_hari_kerja_id_seq', (SELECT MAX(id) FROM tunjangan_lembur_hari_kerja)+1)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tunjangan_lembur_hari_kerja');
    }
}
