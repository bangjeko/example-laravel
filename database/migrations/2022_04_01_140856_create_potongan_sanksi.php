<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreatePotonganSanksi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('potongan_sanksi', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('periode_month')->unique();
            $table->float('total_potongan_value', 65, 2)->nullable(true)->default(0);
            $table->text('description')->nullable();
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->timestampsTz($precision = 0);
        });

        $data = [
            "periode_month" => "2022-01-01",
            "total_potongan_value" => 2200000
        ];

        DB::table('potongan_sanksi')->insert($data);
        DB::statement("SELECT setval('potongan_sanksi_id_seq', (SELECT MAX(id) FROM potongan_sanksi)+1)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('potongan_sanksi');
    }
}
