<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateMappingUpahBpjs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mapping_upah_bpjs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('job_position_category_id')->constrained('master_job_position_categories');
            $table->foreignId('status_employment_id')->constrained('master_status_employments')->onDelete('cascade');
            $table->foreignId('salary_component_id')->constrained('master_salary_components');
            $table->float('koefisien')->nullable(true)->default(1);
            $table->text('description')->nullable(true);
            $table->integer('active')->nullable(true)->default('1');
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->timestampsTz($precision = 0);
        });
        $data = [
            [
                "id" => 1,
                "job_position_category_id" => 1,
                "status_employment_id" => 1,
                "salary_component_id" => 1,
                "koefisien" => 1,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-03-30 05:33:11+00",
                "updated_at" => "2022-03-30 05:33:11+00"
            ],
            [
                "id" => 7,
                "job_position_category_id" => 5,
                "status_employment_id" => 1,
                "salary_component_id" => 1,
                "koefisien" => 1,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-03-30 05:44:54+00",
                "updated_at" => "2022-03-30 05:44:54+00"
            ],
            [
                "id" => 8,
                "job_position_category_id" => 6,
                "status_employment_id" => 2,
                "salary_component_id" => 1,
                "koefisien" => 1,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-03-30 05:45:44+00",
                "updated_at" => "2022-03-30 05:45:44+00"
            ],
            [
                "id" => 9,
                "job_position_category_id" => 6,
                "status_employment_id" => 2,
                "salary_component_id" => 2,
                "koefisien" => 1,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-03-30 05:50:36+00",
                "updated_at" => "2022-03-30 05:50:36+00"
            ],
            [
                "id" => 10,
                "job_position_category_id" => 6,
                "status_employment_id" => 2,
                "salary_component_id" => 4,
                "koefisien" => 1,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-03-30 05:50:43+00",
                "updated_at" => "2022-03-30 05:50:43+00"
            ],
            [
                "id" => 11,
                "job_position_category_id" => 6,
                "status_employment_id" => 2,
                "salary_component_id" => 6,
                "koefisien" => 23,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-03-30 05:50:48+00",
                "updated_at" => "2022-03-30 05:50:48+00"
            ],
            [
                "id" => 12,
                "job_position_category_id" => 6,
                "status_employment_id" => 2,
                "salary_component_id" => 7,
                "koefisien" => 23,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-03-30 05:50:52+00",
                "updated_at" => "2022-03-30 05:50:52+00"
            ],
            [
                "id" => 13,
                "job_position_category_id" => 7,
                "status_employment_id" => 2,
                "salary_component_id" => 1,
                "koefisien" => 1,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-03-30 05:58:53+00",
                "updated_at" => "2022-03-30 05:58:53+00"
            ],
            [
                "id" => 14,
                "job_position_category_id" => 7,
                "status_employment_id" => 2,
                "salary_component_id" => 2,
                "koefisien" => 1,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-03-30 05:58:57+00",
                "updated_at" => "2022-03-30 05:58:57+00"
            ],
            [
                "id" => 15,
                "job_position_category_id" => 7,
                "status_employment_id" => 2,
                "salary_component_id" => 4,
                "koefisien" => 1,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-03-30 05:59:03+00",
                "updated_at" => "2022-03-30 05:59:03+00"
            ],
            [
                "id" => 16,
                "job_position_category_id" => 7,
                "status_employment_id" => 2,
                "salary_component_id" => 6,
                "koefisien" => 23,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-03-30 05:59:08+00",
                "updated_at" => "2022-03-30 05:59:08+00"
            ],
            [
                "id" => 17,
                "job_position_category_id" => 7,
                "status_employment_id" => 2,
                "salary_component_id" => 7,
                "koefisien" => 23,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-03-30 05:59:12+00",
                "updated_at" => "2022-03-30 05:59:12+00"
            ],
            [
                "id" => 18,
                "job_position_category_id" => 8,
                "status_employment_id" => 2,
                "salary_component_id" => 1,
                "koefisien" => 1,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-03-30 06:00:37+00",
                "updated_at" => "2022-03-30 06:00:37+00"
            ],
            [
                "id" => 19,
                "job_position_category_id" => 8,
                "status_employment_id" => 2,
                "salary_component_id" => 2,
                "koefisien" => 1,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-03-30 06:00:41+00",
                "updated_at" => "2022-03-30 06:00:41+00"
            ],
            [
                "id" => 20,
                "job_position_category_id" => 8,
                "status_employment_id" => 2,
                "salary_component_id" => 4,
                "koefisien" => 1,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-03-30 06:00:46+00",
                "updated_at" => "2022-03-30 06:00:46+00"
            ],
            [
                "id" => 21,
                "job_position_category_id" => 8,
                "status_employment_id" => 2,
                "salary_component_id" => 6,
                "koefisien" => 23,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-03-30 06:00:54+00",
                "updated_at" => "2022-03-30 06:00:54+00"
            ],
            [
                "id" => 22,
                "job_position_category_id" => 8,
                "status_employment_id" => 2,
                "salary_component_id" => 7,
                "koefisien" => 23,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-03-30 06:00:58+00",
                "updated_at" => "2022-03-30 06:00:58+00"
            ],
            [
                "id" => 23,
                "job_position_category_id" => 9,
                "status_employment_id" => 2,
                "salary_component_id" => 1,
                "koefisien" => 1,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-03-30 06:02:17+00",
                "updated_at" => "2022-03-30 06:02:17+00"
            ],
            [
                "id" => 24,
                "job_position_category_id" => 9,
                "status_employment_id" => 3,
                "salary_component_id" => 1,
                "koefisien" => 1,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-03-30 06:02:23+00",
                "updated_at" => "2022-03-30 06:02:23+00"
            ],
            [
                "id" => 25,
                "job_position_category_id" => 10,
                "status_employment_id" => 2,
                "salary_component_id" => 1,
                "koefisien" => 1,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-03-30 06:02:36+00",
                "updated_at" => "2022-03-30 06:02:36+00"
            ],
            [
                "id" => 26,
                "job_position_category_id" => 10,
                "status_employment_id" => 2,
                "salary_component_id" => 2,
                "koefisien" => 1,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-03-30 06:02:40+00",
                "updated_at" => "2022-03-30 06:02:40+00"
            ],
            [
                "id" => 27,
                "job_position_category_id" => 10,
                "status_employment_id" => 2,
                "salary_component_id" => 4,
                "koefisien" => 1,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-03-30 06:04:07+00",
                "updated_at" => "2022-03-30 06:04:07+00"
            ],
            [
                "id" => 28,
                "job_position_category_id" => 10,
                "status_employment_id" => 2,
                "salary_component_id" => 6,
                "koefisien" => 23,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-03-30 06:04:13+00",
                "updated_at" => "2022-03-30 06:04:13+00"
            ],
            [
                "id" => 29,
                "job_position_category_id" => 10,
                "status_employment_id" => 2,
                "salary_component_id" => 7,
                "koefisien" => 23,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-03-30 06:04:18+00",
                "updated_at" => "2022-03-30 06:04:18+00"
            ],
            [
                "id" => 30,
                "job_position_category_id" => 11,
                "status_employment_id" => 2,
                "salary_component_id" => 1,
                "koefisien" => 1,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-03-30 06:04:37+00",
                "updated_at" => "2022-03-30 06:04:37+00"
            ],
            [
                "id" => 31,
                "job_position_category_id" => 11,
                "status_employment_id" => 2,
                "salary_component_id" => 2,
                "koefisien" => 1,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-03-30 06:04:41+00",
                "updated_at" => "2022-03-30 06:04:41+00"
            ],
            [
                "id" => 32,
                "job_position_category_id" => 11,
                "status_employment_id" => 2,
                "salary_component_id" => 4,
                "koefisien" => 1,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-03-30 06:04:45+00",
                "updated_at" => "2022-03-30 06:04:45+00"
            ],
            [
                "id" => 33,
                "job_position_category_id" => 11,
                "status_employment_id" => 2,
                "salary_component_id" => 6,
                "koefisien" => 23,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-03-30 06:04:51+00",
                "updated_at" => "2022-03-30 06:04:51+00"
            ],
            [
                "id" => 34,
                "job_position_category_id" => 11,
                "status_employment_id" => 2,
                "salary_component_id" => 7,
                "koefisien" => 23,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-03-30 06:04:56+00",
                "updated_at" => "2022-03-30 06:04:56+00"
            ],
            [
                "id" => 35,
                "job_position_category_id" => 11,
                "status_employment_id" => 3,
                "salary_component_id" => 1,
                "koefisien" => 1,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-03-30 06:05:03+00",
                "updated_at" => "2022-03-30 06:05:03+00"
            ],
            [
                "id" => 36,
                "job_position_category_id" => 11,
                "status_employment_id" => 3,
                "salary_component_id" => 7,
                "koefisien" => 23,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-03-30 06:06:41+00",
                "updated_at" => "2022-03-30 06:06:41+00"
            ],
            [
                "id" => 3,
                "job_position_category_id" => 2,
                "status_employment_id" => 1,
                "salary_component_id" => 6,
                "koefisien" => 1,
                "description" => null,
                "active" => 0,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-03-30 05:41:14+00",
                "updated_at" => "2022-04-19 09:47:30+00"
            ],
            [
                "id" => 4,
                "job_position_category_id" => 2,
                "status_employment_id" => 1,
                "salary_component_id" => 16,
                "koefisien" => 1,
                "description" => null,
                "active" => 0,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-03-30 05:41:54+00",
                "updated_at" => "2022-04-19 09:47:36+00"
            ],
            [
                "id" => 2,
                "job_position_category_id" => 2,
                "status_employment_id" => 1,
                "salary_component_id" => 1,
                "koefisien" => 1,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-03-30 05:41:02+00",
                "updated_at" => "2022-04-19 14:21:32+00"
            ],
            [
                "id" => 5,
                "job_position_category_id" => 3,
                "status_employment_id" => 1,
                "salary_component_id" => 1,
                "koefisien" => 1,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-03-30 05:42:15+00",
                "updated_at" => "2022-04-19 14:22:10+00"
            ],
            [
                "id" => 6,
                "job_position_category_id" => 4,
                "status_employment_id" => 1,
                "salary_component_id" => 1,
                "koefisien" => 1,
                "description" => null,
                "active" => 1,
                "created_by" => 1,
                "updated_by" => 1,
                "created_at" => "2022-03-30 05:43:41+00",
                "updated_at" => "2022-04-20 09:17:05+00"
            ]
        ];
        DB::table('mapping_upah_bpjs')->insert($data);
        DB::statement("SELECT setval('mapping_upah_bpjs_id_seq', (SELECT MAX(id) FROM mapping_upah_bpjs)+1)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mapping_upah_bpjs');
    }
}
