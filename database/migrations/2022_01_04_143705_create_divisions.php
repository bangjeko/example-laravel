<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateDivisions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('divisions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('division_name');
            $table->integer('order_number')->nullable(true);
            $table->integer('active')->nullable(true)->default('1');
            $table->foreignId('created_by')->nullable()->constrained('users');
            $table->foreignId('updated_by')->nullable()->constrained('users');
            $table->timestampsTz($precision = 0);
        });

        $data = [
            [
                "division_name" => 'Corporate Secretariat',
            ],
            [
                "division_name" => 'Accounting And Finance',
            ],
            [
                "division_name" => 'Human Capital And General Affairs',
            ],
            [
                "division_name" => 'Risk Management',
            ],
            [
                "division_name" => 'Marketing',
            ],
            [
                "division_name" => 'Bussiness Development',
            ],
            [
                "division_name" => 'Engineering',
            ],
            [
                "division_name" => 'Internal Audit',
            ]
        ];
        DB::table('divisions')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('divisions');
    }
}
