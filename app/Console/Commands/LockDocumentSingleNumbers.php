<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class LockDocumentSingleNumbers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lock:document-single-numbers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $sql = "WITH up_smkkkl AS (
                UPDATE smkkkl_appraisals SET is_lock = 'y', locked_form_by = null, locked_form_at = now() WHERE periode_date < now()
            ), up_mm AS (
                UPDATE mm_appraisals SET is_lock = 'y', locked_form_by = null, locked_form_at = now() WHERE periode_date < now()
            ), up_r AS (
                UPDATE r_appraisals SET is_lock = 'y', locked_form_by = null, locked_form_at = now() WHERE periode_date < now()
            ), up_qpass AS (
                UPDATE qpass_appraisals SET is_lock = 'y', locked_form_by = null, locked_form_at = now() WHERE periode_date < now()
            )
            SELECT 'successfully locked single number forms' ";
        $object =  DB::select($sql);
        Log::debug(date("Y-m-d H:i:s"));
        $this->info("Lock Document Succesfully");
    }
}
