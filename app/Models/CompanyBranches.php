<?php 

namespace App\Models;

use App\CoreService\CallService;
use DateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class CompanyBranches extends Model
{
    protected $table = 'company_branches';
    protected $dateFormat = 'c';
    const TABLE = "company_branches";
    const FILEROOT = "/company_branches";
    const IS_LIST = true;
    const IS_ADD = true;
    const IS_EDIT = true;
    const IS_DELETE = true;
    const IS_VIEW = true;
    const FIELD_LIST = ["id", "company_branch_name", "company_type", "nomor_nib", "nomor_npwp", "primary_address", "secondary_address", "postal_code", "primary_telephone", "secondary_telephone", "email", "url_website", "img_logo", "active", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_ADD = ["company_branch_name", "company_type", "nomor_nib", "nomor_npwp", "primary_address", "secondary_address", "postal_code", "primary_telephone", "secondary_telephone", "email", "url_website", "img_logo", "active", "created_by", "updated_by"];
    const FIELD_EDIT = ["company_branch_name", "company_type", "nomor_nib", "nomor_npwp", "primary_address", "secondary_address", "postal_code", "primary_telephone", "secondary_telephone", "email", "url_website", "img_logo", "active", "updated_by"];
    const FIELD_VIEW = ["id", "company_branch_name", "company_type", "nomor_nib", "nomor_npwp", "primary_address", "secondary_address", "postal_code", "primary_telephone", "secondary_telephone", "email", "url_website", "img_logo", "active", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_READONLY = [];
    const FIELD_FILTERABLE = [
        "id" => [
            "operator" => "=",
        ],
        "company_branch_name" => [
            "operator" => "=",
        ],
        "company_type" => [
            "operator" => "=",
        ],
        "nomor_nib" => [
            "operator" => "=",
        ],
        "nomor_npwp" => [
            "operator" => "=",
        ],
        "primary_address" => [
            "operator" => "=",
        ],
        "secondary_address" => [
            "operator" => "=",
        ],
        "postal_code" => [
            "operator" => "=",
        ],
        "primary_telephone" => [
            "operator" => "=",
        ],
        "secondary_telephone" => [
            "operator" => "=",
        ],
        "email" => [
            "operator" => "=",
        ],
        "url_website" => [
            "operator" => "=",
        ],
        "img_logo" => [
            "operator" => "=",
        ],
        "active" => [
            "operator" => "=",
        ],
        "created_by" => [
            "operator" => "=",
        ],
        "updated_by" => [
            "operator" => "=",
        ],
        "created_at" => [
            "operator" => "=",
        ],
        "updated_at" => [
            "operator" => "=",
        ],
    ];
    const FIELD_SEARCHABLE = ["company_branch_name", "company_type", "nomor_nib", "nomor_npwp", "postal_code", "primary_telephone", "secondary_telephone", "email", "url_website"];
    const FIELD_ARRAY = [];
    const FIELD_SORTABLE = ["id", "company_branch_name", "company_type", "nomor_nib", "nomor_npwp", "primary_address", "secondary_address", "postal_code", "primary_telephone", "secondary_telephone", "email", "url_website", "img_logo", "active", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_UNIQUE = [];
    const FIELD_UPLOAD = ["img_logo"];
    const FIELD_TYPE = [
        "id" => "bigint",
        "company_branch_name" => "character_varying",
        "company_type" => "character_varying",
        "nomor_nib" => "character_varying",
        "nomor_npwp" => "character_varying",
        "primary_address" => "text",
        "secondary_address" => "text",
        "postal_code" => "character_varying",
        "primary_telephone" => "character_varying",
        "secondary_telephone" => "character_varying",
        "email" => "character_varying",
        "url_website" => "character_varying",
        "img_logo" => "text",
        "active" => "integer",
        "created_by" => "bigint",
        "updated_by" => "bigint",
        "created_at" => "timestamp_with_time_zone",
        "updated_at" => "timestamp_with_time_zone",
    ];

    const FIELD_DEFAULT_VALUE = [
        "company_branch_name" => "",
        "company_type" => "",
        "nomor_nib" => "",
        "nomor_npwp" => "",
        "primary_address" => "",
        "secondary_address" => "",
        "postal_code" => "",
        "primary_telephone" => "",
        "secondary_telephone" => "",
        "email" => "",
        "url_website" => "",
        "img_logo" => "",
        "active" => "1",
        "created_by" => "",
        "updated_by" => "",
        "created_at" => "",
        "updated_at" => "",
    ];
    const FIELD_RELATION = [
        "created_by" => [
            "linkTable" => "users",
            "aliasTable" => "B",
            "linkField" => "id",
            "displayName" => "rel_created_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_created_by"
        ],
        "updated_by" => [
            "linkTable" => "users",
            "aliasTable" => "C",
            "linkField" => "id",
            "displayName" => "rel_updated_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_updated_by"
        ],
    ];
    const CUSTOM_SELECT = "";
    const FIELD_VALIDATION = [
        "company_branch_name" => "required|string|max:255",
        "company_type" => "required|string|max:255",
        "nomor_nib" => "nullable|string|max:255",
        "nomor_npwp" => "nullable|string|max:255",
        "primary_address" => "required|string",
        "secondary_address" => "nullable|string",
        "postal_code" => "nullable|string|max:255",
        "primary_telephone" => "required|string|max:255",
        "secondary_telephone" => "nullable|string|max:255",
        "email" => "required|string|max:255",
        "url_website" => "nullable|string|max:255",
        "img_logo" => "required|string|exists_file",
        "active" => "nullable|integer",
        "created_by" => "nullable|integer",
        "updated_by" => "nullable|integer",
        "created_at" => "nullable|date",
        "updated_at" => "nullable|date",
    ];
    const PARENT_CHILD = [];
    // start custom
    const CUSTOM_LIST_FILTER = [];
    const FIELD_CASTING = [];
    const CHILD_TABLE = [
        //"child_table" => [
        //    "foreignField" => "field"
        //]
    ];

    public static function beforeInsert($input)
    {
        return $input;
    }

    public static function afterInsert($object, $input)
    {
        return $input;
    }
    
    public static function beforeUpdate($input)
    {
        return $input;
    }
    
    public static function afterUpdate($object, $input)
    {
        return $input;
    }
    
    public static function beforeDelete($input)
    {
        return $input;
    }

    public static function afterDelete($object, $input)
    {
        return $input;
    }// end custom
}
