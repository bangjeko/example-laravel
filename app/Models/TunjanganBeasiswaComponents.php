<?php 

namespace App\Models;

use App\CoreService\CallService;
use DateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class TunjanganBeasiswaComponents extends Model
{
    protected $table = 'tunjangan_beasiswa_components';
    protected $dateFormat = 'c';
    const TABLE = "tunjangan_beasiswa_components";
    const FILEROOT = "/tunjangan_beasiswa_components";
    const IS_LIST = true;
    const IS_ADD = true;
    const IS_EDIT = true;
    const IS_DELETE = true;
    const IS_VIEW = true;
    const FIELD_LIST = ["id", "tunjangan_beasiswa_detail_id", "employee_family_id", "education_id", "rank", "component_value", "description", "active", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_ADD = ["tunjangan_beasiswa_detail_id", "employee_family_id", "education_id", "rank", "component_value", "description", "active", "created_by", "updated_by"];
    const FIELD_EDIT = ["tunjangan_beasiswa_detail_id", "employee_family_id", "education_id", "rank", "component_value", "description", "active", "updated_by"];
    const FIELD_VIEW = ["id", "tunjangan_beasiswa_detail_id", "employee_family_id", "education_id", "rank", "component_value", "description", "active", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_READONLY = [];
    const FIELD_FILTERABLE = [
        "id" => [
            "operator" => "=",
        ],
        "tunjangan_beasiswa_detail_id" => [
            "operator" => "=",
        ],
        "employee_family_id" => [
            "operator" => "=",
        ],
        "education_id" => [
            "operator" => "=",
        ],
        "rank" => [
            "operator" => "=",
        ],
        "component_value" => [
            "operator" => "=",
        ],
        "description" => [
            "operator" => "=",
        ],
        "active" => [
            "operator" => "=",
        ],
        "created_by" => [
            "operator" => "=",
        ],
        "updated_by" => [
            "operator" => "=",
        ],
        "created_at" => [
            "operator" => "=",
        ],
        "updated_at" => [
            "operator" => "=",
        ],
    ];
    const FIELD_SEARCHABLE = [];
    const FIELD_ARRAY = [];
    const FIELD_SORTABLE = ["id", "tunjangan_beasiswa_detail_id", "employee_family_id", "education_id", "rank", "component_value", "description", "active", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_UNIQUE = [["tunjangan_beasiswa_detail_id", "employee_family_id"]];
    const FIELD_UPLOAD = [];
    const FIELD_TYPE = [
        "id" => "bigint",
        "tunjangan_beasiswa_detail_id" => "bigint",
        "employee_family_id" => "bigint",
        "education_id" => "bigint",
        "rank" => "integer",
        "component_value" => "double_precision",
        "description" => "text",
        "active" => "integer",
        "created_by" => "bigint",
        "updated_by" => "bigint",
        "created_at" => "timestamp_with_time_zone",
        "updated_at" => "timestamp_with_time_zone",
    ];

    const FIELD_DEFAULT_VALUE = [
        "tunjangan_beasiswa_detail_id" => "",
        "employee_family_id" => "",
        "education_id" => "",
        "rank" => "",
        "component_value" => "&#039;0&#039;::double precision",
        "description" => "",
        "active" => "1",
        "created_by" => "",
        "updated_by" => "",
        "created_at" => "",
        "updated_at" => "",
    ];
    const FIELD_RELATION = [
        "tunjangan_beasiswa_detail_id" => [
            "linkTable" => "tunjangan_beasiswa_details",
            "aliasTable" => "B",
            "linkField" => "id",
            "displayName" => "rel_tunjangan_beasiswa_detail_id",
            "selectFields" => ["id"],
            "selectValue" => "id AS rel_tunjangan_beasiswa_detail_id"
        ],
        "employee_family_id" => [
            "linkTable" => "employee_families",
            "aliasTable" => "C",
            "linkField" => "id",
            "displayName" => "rel_employee_family_id",
            "selectFields" => ["fullname"],
            "selectValue" => "id AS rel_employee_family_id"
        ],
        "education_id" => [
            "linkTable" => "master_educations",
            "aliasTable" => "D",
            "linkField" => "id",
            "displayName" => "rel_education_id",
            "selectFields" => ["name"],
            "selectValue" => "id AS rel_education_id"
        ],
        "created_by" => [
            "linkTable" => "users",
            "aliasTable" => "E",
            "linkField" => "id",
            "displayName" => "rel_created_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_created_by"
        ],
        "updated_by" => [
            "linkTable" => "users",
            "aliasTable" => "F",
            "linkField" => "id",
            "displayName" => "rel_updated_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_updated_by"
        ],
    ];
    const CUSTOM_SELECT = "";
    const FIELD_VALIDATION = [
        "tunjangan_beasiswa_detail_id" => "required|integer",
        "employee_family_id" => "required|integer",
        "education_id" => "required|integer",
        "rank" => "nullable|integer",
        "component_value" => "nullable",
        "description" => "nullable|string",
        "active" => "nullable|integer",
        "created_by" => "nullable|integer",
        "updated_by" => "nullable|integer",
        "created_at" => "nullable|date",
        "updated_at" => "nullable|date",
    ];
    const PARENT_CHILD = [];
    // start custom
    const CUSTOM_LIST_FILTER = [];
    const FIELD_CASTING = [
        "component_value" => "float",
    ];
    const CHILD_TABLE = [
        //"child_table" => [
        //    "foreignField" => "field"
        //]
    ];

    public static function beforeInsert($input)
    {
        return $input;
    }

    public static function afterInsert($object, $input)
    {
        return $input;
    }
    
    public static function beforeUpdate($input)
    {
        return $input;
    }
    
    public static function afterUpdate($object, $input)
    {
        return $input;
    }
    
    public static function beforeDelete($input)
    {
        return $input;
    }

    public static function afterDelete($object, $input)
    {
        return $input;
    }// end custom
}
