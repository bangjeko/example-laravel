<?php 

namespace App\Models;

use App\CoreService\CallService;
use DateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class KoperasiPinjamanDetails extends Model
{
    protected $table = 'koperasi_pinjaman_details';
    protected $dateFormat = 'c';
    const TABLE = "koperasi_pinjaman_details";
    const FILEROOT = "/koperasi_pinjaman_details";
    const IS_LIST = true;
    const IS_ADD = true;
    const IS_EDIT = true;
    const IS_DELETE = true;
    const IS_VIEW = true;
    const FIELD_LIST = ["id", "koperasi_simpan_pinjam_detail_id", "ada_angsuran", "pinjaman_value", "count_pembayaran", "prosentase_bunga", "angsuran_ke", "sisa_angsuran", "pokok_pinjaman_value", "bunga_pinjaman_value", "description", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_ADD = ["koperasi_simpan_pinjam_detail_id", "ada_angsuran", "pinjaman_value", "count_pembayaran", "prosentase_bunga", "angsuran_ke", "sisa_angsuran", "pokok_pinjaman_value", "bunga_pinjaman_value", "description", "created_by", "updated_by"];
    const FIELD_EDIT = ["koperasi_simpan_pinjam_detail_id", "ada_angsuran", "pinjaman_value", "count_pembayaran", "prosentase_bunga", "angsuran_ke", "sisa_angsuran", "pokok_pinjaman_value", "bunga_pinjaman_value", "description", "updated_by"];
    const FIELD_VIEW = ["id", "koperasi_simpan_pinjam_detail_id", "ada_angsuran", "pinjaman_value", "count_pembayaran", "prosentase_bunga", "angsuran_ke", "sisa_angsuran", "pokok_pinjaman_value", "bunga_pinjaman_value", "description", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_READONLY = [];
    const FIELD_FILTERABLE = [
        "id" => [
            "operator" => "=",
        ],
        "koperasi_simpan_pinjam_detail_id" => [
            "operator" => "=",
        ],
        "ada_angsuran" => [
            "operator" => "=",
        ],
        "pinjaman_value" => [
            "operator" => "=",
        ],
        "count_pembayaran" => [
            "operator" => "=",
        ],
        "prosentase_bunga" => [
            "operator" => "=",
        ],
        "angsuran_ke" => [
            "operator" => "=",
        ],
        "sisa_angsuran" => [
            "operator" => "=",
        ],
        "pokok_pinjaman_value" => [
            "operator" => "=",
        ],
        "bunga_pinjaman_value" => [
            "operator" => "=",
        ],
        "description" => [
            "operator" => "=",
        ],
        "created_by" => [
            "operator" => "=",
        ],
        "updated_by" => [
            "operator" => "=",
        ],
        "created_at" => [
            "operator" => "=",
        ],
        "updated_at" => [
            "operator" => "=",
        ],
    ];
    const FIELD_SEARCHABLE = [];
    const FIELD_ARRAY = [];
    const FIELD_SORTABLE = ["id", "koperasi_simpan_pinjam_detail_id", "ada_angsuran", "pinjaman_value", "count_pembayaran", "prosentase_bunga", "angsuran_ke", "sisa_angsuran", "pokok_pinjaman_value", "bunga_pinjaman_value", "description", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_UNIQUE = [];
    const FIELD_UPLOAD = [];
    const FIELD_TYPE = [
        "id" => "bigint",
        "koperasi_simpan_pinjam_detail_id" => "bigint",
        "ada_angsuran" => "boolean",
        "pinjaman_value" => "double_precision",
        "count_pembayaran" => "integer",
        "prosentase_bunga" => "double_precision",
        "angsuran_ke" => "integer",
        "sisa_angsuran" => "integer",
        "pokok_pinjaman_value" => "double_precision",
        "bunga_pinjaman_value" => "double_precision",
        "description" => "text",
        "created_by" => "bigint",
        "updated_by" => "bigint",
        "created_at" => "timestamp_with_time_zone",
        "updated_at" => "timestamp_with_time_zone",
    ];

    const FIELD_DEFAULT_VALUE = [
        "koperasi_simpan_pinjam_detail_id" => "",
        "ada_angsuran" => "false",
        "pinjaman_value" => "&#039;0&#039;::double precision",
        "count_pembayaran" => "0",
        "prosentase_bunga" => "&#039;0&#039;::double precision",
        "angsuran_ke" => "0",
        "sisa_angsuran" => "0",
        "pokok_pinjaman_value" => "&#039;0&#039;::double precision",
        "bunga_pinjaman_value" => "&#039;0&#039;::double precision",
        "description" => "",
        "created_by" => "",
        "updated_by" => "",
        "created_at" => "",
        "updated_at" => "",
    ];
    const FIELD_RELATION = [
        "koperasi_simpan_pinjam_detail_id" => [
            "linkTable" => "koperasi_simpan_pinjam_details",
            "aliasTable" => "B",
            "linkField" => "id",
            "displayName" => "rel_koperasi_simpan_pinjam_detail_id",
            "selectFields" => ["id"],
            "selectValue" => "id AS rel_koperasi_simpan_pinjam_detail_id"
        ],
        "created_by" => [
            "linkTable" => "users",
            "aliasTable" => "C",
            "linkField" => "id",
            "displayName" => "rel_created_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_created_by"
        ],
        "updated_by" => [
            "linkTable" => "users",
            "aliasTable" => "D",
            "linkField" => "id",
            "displayName" => "rel_updated_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_updated_by"
        ],
    ];
    const CUSTOM_SELECT = "";
    const FIELD_VALIDATION = [
        "koperasi_simpan_pinjam_detail_id" => "required|integer",
        "ada_angsuran" => "required",
        "pinjaman_value" => "nullable",
        "count_pembayaran" => "nullable|integer",
        "prosentase_bunga" => "nullable",
        "angsuran_ke" => "nullable|integer",
        "sisa_angsuran" => "nullable|integer",
        "pokok_pinjaman_value" => "nullable",
        "bunga_pinjaman_value" => "nullable",
        "description" => "nullable|string",
        "created_by" => "nullable|integer",
        "updated_by" => "nullable|integer",
        "created_at" => "nullable|date",
        "updated_at" => "nullable|date",
    ];
    const PARENT_CHILD = [];
    // start custom
    const CUSTOM_LIST_FILTER = [];
    const FIELD_CASTING = [];
    const CHILD_TABLE = [
        //"child_table" => [
        //    "foreignField" => "field"
        //]
    ];

    public static function beforeInsert($input)
    {
        return $input;
    }

    public static function afterInsert($object, $input)
    {
        return $input;
    }
    
    public static function beforeUpdate($input)
    {
        return $input;
    }
    
    public static function afterUpdate($object, $input)
    {
        return $input;
    }
    
    public static function beforeDelete($input)
    {
        return $input;
    }

    public static function afterDelete($object, $input)
    {
        return $input;
    }// end custom
}
