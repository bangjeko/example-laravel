<?php 

namespace App\Models;

use App\CoreService\CallService;
use DateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class JobPositions extends Model
{
    protected $table = 'job_positions';
    protected $dateFormat = 'c';
    const TABLE = "job_positions";
    const FILEROOT = "/job_positions";
    const IS_LIST = true;
    const IS_ADD = true;
    const IS_EDIT = true;
    const IS_DELETE = true;
    const IS_VIEW = true;
    const FIELD_LIST = ["id", "job_position_category_id", "job_position_name", "division_id", "parent_id", "is_structural", "status_company", "order_number", "active", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_ADD = ["job_position_category_id", "job_position_name", "division_id", "parent_id", "is_structural", "status_company", "order_number", "active", "created_by", "updated_by"];
    const FIELD_EDIT = ["job_position_category_id", "job_position_name", "division_id", "parent_id", "is_structural", "status_company", "order_number", "active", "updated_by"];
    const FIELD_VIEW = ["id", "job_position_category_id", "job_position_name", "division_id", "parent_id", "is_structural", "status_company", "order_number", "active", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_READONLY = [];
    const FIELD_FILTERABLE = [
        "id" => [
            "operator" => "=",
        ],
        "job_position_category_id" => [
            "operator" => "=",
        ],
        "job_position_name" => [
            "operator" => "=",
        ],
        "division_id" => [
            "operator" => "=",
        ],
        "parent_id" => [
            "operator" => "=",
        ],
        "is_structural" => [
            "operator" => "=",
        ],
        "status_company" => [
            "operator" => "=",
        ],
        "order_number" => [
            "operator" => "=",
        ],
        "active" => [
            "operator" => "=",
        ],
        "created_by" => [
            "operator" => "=",
        ],
        "updated_by" => [
            "operator" => "=",
        ],
        "created_at" => [
            "operator" => "=",
        ],
        "updated_at" => [
            "operator" => "=",
        ],
    ];
    const FIELD_SEARCHABLE = ["job_position_name", "status_company"];
    const FIELD_ARRAY = [];
    const FIELD_SORTABLE = ["id", "job_position_category_id", "job_position_name", "division_id", "parent_id", "is_structural", "status_company", "order_number", "active", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_UNIQUE = [["job_position_name"]];
    const FIELD_UPLOAD = [];
    const FIELD_TYPE = [
        "id" => "bigint",
        "job_position_category_id" => "bigint",
        "job_position_name" => "character_varying",
        "division_id" => "bigint",
        "parent_id" => "bigint",
        "is_structural" => "boolean",
        "status_company" => "character_varying",
        "order_number" => "integer",
        "active" => "integer",
        "created_by" => "bigint",
        "updated_by" => "bigint",
        "created_at" => "timestamp_with_time_zone",
        "updated_at" => "timestamp_with_time_zone",
    ];

    const FIELD_DEFAULT_VALUE = [
        "job_position_category_id" => "",
        "job_position_name" => "",
        "division_id" => "",
        "parent_id" => "",
        "is_structural" => "false",
        "status_company" => "",
        "order_number" => "",
        "active" => "1",
        "created_by" => "",
        "updated_by" => "",
        "created_at" => "",
        "updated_at" => "",
    ];
    const FIELD_RELATION = [
        "job_position_category_id" => [
            "linkTable" => "master_job_position_categories",
            "aliasTable" => "B",
            "linkField" => "id",
            "displayName" => "rel_job_position_category_id",
            "selectFields" => ["name"],
            "selectValue" => "id AS rel_job_position_category_id"
        ],
        "division_id" => [
            "linkTable" => "divisions",
            "aliasTable" => "C",
            "linkField" => "id",
            "displayName" => "rel_division_id",
            "selectFields" => ["division_name"],
            "selectValue" => "id AS rel_division_id"
        ],
        "parent_id" => [
            "linkTable" => "job_positions",
            "aliasTable" => "D",
            "linkField" => "id",
            "displayName" => "rel_parent_id",
            "selectFields" => ["job_position_name"],
            "selectValue" => "id AS rel_parent_id"
        ],
        "created_by" => [
            "linkTable" => "users",
            "aliasTable" => "E",
            "linkField" => "id",
            "displayName" => "rel_created_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_created_by"
        ],
        "updated_by" => [
            "linkTable" => "users",
            "aliasTable" => "F",
            "linkField" => "id",
            "displayName" => "rel_updated_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_updated_by"
        ],
    ];
    const CUSTOM_SELECT = "";
    const FIELD_VALIDATION = [
        "job_position_category_id" => "required|integer",
        "job_position_name" => "required|string|max:255",
        "division_id" => "nullable|integer",
        "parent_id" => "nullable|integer",
        "is_structural" => "required",
        "status_company" => "required|string|max:255",
        "order_number" => "nullable|integer",
        "active" => "nullable|integer",
        "created_by" => "nullable|integer",
        "updated_by" => "nullable|integer",
        "created_at" => "nullable|date",
        "updated_at" => "nullable|date",
    ];
    const PARENT_CHILD = [];
    // start custom
    const CUSTOM_LIST_FILTER = [];
    const FIELD_CASTING = [];
    const CHILD_TABLE = [
        //"child_table" => [
        //    "foreignField" => "field"
        //]
    ];

    public static function beforeInsert($input)
    {
        return $input;
    }

    public static function afterInsert($object, $input)
    {
        return $input;
    }
    
    public static function beforeUpdate($input)
    {
        return $input;
    }
    
    public static function afterUpdate($object, $input)
    {
        return $input;
    }
    
    public static function beforeDelete($input)
    {
        return $input;
    }

    public static function afterDelete($object, $input)
    {
        return $input;
    }// end custom
}
