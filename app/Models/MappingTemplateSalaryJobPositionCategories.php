<?php 

namespace App\Models;

use App\CoreService\CallService;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;


class MappingTemplateSalaryJobPositionCategories extends Model
{
    protected $table = 'mapping_template_salary_job_position_categories';
    protected $dateFormat = 'c';
    const TABLE = "mapping_template_salary_job_position_categories";
    const FILEROOT = "/mapping_template_salary_job_position_categories";
    const IS_LIST = true;
    const IS_ADD = true;
    const IS_EDIT = true;
    const IS_DELETE = true;
    const IS_VIEW = true;
    const FIELD_LIST = ["id", "job_position_category_id", "salary_component_id", "fina_salary_account_id", "active", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_ADD = ["job_position_category_id", "salary_component_id", "fina_salary_account_id", "active", "created_by", "updated_by"];
    const FIELD_EDIT = ["job_position_category_id", "salary_component_id", "fina_salary_account_id", "active", "updated_by"];
    const FIELD_VIEW = ["id", "job_position_category_id", "salary_component_id", "fina_salary_account_id", "active", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_READONLY = [];
    const FIELD_FILTERABLE = [
        "id" => [
            "operator" => "=",
        ],
        "job_position_category_id" => [
            "operator" => "=",
        ],
        "salary_component_id" => [
            "operator" => "=",
        ],
        "fina_salary_account_id" => [
            "operator" => "=",
        ],
        "active" => [
            "operator" => "=",
        ],
        "created_by" => [
            "operator" => "=",
        ],
        "updated_by" => [
            "operator" => "=",
        ],
        "created_at" => [
            "operator" => "=",
        ],
        "updated_at" => [
            "operator" => "=",
        ],
    ];
    const FIELD_SEARCHABLE = [];
    const FIELD_ARRAY = [];
    const FIELD_SORTABLE = ["id", "job_position_category_id", "salary_component_id", "fina_salary_account_id", "active", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_UNIQUE = [];
    const FIELD_UPLOAD = [];
    const FIELD_TYPE = [
        "id" => "bigint",
        "job_position_category_id" => "bigint",
        "salary_component_id" => "bigint",
        "fina_salary_account_id" => "bigint",
        "active" => "integer",
        "created_by" => "bigint",
        "updated_by" => "bigint",
        "created_at" => "timestamp_with_time_zone",
        "updated_at" => "timestamp_with_time_zone",
    ];

    const FIELD_DEFAULT_VALUE = [
        "job_position_category_id" => "",
        "salary_component_id" => "",
        "fina_salary_account_id" => "",
        "active" => "1",
        "created_by" => "",
        "updated_by" => "",
        "created_at" => "",
        "updated_at" => "",
    ];
    const FIELD_RELATION = [
        "job_position_category_id" => [
            "linkTable" => "master_job_position_categories",
            "aliasTable" => "B",
            "linkField" => "id",
            "displayName" => "rel_job_position_category_id",
            "selectFields" => ["id"],
            "selectValue" => "id AS rel_job_position_category_id"
        ],
        "salary_component_id" => [
            "linkTable" => "master_salary_components",
            "aliasTable" => "C",
            "linkField" => "id",
            "displayName" => "rel_salary_component_id",
            "selectFields" => ["id"],
            "selectValue" => "id AS rel_salary_component_id"
        ],
        "fina_salary_account_id" => [
            "linkTable" => "fina_salary_accounts",
            "aliasTable" => "D",
            "linkField" => "id",
            "displayName" => "rel_fina_salary_account_id",
            "selectFields" => ["id"],
            "selectValue" => "id AS rel_fina_salary_account_id"
        ],
        "created_by" => [
            "linkTable" => "users",
            "aliasTable" => "E",
            "linkField" => "id",
            "displayName" => "rel_created_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_created_by"
        ],
        "updated_by" => [
            "linkTable" => "users",
            "aliasTable" => "F",
            "linkField" => "id",
            "displayName" => "rel_updated_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_updated_by"
        ],
    ];
    const CUSTOM_SELECT = "";
    const FIELD_VALIDATION = [
        "job_position_category_id" => "required|integer",
        "salary_component_id" => "required|integer",
        "fina_salary_account_id" => "nullable|integer",
        "active" => "nullable|integer",
        "created_by" => "nullable|integer",
        "updated_by" => "nullable|integer",
        "created_at" => "nullable|date",
        "updated_at" => "nullable|date",
    ];
    const PARENT_CHILD = [];
    // start custom
    const CUSTOM_LIST_FILTER = [];
    const FIELD_CASTING = [];
    const CHILD_TABLE = [
        //"child_table" => [
        //    "foreignField" => "field"
        //]
    ];

    public static function beforeInsert($input)
    {
        return $input;
    }

    public static function afterInsert($object, $input)
    {
        return $input;
    }
    
    public static function beforeUpdate($input)
    {
        return $input;
    }
    
    public static function afterUpdate($object, $input)
    {
        return $input;
    }
    
    public static function beforeDelete($input)
    {
        return $input;
    }

    public static function afterDelete($object, $input)
    {
        return $input;
    }// end custom
}
