<?php 

namespace App\Models;

use App\CoreService\CallService;
use DateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class ConfigControlSheetVerificationOrders extends Model
{
    protected $table = 'config_control_sheet_verification_orders';
    protected $dateFormat = 'c';
    const TABLE = "config_control_sheet_verification_orders";
    const FILEROOT = "/config_control_sheet_verification_orders";
    const IS_LIST = true;
    const IS_ADD = true;
    const IS_EDIT = true;
    const IS_DELETE = true;
    const IS_VIEW = true;
    const FIELD_LIST = ["id", "role_id", "verification_order", "active", "created_at", "updated_at"];
    const FIELD_ADD = ["role_id", "verification_order", "active"];
    const FIELD_EDIT = ["role_id", "verification_order", "active"];
    const FIELD_VIEW = ["id", "role_id", "verification_order", "active", "created_at", "updated_at"];
    const FIELD_READONLY = [];
    const FIELD_FILTERABLE = [
        "id" => [
            "operator" => "=",
        ],
        "role_id" => [
            "operator" => "=",
        ],
        "verification_order" => [
            "operator" => "=",
        ],
        "active" => [
            "operator" => "=",
        ],
        "created_at" => [
            "operator" => "=",
        ],
        "updated_at" => [
            "operator" => "=",
        ],
    ];
    const FIELD_SEARCHABLE = [];
    const FIELD_ARRAY = [];
    const FIELD_SORTABLE = ["id", "role_id", "verification_order", "active", "created_at", "updated_at"];
    const FIELD_UNIQUE = [];
    const FIELD_UPLOAD = [];
    const FIELD_TYPE = [
        "id" => "bigint",
        "role_id" => "bigint",
        "verification_order" => "integer",
        "active" => "integer",
        "created_at" => "timestamp_with_time_zone",
        "updated_at" => "timestamp_with_time_zone",
    ];

    const FIELD_DEFAULT_VALUE = [
        "role_id" => "",
        "verification_order" => "",
        "active" => "1",
        "created_at" => "",
        "updated_at" => "",
    ];
    const FIELD_RELATION = [
        "role_id" => [
            "linkTable" => "roles",
            "aliasTable" => "B",
            "linkField" => "id",
            "displayName" => "rel_role_id",
            "selectFields" => ["id"],
            "selectValue" => "id AS rel_role_id"
        ],
    ];
    const CUSTOM_SELECT = "";
    const FIELD_VALIDATION = [
        "role_id" => "required|integer",
        "verification_order" => "required|integer",
        "active" => "nullable|integer",
        "created_at" => "nullable|date",
        "updated_at" => "nullable|date",
    ];
    const PARENT_CHILD = [];
    // start custom
    const CUSTOM_LIST_FILTER = [];
    const FIELD_CASTING = [
        //"nama field" => "float",
    ];
    const CHILD_TABLE = [
        //"child_table" => [
        //    "foreignField" => "field"
        //]
    ];

    public static function beforeInsert($input)
    {
        return $input;
    }

    public static function afterInsert($object, $input)
    {
        return $input;
    }
    
    public static function beforeUpdate($input)
    {
        return $input;
    }
    
    public static function afterUpdate($object, $input)
    {
        return $input;
    }
    
    public static function beforeDelete($input)
    {
        return $input;
    }

    public static function afterDelete($object, $input)
    {
        return $input;
    }// end custom
}
