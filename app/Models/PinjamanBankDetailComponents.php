<?php 

namespace App\Models;

use App\CoreService\CallService;
use DateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class PinjamanBankDetailComponents extends Model
{
    protected $table = 'pinjaman_bank_detail_components';
    protected $dateFormat = 'c';
    const TABLE = "pinjaman_bank_detail_components";
    const FILEROOT = "/pinjaman_bank_detail_components";
    const IS_LIST = true;
    const IS_ADD = true;
    const IS_EDIT = true;
    const IS_DELETE = true;
    const IS_VIEW = true;
    const FIELD_LIST = ["id", "pinjaman_bank_detail_id", "comp_pinjaman_bank_id", "comp_value", "description", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_ADD = ["pinjaman_bank_detail_id", "comp_pinjaman_bank_id", "comp_value", "description", "created_by", "updated_by"];
    const FIELD_EDIT = ["pinjaman_bank_detail_id", "comp_pinjaman_bank_id", "comp_value", "description", "updated_by"];
    const FIELD_VIEW = ["id", "pinjaman_bank_detail_id", "comp_pinjaman_bank_id", "comp_value", "description", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_READONLY = [];
    const FIELD_FILTERABLE = [
        "id" => [
            "operator" => "=",
        ],
        "pinjaman_bank_detail_id" => [
            "operator" => "=",
        ],
        "comp_pinjaman_bank_id" => [
            "operator" => "=",
        ],
        "comp_value" => [
            "operator" => "=",
        ],
        "description" => [
            "operator" => "=",
        ],
        "created_by" => [
            "operator" => "=",
        ],
        "updated_by" => [
            "operator" => "=",
        ],
        "created_at" => [
            "operator" => "=",
        ],
        "updated_at" => [
            "operator" => "=",
        ],
    ];
    const FIELD_SEARCHABLE = [];
    const FIELD_ARRAY = [];
    const FIELD_SORTABLE = ["id", "pinjaman_bank_detail_id", "comp_pinjaman_bank_id", "comp_value", "description", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_UNIQUE = [["pinjaman_bank_detail_id", "comp_pinjaman_bank_id"]];
    const FIELD_UPLOAD = [];
    const FIELD_TYPE = [
        "id" => "bigint",
        "pinjaman_bank_detail_id" => "bigint",
        "comp_pinjaman_bank_id" => "bigint",
        "comp_value" => "double_precision",
        "description" => "text",
        "created_by" => "bigint",
        "updated_by" => "bigint",
        "created_at" => "timestamp_with_time_zone",
        "updated_at" => "timestamp_with_time_zone",
    ];

    const FIELD_DEFAULT_VALUE = [
        "pinjaman_bank_detail_id" => "",
        "comp_pinjaman_bank_id" => "",
        "comp_value" => "&#039;0&#039;::double precision",
        "description" => "",
        "created_by" => "",
        "updated_by" => "",
        "created_at" => "",
        "updated_at" => "",
    ];
    const FIELD_RELATION = [
        "pinjaman_bank_detail_id" => [
            "linkTable" => "pinjaman_bank_details",
            "aliasTable" => "B",
            "linkField" => "id",
            "displayName" => "rel_pinjaman_bank_detail_id",
            "selectFields" => ["id"],
            "selectValue" => "id AS rel_pinjaman_bank_detail_id"
        ],
        "comp_pinjaman_bank_id" => [
            "linkTable" => "master_comp_pinjaman_bank",
            "aliasTable" => "C",
            "linkField" => "id",
            "displayName" => "rel_comp_pinjaman_bank_id",
            "selectFields" => ["name"],
            "selectValue" => "id AS rel_comp_pinjaman_bank_id"
        ],
        "created_by" => [
            "linkTable" => "users",
            "aliasTable" => "D",
            "linkField" => "id",
            "displayName" => "rel_created_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_created_by"
        ],
        "updated_by" => [
            "linkTable" => "users",
            "aliasTable" => "E",
            "linkField" => "id",
            "displayName" => "rel_updated_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_updated_by"
        ],
    ];
    const CUSTOM_SELECT = "";
    const FIELD_VALIDATION = [
        "pinjaman_bank_detail_id" => "required|integer",
        "comp_pinjaman_bank_id" => "required|integer",
        "comp_value" => "nullable",
        "description" => "nullable|string",
        "created_by" => "nullable|integer",
        "updated_by" => "nullable|integer",
        "created_at" => "nullable|date",
        "updated_at" => "nullable|date",
    ];
    const PARENT_CHILD = [];
    // start custom
    const CUSTOM_LIST_FILTER = [];
    const FIELD_CASTING = [
        "comp_value" => "float",
    ];
    const CHILD_TABLE = [
        //"child_table" => [
        //    "foreignField" => "field"
        //]
    ];

    public static function beforeInsert($input)
    {
        return $input;
    }

    public static function afterInsert($object, $input)
    {
        return $input;
    }
    
    public static function beforeUpdate($input)
    {
        return $input;
    }
    
    public static function afterUpdate($object, $input)
    {
        return $input;
    }
    
    public static function beforeDelete($input)
    {
        return $input;
    }

    public static function afterDelete($object, $input)
    {
        return $input;
    }// end custom
}
