<?php 

namespace App\Models;

use App\CoreService\CallService;
use DateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class EmployeeOnPeriodes extends Model
{
    protected $table = 'employee_on_periodes';
    protected $dateFormat = 'c';
    const TABLE = "employee_on_periodes";
    const FILEROOT = "/employee_on_periodes";
    const IS_LIST = true;
    const IS_ADD = true;
    const IS_EDIT = true;
    const IS_DELETE = true;
    const IS_VIEW = true;
    const FIELD_LIST = ["id", "periode_month", "employee_id", "job_position_id", "status_employment_id", "is_mpp", "is_plt", "salary_value", "pembulatan_salary", "selisih_pembulatan", "status_salary", "salary_calculated_by", "salary_calculated_at", "is_lock", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_ADD = ["periode_month", "employee_id", "job_position_id", "status_employment_id", "is_mpp", "is_plt", "salary_value", "pembulatan_salary", "selisih_pembulatan", "status_salary", "salary_calculated_by", "salary_calculated_at", "is_lock", "created_by", "updated_by"];
    const FIELD_EDIT = ["periode_month", "employee_id", "job_position_id", "status_employment_id", "is_mpp", "is_plt", "salary_value", "pembulatan_salary", "selisih_pembulatan", "status_salary", "salary_calculated_by", "salary_calculated_at", "is_lock", "updated_by"];
    const FIELD_VIEW = ["id", "periode_month", "employee_id", "job_position_id", "status_employment_id", "is_mpp", "is_plt", "salary_value", "pembulatan_salary", "selisih_pembulatan", "status_salary", "salary_calculated_by", "salary_calculated_at", "is_lock", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_READONLY = [];
    const FIELD_FILTERABLE = [
        "id" => [
            "operator" => "=",
        ],
        "periode_month" => [
            "operator" => "=",
        ],
        "employee_id" => [
            "operator" => "=",
        ],
        "job_position_id" => [
            "operator" => "=",
        ],
        "status_employment_id" => [
            "operator" => "=",
        ],
        "is_mpp" => [
            "operator" => "=",
        ],
        "is_plt" => [
            "operator" => "=",
        ],
        "salary_value" => [
            "operator" => "=",
        ],
        "pembulatan_salary" => [
            "operator" => "=",
        ],
        "selisih_pembulatan" => [
            "operator" => "=",
        ],
        "status_salary" => [
            "operator" => "=",
        ],
        "salary_calculated_by" => [
            "operator" => "=",
        ],
        "salary_calculated_at" => [
            "operator" => "=",
        ],
        "is_lock" => [
            "operator" => "=",
        ],
        "created_by" => [
            "operator" => "=",
        ],
        "updated_by" => [
            "operator" => "=",
        ],
        "created_at" => [
            "operator" => "=",
        ],
        "updated_at" => [
            "operator" => "=",
        ],
    ];
    const FIELD_SEARCHABLE = ["status_salary"];
    const FIELD_ARRAY = [];
    const FIELD_SORTABLE = ["id", "periode_month", "employee_id", "job_position_id", "status_employment_id", "is_mpp", "is_plt", "salary_value", "pembulatan_salary", "selisih_pembulatan", "status_salary", "salary_calculated_by", "salary_calculated_at", "is_lock", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_UNIQUE = [["periode_month", "employee_id"]];
    const FIELD_UPLOAD = [];
    const FIELD_TYPE = [
        "id" => "bigint",
        "periode_month" => "date",
        "employee_id" => "bigint",
        "job_position_id" => "bigint",
        "status_employment_id" => "bigint",
        "is_mpp" => "boolean",
        "is_plt" => "boolean",
        "salary_value" => "double_precision",
        "pembulatan_salary" => "double_precision",
        "selisih_pembulatan" => "double_precision",
        "status_salary" => "character_varying",
        "salary_calculated_by" => "bigint",
        "salary_calculated_at" => "timestamp_with_time_zone",
        "is_lock" => "boolean",
        "created_by" => "bigint",
        "updated_by" => "bigint",
        "created_at" => "timestamp_with_time_zone",
        "updated_at" => "timestamp_with_time_zone",
    ];

    const FIELD_DEFAULT_VALUE = [
        "periode_month" => "",
        "employee_id" => "",
        "job_position_id" => "",
        "status_employment_id" => "",
        "is_mpp" => "false",
        "is_plt" => "false",
        "salary_value" => "&#039;0&#039;::double precision",
        "pembulatan_salary" => "&#039;0&#039;::double precision",
        "selisih_pembulatan" => "&#039;0&#039;::double precision",
        "status_salary" => "&#039;not_calculated_yet&#039;::character varying",
        "salary_calculated_by" => "",
        "salary_calculated_at" => "",
        "is_lock" => "false",
        "created_by" => "",
        "updated_by" => "",
        "created_at" => "",
        "updated_at" => "",
    ];
    const FIELD_RELATION = [
        "employee_id" => [
            "linkTable" => "employees",
            "aliasTable" => "B",
            "linkField" => "id",
            "displayName" => "rel_employee_id",
            "selectFields" => ["fullname"],
            "selectValue" => "id AS rel_employee_id"
        ],
        "job_position_id" => [
            "linkTable" => "job_positions",
            "aliasTable" => "C",
            "linkField" => "id",
            "displayName" => "rel_job_position_id",
            "selectFields" => ["job_position_name"],
            "selectValue" => "id AS rel_job_position_id"
        ],
        "status_employment_id" => [
            "linkTable" => "master_status_employments",
            "aliasTable" => "D",
            "linkField" => "id",
            "displayName" => "rel_status_employment_id",
            "selectFields" => ["name"],
            "selectValue" => "id AS rel_status_employment_id"
        ],
        "salary_calculated_by" => [
            "linkTable" => "users",
            "aliasTable" => "E",
            "linkField" => "id",
            "displayName" => "rel_salary_calculated_by",
            "selectFields" => ["id"],
            "selectValue" => "id AS rel_salary_calculated_by"
        ],
        "created_by" => [
            "linkTable" => "users",
            "aliasTable" => "F",
            "linkField" => "id",
            "displayName" => "rel_created_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_created_by"
        ],
        "updated_by" => [
            "linkTable" => "users",
            "aliasTable" => "G",
            "linkField" => "id",
            "displayName" => "rel_updated_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_updated_by"
        ],
    ];
    const CUSTOM_SELECT = "";
    const FIELD_VALIDATION = [
        "periode_month" => "required",
        "employee_id" => "required|integer",
        "job_position_id" => "required|integer",
        "status_employment_id" => "required|integer",
        "is_mpp" => "required",
        "is_plt" => "required",
        "salary_value" => "nullable",
        "pembulatan_salary" => "nullable",
        "selisih_pembulatan" => "nullable",
        "status_salary" => "nullable|string|max:255",
        "salary_calculated_by" => "nullable|integer",
        "salary_calculated_at" => "nullable|date",
        "is_lock" => "nullable",
        "created_by" => "nullable|integer",
        "updated_by" => "nullable|integer",
        "created_at" => "nullable|date",
        "updated_at" => "nullable|date",
    ];
    const PARENT_CHILD = [];
    // start custom
    const CUSTOM_LIST_FILTER = [];
    const FIELD_CASTING = [
        //"nama field" => "float",
    ];
    const CHILD_TABLE = [
        //"child_table" => [
        //    "foreignField" => "field"
        //]
    ];

    public static function beforeInsert($input)
    {
        return $input;
    }

    public static function afterInsert($object, $input)
    {
        return $input;
    }
    
    public static function beforeUpdate($input)
    {
        return $input;
    }
    
    public static function afterUpdate($object, $input)
    {
        return $input;
    }
    
    public static function beforeDelete($input)
    {
        return $input;
    }

    public static function afterDelete($object, $input)
    {
        return $input;
    }// end custom
}
