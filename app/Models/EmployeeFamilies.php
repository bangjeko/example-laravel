<?php 

namespace App\Models;

use App\CoreService\CallService;
use DateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class EmployeeFamilies extends Model
{
    protected $table = 'employee_families';
    protected $dateFormat = 'c';
    const TABLE = "employee_families";
    const FILEROOT = "/employee_families";
    const IS_LIST = true;
    const IS_ADD = true;
    const IS_EDIT = true;
    const IS_DELETE = true;
    const IS_VIEW = true;
    const FIELD_LIST = ["id", "employee_id", "family_relationship_id", "fullname", "nik", "place_of_birth", "date_of_birth", "gender", "address", "telephone", "education_id", "work_description", "description", "is_bpjs_tambahan", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_ADD = ["employee_id", "family_relationship_id", "fullname", "nik", "place_of_birth", "date_of_birth", "gender", "address", "telephone", "education_id", "work_description", "description", "is_bpjs_tambahan", "created_by", "updated_by"];
    const FIELD_EDIT = ["employee_id", "family_relationship_id", "fullname", "nik", "place_of_birth", "date_of_birth", "gender", "address", "telephone", "education_id", "work_description", "description", "is_bpjs_tambahan", "updated_by"];
    const FIELD_VIEW = ["id", "employee_id", "family_relationship_id", "fullname", "nik", "place_of_birth", "date_of_birth", "gender", "address", "telephone", "education_id", "work_description", "description", "is_bpjs_tambahan", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_READONLY = [];
    const FIELD_FILTERABLE = [
        "id" => [
            "operator" => "=",
        ],
        "employee_id" => [
            "operator" => "=",
        ],
        "family_relationship_id" => [
            "operator" => "=",
        ],
        "fullname" => [
            "operator" => "=",
        ],
        "nik" => [
            "operator" => "=",
        ],
        "place_of_birth" => [
            "operator" => "=",
        ],
        "date_of_birth" => [
            "operator" => "=",
        ],
        "gender" => [
            "operator" => "=",
        ],
        "address" => [
            "operator" => "=",
        ],
        "telephone" => [
            "operator" => "=",
        ],
        "education_id" => [
            "operator" => "=",
        ],
        "work_description" => [
            "operator" => "=",
        ],
        "description" => [
            "operator" => "=",
        ],
        "is_bpjs_tambahan" => [
            "operator" => "=",
        ],
        "created_by" => [
            "operator" => "=",
        ],
        "updated_by" => [
            "operator" => "=",
        ],
        "created_at" => [
            "operator" => "=",
        ],
        "updated_at" => [
            "operator" => "=",
        ],
    ];
    const FIELD_SEARCHABLE = ["fullname", "nik", "place_of_birth", "gender", "address", "telephone"];
    const FIELD_ARRAY = [];
    const FIELD_SORTABLE = ["id", "employee_id", "family_relationship_id", "fullname", "nik", "place_of_birth", "date_of_birth", "gender", "address", "telephone", "education_id", "work_description", "description", "is_bpjs_tambahan", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_UNIQUE = [["nik"]];
    const FIELD_UPLOAD = [];
    const FIELD_TYPE = [
        "id" => "bigint",
        "employee_id" => "bigint",
        "family_relationship_id" => "bigint",
        "fullname" => "character_varying",
        "nik" => "character_varying",
        "place_of_birth" => "character_varying",
        "date_of_birth" => "date",
        "gender" => "character_varying",
        "address" => "character_varying",
        "telephone" => "character_varying",
        "education_id" => "bigint",
        "work_description" => "text",
        "description" => "text",
        "is_bpjs_tambahan" => "boolean",
        "created_by" => "bigint",
        "updated_by" => "bigint",
        "created_at" => "timestamp_with_time_zone",
        "updated_at" => "timestamp_with_time_zone",
    ];

    const FIELD_DEFAULT_VALUE = [
        "employee_id" => "",
        "family_relationship_id" => "",
        "fullname" => "",
        "nik" => "",
        "place_of_birth" => "",
        "date_of_birth" => "",
        "gender" => "",
        "address" => "",
        "telephone" => "",
        "education_id" => "",
        "work_description" => "",
        "description" => "",
        "is_bpjs_tambahan" => "false",
        "created_by" => "",
        "updated_by" => "",
        "created_at" => "",
        "updated_at" => "",
    ];
    const FIELD_RELATION = [
        "employee_id" => [
            "linkTable" => "employees",
            "aliasTable" => "B",
            "linkField" => "id",
            "displayName" => "rel_employee_id",
            "selectFields" => ["fullname"],
            "selectValue" => "id AS rel_employee_id"
        ],
        "family_relationship_id" => [
            "linkTable" => "master_family_relationships",
            "aliasTable" => "C",
            "linkField" => "id",
            "displayName" => "rel_family_relationship_id",
            "selectFields" => ["name"],
            "selectValue" => "id AS rel_family_relationship_id"
        ],
        "education_id" => [
            "linkTable" => "master_educations",
            "aliasTable" => "D",
            "linkField" => "id",
            "displayName" => "rel_education_id",
            "selectFields" => ["name"],
            "selectValue" => "id AS rel_education_id"
        ],
        "created_by" => [
            "linkTable" => "users",
            "aliasTable" => "E",
            "linkField" => "id",
            "displayName" => "rel_created_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_created_by"
        ],
        "updated_by" => [
            "linkTable" => "users",
            "aliasTable" => "F",
            "linkField" => "id",
            "displayName" => "rel_updated_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_updated_by"
        ],
    ];
    const CUSTOM_SELECT = "";
    const FIELD_VALIDATION = [
        "employee_id" => "required|integer",
        "family_relationship_id" => "required|integer",
        "fullname" => "required|string|max:255",
        "nik" => "nullable|string|max:255",
        "place_of_birth" => "nullable|string|max:255",
        "date_of_birth" => "nullable",
        "gender" => "required|string|max:255",
        "address" => "nullable|string|max:255",
        "telephone" => "nullable|string|max:255",
        "education_id" => "nullable|integer",
        "work_description" => "nullable|string",
        "description" => "nullable|string",
        "is_bpjs_tambahan" => "nullable",
        "created_by" => "nullable|integer",
        "updated_by" => "nullable|integer",
        "created_at" => "nullable|date",
        "updated_at" => "nullable|date",
    ];
    const PARENT_CHILD = [];
    // start custom
    const CUSTOM_LIST_FILTER = [];
    const FIELD_CASTING = [];
    const CHILD_TABLE = [
        //"child_table" => [
        //    "foreignField" => "field"
        //]
    ];

    public static function beforeInsert($input)
    {
        $input["is_bpjs_tambahan"] = false;
        return $input;
    }

    public static function afterInsert($object, $input)
    {
        return $input;
    }

    public static function beforeUpdate($input)
    {
        return $input;
    }

    public static function afterUpdate($object, $input)
    {
        return $input;
    }

    public static function beforeDelete($input)
    {
        return $input;
    }

    public static function afterDelete($object, $input)
    {
        return $input;
    } // end custom
}
