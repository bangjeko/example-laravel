<?php 

namespace App\Models;

use App\CoreService\CallService;
use DateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class PayrollPeriodes extends Model
{
    protected $table = 'payroll_periodes';
    protected $dateFormat = 'c';
    const TABLE = "payroll_periodes";
    const FILEROOT = "/payroll_periodes";
    const IS_LIST = true;
    const IS_ADD = true;
    const IS_EDIT = true;
    const IS_DELETE = true;
    const IS_VIEW = true;
    const FIELD_LIST = ["id", "periode_month", "start_date", "end_date", "count_day_of_work", "description", "is_lock", "submitted_by", "submitted_at", "unlocked_by", "unlocked_at", "status_code", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_ADD = ["periode_month", "start_date", "end_date", "count_day_of_work", "description", "is_lock", "submitted_by", "submitted_at", "unlocked_by", "unlocked_at", "status_code", "created_by", "updated_by"];
    const FIELD_EDIT = ["periode_month", "start_date", "end_date", "count_day_of_work", "description", "is_lock", "submitted_by", "submitted_at", "unlocked_by", "unlocked_at", "status_code", "updated_by"];
    const FIELD_VIEW = ["id", "periode_month", "start_date", "end_date", "count_day_of_work", "description", "is_lock", "submitted_by", "submitted_at", "unlocked_by", "unlocked_at", "status_code", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_READONLY = [];
    const FIELD_FILTERABLE = [
        "id" => [
            "operator" => "=",
        ],
        "periode_month" => [
            "operator" => "=",
        ],
        "start_date" => [
            "operator" => "=",
        ],
        "end_date" => [
            "operator" => "=",
        ],
        "count_day_of_work" => [
            "operator" => "=",
        ],
        "description" => [
            "operator" => "=",
        ],
        "is_lock" => [
            "operator" => "=",
        ],
        "submitted_by" => [
            "operator" => "=",
        ],
        "submitted_at" => [
            "operator" => "=",
        ],
        "unlocked_by" => [
            "operator" => "=",
        ],
        "unlocked_at" => [
            "operator" => "=",
        ],
        "status_code" => [
            "operator" => "=",
        ],
        "created_by" => [
            "operator" => "=",
        ],
        "updated_by" => [
            "operator" => "=",
        ],
        "created_at" => [
            "operator" => "=",
        ],
        "updated_at" => [
            "operator" => "=",
        ],
    ];
    const FIELD_SEARCHABLE = [];
    const FIELD_ARRAY = [];
    const FIELD_SORTABLE = ["id", "periode_month", "start_date", "end_date", "count_day_of_work", "description", "is_lock", "submitted_by", "submitted_at", "unlocked_by", "unlocked_at", "status_code", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_UNIQUE = [["periode_month"]];
    const FIELD_UPLOAD = [];
    const FIELD_TYPE = [
        "id" => "bigint",
        "periode_month" => "date",
        "start_date" => "date",
        "end_date" => "date",
        "count_day_of_work" => "integer",
        "description" => "text",
        "is_lock" => "boolean",
        "submitted_by" => "bigint",
        "submitted_at" => "timestamp_with_time_zone",
        "unlocked_by" => "bigint",
        "unlocked_at" => "timestamp_with_time_zone",
        "status_code" => "character_varying",
        "created_by" => "bigint",
        "updated_by" => "bigint",
        "created_at" => "timestamp_with_time_zone",
        "updated_at" => "timestamp_with_time_zone",
    ];

    const FIELD_DEFAULT_VALUE = [
        "periode_month" => "",
        "start_date" => "",
        "end_date" => "",
        "count_day_of_work" => "21",
        "description" => "",
        "is_lock" => "false",
        "submitted_by" => "",
        "submitted_at" => "",
        "unlocked_by" => "",
        "unlocked_at" => "",
        "status_code" => "",
        "created_by" => "",
        "updated_by" => "",
        "created_at" => "",
        "updated_at" => "",
    ];
    const FIELD_RELATION = [
        "submitted_by" => [
            "linkTable" => "users",
            "aliasTable" => "B",
            "linkField" => "id",
            "displayName" => "rel_submitted_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_submitted_by"
        ],
        "unlocked_by" => [
            "linkTable" => "users",
            "aliasTable" => "C",
            "linkField" => "id",
            "displayName" => "rel_unlocked_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_unlocked_by"
        ],
        "created_by" => [
            "linkTable" => "users",
            "aliasTable" => "D",
            "linkField" => "id",
            "displayName" => "rel_created_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_created_by"
        ],
        "updated_by" => [
            "linkTable" => "users",
            "aliasTable" => "E",
            "linkField" => "id",
            "displayName" => "rel_updated_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_updated_by"
        ],
    ];
    const CUSTOM_SELECT = "";
    const FIELD_VALIDATION = [
        "periode_month" => "required",
        "start_date" => "required",
        "end_date" => "required",
        "count_day_of_work" => "nullable|integer",
        "description" => "nullable|string",
        "is_lock" => "nullable",
        "submitted_by" => "nullable|integer",
        "submitted_at" => "nullable|date",
        "unlocked_by" => "nullable|integer",
        "unlocked_at" => "nullable|date",
        "status_code" => "nullable|string|max:255",
        "created_by" => "nullable|integer",
        "updated_by" => "nullable|integer",
        "created_at" => "nullable|date",
        "updated_at" => "nullable|date",
    ];
    const PARENT_CHILD = [];
    // start custom
    const CUSTOM_LIST_FILTER = [];
    const FIELD_CASTING = [
        //"nama field" => "float",
    ];
    const CHILD_TABLE = [
        //"child_table" => [
        //    "foreignField" => "field"
        //]
    ];

    public static function beforeInsert($input)
    {
        return $input;
    }

    public static function afterInsert($object, $input)
    {
        return $input;
    }
    
    public static function beforeUpdate($input)
    {
        return $input;
    }
    
    public static function afterUpdate($object, $input)
    {
        return $input;
    }
    
    public static function beforeDelete($input)
    {
        return $input;
    }

    public static function afterDelete($object, $input)
    {
        return $input;
    }// end custom
}
