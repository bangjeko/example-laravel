<?php 

namespace App\Models;

use App\CoreService\CallService;
use DateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class PresencePermits extends Model
{
    protected $table = 'presence_permits';
    protected $dateFormat = 'c';
    const TABLE = "presence_permits";
    const FILEROOT = "/presence_permits";
    const IS_LIST = true;
    const IS_ADD = true;
    const IS_EDIT = true;
    const IS_DELETE = true;
    const IS_VIEW = true;
    const FIELD_LIST = ["id", "employee_id", "presence_permit_category_id", "title", "reason", "start_date", "end_date", "duration", "file_filename", "status_code", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_ADD = ["employee_id", "presence_permit_category_id", "title", "reason", "start_date", "end_date", "duration", "file_filename", "status_code", "created_by", "updated_by"];
    const FIELD_EDIT = ["employee_id", "presence_permit_category_id", "title", "reason", "start_date", "end_date", "duration", "file_filename", "status_code", "updated_by"];
    const FIELD_VIEW = ["id", "employee_id", "presence_permit_category_id", "title", "reason", "start_date", "end_date", "duration", "file_filename", "status_code", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_READONLY = [];
    const FIELD_FILTERABLE = [
        "id" => [
            "operator" => "=",
        ],
        "employee_id" => [
            "operator" => "=",
        ],
        "presence_permit_category_id" => [
            "operator" => "=",
        ],
        "title" => [
            "operator" => "=",
        ],
        "reason" => [
            "operator" => "=",
        ],
        "start_date" => [
            "operator" => "=",
        ],
        "end_date" => [
            "operator" => "=",
        ],
        "duration" => [
            "operator" => "=",
        ],
        "file_filename" => [
            "operator" => "=",
        ],
        "status_code" => [
            "operator" => "=",
        ],
        "created_by" => [
            "operator" => "=",
        ],
        "updated_by" => [
            "operator" => "=",
        ],
        "created_at" => [
            "operator" => "=",
        ],
        "updated_at" => [
            "operator" => "=",
        ],
    ];
    const FIELD_SEARCHABLE = ["title"];
    const FIELD_ARRAY = [];
    const FIELD_SORTABLE = ["id", "employee_id", "presence_permit_category_id", "title", "reason", "start_date", "end_date", "duration", "file_filename", "status_code", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_UNIQUE = [];
    const FIELD_UPLOAD = ["file_filename"];
    const FIELD_TYPE = [
        "id" => "bigint",
        "employee_id" => "bigint",
        "presence_permit_category_id" => "bigint",
        "title" => "character_varying",
        "reason" => "text",
        "start_date" => "date",
        "end_date" => "date",
        "duration" => "integer",
        "file_filename" => "text",
        "status_code" => "character_varying",
        "created_by" => "bigint",
        "updated_by" => "bigint",
        "created_at" => "timestamp_with_time_zone",
        "updated_at" => "timestamp_with_time_zone",
    ];

    const FIELD_DEFAULT_VALUE = [
        "employee_id" => "",
        "presence_permit_category_id" => "",
        "title" => "",
        "reason" => "",
        "start_date" => "",
        "end_date" => "",
        "duration" => "0",
        "file_filename" => "",
        "status_code" => "",
        "created_by" => "",
        "updated_by" => "",
        "created_at" => "",
        "updated_at" => "",
    ];
    const FIELD_RELATION = [
        "employee_id" => [
            "linkTable" => "employees",
            "aliasTable" => "B",
            "linkField" => "id",
            "displayName" => "rel_employee_id",
            "selectFields" => ["id"],
            "selectValue" => "id AS rel_employee_id"
        ],
        "presence_permit_category_id" => [
            "linkTable" => "master_presence_permit_categories",
            "aliasTable" => "C",
            "linkField" => "id",
            "displayName" => "rel_presence_permit_category_id",
            "selectFields" => ["id"],
            "selectValue" => "id AS rel_presence_permit_category_id"
        ],
        "created_by" => [
            "linkTable" => "users",
            "aliasTable" => "D",
            "linkField" => "id",
            "displayName" => "rel_created_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_created_by"
        ],
        "updated_by" => [
            "linkTable" => "users",
            "aliasTable" => "E",
            "linkField" => "id",
            "displayName" => "rel_updated_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_updated_by"
        ],
    ];
    const CUSTOM_SELECT = "";
    const FIELD_VALIDATION = [
        "employee_id" => "required|integer",
        "presence_permit_category_id" => "required|integer",
        "title" => "nullable|string|max:255",
        "reason" => "required|string",
        "start_date" => "required",
        "end_date" => "required",
        "duration" => "nullable|integer",
        "file_filename" => "nullable|string|exists_file",
        "status_code" => "nullable|string|max:255",
        "created_by" => "nullable|integer",
        "updated_by" => "nullable|integer",
        "created_at" => "nullable|date",
        "updated_at" => "nullable|date",
    ];
    const PARENT_CHILD = [];
    // start custom
    const CUSTOM_LIST_FILTER = [];
    const FIELD_CASTING = [
        //"nama field" => "float",
    ];
    const CHILD_TABLE = [
        //"child_table" => [
        //    "foreignField" => "field"
        //]
    ];

    public static function beforeInsert($input)
    {
        $startDate = $input["start_date"];
        $endDate = $input["end_date"];
        $input["duration"] = (int) Carbon::parse($startDate)->diff($endDate)->format('%d') + 1;

        return $input;
    }

    public static function afterInsert($object, $input)
    {
        return $input;
    }

    public static function beforeUpdate($input)
    {
        return $input;
    }

    public static function afterUpdate($object, $input)
    {
        return $input;
    }

    public static function beforeDelete($input)
    {
        return $input;
    }

    public static function afterDelete($object, $input)
    {
        return $input;
    } // end custom
}
