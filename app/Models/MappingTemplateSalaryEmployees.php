<?php 

namespace App\Models;

use App\CoreService\CallService;
use DateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class MappingTemplateSalaryEmployees extends Model
{
    protected $table = 'mapping_template_salary_employees';
    protected $dateFormat = 'c';
    const TABLE = "mapping_template_salary_employees";
    const FILEROOT = "/mapping_template_salary_employees";
    const IS_LIST = true;
    const IS_ADD = true;
    const IS_EDIT = true;
    const IS_DELETE = true;
    const IS_VIEW = true;
    const FIELD_LIST = ["id", "periode_month", "employee_id", "salary_component_id", "default_value", "active", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_ADD = ["periode_month", "employee_id", "salary_component_id", "default_value", "active", "created_by", "updated_by"];
    const FIELD_EDIT = ["periode_month", "employee_id", "salary_component_id", "default_value", "active", "updated_by"];
    const FIELD_VIEW = ["id", "periode_month", "employee_id", "salary_component_id", "default_value", "active", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_READONLY = [];
    const FIELD_FILTERABLE = [
        "id" => [
            "operator" => "=",
        ],
        "periode_month" => [
            "operator" => "=",
        ],
        "employee_id" => [
            "operator" => "=",
        ],
        "salary_component_id" => [
            "operator" => "=",
        ],
        "default_value" => [
            "operator" => "=",
        ],
        "active" => [
            "operator" => "=",
        ],
        "created_by" => [
            "operator" => "=",
        ],
        "updated_by" => [
            "operator" => "=",
        ],
        "created_at" => [
            "operator" => "=",
        ],
        "updated_at" => [
            "operator" => "=",
        ],
    ];
    const FIELD_SEARCHABLE = [];
    const FIELD_ARRAY = [];
    const FIELD_SORTABLE = ["id", "periode_month", "employee_id", "salary_component_id", "default_value", "active", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_UNIQUE = [["periode_month", "employee_id", "salary_component_id"]];
    const FIELD_UPLOAD = [];
    const FIELD_TYPE = [
        "id" => "bigint",
        "periode_month" => "date",
        "employee_id" => "bigint",
        "salary_component_id" => "bigint",
        "default_value" => "double_precision",
        "active" => "integer",
        "created_by" => "bigint",
        "updated_by" => "bigint",
        "created_at" => "timestamp_with_time_zone",
        "updated_at" => "timestamp_with_time_zone",
    ];

    const FIELD_DEFAULT_VALUE = [
        "periode_month" => "",
        "employee_id" => "",
        "salary_component_id" => "",
        "default_value" => "&#039;0&#039;::double precision",
        "active" => "1",
        "created_by" => "",
        "updated_by" => "",
        "created_at" => "",
        "updated_at" => "",
    ];
    const FIELD_RELATION = [
        "employee_id" => [
            "linkTable" => "employees",
            "aliasTable" => "B",
            "linkField" => "id",
            "displayName" => "rel_employee_id",
            "selectFields" => ["id"],
            "selectValue" => "id AS rel_employee_id"
        ],
        "salary_component_id" => [
            "linkTable" => "master_salary_components",
            "aliasTable" => "C",
            "linkField" => "id",
            "displayName" => "rel_salary_component_id",
            "selectFields" => ["id"],
            "selectValue" => "id AS rel_salary_component_id"
        ],
        "created_by" => [
            "linkTable" => "users",
            "aliasTable" => "D",
            "linkField" => "id",
            "displayName" => "rel_created_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_created_by"
        ],
        "updated_by" => [
            "linkTable" => "users",
            "aliasTable" => "E",
            "linkField" => "id",
            "displayName" => "rel_updated_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_updated_by"
        ],
    ];
    const CUSTOM_SELECT = "";
    const FIELD_VALIDATION = [
        "periode_month" => "required",
        "employee_id" => "required|integer",
        "salary_component_id" => "required|integer",
        "default_value" => "nullable",
        "active" => "nullable|integer",
        "created_by" => "nullable|integer",
        "updated_by" => "nullable|integer",
        "created_at" => "nullable|date",
        "updated_at" => "nullable|date",
    ];
    const PARENT_CHILD = [];
    // start custom
    const CUSTOM_LIST_FILTER = [];
    const FIELD_CASTING = [];
    const CHILD_TABLE = [
        //"child_table" => [
        //    "foreignField" => "field"
        //]
    ];

    public static function beforeInsert($input)
    {
        return $input;
    }

    public static function afterInsert($object, $input)
    {
        return $input;
    }
    
    public static function beforeUpdate($input)
    {
        return $input;
    }
    
    public static function afterUpdate($object, $input)
    {
        return $input;
    }
    
    public static function beforeDelete($input)
    {
        return $input;
    }

    public static function afterDelete($object, $input)
    {
        return $input;
    }// end custom
}
