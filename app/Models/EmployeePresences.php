<?php 

namespace App\Models;

use App\CoreService\CallService;
use DateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class EmployeePresences extends Model
{
    protected $table = 'employee_presences';
    protected $dateFormat = 'c';
    const TABLE = "employee_presences";
    const FILEROOT = "/employee_presences";
    const IS_LIST = true;
    const IS_ADD = true;
    const IS_EDIT = true;
    const IS_DELETE = true;
    const IS_VIEW = true;
    const FIELD_LIST = ["id", "date", "employee_id", "working_category_id", "checkin", "checkout", "late", "overtime", "description", "permit_id", "status_presence", "is_lock", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_ADD = ["date", "employee_id", "working_category_id", "checkin", "checkout", "late", "overtime", "description", "permit_id", "status_presence", "is_lock", "created_by", "updated_by"];
    const FIELD_EDIT = ["date", "employee_id", "working_category_id", "checkin", "checkout", "late", "overtime", "description", "permit_id", "status_presence", "is_lock", "updated_by"];
    const FIELD_VIEW = ["id", "date", "employee_id", "working_category_id", "checkin", "checkout", "late", "overtime", "description", "permit_id", "status_presence", "is_lock", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_READONLY = [];
    const FIELD_FILTERABLE = [
        "id" => [
            "operator" => "=",
        ],
        "date" => [
            "operator" => "=",
        ],
        "employee_id" => [
            "operator" => "=",
        ],
        "working_category_id" => [
            "operator" => "=",
        ],
        "checkin" => [
            "operator" => "=",
        ],
        "checkout" => [
            "operator" => "=",
        ],
        "late" => [
            "operator" => "=",
        ],
        "overtime" => [
            "operator" => "=",
        ],
        "description" => [
            "operator" => "=",
        ],
        "permit_id" => [
            "operator" => "=",
        ],
        "status_presence" => [
            "operator" => "=",
        ],
        "is_lock" => [
            "operator" => "=",
        ],
        "created_by" => [
            "operator" => "=",
        ],
        "updated_by" => [
            "operator" => "=",
        ],
        "created_at" => [
            "operator" => "=",
        ],
        "updated_at" => [
            "operator" => "=",
        ],
    ];
    const FIELD_SEARCHABLE = ["late", "overtime", "status_presence"];
    const FIELD_ARRAY = [];
    const FIELD_SORTABLE = ["id", "date", "employee_id", "working_category_id", "checkin", "checkout", "late", "overtime", "description", "permit_id", "status_presence", "is_lock", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_UNIQUE = [["date", "employee_id"]];
    const FIELD_UPLOAD = [];
    const FIELD_TYPE = [
        "id" => "bigint",
        "date" => "date",
        "employee_id" => "bigint",
        "working_category_id" => "bigint",
        "checkin" => "timestamp_with_time_zone",
        "checkout" => "timestamp_with_time_zone",
        "late" => "character_varying",
        "overtime" => "character_varying",
        "description" => "text",
        "permit_id" => "bigint",
        "status_presence" => "character_varying",
        "is_lock" => "boolean",
        "created_by" => "bigint",
        "updated_by" => "bigint",
        "created_at" => "timestamp_with_time_zone",
        "updated_at" => "timestamp_with_time_zone",
    ];

    const FIELD_DEFAULT_VALUE = [
        "date" => "",
        "employee_id" => "",
        "working_category_id" => "",
        "checkin" => "",
        "checkout" => "",
        "late" => "",
        "overtime" => "",
        "description" => "",
        "permit_id" => "",
        "status_presence" => "",
        "is_lock" => "false",
        "created_by" => "",
        "updated_by" => "",
        "created_at" => "",
        "updated_at" => "",
    ];
    const FIELD_RELATION = [
        "employee_id" => [
            "linkTable" => "employees",
            "aliasTable" => "B",
            "linkField" => "id",
            "displayName" => "rel_employee_id",
            "selectFields" => ["id"],
            "selectValue" => "id AS rel_employee_id"
        ],
        "working_category_id" => [
            "linkTable" => "master_working_categories",
            "aliasTable" => "C",
            "linkField" => "id",
            "displayName" => "rel_working_category_id",
            "selectFields" => ["id"],
            "selectValue" => "id AS rel_working_category_id"
        ],
        "created_by" => [
            "linkTable" => "users",
            "aliasTable" => "D",
            "linkField" => "id",
            "displayName" => "rel_created_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_created_by"
        ],
        "updated_by" => [
            "linkTable" => "users",
            "aliasTable" => "E",
            "linkField" => "id",
            "displayName" => "rel_updated_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_updated_by"
        ],
    ];
    const CUSTOM_SELECT = "";
    const FIELD_VALIDATION = [
        "date" => "required",
        "employee_id" => "required|integer",
        "working_category_id" => "required|integer",
        "checkin" => "required|date",
        "checkout" => "required|date",
        "late" => "nullable|string|max:255",
        "overtime" => "nullable|string|max:255",
        "description" => "nullable|string",
        "permit_id" => "nullable|integer",
        "status_presence" => "nullable|string|max:255",
        "is_lock" => "nullable",
        "created_by" => "nullable|integer",
        "updated_by" => "nullable|integer",
        "created_at" => "nullable|date",
        "updated_at" => "nullable|date",
    ];
    const PARENT_CHILD = [];
    // start custom
    const CUSTOM_LIST_FILTER = [];
    const FIELD_CASTING = [
        //"nama field" => "float",
    ];
    const CHILD_TABLE = [
        //"child_table" => [
        //    "foreignField" => "field"
        //]
    ];

    public static function beforeInsert($input)
    {
        return $input;
    }

    public static function afterInsert($object, $input)
    {
        return $input;
    }
    
    public static function beforeUpdate($input)
    {
        return $input;
    }
    
    public static function afterUpdate($object, $input)
    {
        return $input;
    }
    
    public static function beforeDelete($input)
    {
        return $input;
    }

    public static function afterDelete($object, $input)
    {
        return $input;
    }// end custom
}
