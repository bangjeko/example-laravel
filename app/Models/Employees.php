<?php 

namespace App\Models;

use App\CoreService\CallService;
use DateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class Employees extends Model
{
    protected $table = 'employees';
    protected $dateFormat = 'c';
    const TABLE = "employees";
    const FILEROOT = "/employees";
    const IS_LIST = true;
    const IS_ADD = true;
    const IS_EDIT = true;
    const IS_DELETE = true;
    const IS_VIEW = true;
    const FIELD_LIST = ["id", "fullname", "nik", "nip", "job_position_id", "status_employment_id", "place_of_birth", "date_of_birth", "gender", "blood_type", "marital_status", "address", "religion_id", "email", "telephone", "emergency_contact", "facebook_name", "instagram_name", "linkedin_name", "twitter_name", "personal_value_description", "personal_visi_description", "passion_interest", "skills", "img_photo", "education_id", "program_study", "institution_name", "nationality", "gelar_depan", "gelar_belakang", "nomor_kk", "nomor_kpj_bpjs_tk", "nomor_bpjs_kes", "npwp", "user_id", "taptap_user_id", "entry_date", "out_date", "appointment_date", "start_date_contract", "end_date_contract", "is_mpp", "is_plt", "status_code", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_ADD = ["fullname", "nik", "nip", "job_position_id", "status_employment_id", "place_of_birth", "date_of_birth", "gender", "blood_type", "marital_status", "address", "religion_id", "email", "telephone", "emergency_contact", "facebook_name", "instagram_name", "linkedin_name", "twitter_name", "personal_value_description", "personal_visi_description", "passion_interest", "skills", "img_photo", "education_id", "program_study", "institution_name", "nationality", "gelar_depan", "gelar_belakang", "nomor_kk", "nomor_kpj_bpjs_tk", "nomor_bpjs_kes", "npwp", "user_id", "taptap_user_id", "entry_date", "out_date", "appointment_date", "start_date_contract", "end_date_contract", "is_mpp", "is_plt", "status_code", "created_by", "updated_by"];
    const FIELD_EDIT = ["fullname", "nik", "nip", "job_position_id", "status_employment_id", "place_of_birth", "date_of_birth", "gender", "blood_type", "marital_status", "address", "religion_id", "email", "telephone", "emergency_contact", "facebook_name", "instagram_name", "linkedin_name", "twitter_name", "personal_value_description", "personal_visi_description", "passion_interest", "skills", "img_photo", "education_id", "program_study", "institution_name", "nationality", "gelar_depan", "gelar_belakang", "nomor_kk", "nomor_kpj_bpjs_tk", "nomor_bpjs_kes", "npwp", "user_id", "taptap_user_id", "entry_date", "out_date", "appointment_date", "start_date_contract", "end_date_contract", "is_mpp", "is_plt", "status_code", "updated_by"];
    const FIELD_VIEW = ["id", "fullname", "nik", "nip", "job_position_id", "status_employment_id", "place_of_birth", "date_of_birth", "gender", "blood_type", "marital_status", "address", "religion_id", "email", "telephone", "emergency_contact", "facebook_name", "instagram_name", "linkedin_name", "twitter_name", "personal_value_description", "personal_visi_description", "passion_interest", "skills", "img_photo", "education_id", "program_study", "institution_name", "nationality", "gelar_depan", "gelar_belakang", "nomor_kk", "nomor_kpj_bpjs_tk", "nomor_bpjs_kes", "npwp", "user_id", "taptap_user_id", "entry_date", "out_date", "appointment_date", "start_date_contract", "end_date_contract", "is_mpp", "is_plt", "status_code", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_READONLY = [];
    const FIELD_FILTERABLE = [
        "id" => [
            "operator" => "=",
        ],
        "fullname" => [
            "operator" => "=",
        ],
        "nik" => [
            "operator" => "=",
        ],
        "nip" => [
            "operator" => "=",
        ],
        "job_position_id" => [
            "operator" => "=",
        ],
        "status_employment_id" => [
            "operator" => "=",
        ],
        "place_of_birth" => [
            "operator" => "=",
        ],
        "date_of_birth" => [
            "operator" => "=",
        ],
        "gender" => [
            "operator" => "=",
        ],
        "blood_type" => [
            "operator" => "=",
        ],
        "marital_status" => [
            "operator" => "=",
        ],
        "address" => [
            "operator" => "=",
        ],
        "religion_id" => [
            "operator" => "=",
        ],
        "email" => [
            "operator" => "=",
        ],
        "telephone" => [
            "operator" => "=",
        ],
        "emergency_contact" => [
            "operator" => "=",
        ],
        "facebook_name" => [
            "operator" => "=",
        ],
        "instagram_name" => [
            "operator" => "=",
        ],
        "linkedin_name" => [
            "operator" => "=",
        ],
        "twitter_name" => [
            "operator" => "=",
        ],
        "personal_value_description" => [
            "operator" => "=",
        ],
        "personal_visi_description" => [
            "operator" => "=",
        ],
        "passion_interest" => [
            "operator" => "=",
        ],
        "skills" => [
            "operator" => "=",
        ],
        "img_photo" => [
            "operator" => "=",
        ],
        "education_id" => [
            "operator" => "=",
        ],
        "program_study" => [
            "operator" => "=",
        ],
        "institution_name" => [
            "operator" => "=",
        ],
        "nationality" => [
            "operator" => "=",
        ],
        "gelar_depan" => [
            "operator" => "=",
        ],
        "gelar_belakang" => [
            "operator" => "=",
        ],
        "nomor_kk" => [
            "operator" => "=",
        ],
        "nomor_kpj_bpjs_tk" => [
            "operator" => "=",
        ],
        "nomor_bpjs_kes" => [
            "operator" => "=",
        ],
        "npwp" => [
            "operator" => "=",
        ],
        "user_id" => [
            "operator" => "=",
        ],
        "taptap_user_id" => [
            "operator" => "=",
        ],
        "entry_date" => [
            "operator" => "=",
        ],
        "out_date" => [
            "operator" => "=",
        ],
        "appointment_date" => [
            "operator" => "=",
        ],
        "start_date_contract" => [
            "operator" => "=",
        ],
        "end_date_contract" => [
            "operator" => "=",
        ],
        "is_mpp" => [
            "operator" => "=",
        ],
        "is_plt" => [
            "operator" => "=",
        ],
        "status_code" => [
            "operator" => "=",
        ],
        "created_by" => [
            "operator" => "=",
        ],
        "updated_by" => [
            "operator" => "=",
        ],
        "created_at" => [
            "operator" => "=",
        ],
        "updated_at" => [
            "operator" => "=",
        ],
    ];
    const FIELD_SEARCHABLE = ["fullname", "nik", "nip", "place_of_birth", "gender", "blood_type", "marital_status", "email", "telephone", "emergency_contact", "facebook_name", "instagram_name", "linkedin_name", "twitter_name", "program_study", "institution_name", "nationality", "gelar_depan", "gelar_belakang", "nomor_kk", "nomor_kpj_bpjs_tk", "nomor_bpjs_kes", "npwp", "taptap_user_id"];
    const FIELD_ARRAY = [];
    const FIELD_SORTABLE = ["id", "fullname", "nik", "nip", "job_position_id", "status_employment_id", "place_of_birth", "date_of_birth", "gender", "blood_type", "marital_status", "address", "religion_id", "email", "telephone", "emergency_contact", "facebook_name", "instagram_name", "linkedin_name", "twitter_name", "personal_value_description", "personal_visi_description", "passion_interest", "skills", "img_photo", "education_id", "program_study", "institution_name", "nationality", "gelar_depan", "gelar_belakang", "nomor_kk", "nomor_kpj_bpjs_tk", "nomor_bpjs_kes", "npwp", "user_id", "taptap_user_id", "entry_date", "out_date", "appointment_date", "start_date_contract", "end_date_contract", "is_mpp", "is_plt", "status_code", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_UNIQUE = [["email"], ["nik"], ["taptap_user_id"]];
    const FIELD_UPLOAD = ["img_photo"];
    const FIELD_TYPE = [
        "id" => "bigint",
        "fullname" => "character_varying",
        "nik" => "character_varying",
        "nip" => "character_varying",
        "job_position_id" => "bigint",
        "status_employment_id" => "bigint",
        "place_of_birth" => "character_varying",
        "date_of_birth" => "date",
        "gender" => "character_varying",
        "blood_type" => "character_varying",
        "marital_status" => "character_varying",
        "address" => "text",
        "religion_id" => "bigint",
        "email" => "character_varying",
        "telephone" => "character_varying",
        "emergency_contact" => "character_varying",
        "facebook_name" => "character_varying",
        "instagram_name" => "character_varying",
        "linkedin_name" => "character_varying",
        "twitter_name" => "character_varying",
        "personal_value_description" => "text",
        "personal_visi_description" => "text",
        "passion_interest" => "text",
        "skills" => "text",
        "img_photo" => "text",
        "education_id" => "bigint",
        "program_study" => "character_varying",
        "institution_name" => "character_varying",
        "nationality" => "character_varying",
        "gelar_depan" => "character_varying",
        "gelar_belakang" => "character_varying",
        "nomor_kk" => "character_varying",
        "nomor_kpj_bpjs_tk" => "character_varying",
        "nomor_bpjs_kes" => "character_varying",
        "npwp" => "character_varying",
        "user_id" => "bigint",
        "taptap_user_id" => "character_varying",
        "entry_date" => "date",
        "out_date" => "date",
        "appointment_date" => "date",
        "start_date_contract" => "date",
        "end_date_contract" => "date",
        "is_mpp" => "boolean",
        "is_plt" => "boolean",
        "status_code" => "character_varying",
        "created_by" => "bigint",
        "updated_by" => "bigint",
        "created_at" => "timestamp_with_time_zone",
        "updated_at" => "timestamp_with_time_zone",
    ];

    const FIELD_DEFAULT_VALUE = [
        "fullname" => "",
        "nik" => "",
        "nip" => "",
        "job_position_id" => "",
        "status_employment_id" => "",
        "place_of_birth" => "",
        "date_of_birth" => "",
        "gender" => "",
        "blood_type" => "",
        "marital_status" => "",
        "address" => "",
        "religion_id" => "",
        "email" => "",
        "telephone" => "",
        "emergency_contact" => "",
        "facebook_name" => "",
        "instagram_name" => "",
        "linkedin_name" => "",
        "twitter_name" => "",
        "personal_value_description" => "",
        "personal_visi_description" => "",
        "passion_interest" => "",
        "skills" => "",
        "img_photo" => "",
        "education_id" => "",
        "program_study" => "",
        "institution_name" => "",
        "nationality" => "",
        "gelar_depan" => "",
        "gelar_belakang" => "",
        "nomor_kk" => "",
        "nomor_kpj_bpjs_tk" => "",
        "nomor_bpjs_kes" => "",
        "npwp" => "",
        "user_id" => "",
        "taptap_user_id" => "",
        "entry_date" => "",
        "out_date" => "",
        "appointment_date" => "",
        "start_date_contract" => "",
        "end_date_contract" => "",
        "is_mpp" => "false",
        "is_plt" => "false",
        "status_code" => "",
        "created_by" => "",
        "updated_by" => "",
        "created_at" => "",
        "updated_at" => "",
    ];
    const FIELD_RELATION = [
        "job_position_id" => [
            "linkTable" => "job_positions",
            "aliasTable" => "B",
            "linkField" => "id",
            "displayName" => "rel_job_position_id",
            "selectFields" => ["job_position_name"],
            "selectValue" => "id AS rel_job_position_id"
        ],
        "status_employment_id" => [
            "linkTable" => "master_status_employments",
            "aliasTable" => "C",
            "linkField" => "id",
            "displayName" => "rel_status_employment_id",
            "selectFields" => ["name"],
            "selectValue" => "id AS rel_status_employment_id"
        ],
        "religion_id" => [
            "linkTable" => "master_religions",
            "aliasTable" => "D",
            "linkField" => "id",
            "displayName" => "rel_religion_id",
            "selectFields" => ["name"],
            "selectValue" => "id AS rel_religion_id"
        ],
        "education_id" => [
            "linkTable" => "master_educations",
            "aliasTable" => "E",
            "linkField" => "id",
            "displayName" => "rel_education_id",
            "selectFields" => ["name"],
            "selectValue" => "id AS rel_education_id"
        ],
        "user_id" => [
            "linkTable" => "users",
            "aliasTable" => "F",
            "linkField" => "id",
            "displayName" => "rel_user_id",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_user_id"
        ],
        "created_by" => [
            "linkTable" => "users",
            "aliasTable" => "G",
            "linkField" => "id",
            "displayName" => "rel_created_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_created_by"
        ],
        "updated_by" => [
            "linkTable" => "users",
            "aliasTable" => "H",
            "linkField" => "id",
            "displayName" => "rel_updated_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_updated_by"
        ],
    ];
    const CUSTOM_SELECT = "";
    const FIELD_VALIDATION = [
        "fullname" => "required|string|max:255",
        "nik" => "required|string|max:255",
        "nip" => "nullable|string|max:255",
        "job_position_id" => "nullable|integer",
        "status_employment_id" => "nullable|integer",
        "place_of_birth" => "nullable|string|max:255",
        "date_of_birth" => "required",
        "gender" => "nullable|string|max:255",
        "blood_type" => "nullable|string|max:255",
        "marital_status" => "nullable|string|max:255",
        "address" => "nullable|string",
        "religion_id" => "nullable|integer",
        "email" => "nullable|string|max:255",
        "telephone" => "nullable|string|max:255",
        "emergency_contact" => "nullable|string|max:255",
        "facebook_name" => "nullable|string|max:255",
        "instagram_name" => "nullable|string|max:255",
        "linkedin_name" => "nullable|string|max:255",
        "twitter_name" => "nullable|string|max:255",
        "personal_value_description" => "nullable|string",
        "personal_visi_description" => "nullable|string",
        "passion_interest" => "nullable|string",
        "skills" => "nullable|string",
        "img_photo" => "nullable|string|exists_file",
        "education_id" => "nullable|integer",
        "program_study" => "nullable|string|max:255",
        "institution_name" => "nullable|string|max:255",
        "nationality" => "nullable|string|max:255",
        "gelar_depan" => "nullable|string|max:255",
        "gelar_belakang" => "nullable|string|max:255",
        "nomor_kk" => "nullable|string|max:255",
        "nomor_kpj_bpjs_tk" => "nullable|string|max:255",
        "nomor_bpjs_kes" => "nullable|string|max:255",
        "npwp" => "nullable|string|max:255",
        "user_id" => "nullable|integer",
        "taptap_user_id" => "nullable|string|max:255",
        "entry_date" => "required",
        "out_date" => "nullable",
        "appointment_date" => "nullable",
        "start_date_contract" => "nullable",
        "end_date_contract" => "nullable",
        "is_mpp" => "nullable",
        "is_plt" => "nullable",
        "status_code" => "nullable|string|max:255",
        "created_by" => "nullable|integer",
        "updated_by" => "nullable|integer",
        "created_at" => "nullable|date",
        "updated_at" => "nullable|date",
    ];
    const PARENT_CHILD = [];
    // start custom
    const CUSTOM_LIST_FILTER = [];
    const FIELD_CASTING = [];
    const CHILD_TABLE = [
        //"child_table" => [
        //    "foreignField" => "field"
        //]
    ];

    public static function beforeInsert($input)
    {
        return $input;
    }

    public static function afterInsert($object, $input)
    {
        return $input;
    }
    
    public static function beforeUpdate($input)
    {
        return $input;
    }
    
    public static function afterUpdate($object, $input)
    {
        return $input;
    }
    
    public static function beforeDelete($input)
    {
        return $input;
    }

    public static function afterDelete($object, $input)
    {
        return $input;
    }// end custom
}
