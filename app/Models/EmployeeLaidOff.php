<?php 

namespace App\Models;

use App\CoreService\CallService;
use DateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class EmployeeLaidOff extends Model
{
    protected $table = 'employee_laid_off';
    protected $dateFormat = 'c';
    const TABLE = "employee_laid_off";
    const FILEROOT = "/employee_laid_off";
    const IS_LIST = true;
    const IS_ADD = true;
    const IS_EDIT = true;
    const IS_DELETE = true;
    const IS_VIEW = true;
    const FIELD_LIST = ["id", "employee_id", "date", "laid_off_type_id", "description", "file_document", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_ADD = ["employee_id", "date", "laid_off_type_id", "description", "file_document", "created_by", "updated_by"];
    const FIELD_EDIT = ["employee_id", "date", "laid_off_type_id", "description", "file_document", "updated_by"];
    const FIELD_VIEW = ["id", "employee_id", "date", "laid_off_type_id", "description", "file_document", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_READONLY = [];
    const FIELD_FILTERABLE = [
        "id" => [
            "operator" => "=",
        ],
        "employee_id" => [
            "operator" => "=",
        ],
        "date" => [
            "operator" => "=",
        ],
        "laid_off_type_id" => [
            "operator" => "=",
        ],
        "description" => [
            "operator" => "=",
        ],
        "file_document" => [
            "operator" => "=",
        ],
        "created_by" => [
            "operator" => "=",
        ],
        "updated_by" => [
            "operator" => "=",
        ],
        "created_at" => [
            "operator" => "=",
        ],
        "updated_at" => [
            "operator" => "=",
        ],
    ];
    const FIELD_SEARCHABLE = [];
    const FIELD_ARRAY = [];
    const FIELD_SORTABLE = ["id", "employee_id", "date", "laid_off_type_id", "description", "file_document", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_UNIQUE = [];
    const FIELD_UPLOAD = ["file_document"];
    const FIELD_TYPE = [
        "id" => "bigint",
        "employee_id" => "bigint",
        "date" => "date",
        "laid_off_type_id" => "bigint",
        "description" => "text",
        "file_document" => "text",
        "created_by" => "bigint",
        "updated_by" => "bigint",
        "created_at" => "timestamp_with_time_zone",
        "updated_at" => "timestamp_with_time_zone",
    ];

    const FIELD_DEFAULT_VALUE = [
        "employee_id" => "",
        "date" => "",
        "laid_off_type_id" => "",
        "description" => "",
        "file_document" => "",
        "created_by" => "",
        "updated_by" => "",
        "created_at" => "",
        "updated_at" => "",
    ];
    const FIELD_RELATION = [
        "employee_id" => [
            "linkTable" => "employees",
            "aliasTable" => "B",
            "linkField" => "id",
            "displayName" => "rel_employee_id",
            "selectFields" => ["fullname"],
            "selectValue" => "id AS rel_employee_id"
        ],
        "laid_off_type_id" => [
            "linkTable" => "master_laid_off_types",
            "aliasTable" => "C",
            "linkField" => "id",
            "displayName" => "rel_laid_off_type_id",
            "selectFields" => ["name"],
            "selectValue" => "id AS rel_laid_off_type_id"
        ],
        "created_by" => [
            "linkTable" => "users",
            "aliasTable" => "D",
            "linkField" => "id",
            "displayName" => "rel_created_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_created_by"
        ],
        "updated_by" => [
            "linkTable" => "users",
            "aliasTable" => "E",
            "linkField" => "id",
            "displayName" => "rel_updated_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_updated_by"
        ],
    ];
    const CUSTOM_SELECT = "";
    const FIELD_VALIDATION = [
        "employee_id" => "required|integer",
        "date" => "required",
        "laid_off_type_id" => "required|integer",
        "description" => "nullable|string",
        "file_document" => "nullable|string|exists_file",
        "created_by" => "nullable|integer",
        "updated_by" => "nullable|integer",
        "created_at" => "nullable|date",
        "updated_at" => "nullable|date",
    ];
    const PARENT_CHILD = [];
    // start custom
    const CUSTOM_LIST_FILTER = [];
    const FIELD_CASTING = [];
    const CHILD_TABLE = [
        //"child_table" => [
        //    "foreignField" => "field"
        //]
    ];

    public static function beforeInsert($input)
    {
        return $input;
    }

    public static function afterInsert($object, $input)
    {
        return $input;
    }
    
    public static function beforeUpdate($input)
    {
        return $input;
    }
    
    public static function afterUpdate($object, $input)
    {
        return $input;
    }
    
    public static function beforeDelete($input)
    {
        return $input;
    }

    public static function afterDelete($object, $input)
    {
        return $input;
    }// end custom
}
