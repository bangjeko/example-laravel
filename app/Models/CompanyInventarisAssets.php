<?php 

namespace App\Models;

use App\CoreService\CallService;
use DateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class CompanyInventarisAssets extends Model
{
    protected $table = 'company_inventaris_assets';
    protected $dateFormat = 'c';
    const TABLE = "company_inventaris_assets";
    const FILEROOT = "/company_inventaris_assets";
    const IS_LIST = true;
    const IS_ADD = true;
    const IS_EDIT = true;
    const IS_DELETE = true;
    const IS_VIEW = true;
    const FIELD_LIST = ["id", "inventaris_asset_type_id", "asset_name", "asset_code", "asset_qty", "asset_condition", "pic", "current_location", "date_purchased", "faktur_number", "serial_number", "price_purchased", "img_photo_asset", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_ADD = ["inventaris_asset_type_id", "asset_name", "asset_code", "asset_qty", "asset_condition", "pic", "current_location", "date_purchased", "faktur_number", "serial_number", "price_purchased", "img_photo_asset", "created_by", "updated_by"];
    const FIELD_EDIT = ["inventaris_asset_type_id", "asset_name", "asset_code", "asset_qty", "asset_condition", "pic", "current_location", "date_purchased", "faktur_number", "serial_number", "price_purchased", "img_photo_asset", "updated_by"];
    const FIELD_VIEW = ["id", "inventaris_asset_type_id", "asset_name", "asset_code", "asset_qty", "asset_condition", "pic", "current_location", "date_purchased", "faktur_number", "serial_number", "price_purchased", "img_photo_asset", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_READONLY = [];
    const FIELD_FILTERABLE = [
        "id" => [
            "operator" => "=",
        ],
        "inventaris_asset_type_id" => [
            "operator" => "=",
        ],
        "asset_name" => [
            "operator" => "=",
        ],
        "asset_code" => [
            "operator" => "=",
        ],
        "asset_qty" => [
            "operator" => "=",
        ],
        "asset_condition" => [
            "operator" => "=",
        ],
        "pic" => [
            "operator" => "=",
        ],
        "current_location" => [
            "operator" => "=",
        ],
        "date_purchased" => [
            "operator" => "=",
        ],
        "faktur_number" => [
            "operator" => "=",
        ],
        "serial_number" => [
            "operator" => "=",
        ],
        "price_purchased" => [
            "operator" => "=",
        ],
        "img_photo_asset" => [
            "operator" => "=",
        ],
        "created_by" => [
            "operator" => "=",
        ],
        "updated_by" => [
            "operator" => "=",
        ],
        "created_at" => [
            "operator" => "=",
        ],
        "updated_at" => [
            "operator" => "=",
        ],
    ];
    const FIELD_SEARCHABLE = ["asset_name", "asset_code", "pic", "faktur_number", "serial_number"];
    const FIELD_ARRAY = [];
    const FIELD_SORTABLE = ["id", "inventaris_asset_type_id", "asset_name", "asset_code", "asset_qty", "asset_condition", "pic", "current_location", "date_purchased", "faktur_number", "serial_number", "price_purchased", "img_photo_asset", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_UNIQUE = [];
    const FIELD_UPLOAD = ["img_photo_asset"];
    const FIELD_TYPE = [
        "id" => "bigint",
        "inventaris_asset_type_id" => "bigint",
        "asset_name" => "character_varying",
        "asset_code" => "character_varying",
        "asset_qty" => "integer",
        "asset_condition" => "text",
        "pic" => "character_varying",
        "current_location" => "text",
        "date_purchased" => "date",
        "faktur_number" => "character_varying",
        "serial_number" => "character_varying",
        "price_purchased" => "double_precision",
        "img_photo_asset" => "text",
        "created_by" => "bigint",
        "updated_by" => "bigint",
        "created_at" => "timestamp_with_time_zone",
        "updated_at" => "timestamp_with_time_zone",
    ];

    const FIELD_DEFAULT_VALUE = [
        "inventaris_asset_type_id" => "",
        "asset_name" => "",
        "asset_code" => "",
        "asset_qty" => "",
        "asset_condition" => "",
        "pic" => "",
        "current_location" => "",
        "date_purchased" => "",
        "faktur_number" => "",
        "serial_number" => "",
        "price_purchased" => "",
        "img_photo_asset" => "",
        "created_by" => "",
        "updated_by" => "",
        "created_at" => "",
        "updated_at" => "",
    ];
    const FIELD_RELATION = [
        "inventaris_asset_type_id" => [
            "linkTable" => "master_inventaris_asset_types",
            "aliasTable" => "B",
            "linkField" => "id",
            "displayName" => "rel_inventaris_asset_type_id",
            "selectFields" => ["name"],
            "selectValue" => "id AS rel_inventaris_asset_type_id"
        ],
        "created_by" => [
            "linkTable" => "users",
            "aliasTable" => "C",
            "linkField" => "id",
            "displayName" => "rel_created_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_created_by"
        ],
        "updated_by" => [
            "linkTable" => "users",
            "aliasTable" => "D",
            "linkField" => "id",
            "displayName" => "rel_updated_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_updated_by"
        ],
    ];
    const CUSTOM_SELECT = "";
    const FIELD_VALIDATION = [
        "inventaris_asset_type_id" => "nullable|integer",
        "asset_name" => "required|string|max:255",
        "asset_code" => "nullable|string|max:255",
        "asset_qty" => "nullable|integer",
        "asset_condition" => "nullable|string",
        "pic" => "nullable|string|max:255",
        "current_location" => "nullable|string",
        "date_purchased" => "nullable",
        "faktur_number" => "nullable|string|max:255",
        "serial_number" => "nullable|string|max:255",
        "price_purchased" => "nullable",
        "img_photo_asset" => "nullable|string|exists_file",
        "created_by" => "nullable|integer",
        "updated_by" => "nullable|integer",
        "created_at" => "nullable|date",
        "updated_at" => "nullable|date",
    ];
    const PARENT_CHILD = [];
    // start custom
    const CUSTOM_LIST_FILTER = [];
    const FIELD_CASTING = [];
    const CHILD_TABLE = [
        //"child_table" => [
        //    "foreignField" => "field"
        //]
    ];

    public static function beforeInsert($input)
    {
        return $input;
    }

    public static function afterInsert($object, $input)
    {
        return $input;
    }
    
    public static function beforeUpdate($input)
    {
        return $input;
    }
    
    public static function afterUpdate($object, $input)
    {
        return $input;
    }
    
    public static function beforeDelete($input)
    {
        return $input;
    }

    public static function afterDelete($object, $input)
    {
        return $input;
    }// end custom
}
