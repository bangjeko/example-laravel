<?php 

namespace App\Models;

use App\CoreService\CallService;
use DateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class EmployeeCertifications extends Model
{
    protected $table = 'employee_certifications';
    protected $dateFormat = 'c';
    const TABLE = "employee_certifications";
    const FILEROOT = "/employee_certifications";
    const IS_LIST = true;
    const IS_ADD = true;
    const IS_EDIT = true;
    const IS_DELETE = true;
    const IS_VIEW = true;
    const FIELD_LIST = ["id", "employee_id", "certification_name", "institution_name", "start_date", "end_date", "description", "file_filename", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_ADD = ["employee_id", "certification_name", "institution_name", "start_date", "end_date", "description", "file_filename", "created_by", "updated_by"];
    const FIELD_EDIT = ["employee_id", "certification_name", "institution_name", "start_date", "end_date", "description", "file_filename", "updated_by"];
    const FIELD_VIEW = ["id", "employee_id", "certification_name", "institution_name", "start_date", "end_date", "description", "file_filename", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_READONLY = [];
    const FIELD_FILTERABLE = [
        "id" => [
            "operator" => "=",
        ],
        "employee_id" => [
            "operator" => "=",
        ],
        "certification_name" => [
            "operator" => "=",
        ],
        "institution_name" => [
            "operator" => "=",
        ],
        "start_date" => [
            "operator" => "=",
        ],
        "end_date" => [
            "operator" => "=",
        ],
        "description" => [
            "operator" => "=",
        ],
        "file_filename" => [
            "operator" => "=",
        ],
        "created_by" => [
            "operator" => "=",
        ],
        "updated_by" => [
            "operator" => "=",
        ],
        "created_at" => [
            "operator" => "=",
        ],
        "updated_at" => [
            "operator" => "=",
        ],
    ];
    const FIELD_SEARCHABLE = ["certification_name", "institution_name"];
    const FIELD_ARRAY = [];
    const FIELD_SORTABLE = ["id", "employee_id", "certification_name", "institution_name", "start_date", "end_date", "description", "file_filename", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_UNIQUE = [];
    const FIELD_UPLOAD = ["file_filename"];
    const FIELD_TYPE = [
        "id" => "bigint",
        "employee_id" => "bigint",
        "certification_name" => "character_varying",
        "institution_name" => "character_varying",
        "start_date" => "date",
        "end_date" => "date",
        "description" => "text",
        "file_filename" => "text",
        "created_by" => "bigint",
        "updated_by" => "bigint",
        "created_at" => "timestamp_with_time_zone",
        "updated_at" => "timestamp_with_time_zone",
    ];

    const FIELD_DEFAULT_VALUE = [
        "employee_id" => "",
        "certification_name" => "",
        "institution_name" => "",
        "start_date" => "",
        "end_date" => "",
        "description" => "",
        "file_filename" => "",
        "created_by" => "",
        "updated_by" => "",
        "created_at" => "",
        "updated_at" => "",
    ];
    const FIELD_RELATION = [
        "employee_id" => [
            "linkTable" => "employees",
            "aliasTable" => "B",
            "linkField" => "id",
            "displayName" => "rel_employee_id",
            "selectFields" => ["fullname"],
            "selectValue" => "id AS rel_employee_id"
        ],
        "created_by" => [
            "linkTable" => "users",
            "aliasTable" => "C",
            "linkField" => "id",
            "displayName" => "rel_created_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_created_by"
        ],
        "updated_by" => [
            "linkTable" => "users",
            "aliasTable" => "D",
            "linkField" => "id",
            "displayName" => "rel_updated_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_updated_by"
        ],
    ];
    const CUSTOM_SELECT = "";
    const FIELD_VALIDATION = [
        "employee_id" => "required|integer",
        "certification_name" => "required|string|max:255",
        "institution_name" => "nullable|string|max:255",
        "start_date" => "required",
        "end_date" => "nullable",
        "description" => "nullable|string",
        "file_filename" => "nullable|string|exists_file",
        "created_by" => "nullable|integer",
        "updated_by" => "nullable|integer",
        "created_at" => "nullable|date",
        "updated_at" => "nullable|date",
    ];
    const PARENT_CHILD = [];
    // start custom
    const CUSTOM_LIST_FILTER = [];
    const FIELD_CASTING = [
        //"nama field" => "float",
    ];
    const CHILD_TABLE = [
        //"child_table" => [
        //    "foreignField" => "field"
        //]
    ];

    public static function beforeInsert($input)
    {
        return $input;
    }

    public static function afterInsert($object, $input)
    {
        return $input;
    }
    
    public static function beforeUpdate($input)
    {
        return $input;
    }
    
    public static function afterUpdate($object, $input)
    {
        return $input;
    }
    
    public static function beforeDelete($input)
    {
        return $input;
    }

    public static function afterDelete($object, $input)
    {
        return $input;
    }// end custom
}
