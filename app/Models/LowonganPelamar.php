<?php 

namespace App\Models;

use App\CoreService\CallService;
use DateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class LowonganPelamar extends Model
{
    protected $table = 'lowongan_pelamar';
    protected $dateFormat = 'c';
    const TABLE = "lowongan_pelamar";
    const FILEROOT = "/lowongan_pelamar";
    const IS_LIST = true;
    const IS_ADD = true;
    const IS_EDIT = true;
    const IS_DELETE = true;
    const IS_VIEW = true;
    const FIELD_LIST = ["id", "lowongan_id", "fullname", "nik", "place_of_birth", "date_of_birth", "gender", "marital_status", "address", "religion_id", "email", "telephone", "img_photo", "education_id", "program_study", "institution_name", "nationality", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_ADD = ["lowongan_id", "fullname", "nik", "place_of_birth", "date_of_birth", "gender", "marital_status", "address", "religion_id", "email", "telephone", "img_photo", "education_id", "program_study", "institution_name", "nationality", "created_by", "updated_by"];
    const FIELD_EDIT = ["lowongan_id", "fullname", "nik", "place_of_birth", "date_of_birth", "gender", "marital_status", "address", "religion_id", "email", "telephone", "img_photo", "education_id", "program_study", "institution_name", "nationality", "updated_by"];
    const FIELD_VIEW = ["id", "lowongan_id", "fullname", "nik", "place_of_birth", "date_of_birth", "gender", "marital_status", "address", "religion_id", "email", "telephone", "img_photo", "education_id", "program_study", "institution_name", "nationality", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_READONLY = [];
    const FIELD_FILTERABLE = [
        "id" => [
            "operator" => "=",
        ],
        "lowongan_id" => [
            "operator" => "=",
        ],
        "fullname" => [
            "operator" => "=",
        ],
        "nik" => [
            "operator" => "=",
        ],
        "place_of_birth" => [
            "operator" => "=",
        ],
        "date_of_birth" => [
            "operator" => "=",
        ],
        "gender" => [
            "operator" => "=",
        ],
        "marital_status" => [
            "operator" => "=",
        ],
        "address" => [
            "operator" => "=",
        ],
        "religion_id" => [
            "operator" => "=",
        ],
        "email" => [
            "operator" => "=",
        ],
        "telephone" => [
            "operator" => "=",
        ],
        "img_photo" => [
            "operator" => "=",
        ],
        "education_id" => [
            "operator" => "=",
        ],
        "program_study" => [
            "operator" => "=",
        ],
        "institution_name" => [
            "operator" => "=",
        ],
        "nationality" => [
            "operator" => "=",
        ],
        "created_by" => [
            "operator" => "=",
        ],
        "updated_by" => [
            "operator" => "=",
        ],
        "created_at" => [
            "operator" => "=",
        ],
        "updated_at" => [
            "operator" => "=",
        ],
    ];
    const FIELD_SEARCHABLE = ["fullname", "nik", "place_of_birth", "gender", "marital_status", "email", "telephone", "program_study", "institution_name", "nationality"];
    const FIELD_ARRAY = [];
    const FIELD_SORTABLE = ["id", "lowongan_id", "fullname", "nik", "place_of_birth", "date_of_birth", "gender", "marital_status", "address", "religion_id", "email", "telephone", "img_photo", "education_id", "program_study", "institution_name", "nationality", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_UNIQUE = [["lowongan_id", "nik"]];
    const FIELD_UPLOAD = ["img_photo"];
    const FIELD_TYPE = [
        "id" => "bigint",
        "lowongan_id" => "bigint",
        "fullname" => "character_varying",
        "nik" => "character_varying",
        "place_of_birth" => "character_varying",
        "date_of_birth" => "date",
        "gender" => "character_varying",
        "marital_status" => "character_varying",
        "address" => "text",
        "religion_id" => "bigint",
        "email" => "character_varying",
        "telephone" => "character_varying",
        "img_photo" => "text",
        "education_id" => "bigint",
        "program_study" => "character_varying",
        "institution_name" => "character_varying",
        "nationality" => "character_varying",
        "created_by" => "bigint",
        "updated_by" => "bigint",
        "created_at" => "timestamp_with_time_zone",
        "updated_at" => "timestamp_with_time_zone",
    ];

    const FIELD_DEFAULT_VALUE = [
        "lowongan_id" => "",
        "fullname" => "",
        "nik" => "",
        "place_of_birth" => "",
        "date_of_birth" => "",
        "gender" => "",
        "marital_status" => "",
        "address" => "",
        "religion_id" => "",
        "email" => "",
        "telephone" => "",
        "img_photo" => "",
        "education_id" => "",
        "program_study" => "",
        "institution_name" => "",
        "nationality" => "",
        "created_by" => "",
        "updated_by" => "",
        "created_at" => "",
        "updated_at" => "",
    ];
    const FIELD_RELATION = [
        "lowongan_id" => [
            "linkTable" => "lowongan",
            "aliasTable" => "B",
            "linkField" => "id",
            "displayName" => "rel_lowongan_id",
            "selectFields" => ["lowongan_title"],
            "selectValue" => "id AS rel_lowongan_id"
        ],
        "religion_id" => [
            "linkTable" => "master_religions",
            "aliasTable" => "C",
            "linkField" => "id",
            "displayName" => "rel_religion_id",
            "selectFields" => ["name"],
            "selectValue" => "id AS rel_religion_id"
        ],
        "education_id" => [
            "linkTable" => "master_educations",
            "aliasTable" => "D",
            "linkField" => "id",
            "displayName" => "rel_education_id",
            "selectFields" => ["name"],
            "selectValue" => "id AS rel_education_id"
        ],
        "created_by" => [
            "linkTable" => "users",
            "aliasTable" => "E",
            "linkField" => "id",
            "displayName" => "rel_created_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_created_by"
        ],
        "updated_by" => [
            "linkTable" => "users",
            "aliasTable" => "F",
            "linkField" => "id",
            "displayName" => "rel_updated_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_updated_by"
        ],
    ];
    const CUSTOM_SELECT = "";
    const FIELD_VALIDATION = [
        "lowongan_id" => "nullable|integer",
        "fullname" => "required|string|max:255",
        "nik" => "required|string|max:255",
        "place_of_birth" => "nullable|string|max:255",
        "date_of_birth" => "required",
        "gender" => "nullable|string|max:255",
        "marital_status" => "nullable|string|max:255",
        "address" => "nullable|string",
        "religion_id" => "nullable|integer",
        "email" => "required|string|max:255",
        "telephone" => "required|string|max:255",
        "img_photo" => "nullable|string|exists_file",
        "education_id" => "nullable|integer",
        "program_study" => "nullable|string|max:255",
        "institution_name" => "nullable|string|max:255",
        "nationality" => "nullable|string|max:255",
        "created_by" => "nullable|integer",
        "updated_by" => "nullable|integer",
        "created_at" => "nullable|date",
        "updated_at" => "nullable|date",
    ];
    const PARENT_CHILD = [];
    // start custom
    const CUSTOM_LIST_FILTER = [];
    const FIELD_CASTING = [
        //"nama field" => "float",
    ];
    const CHILD_TABLE = [
        //"child_table" => [
        //    "foreignField" => "field"
        //]
    ];

    public static function beforeInsert($input)
    {
        return $input;
    }

    public static function afterInsert($object, $input)
    {
        return $input;
    }
    
    public static function beforeUpdate($input)
    {
        return $input;
    }
    
    public static function afterUpdate($object, $input)
    {
        return $input;
    }
    
    public static function beforeDelete($input)
    {
        return $input;
    }

    public static function afterDelete($object, $input)
    {
        return $input;
    }// end custom
}
