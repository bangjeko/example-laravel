<?php 

namespace App\Models;

use App\CoreService\CallService;
use DateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class Events extends Model
{
    protected $table = 'events';
    protected $dateFormat = 'c';
    const TABLE = "events";
    const FILEROOT = "/events";
    const IS_LIST = true;
    const IS_ADD = true;
    const IS_EDIT = true;
    const IS_DELETE = true;
    const IS_VIEW = true;
    const FIELD_LIST = ["id", "title", "start_time", "end_time", "place_of_event", "description", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_ADD = ["title", "start_time", "end_time", "place_of_event", "description", "created_by", "updated_by"];
    const FIELD_EDIT = ["title", "start_time", "end_time", "place_of_event", "description", "updated_by"];
    const FIELD_VIEW = ["id", "title", "start_time", "end_time", "place_of_event", "description", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_READONLY = [];
    const FIELD_FILTERABLE = [
        "id" => [
            "operator" => "=",
        ],
        "title" => [
            "operator" => "=",
        ],
        "start_time" => [
            "operator" => "=",
        ],
        "end_time" => [
            "operator" => "=",
        ],
        "place_of_event" => [
            "operator" => "=",
        ],
        "description" => [
            "operator" => "=",
        ],
        "created_by" => [
            "operator" => "=",
        ],
        "updated_by" => [
            "operator" => "=",
        ],
        "created_at" => [
            "operator" => "=",
        ],
        "updated_at" => [
            "operator" => "=",
        ],
    ];
    const FIELD_SEARCHABLE = ["title", "place_of_event"];
    const FIELD_ARRAY = [];
    const FIELD_SORTABLE = ["id", "title", "start_time", "end_time", "place_of_event", "description", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_UNIQUE = [];
    const FIELD_UPLOAD = [];
    const FIELD_TYPE = [
        "id" => "bigint",
        "title" => "character_varying",
        "start_time" => "timestamp_with_time_zone",
        "end_time" => "timestamp_with_time_zone",
        "place_of_event" => "character_varying",
        "description" => "text",
        "created_by" => "bigint",
        "updated_by" => "bigint",
        "created_at" => "timestamp_with_time_zone",
        "updated_at" => "timestamp_with_time_zone",
    ];

    const FIELD_DEFAULT_VALUE = [
        "title" => "",
        "start_time" => "",
        "end_time" => "",
        "place_of_event" => "",
        "description" => "",
        "created_by" => "",
        "updated_by" => "",
        "created_at" => "",
        "updated_at" => "",
    ];
    const FIELD_RELATION = [
        "created_by" => [
            "linkTable" => "users",
            "aliasTable" => "B",
            "linkField" => "id",
            "displayName" => "rel_created_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_created_by"
        ],
        "updated_by" => [
            "linkTable" => "users",
            "aliasTable" => "C",
            "linkField" => "id",
            "displayName" => "rel_updated_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_updated_by"
        ],
    ];
    const CUSTOM_SELECT = "";
    const FIELD_VALIDATION = [
        "title" => "required|string|max:255",
        "start_time" => "required|date",
        "end_time" => "required|date",
        "place_of_event" => "required|string|max:255",
        "description" => "required|string",
        "created_by" => "nullable|integer",
        "updated_by" => "nullable|integer",
        "created_at" => "nullable|date",
        "updated_at" => "nullable|date",
    ];
    const PARENT_CHILD = [];
    // start custom
    const CUSTOM_LIST_FILTER = [];
    const FIELD_CASTING = [];
    const CHILD_TABLE = [
        "event_documentations" => [
            "foreignField" => "event_id"
        ]
    ];

    public static function beforeInsert($input)
    {
        return $input;
    }

    public static function afterInsert($object, $input)
    {
        return $input;
    }

    public static function beforeUpdate($input)
    {
        return $input;
    }

    public static function afterUpdate($object, $input)
    {
        return $input;
    }

    public static function beforeDelete($input)
    {
        return $input;
    }

    public static function afterDelete($object, $input)
    {
        return $input;
    } // end custom
}
