<?php 

namespace App\Models;

use App\CoreService\CallService;
use DateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class Lowongan extends Model
{
    protected $table = 'lowongan';
    protected $dateFormat = 'c';
    const TABLE = "lowongan";
    const FILEROOT = "/lowongan";
    const IS_LIST = true;
    const IS_ADD = true;
    const IS_EDIT = true;
    const IS_DELETE = true;
    const IS_VIEW = true;
    const FIELD_LIST = ["id", "lowongan_code", "lowongan_title", "job_position_id", "status_employment_id", "position_count", "description", "img_poster", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_ADD = ["lowongan_code", "lowongan_title", "job_position_id", "status_employment_id", "position_count", "description", "img_poster", "created_by", "updated_by"];
    const FIELD_EDIT = ["lowongan_code", "lowongan_title", "job_position_id", "status_employment_id", "position_count", "description", "img_poster", "updated_by"];
    const FIELD_VIEW = ["id", "lowongan_code", "lowongan_title", "job_position_id", "status_employment_id", "position_count", "description", "img_poster", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_READONLY = [];
    const FIELD_FILTERABLE = [
        "id" => [
            "operator" => "=",
        ],
        "lowongan_code" => [
            "operator" => "=",
        ],
        "lowongan_title" => [
            "operator" => "=",
        ],
        "job_position_id" => [
            "operator" => "=",
        ],
        "status_employment_id" => [
            "operator" => "=",
        ],
        "position_count" => [
            "operator" => "=",
        ],
        "description" => [
            "operator" => "=",
        ],
        "img_poster" => [
            "operator" => "=",
        ],
        "created_by" => [
            "operator" => "=",
        ],
        "updated_by" => [
            "operator" => "=",
        ],
        "created_at" => [
            "operator" => "=",
        ],
        "updated_at" => [
            "operator" => "=",
        ],
    ];
    const FIELD_SEARCHABLE = ["lowongan_code", "lowongan_title"];
    const FIELD_ARRAY = [];
    const FIELD_SORTABLE = ["id", "lowongan_code", "lowongan_title", "job_position_id", "status_employment_id", "position_count", "description", "img_poster", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_UNIQUE = [["lowongan_code"]];
    const FIELD_UPLOAD = ["img_poster"];
    const FIELD_TYPE = [
        "id" => "bigint",
        "lowongan_code" => "character_varying",
        "lowongan_title" => "character_varying",
        "job_position_id" => "bigint",
        "status_employment_id" => "bigint",
        "position_count" => "integer",
        "description" => "text",
        "img_poster" => "text",
        "created_by" => "bigint",
        "updated_by" => "bigint",
        "created_at" => "timestamp_with_time_zone",
        "updated_at" => "timestamp_with_time_zone",
    ];

    const FIELD_DEFAULT_VALUE = [
        "lowongan_code" => "",
        "lowongan_title" => "",
        "job_position_id" => "",
        "status_employment_id" => "",
        "position_count" => "1",
        "description" => "",
        "img_poster" => "",
        "created_by" => "",
        "updated_by" => "",
        "created_at" => "",
        "updated_at" => "",
    ];
    const FIELD_RELATION = [
        "job_position_id" => [
            "linkTable" => "job_positions",
            "aliasTable" => "B",
            "linkField" => "id",
            "displayName" => "rel_job_position_id",
            "selectFields" => ["job_position_name"],
            "selectValue" => "id AS rel_job_position_id"
        ],
        "status_employment_id" => [
            "linkTable" => "master_status_employments",
            "aliasTable" => "C",
            "linkField" => "id",
            "displayName" => "rel_status_employment_id",
            "selectFields" => ["name"],
            "selectValue" => "id AS rel_status_employment_id"
        ],
        "created_by" => [
            "linkTable" => "users",
            "aliasTable" => "D",
            "linkField" => "id",
            "displayName" => "rel_created_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_created_by"
        ],
        "updated_by" => [
            "linkTable" => "users",
            "aliasTable" => "E",
            "linkField" => "id",
            "displayName" => "rel_updated_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_updated_by"
        ],
    ];
    const CUSTOM_SELECT = "";
    const FIELD_VALIDATION = [
        "lowongan_code" => "required|string|max:255",
        "lowongan_title" => "required|string|max:255",
        "job_position_id" => "required|integer",
        "status_employment_id" => "nullable|integer",
        "position_count" => "required|integer",
        "description" => "nullable|string",
        "img_poster" => "nullable|string|exists_file",
        "created_by" => "nullable|integer",
        "updated_by" => "nullable|integer",
        "created_at" => "nullable|date",
        "updated_at" => "nullable|date",
    ];
    const PARENT_CHILD = [];
    // start custom
    const CUSTOM_LIST_FILTER = [];
    const FIELD_CASTING = [
        //"nama field" => "float",
    ];
    const CHILD_TABLE = [
        //"child_table" => [
        //    "foreignField" => "field"
        //]
    ];

    public static function beforeInsert($input)
    {
        return $input;
    }

    public static function afterInsert($object, $input)
    {
        return $input;
    }
    
    public static function beforeUpdate($input)
    {
        return $input;
    }
    
    public static function afterUpdate($object, $input)
    {
        return $input;
    }
    
    public static function beforeDelete($input)
    {
        return $input;
    }

    public static function afterDelete($object, $input)
    {
        return $input;
    }// end custom
}
