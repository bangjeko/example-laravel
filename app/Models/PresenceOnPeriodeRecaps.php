<?php 

namespace App\Models;

use App\CoreService\CallService;
use DateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class PresenceOnPeriodeRecaps extends Model
{
    protected $table = 'presence_on_periode_recaps';
    protected $dateFormat = 'c';
    const TABLE = "presence_on_periode_recaps";
    const FILEROOT = "/presence_on_periode_recaps";
    const IS_LIST = true;
    const IS_ADD = true;
    const IS_EDIT = true;
    const IS_DELETE = true;
    const IS_VIEW = true;
    const FIELD_LIST = ["id", "presence_on_periode_id", "employee_id", "work_day_count", "work_day_lost_count", "work_day_late_count", "work_day_minute_late_count", "description", "is_lock", "status_code", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_ADD = ["presence_on_periode_id", "employee_id", "work_day_count", "work_day_lost_count", "work_day_late_count", "work_day_minute_late_count", "description", "is_lock", "status_code", "created_by", "updated_by"];
    const FIELD_EDIT = ["presence_on_periode_id", "employee_id", "work_day_count", "work_day_lost_count", "work_day_late_count", "work_day_minute_late_count", "description", "is_lock", "status_code", "updated_by"];
    const FIELD_VIEW = ["id", "presence_on_periode_id", "employee_id", "work_day_count", "work_day_lost_count", "work_day_late_count", "work_day_minute_late_count", "description", "is_lock", "status_code", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_READONLY = [];
    const FIELD_FILTERABLE = [
        "id" => [
            "operator" => "=",
        ],
        "presence_on_periode_id" => [
            "operator" => "=",
        ],
        "employee_id" => [
            "operator" => "=",
        ],
        "work_day_count" => [
            "operator" => "=",
        ],
        "work_day_lost_count" => [
            "operator" => "=",
        ],
        "work_day_late_count" => [
            "operator" => "=",
        ],
        "work_day_minute_late_count" => [
            "operator" => "=",
        ],
        "description" => [
            "operator" => "=",
        ],
        "is_lock" => [
            "operator" => "=",
        ],
        "status_code" => [
            "operator" => "=",
        ],
        "created_by" => [
            "operator" => "=",
        ],
        "updated_by" => [
            "operator" => "=",
        ],
        "created_at" => [
            "operator" => "=",
        ],
        "updated_at" => [
            "operator" => "=",
        ],
    ];
    const FIELD_SEARCHABLE = [];
    const FIELD_ARRAY = [];
    const FIELD_SORTABLE = ["id", "presence_on_periode_id", "employee_id", "work_day_count", "work_day_lost_count", "work_day_late_count", "work_day_minute_late_count", "description", "is_lock", "status_code", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_UNIQUE = [["presence_on_periode_id", "employee_id"]];
    const FIELD_UPLOAD = [];
    const FIELD_TYPE = [
        "id" => "bigint",
        "presence_on_periode_id" => "bigint",
        "employee_id" => "bigint",
        "work_day_count" => "integer",
        "work_day_lost_count" => "integer",
        "work_day_late_count" => "integer",
        "work_day_minute_late_count" => "integer",
        "description" => "text",
        "is_lock" => "boolean",
        "status_code" => "character_varying",
        "created_by" => "bigint",
        "updated_by" => "bigint",
        "created_at" => "timestamp_with_time_zone",
        "updated_at" => "timestamp_with_time_zone",
    ];

    const FIELD_DEFAULT_VALUE = [
        "presence_on_periode_id" => "",
        "employee_id" => "",
        "work_day_count" => "0",
        "work_day_lost_count" => "0",
        "work_day_late_count" => "0",
        "work_day_minute_late_count" => "0",
        "description" => "",
        "is_lock" => "false",
        "status_code" => "&#039;not_synchronized_yet&#039;::character varying",
        "created_by" => "",
        "updated_by" => "",
        "created_at" => "",
        "updated_at" => "",
    ];
    const FIELD_RELATION = [
        "presence_on_periode_id" => [
            "linkTable" => "presence_on_periodes",
            "aliasTable" => "B",
            "linkField" => "id",
            "displayName" => "rel_presence_on_periode_id",
            "selectFields" => ["id"],
            "selectValue" => "id AS rel_presence_on_periode_id"
        ],
        "employee_id" => [
            "linkTable" => "employees",
            "aliasTable" => "C",
            "linkField" => "id",
            "displayName" => "rel_employee_id",
            "selectFields" => ["fullname"],
            "selectValue" => "id AS rel_employee_id"
        ],
        "created_by" => [
            "linkTable" => "users",
            "aliasTable" => "D",
            "linkField" => "id",
            "displayName" => "rel_created_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_created_by"
        ],
        "updated_by" => [
            "linkTable" => "users",
            "aliasTable" => "E",
            "linkField" => "id",
            "displayName" => "rel_updated_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_updated_by"
        ],
    ];
    const CUSTOM_SELECT = "";
    const FIELD_VALIDATION = [
        "presence_on_periode_id" => "required|integer",
        "employee_id" => "required|integer",
        "work_day_count" => "nullable|integer",
        "work_day_lost_count" => "nullable|integer",
        "work_day_late_count" => "nullable|integer",
        "work_day_minute_late_count" => "nullable|integer",
        "description" => "nullable|string",
        "is_lock" => "nullable",
        "status_code" => "nullable|string|max:255",
        "created_by" => "nullable|integer",
        "updated_by" => "nullable|integer",
        "created_at" => "nullable|date",
        "updated_at" => "nullable|date",
    ];
    const PARENT_CHILD = [];
    // start custom
    const CUSTOM_LIST_FILTER = [];
    const FIELD_CASTING = [
        //"nama field" => "float",
    ];
    const CHILD_TABLE = [
        //"child_table" => [
        //    "foreignField" => "field"
        //]
    ];

    public static function beforeInsert($input)
    {
        return $input;
    }

    public static function afterInsert($object, $input)
    {
        return $input;
    }
    
    public static function beforeUpdate($input)
    {
        return $input;
    }
    
    public static function afterUpdate($object, $input)
    {
        return $input;
    }
    
    public static function beforeDelete($input)
    {
        return $input;
    }

    public static function afterDelete($object, $input)
    {
        return $input;
    }// end custom
}
