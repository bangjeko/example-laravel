<?php 

namespace App\Models;

use App\CoreService\CallService;
use DateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class LemburOnPeriodeRecaps extends Model
{
    protected $table = 'lembur_on_periode_recaps';
    protected $dateFormat = 'c';
    const TABLE = "lembur_on_periode_recaps";
    const FILEROOT = "/lembur_on_periode_recaps";
    const IS_LIST = true;
    const IS_ADD = true;
    const IS_EDIT = true;
    const IS_DELETE = true;
    const IS_VIEW = true;
    const FIELD_LIST = ["id", "lembur_on_periode_id", "employee_id", "date", "type", "lembur_description", "start_time", "end_time", "duration", "data_source", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_ADD = ["lembur_on_periode_id", "employee_id", "date", "type", "lembur_description", "start_time", "end_time", "duration", "data_source", "created_by", "updated_by"];
    const FIELD_EDIT = ["lembur_on_periode_id", "employee_id", "date", "type", "lembur_description", "start_time", "end_time", "duration", "data_source", "updated_by"];
    const FIELD_VIEW = ["id", "lembur_on_periode_id", "employee_id", "date", "type", "lembur_description", "start_time", "end_time", "duration", "data_source", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_READONLY = [];
    const FIELD_FILTERABLE = [
        "id" => [
            "operator" => "=",
        ],
        "lembur_on_periode_id" => [
            "operator" => "=",
        ],
        "employee_id" => [
            "operator" => "=",
        ],
        "date" => [
            "operator" => "=",
        ],
        "type" => [
            "operator" => "=",
        ],
        "lembur_description" => [
            "operator" => "=",
        ],
        "start_time" => [
            "operator" => "=",
        ],
        "end_time" => [
            "operator" => "=",
        ],
        "duration" => [
            "operator" => "=",
        ],
        "data_source" => [
            "operator" => "=",
        ],
        "created_by" => [
            "operator" => "=",
        ],
        "updated_by" => [
            "operator" => "=",
        ],
        "created_at" => [
            "operator" => "=",
        ],
        "updated_at" => [
            "operator" => "=",
        ],
    ];
    const FIELD_SEARCHABLE = ["type", "data_source"];
    const FIELD_ARRAY = [];
    const FIELD_SORTABLE = ["id", "lembur_on_periode_id", "employee_id", "date", "type", "lembur_description", "start_time", "end_time", "duration", "data_source", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_UNIQUE = [];
    const FIELD_UPLOAD = [];
    const FIELD_TYPE = [
        "id" => "bigint",
        "lembur_on_periode_id" => "bigint",
        "employee_id" => "bigint",
        "date" => "date",
        "type" => "character_varying",
        "lembur_description" => "text",
        "start_time" => "timestamp_with_time_zone",
        "end_time" => "timestamp_with_time_zone",
        "duration" => "double_precision",
        "data_source" => "character_varying",
        "created_by" => "bigint",
        "updated_by" => "bigint",
        "created_at" => "timestamp_with_time_zone",
        "updated_at" => "timestamp_with_time_zone",
    ];

    const FIELD_DEFAULT_VALUE = [
        "lembur_on_periode_id" => "",
        "employee_id" => "",
        "date" => "",
        "type" => "",
        "lembur_description" => "&#039;0&#039;::text",
        "start_time" => "",
        "end_time" => "",
        "duration" => "&#039;0&#039;::double precision",
        "data_source" => "&#039;hrm&#039;::character varying",
        "created_by" => "",
        "updated_by" => "",
        "created_at" => "",
        "updated_at" => "",
    ];
    const FIELD_RELATION = [
        "lembur_on_periode_id" => [
            "linkTable" => "lembur_on_periodes",
            "aliasTable" => "B",
            "linkField" => "id",
            "displayName" => "rel_lembur_on_periode_id",
            "selectFields" => ["id"],
            "selectValue" => "id AS rel_lembur_on_periode_id"
        ],
        "employee_id" => [
            "linkTable" => "employees",
            "aliasTable" => "C",
            "linkField" => "id",
            "displayName" => "rel_employee_id",
            "selectFields" => ["fullname"],
            "selectValue" => "id AS rel_employee_id"
        ],
        "created_by" => [
            "linkTable" => "users",
            "aliasTable" => "D",
            "linkField" => "id",
            "displayName" => "rel_created_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_created_by"
        ],
        "updated_by" => [
            "linkTable" => "users",
            "aliasTable" => "E",
            "linkField" => "id",
            "displayName" => "rel_updated_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_updated_by"
        ],
    ];
    const CUSTOM_SELECT = "";
    const FIELD_VALIDATION = [
        "lembur_on_periode_id" => "required|integer",
        "employee_id" => "required|integer",
        "date" => "required",
        "type" => "required|string|max:255",
        "lembur_description" => "nullable|string",
        "start_time" => "nullable|date",
        "end_time" => "nullable|date",
        "duration" => "nullable",
        "data_source" => "nullable|string|max:255",
        "created_by" => "nullable|integer",
        "updated_by" => "nullable|integer",
        "created_at" => "nullable|date",
        "updated_at" => "nullable|date",
    ];
    const PARENT_CHILD = [];
    // start custom
    const CUSTOM_LIST_FILTER = [];
    const FIELD_CASTING = [
        //"nama field" => "float",
    ];
    const CHILD_TABLE = [
        //"child_table" => [
        //    "foreignField" => "field"
        //]
    ];

    public static function beforeInsert($input)
    {
        return $input;
    }

    public static function afterInsert($object, $input)
    {
        return $input;
    }
    
    public static function beforeUpdate($input)
    {
        return $input;
    }
    
    public static function afterUpdate($object, $input)
    {
        return $input;
    }
    
    public static function beforeDelete($input)
    {
        return $input;
    }

    public static function afterDelete($object, $input)
    {
        return $input;
    }// end custom
}
