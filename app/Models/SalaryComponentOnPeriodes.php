<?php 

namespace App\Models;

use App\CoreService\CallService;
use DateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class SalaryComponentOnPeriodes extends Model
{
    protected $table = 'salary_component_on_periodes';
    protected $dateFormat = 'c';
    const TABLE = "salary_component_on_periodes";
    const FILEROOT = "/salary_component_on_periodes";
    const IS_LIST = true;
    const IS_ADD = true;
    const IS_EDIT = true;
    const IS_DELETE = true;
    const IS_VIEW = true;
    const FIELD_LIST = ["id", "employee_on_periode_id", "salary_component_id", "salary_component_value", "fina_salary_account_id", "value_indicator", "active", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_ADD = ["employee_on_periode_id", "salary_component_id", "salary_component_value", "fina_salary_account_id", "value_indicator", "active", "created_by", "updated_by"];
    const FIELD_EDIT = ["employee_on_periode_id", "salary_component_id", "salary_component_value", "fina_salary_account_id", "value_indicator", "active", "updated_by"];
    const FIELD_VIEW = ["id", "employee_on_periode_id", "salary_component_id", "salary_component_value", "fina_salary_account_id", "value_indicator", "active", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_READONLY = [];
    const FIELD_FILTERABLE = [
        "id" => [
            "operator" => "=",
        ],
        "employee_on_periode_id" => [
            "operator" => "=",
        ],
        "salary_component_id" => [
            "operator" => "=",
        ],
        "salary_component_value" => [
            "operator" => "=",
        ],
        "fina_salary_account_id" => [
            "operator" => "=",
        ],
        "value_indicator" => [
            "operator" => "=",
        ],
        "active" => [
            "operator" => "=",
        ],
        "created_by" => [
            "operator" => "=",
        ],
        "updated_by" => [
            "operator" => "=",
        ],
        "created_at" => [
            "operator" => "=",
        ],
        "updated_at" => [
            "operator" => "=",
        ],
    ];
    const FIELD_SEARCHABLE = ["value_indicator"];
    const FIELD_ARRAY = [];
    const FIELD_SORTABLE = ["id", "employee_on_periode_id", "salary_component_id", "salary_component_value", "fina_salary_account_id", "value_indicator", "active", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_UNIQUE = [["employee_on_periode_id", "salary_component_id"]];
    const FIELD_UPLOAD = [];
    const FIELD_TYPE = [
        "id" => "bigint",
        "employee_on_periode_id" => "bigint",
        "salary_component_id" => "bigint",
        "salary_component_value" => "double_precision",
        "fina_salary_account_id" => "bigint",
        "value_indicator" => "character_varying",
        "active" => "integer",
        "created_by" => "bigint",
        "updated_by" => "bigint",
        "created_at" => "timestamp_with_time_zone",
        "updated_at" => "timestamp_with_time_zone",
    ];

    const FIELD_DEFAULT_VALUE = [
        "employee_on_periode_id" => "",
        "salary_component_id" => "",
        "salary_component_value" => "&#039;0&#039;::double precision",
        "fina_salary_account_id" => "",
        "value_indicator" => "",
        "active" => "1",
        "created_by" => "",
        "updated_by" => "",
        "created_at" => "",
        "updated_at" => "",
    ];
    const FIELD_RELATION = [
        "employee_on_periode_id" => [
            "linkTable" => "employee_on_periodes",
            "aliasTable" => "B",
            "linkField" => "id",
            "displayName" => "rel_employee_on_periode_id",
            "selectFields" => ["id"],
            "selectValue" => "id AS rel_employee_on_periode_id"
        ],
        "salary_component_id" => [
            "linkTable" => "master_salary_components",
            "aliasTable" => "C",
            "linkField" => "id",
            "displayName" => "rel_salary_component_id",
            "selectFields" => ["name"],
            "selectValue" => "id AS rel_salary_component_id"
        ],
        "fina_salary_account_id" => [
            "linkTable" => "fina_salary_accounts",
            "aliasTable" => "D",
            "linkField" => "id",
            "displayName" => "rel_fina_salary_account_id",
            "selectFields" => ["account_code", "account_name"],
            "selectValue" => "id AS rel_fina_salary_account_id"
        ],
        "created_by" => [
            "linkTable" => "users",
            "aliasTable" => "E",
            "linkField" => "id",
            "displayName" => "rel_created_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_created_by"
        ],
        "updated_by" => [
            "linkTable" => "users",
            "aliasTable" => "F",
            "linkField" => "id",
            "displayName" => "rel_updated_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_updated_by"
        ],
    ];
    const CUSTOM_SELECT = "";
    const FIELD_VALIDATION = [
        "employee_on_periode_id" => "required|integer",
        "salary_component_id" => "required|integer",
        "salary_component_value" => "nullable",
        "fina_salary_account_id" => "nullable|integer",
        "value_indicator" => "nullable|string|max:255",
        "active" => "nullable|integer",
        "created_by" => "nullable|integer",
        "updated_by" => "nullable|integer",
        "created_at" => "nullable|date",
        "updated_at" => "nullable|date",
    ];
    const PARENT_CHILD = [];
    // start custom
    const CUSTOM_LIST_FILTER = [];
    const FIELD_CASTING = [
        //"nama field" => "float",
    ];
    const CHILD_TABLE = [
        //"child_table" => [
        //    "foreignField" => "field"
        //]
    ];

    public static function beforeInsert($input)
    {
        return $input;
    }

    public static function afterInsert($object, $input)
    {
        return $input;
    }
    
    public static function beforeUpdate($input)
    {
        return $input;
    }
    
    public static function afterUpdate($object, $input)
    {
        return $input;
    }
    
    public static function beforeDelete($input)
    {
        return $input;
    }

    public static function afterDelete($object, $input)
    {
        return $input;
    }// end custom
}
