<?php 

namespace App\Models;

use App\CoreService\CallService;
use DateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class EmployeeSanksi extends Model
{
    protected $table = 'employee_sanksi';
    protected $dateFormat = 'c';
    const TABLE = "employee_sanksi";
    const FILEROOT = "/employee_sanksi";
    const IS_LIST = true;
    const IS_ADD = true;
    const IS_EDIT = true;
    const IS_DELETE = true;
    const IS_VIEW = true;
    const FIELD_LIST = ["id", "employee_id", "date", "sanksi_type_id", "description", "file_document", "is_reduce_salary", "sanksi_value", "base_payment_value", "count_payment", "total_payment_value", "remaining_sanksi_value", "status_payment", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_ADD = ["employee_id", "date", "sanksi_type_id", "description", "file_document", "is_reduce_salary", "sanksi_value", "base_payment_value", "count_payment", "total_payment_value", "remaining_sanksi_value", "status_payment", "created_by", "updated_by"];
    const FIELD_EDIT = ["employee_id", "date", "sanksi_type_id", "description", "file_document", "is_reduce_salary", "sanksi_value", "base_payment_value", "count_payment", "total_payment_value", "remaining_sanksi_value", "status_payment", "updated_by"];
    const FIELD_VIEW = ["id", "employee_id", "date", "sanksi_type_id", "description", "file_document", "is_reduce_salary", "sanksi_value", "base_payment_value", "count_payment", "total_payment_value", "remaining_sanksi_value", "status_payment", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_READONLY = [];
    const FIELD_FILTERABLE = [
        "id" => [
            "operator" => "=",
        ],
        "employee_id" => [
            "operator" => "=",
        ],
        "date" => [
            "operator" => "=",
        ],
        "sanksi_type_id" => [
            "operator" => "=",
        ],
        "description" => [
            "operator" => "=",
        ],
        "file_document" => [
            "operator" => "=",
        ],
        "is_reduce_salary" => [
            "operator" => "=",
        ],
        "sanksi_value" => [
            "operator" => "=",
        ],
        "base_payment_value" => [
            "operator" => "=",
        ],
        "count_payment" => [
            "operator" => "=",
        ],
        "total_payment_value" => [
            "operator" => "=",
        ],
        "remaining_sanksi_value" => [
            "operator" => "=",
        ],
        "status_payment" => [
            "operator" => "=",
        ],
        "created_by" => [
            "operator" => "=",
        ],
        "updated_by" => [
            "operator" => "=",
        ],
        "created_at" => [
            "operator" => "=",
        ],
        "updated_at" => [
            "operator" => "=",
        ],
    ];
    const FIELD_SEARCHABLE = ["status_payment"];
    const FIELD_ARRAY = [];
    const FIELD_SORTABLE = ["id", "employee_id", "date", "sanksi_type_id", "description", "file_document", "is_reduce_salary", "sanksi_value", "base_payment_value", "count_payment", "total_payment_value", "remaining_sanksi_value", "status_payment", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_UNIQUE = [];
    const FIELD_UPLOAD = ["file_document"];
    const FIELD_TYPE = [
        "id" => "bigint",
        "employee_id" => "bigint",
        "date" => "date",
        "sanksi_type_id" => "bigint",
        "description" => "text",
        "file_document" => "text",
        "is_reduce_salary" => "boolean",
        "sanksi_value" => "double_precision",
        "base_payment_value" => "double_precision",
        "count_payment" => "integer",
        "total_payment_value" => "double_precision",
        "remaining_sanksi_value" => "double_precision",
        "status_payment" => "character_varying",
        "created_by" => "bigint",
        "updated_by" => "bigint",
        "created_at" => "timestamp_with_time_zone",
        "updated_at" => "timestamp_with_time_zone",
    ];

    const FIELD_DEFAULT_VALUE = [
        "employee_id" => "",
        "date" => "",
        "sanksi_type_id" => "",
        "description" => "",
        "file_document" => "",
        "is_reduce_salary" => "false",
        "sanksi_value" => "&#039;0&#039;::double precision",
        "base_payment_value" => "&#039;0&#039;::double precision",
        "count_payment" => "0",
        "total_payment_value" => "&#039;0&#039;::double precision",
        "remaining_sanksi_value" => "&#039;0&#039;::double precision",
        "status_payment" => "&#039;not_finished_yet&#039;::character varying",
        "created_by" => "",
        "updated_by" => "",
        "created_at" => "",
        "updated_at" => "",
    ];
    const FIELD_RELATION = [
        "employee_id" => [
            "linkTable" => "employees",
            "aliasTable" => "B",
            "linkField" => "id",
            "displayName" => "rel_employee_id",
            "selectFields" => ["fullname"],
            "selectValue" => "id AS rel_employee_id"
        ],
        "sanksi_type_id" => [
            "linkTable" => "master_sanksi_types",
            "aliasTable" => "C",
            "linkField" => "id",
            "displayName" => "rel_sanksi_type_id",
            "selectFields" => ["name"],
            "selectValue" => "id AS rel_sanksi_type_id"
        ],
        "created_by" => [
            "linkTable" => "users",
            "aliasTable" => "D",
            "linkField" => "id",
            "displayName" => "rel_created_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_created_by"
        ],
        "updated_by" => [
            "linkTable" => "users",
            "aliasTable" => "E",
            "linkField" => "id",
            "displayName" => "rel_updated_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_updated_by"
        ],
    ];
    const CUSTOM_SELECT = "";
    const FIELD_VALIDATION = [
        "employee_id" => "required|integer",
        "date" => "required",
        "sanksi_type_id" => "required|integer",
        "description" => "nullable|string",
        "file_document" => "nullable|string|exists_file",
        "is_reduce_salary" => "required",
        "sanksi_value" => "nullable",
        "base_payment_value" => "nullable",
        "count_payment" => "nullable|integer",
        "total_payment_value" => "nullable",
        "remaining_sanksi_value" => "nullable",
        "status_payment" => "nullable|string|max:255",
        "created_by" => "nullable|integer",
        "updated_by" => "nullable|integer",
        "created_at" => "nullable|date",
        "updated_at" => "nullable|date",
    ];
    const PARENT_CHILD = [];
    // start custom
    const CUSTOM_LIST_FILTER = [];
    const FIELD_CASTING = [
        "sanksi_value" => "float",
        "base_payment_value" => "float",
        "remaining_sanksi_value" => "float",
    ];
    const CHILD_TABLE = [
        //"child_table" => [
        //    "foreignField" => "field"
        //]
    ];

    public static function beforeInsert($input)
    {
        if (!$input["is_reduce_salary"]) {
            $input["status_payment"] = 'finished';
        }
        return $input;
    }

    public static function afterInsert($object, $input)
    {
        return $input;
    }

    public static function beforeUpdate($input)
    {
        if (!$input["is_reduce_salary"]) {
            $input["status_payment"] = 'finished';
        }
        return $input;
    }

    public static function afterUpdate($object, $input)
    {
        return $input;
    }

    public static function beforeDelete($input)
    {
        return $input;
    }

    public static function afterDelete($object, $input)
    {
        return $input;
    } // end custom
}
