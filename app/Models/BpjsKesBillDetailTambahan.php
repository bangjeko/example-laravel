<?php 

namespace App\Models;

use App\CoreService\CallService;
use DateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class BpjsKesBillDetailTambahan extends Model
{
    protected $table = 'bpjs_kes_bill_detail_tambahan';
    protected $dateFormat = 'c';
    const TABLE = "bpjs_kes_bill_detail_tambahan";
    const FILEROOT = "/bpjs_kes_bill_detail_tambahan";
    const IS_LIST = true;
    const IS_ADD = true;
    const IS_EDIT = true;
    const IS_DELETE = true;
    const IS_VIEW = true;
    const FIELD_LIST = ["id", "bpjs_kes_bill_detail_id", "employee_family_id", "iuran_value", "description", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_ADD = ["bpjs_kes_bill_detail_id", "employee_family_id", "iuran_value", "description", "created_by", "updated_by"];
    const FIELD_EDIT = ["bpjs_kes_bill_detail_id", "employee_family_id", "iuran_value", "description", "updated_by"];
    const FIELD_VIEW = ["id", "bpjs_kes_bill_detail_id", "employee_family_id", "iuran_value", "description", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_READONLY = [];
    const FIELD_FILTERABLE = [
        "id" => [
            "operator" => "=",
        ],
        "bpjs_kes_bill_detail_id" => [
            "operator" => "=",
        ],
        "employee_family_id" => [
            "operator" => "=",
        ],
        "iuran_value" => [
            "operator" => "=",
        ],
        "description" => [
            "operator" => "=",
        ],
        "created_by" => [
            "operator" => "=",
        ],
        "updated_by" => [
            "operator" => "=",
        ],
        "created_at" => [
            "operator" => "=",
        ],
        "updated_at" => [
            "operator" => "=",
        ],
    ];
    const FIELD_SEARCHABLE = [];
    const FIELD_ARRAY = [];
    const FIELD_SORTABLE = ["id", "bpjs_kes_bill_detail_id", "employee_family_id", "iuran_value", "description", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_UNIQUE = [["bpjs_kes_bill_detail_id", "employee_family_id"]];
    const FIELD_UPLOAD = [];
    const FIELD_TYPE = [
        "id" => "bigint",
        "bpjs_kes_bill_detail_id" => "bigint",
        "employee_family_id" => "bigint",
        "iuran_value" => "double_precision",
        "description" => "text",
        "created_by" => "bigint",
        "updated_by" => "bigint",
        "created_at" => "timestamp_with_time_zone",
        "updated_at" => "timestamp_with_time_zone",
    ];

    const FIELD_DEFAULT_VALUE = [
        "bpjs_kes_bill_detail_id" => "",
        "employee_family_id" => "",
        "iuran_value" => "&#039;0&#039;::double precision",
        "description" => "",
        "created_by" => "",
        "updated_by" => "",
        "created_at" => "",
        "updated_at" => "",
    ];
    const FIELD_RELATION = [
        "bpjs_kes_bill_detail_id" => [
            "linkTable" => "bpjs_kes_bill_details",
            "aliasTable" => "B",
            "linkField" => "id",
            "displayName" => "rel_bpjs_kes_bill_detail_id",
            "selectFields" => ["id"],
            "selectValue" => "id AS rel_bpjs_kes_bill_detail_id"
        ],
        "employee_family_id" => [
            "linkTable" => "employee_families",
            "aliasTable" => "C",
            "linkField" => "id",
            "displayName" => "rel_employee_family_id",
            "selectFields" => ["fullname"],
            "selectValue" => "id AS rel_employee_family_id"
        ],
        "created_by" => [
            "linkTable" => "users",
            "aliasTable" => "D",
            "linkField" => "id",
            "displayName" => "rel_created_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_created_by"
        ],
        "updated_by" => [
            "linkTable" => "users",
            "aliasTable" => "E",
            "linkField" => "id",
            "displayName" => "rel_updated_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_updated_by"
        ],
    ];
    const CUSTOM_SELECT = "";
    const FIELD_VALIDATION = [
        "bpjs_kes_bill_detail_id" => "required|integer",
        "employee_family_id" => "required|integer",
        "iuran_value" => "nullable",
        "description" => "nullable|string",
        "created_by" => "nullable|integer",
        "updated_by" => "nullable|integer",
        "created_at" => "nullable|date",
        "updated_at" => "nullable|date",
    ];
    const PARENT_CHILD = [];
    // start custom
    const CUSTOM_LIST_FILTER = [];
    const FIELD_CASTING = [
        "iuran_value" => "float",
    ];
    const CHILD_TABLE = [
        //"child_table" => [
        //    "foreignField" => "field"
        //]
    ];

    public static function beforeInsert($input)
    {
        return $input;
    }

    public static function afterInsert($object, $input)
    {
        return $input;
    }
    
    public static function beforeUpdate($input)
    {
        return $input;
    }
    
    public static function afterUpdate($object, $input)
    {
        return $input;
    }
    
    public static function beforeDelete($input)
    {
        return $input;
    }

    public static function afterDelete($object, $input)
    {
        return $input;
    }// end custom
}
