<?php 

namespace App\Models;

use App\CoreService\CallService;
use DateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class TunjanganLemburHariLibur extends Model
{
    protected $table = 'tunjangan_lembur_hari_libur';
    protected $dateFormat = 'c';
    const TABLE = "tunjangan_lembur_hari_libur";
    const FILEROOT = "/tunjangan_lembur_hari_libur";
    const IS_LIST = true;
    const IS_ADD = true;
    const IS_EDIT = true;
    const IS_DELETE = true;
    const IS_VIEW = true;
    const FIELD_LIST = ["id", "tunjangan_lembur_detail_id", "date", "jam_lembur_count", "lembur_jam_kedelapan_value", "lembur_jam_kesembilan_value", "lembur_jam_selanjutnya_value", "penerimaan_value", "description", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_ADD = ["tunjangan_lembur_detail_id", "date", "jam_lembur_count", "lembur_jam_kedelapan_value", "lembur_jam_kesembilan_value", "lembur_jam_selanjutnya_value", "penerimaan_value", "description", "created_by", "updated_by"];
    const FIELD_EDIT = ["tunjangan_lembur_detail_id", "date", "jam_lembur_count", "lembur_jam_kedelapan_value", "lembur_jam_kesembilan_value", "lembur_jam_selanjutnya_value", "penerimaan_value", "description", "updated_by"];
    const FIELD_VIEW = ["id", "tunjangan_lembur_detail_id", "date", "jam_lembur_count", "lembur_jam_kedelapan_value", "lembur_jam_kesembilan_value", "lembur_jam_selanjutnya_value", "penerimaan_value", "description", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_READONLY = [];
    const FIELD_FILTERABLE = [
        "id" => [
            "operator" => "=",
        ],
        "tunjangan_lembur_detail_id" => [
            "operator" => "=",
        ],
        "date" => [
            "operator" => "=",
        ],
        "jam_lembur_count" => [
            "operator" => "=",
        ],
        "lembur_jam_kedelapan_value" => [
            "operator" => "=",
        ],
        "lembur_jam_kesembilan_value" => [
            "operator" => "=",
        ],
        "lembur_jam_selanjutnya_value" => [
            "operator" => "=",
        ],
        "penerimaan_value" => [
            "operator" => "=",
        ],
        "description" => [
            "operator" => "=",
        ],
        "created_by" => [
            "operator" => "=",
        ],
        "updated_by" => [
            "operator" => "=",
        ],
        "created_at" => [
            "operator" => "=",
        ],
        "updated_at" => [
            "operator" => "=",
        ],
    ];
    const FIELD_SEARCHABLE = [];
    const FIELD_ARRAY = [];
    const FIELD_SORTABLE = ["id", "tunjangan_lembur_detail_id", "date", "jam_lembur_count", "lembur_jam_kedelapan_value", "lembur_jam_kesembilan_value", "lembur_jam_selanjutnya_value", "penerimaan_value", "description", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_UNIQUE = [["tunjangan_lembur_detail_id", "date"]];
    const FIELD_UPLOAD = [];
    const FIELD_TYPE = [
        "id" => "bigint",
        "tunjangan_lembur_detail_id" => "bigint",
        "date" => "date",
        "jam_lembur_count" => "double_precision",
        "lembur_jam_kedelapan_value" => "double_precision",
        "lembur_jam_kesembilan_value" => "double_precision",
        "lembur_jam_selanjutnya_value" => "double_precision",
        "penerimaan_value" => "double_precision",
        "description" => "text",
        "created_by" => "bigint",
        "updated_by" => "bigint",
        "created_at" => "timestamp_with_time_zone",
        "updated_at" => "timestamp_with_time_zone",
    ];

    const FIELD_DEFAULT_VALUE = [
        "tunjangan_lembur_detail_id" => "",
        "date" => "",
        "jam_lembur_count" => "&#039;0&#039;::double precision",
        "lembur_jam_kedelapan_value" => "&#039;0&#039;::double precision",
        "lembur_jam_kesembilan_value" => "&#039;0&#039;::double precision",
        "lembur_jam_selanjutnya_value" => "&#039;0&#039;::double precision",
        "penerimaan_value" => "&#039;0&#039;::double precision",
        "description" => "",
        "created_by" => "",
        "updated_by" => "",
        "created_at" => "",
        "updated_at" => "",
    ];
    const FIELD_RELATION = [
        "tunjangan_lembur_detail_id" => [
            "linkTable" => "tunjangan_lembur_details",
            "aliasTable" => "B",
            "linkField" => "id",
            "displayName" => "rel_tunjangan_lembur_detail_id",
            "selectFields" => ["id"],
            "selectValue" => "id AS rel_tunjangan_lembur_detail_id"
        ],
        "created_by" => [
            "linkTable" => "users",
            "aliasTable" => "C",
            "linkField" => "id",
            "displayName" => "rel_created_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_created_by"
        ],
        "updated_by" => [
            "linkTable" => "users",
            "aliasTable" => "D",
            "linkField" => "id",
            "displayName" => "rel_updated_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_updated_by"
        ],
    ];
    const CUSTOM_SELECT = "";
    const FIELD_VALIDATION = [
        "tunjangan_lembur_detail_id" => "required|integer",
        "date" => "required",
        "jam_lembur_count" => "nullable",
        "lembur_jam_kedelapan_value" => "nullable",
        "lembur_jam_kesembilan_value" => "nullable",
        "lembur_jam_selanjutnya_value" => "nullable",
        "penerimaan_value" => "nullable",
        "description" => "nullable|string",
        "created_by" => "nullable|integer",
        "updated_by" => "nullable|integer",
        "created_at" => "nullable|date",
        "updated_at" => "nullable|date",
    ];
    const PARENT_CHILD = [];
    // start custom
    const CUSTOM_LIST_FILTER = [];
    const FIELD_CASTING = [
        "jam_lembur_count" => "float",
        "lembur_jam_kedelapan_value" => "float",
        "lembur_jam_kesembilan_value" => "float",
        "lembur_jam_selanjutnya_value" => "float",
        "penerimaan_value" => "float",
    ];
    const CHILD_TABLE = [
        //"child_table" => [
        //    "foreignField" => "field"
        //]
    ];

    public static function beforeInsert($input)
    {
        return $input;
    }

    public static function afterInsert($object, $input)
    {
        return $input;
    }
    
    public static function beforeUpdate($input)
    {
        return $input;
    }
    
    public static function afterUpdate($object, $input)
    {
        return $input;
    }
    
    public static function beforeDelete($input)
    {
        return $input;
    }

    public static function afterDelete($object, $input)
    {
        return $input;
    }// end custom
}
