<?php 

namespace App\Models;

use App\CoreService\CallService;
use DateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class PotonganSanksiComponents extends Model
{
    protected $table = 'potongan_sanksi_components';
    protected $dateFormat = 'c';
    const TABLE = "potongan_sanksi_components";
    const FILEROOT = "/potongan_sanksi_components";
    const IS_LIST = true;
    const IS_ADD = true;
    const IS_EDIT = true;
    const IS_DELETE = true;
    const IS_VIEW = true;
    const FIELD_LIST = ["id", "potongan_sanksi_detail_id", "employee_sanksi_id", "component_value", "description", "active", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_ADD = ["potongan_sanksi_detail_id", "employee_sanksi_id", "component_value", "description", "active", "created_by", "updated_by"];
    const FIELD_EDIT = ["potongan_sanksi_detail_id", "employee_sanksi_id", "component_value", "description", "active", "updated_by"];
    const FIELD_VIEW = ["id", "potongan_sanksi_detail_id", "employee_sanksi_id", "component_value", "description", "active", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_READONLY = [];
    const FIELD_FILTERABLE = [
        "id" => [
            "operator" => "=",
        ],
        "potongan_sanksi_detail_id" => [
            "operator" => "=",
        ],
        "employee_sanksi_id" => [
            "operator" => "=",
        ],
        "component_value" => [
            "operator" => "=",
        ],
        "description" => [
            "operator" => "=",
        ],
        "active" => [
            "operator" => "=",
        ],
        "created_by" => [
            "operator" => "=",
        ],
        "updated_by" => [
            "operator" => "=",
        ],
        "created_at" => [
            "operator" => "=",
        ],
        "updated_at" => [
            "operator" => "=",
        ],
    ];
    const FIELD_SEARCHABLE = [];
    const FIELD_ARRAY = [];
    const FIELD_SORTABLE = ["id", "potongan_sanksi_detail_id", "employee_sanksi_id", "component_value", "description", "active", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_UNIQUE = [["potongan_sanksi_detail_id", "employee_sanksi_id"]];
    const FIELD_UPLOAD = [];
    const FIELD_TYPE = [
        "id" => "bigint",
        "potongan_sanksi_detail_id" => "bigint",
        "employee_sanksi_id" => "bigint",
        "component_value" => "double_precision",
        "description" => "text",
        "active" => "integer",
        "created_by" => "bigint",
        "updated_by" => "bigint",
        "created_at" => "timestamp_with_time_zone",
        "updated_at" => "timestamp_with_time_zone",
    ];

    const FIELD_DEFAULT_VALUE = [
        "potongan_sanksi_detail_id" => "",
        "employee_sanksi_id" => "",
        "component_value" => "&#039;0&#039;::double precision",
        "description" => "",
        "active" => "1",
        "created_by" => "",
        "updated_by" => "",
        "created_at" => "",
        "updated_at" => "",
    ];
    const FIELD_RELATION = [
        "potongan_sanksi_detail_id" => [
            "linkTable" => "potongan_sanksi_details",
            "aliasTable" => "B",
            "linkField" => "id",
            "displayName" => "rel_potongan_sanksi_detail_id",
            "selectFields" => ["id"],
            "selectValue" => "id AS rel_potongan_sanksi_detail_id"
        ],
        "employee_sanksi_id" => [
            "linkTable" => "employee_sanksi",
            "aliasTable" => "C",
            "linkField" => "id",
            "displayName" => "rel_employee_sanksi_id",
            "selectFields" => ["id"],
            "selectValue" => "id AS rel_employee_sanksi_id"
        ],
        "created_by" => [
            "linkTable" => "users",
            "aliasTable" => "D",
            "linkField" => "id",
            "displayName" => "rel_created_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_created_by"
        ],
        "updated_by" => [
            "linkTable" => "users",
            "aliasTable" => "E",
            "linkField" => "id",
            "displayName" => "rel_updated_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_updated_by"
        ],
    ];
    const CUSTOM_SELECT = "";
    const FIELD_VALIDATION = [
        "potongan_sanksi_detail_id" => "required|integer",
        "employee_sanksi_id" => "required|integer",
        "component_value" => "nullable",
        "description" => "nullable|string",
        "active" => "nullable|integer",
        "created_by" => "nullable|integer",
        "updated_by" => "nullable|integer",
        "created_at" => "nullable|date",
        "updated_at" => "nullable|date",
    ];
    const PARENT_CHILD = [];
    // start custom
    const CUSTOM_LIST_FILTER = [];
    const FIELD_CASTING = [
        //"nama field" => "float",
    ];
    const CHILD_TABLE = [
        //"child_table" => [
        //    "foreignField" => "field"
        //]
    ];

    public static function beforeInsert($input)
    {
        return $input;
    }

    public static function afterInsert($object, $input)
    {
        if (isset($input["employee_sanksi_id"])) {
            $dataEmployeeSanksi = EmployeeSanksi::find($input["employee_sanksi_id"]);
            if (!is_null($dataEmployeeSanksi)) {
                $sanksiValue = $dataEmployeeSanksi->sanksi_value;
                $countPayment = PotonganSanksiComponents::where('employee_sanksi_id', '=', $dataEmployeeSanksi->id)
                    ->where('active', '=', 1)
                    ->count();
                $sumPayment = PotonganSanksiComponents::where('employee_sanksi_id', '=', $dataEmployeeSanksi->id)
                    ->where('active', '=', 1)
                    ->sum('component_value');
                #############################################
                $remainingSanksiValue = max(0, $sanksiValue - $sumPayment);
                #############################################
                $statusPayment = 'not_finished_yet';
                if ($sanksiValue == $sumPayment or $sumPayment > $sanksiValue) {
                    $statusPayment = 'finished';
                }
                #############################################
                EmployeeSanksi::where('id', '=', $input['employee_sanksi_id'])
                    ->update([
                        'total_payment_value' => $sumPayment,
                        'remaining_sanksi_value' => $remainingSanksiValue,
                        'count_payment' => $countPayment,
                        'status_payment' => $statusPayment,
                    ]);
            }
        }
        return $input;
    }

    public static function beforeUpdate($input)
    {
        return $input;
    }

    public static function afterUpdate($object, $input)
    {
        // UPDATE EMPLOYEE SANKSI
        if (isset($input["employee_sanksi_id"])) {
            $dataEmployeeSanksi = EmployeeSanksi::find($input["employee_sanksi_id"]);
            if (!is_null($dataEmployeeSanksi)) {
                $sanksiValue = $dataEmployeeSanksi->sanksi_value;
                $countPayment = PotonganSanksiComponents::where('employee_sanksi_id', '=', $dataEmployeeSanksi->id)
                    ->where('active', '=', 1)
                    ->count();
                $sumPayment = PotonganSanksiComponents::where('employee_sanksi_id', '=', $dataEmployeeSanksi->id)
                    ->where('active', '=', 1)
                    ->sum('component_value');
                #############################################
                $remainingSanksiValue = max(0, $sanksiValue - $sumPayment);
                #############################################
                $statusPayment = 'not_finished_yet';
                if ($sanksiValue == $sumPayment or $sumPayment > $sanksiValue) {
                    $statusPayment = 'finished';
                }
                #############################################
                EmployeeSanksi::where('id', '=', $input['employee_sanksi_id'])
                    ->update([
                        'total_payment_value' => $sumPayment,
                        'remaining_sanksi_value' => $remainingSanksiValue,
                        'count_payment' => $countPayment,
                        'status_payment' => $statusPayment,
                    ]);
            }
        }

        return $input;
    }

    public static function beforeDelete($input)
    {
        return $input;
    }

    public static function afterDelete($object, $input)
    {
        return $input;
    } // end custom
}
