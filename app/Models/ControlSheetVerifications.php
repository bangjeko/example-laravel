<?php 

namespace App\Models;

use App\CoreService\CallService;
use DateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class ControlSheetVerifications extends Model
{
    protected $table = 'control_sheet_verifications';
    protected $dateFormat = 'c';
    const TABLE = "control_sheet_verifications";
    const FILEROOT = "/control_sheet_verifications";
    const IS_LIST = true;
    const IS_ADD = true;
    const IS_EDIT = true;
    const IS_DELETE = true;
    const IS_VIEW = true;
    const FIELD_LIST = ["id", "payroll_periode_id", "privillege_role_id", "verification_order", "verified_by", "verified_at", "job_position_id", "status_code", "description", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_ADD = ["payroll_periode_id", "privillege_role_id", "verification_order", "verified_by", "verified_at", "job_position_id", "status_code", "description", "created_by", "updated_by"];
    const FIELD_EDIT = ["payroll_periode_id", "privillege_role_id", "verification_order", "verified_by", "verified_at", "job_position_id", "status_code", "description", "updated_by"];
    const FIELD_VIEW = ["id", "payroll_periode_id", "privillege_role_id", "verification_order", "verified_by", "verified_at", "job_position_id", "status_code", "description", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_READONLY = [];
    const FIELD_FILTERABLE = [
        "id" => [
            "operator" => "=",
        ],
        "payroll_periode_id" => [
            "operator" => "=",
        ],
        "privillege_role_id" => [
            "operator" => "=",
        ],
        "verification_order" => [
            "operator" => "=",
        ],
        "verified_by" => [
            "operator" => "=",
        ],
        "verified_at" => [
            "operator" => "=",
        ],
        "job_position_id" => [
            "operator" => "=",
        ],
        "status_code" => [
            "operator" => "=",
        ],
        "description" => [
            "operator" => "=",
        ],
        "created_by" => [
            "operator" => "=",
        ],
        "updated_by" => [
            "operator" => "=",
        ],
        "created_at" => [
            "operator" => "=",
        ],
        "updated_at" => [
            "operator" => "=",
        ],
    ];
    const FIELD_SEARCHABLE = [];
    const FIELD_ARRAY = [];
    const FIELD_SORTABLE = ["id", "payroll_periode_id", "privillege_role_id", "verification_order", "verified_by", "verified_at", "job_position_id", "status_code", "description", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_UNIQUE = [];
    const FIELD_UPLOAD = [];
    const FIELD_TYPE = [
        "id" => "bigint",
        "payroll_periode_id" => "bigint",
        "privillege_role_id" => "bigint",
        "verification_order" => "integer",
        "verified_by" => "bigint",
        "verified_at" => "timestamp_with_time_zone",
        "job_position_id" => "bigint",
        "status_code" => "character_varying",
        "description" => "text",
        "created_by" => "bigint",
        "updated_by" => "bigint",
        "created_at" => "timestamp_with_time_zone",
        "updated_at" => "timestamp_with_time_zone",
    ];

    const FIELD_DEFAULT_VALUE = [
        "payroll_periode_id" => "",
        "privillege_role_id" => "",
        "verification_order" => "",
        "verified_by" => "",
        "verified_at" => "",
        "job_position_id" => "",
        "status_code" => "",
        "description" => "",
        "created_by" => "",
        "updated_by" => "",
        "created_at" => "",
        "updated_at" => "",
    ];
    const FIELD_RELATION = [
        "payroll_periode_id" => [
            "linkTable" => "payroll_periodes",
            "aliasTable" => "B",
            "linkField" => "id",
            "displayName" => "rel_payroll_periode_id",
            "selectFields" => ["id"],
            "selectValue" => "id AS rel_payroll_periode_id"
        ],
        "privillege_role_id" => [
            "linkTable" => "roles",
            "aliasTable" => "C",
            "linkField" => "id",
            "displayName" => "rel_privillege_role_id",
            "selectFields" => ["role_name"],
            "selectValue" => "id AS rel_privillege_role_id"
        ],
        "verified_by" => [
            "linkTable" => "users",
            "aliasTable" => "D",
            "linkField" => "id",
            "displayName" => "rel_verified_by",
            "selectFields" => ["fullname", "username"],
            "selectValue" => "id AS rel_verified_by"
        ],
        "job_position_id" => [
            "linkTable" => "job_positions",
            "aliasTable" => "E",
            "linkField" => "id",
            "displayName" => "rel_job_position_id",
            "selectFields" => ["job_position_name"],
            "selectValue" => "id AS rel_job_position_id"
        ],
        "created_by" => [
            "linkTable" => "users",
            "aliasTable" => "F",
            "linkField" => "id",
            "displayName" => "rel_created_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_created_by"
        ],
        "updated_by" => [
            "linkTable" => "users",
            "aliasTable" => "G",
            "linkField" => "id",
            "displayName" => "rel_updated_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_updated_by"
        ],
    ];
    const CUSTOM_SELECT = "";
    const FIELD_VALIDATION = [
        "payroll_periode_id" => "required|integer",
        "privillege_role_id" => "required|integer",
        "verification_order" => "required|integer",
        "verified_by" => "nullable|integer",
        "verified_at" => "nullable|date",
        "job_position_id" => "nullable|integer",
        "status_code" => "nullable|string|max:255",
        "description" => "nullable|string",
        "created_by" => "nullable|integer",
        "updated_by" => "nullable|integer",
        "created_at" => "nullable|date",
        "updated_at" => "nullable|date",
    ];
    const PARENT_CHILD = [];
    // start custom
    const CUSTOM_LIST_FILTER = [];
    const FIELD_CASTING = [
        //"nama field" => "float",
    ];
    const CHILD_TABLE = [
        //"child_table" => [
        //    "foreignField" => "field"
        //]
    ];

    public static function beforeInsert($input)
    {
        return $input;
    }

    public static function afterInsert($object, $input)
    {
        return $input;
    }
    
    public static function beforeUpdate($input)
    {
        return $input;
    }
    
    public static function afterUpdate($object, $input)
    {
        return $input;
    }
    
    public static function beforeDelete($input)
    {
        return $input;
    }

    public static function afterDelete($object, $input)
    {
        return $input;
    }// end custom
}
