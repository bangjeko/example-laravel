<?php 

namespace App\Models;

use App\CoreService\CallService;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;


class Notifications extends Model
{
    protected $table = 'notifications';
    protected $dateFormat = 'c';
    const TABLE = "notifications";
    const FILEROOT = "/notifications";
    const IS_LIST = true;
    const IS_ADD = true;
    const IS_EDIT = true;
    const IS_DELETE = true;
    const IS_VIEW = true;
    const FIELD_LIST = ["id", "user_id", "title", "content", "status_code", "data", "created_at", "updated_at"];
    const FIELD_ADD = ["user_id", "title", "content", "status_code", "data"];
    const FIELD_EDIT = ["user_id", "title", "content", "status_code", "data"];
    const FIELD_VIEW = ["id", "user_id", "title", "content", "status_code", "data", "created_at", "updated_at"];
    const FIELD_READONLY = [];
    const FIELD_FILTERABLE = [
        "id" => [
            "operator" => "=",
        ],
        "user_id" => [
            "operator" => "=",
        ],
        "title" => [
            "operator" => "=",
        ],
        "content" => [
            "operator" => "=",
        ],
        "status_code" => [
            "operator" => "=",
        ],
        "data" => [
            "operator" => "=",
        ],
        "created_at" => [
            "operator" => "=",
        ],
        "updated_at" => [
            "operator" => "=",
        ],
    ];
    const FIELD_SEARCHABLE = ["title"];
    const FIELD_ARRAY = [];
    const FIELD_SORTABLE = ["id", "user_id", "title", "content", "status_code", "data", "created_at", "updated_at"];
    const FIELD_UNIQUE = [];
    const FIELD_UPLOAD = [];
    const FIELD_TYPE = [
        "id" => "bigint",
        "user_id" => "bigint",
        "title" => "character_varying",
        "content" => "text",
        "status_code" => "character_varying",
        "data" => "text",
        "created_at" => "timestamp_with_time_zone",
        "updated_at" => "timestamp_with_time_zone",
    ];

    const FIELD_DEFAULT_VALUE = [
        "user_id" => "",
        "title" => "",
        "content" => "",
        "status_code" => "",
        "data" => "",
        "created_at" => "",
        "updated_at" => "",
    ];
    const FIELD_RELATION = [
    ];
    const CUSTOM_SELECT = "";
    const FIELD_VALIDATION = [
        "user_id" => "nullable|integer",
        "title" => "required|string|max:255",
        "content" => "required|string",
        "status_code" => "required|string|max:255",
        "data" => "required|string",
        "created_at" => "nullable|date",
        "updated_at" => "nullable|date",
    ];
    const PARENT_CHILD = [];
    // start custom
    const CUSTOM_LIST_FILTER = [];
    const FIELD_CASTING = [];
    const CHILD_TABLE = [
        //"child_table" => [
        //    "foreignField" => "field"
        //]
    ];

    public static function beforeInsert($input)
    {
        return $input;
    }

    public static function afterInsert($object, $input)
    {
        return $input;
    }
    
    public static function beforeUpdate($input)
    {
        return $input;
    }
    
    public static function afterUpdate($object, $input)
    {
        return $input;
    }
    
    public static function beforeDelete($input)
    {
        return $input;
    }

    public static function afterDelete($object, $input)
    {
        return $input;
    }// end custom
}
