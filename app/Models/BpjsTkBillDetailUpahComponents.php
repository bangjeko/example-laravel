<?php 

namespace App\Models;

use App\CoreService\CallService;
use DateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class BpjsTkBillDetailUpahComponents extends Model
{
    protected $table = 'bpjs_tk_bill_detail_upah_components';
    protected $dateFormat = 'c';
    const TABLE = "bpjs_tk_bill_detail_upah_components";
    const FILEROOT = "/bpjs_tk_bill_detail_upah_components";
    const IS_LIST = true;
    const IS_ADD = true;
    const IS_EDIT = true;
    const IS_DELETE = true;
    const IS_VIEW = true;
    const FIELD_LIST = ["id", "bpjs_tk_bill_detail_id", "salary_component_id", "koefisien", "default_value", "component_value", "description", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_ADD = ["bpjs_tk_bill_detail_id", "salary_component_id", "koefisien", "default_value", "component_value", "description", "created_by", "updated_by"];
    const FIELD_EDIT = ["bpjs_tk_bill_detail_id", "salary_component_id", "koefisien", "default_value", "component_value", "description", "updated_by"];
    const FIELD_VIEW = ["id", "bpjs_tk_bill_detail_id", "salary_component_id", "koefisien", "default_value", "component_value", "description", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_READONLY = [];
    const FIELD_FILTERABLE = [
        "id" => [
            "operator" => "=",
        ],
        "bpjs_tk_bill_detail_id" => [
            "operator" => "=",
        ],
        "salary_component_id" => [
            "operator" => "=",
        ],
        "koefisien" => [
            "operator" => "=",
        ],
        "default_value" => [
            "operator" => "=",
        ],
        "component_value" => [
            "operator" => "=",
        ],
        "description" => [
            "operator" => "=",
        ],
        "created_by" => [
            "operator" => "=",
        ],
        "updated_by" => [
            "operator" => "=",
        ],
        "created_at" => [
            "operator" => "=",
        ],
        "updated_at" => [
            "operator" => "=",
        ],
    ];
    const FIELD_SEARCHABLE = [];
    const FIELD_ARRAY = [];
    const FIELD_SORTABLE = ["id", "bpjs_tk_bill_detail_id", "salary_component_id", "koefisien", "default_value", "component_value", "description", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_UNIQUE = [["bpjs_tk_bill_detail_id", "salary_component_id"]];
    const FIELD_UPLOAD = [];
    const FIELD_TYPE = [
        "id" => "bigint",
        "bpjs_tk_bill_detail_id" => "bigint",
        "salary_component_id" => "bigint",
        "koefisien" => "double_precision",
        "default_value" => "double_precision",
        "component_value" => "double_precision",
        "description" => "text",
        "created_by" => "bigint",
        "updated_by" => "bigint",
        "created_at" => "timestamp_with_time_zone",
        "updated_at" => "timestamp_with_time_zone",
    ];

    const FIELD_DEFAULT_VALUE = [
        "bpjs_tk_bill_detail_id" => "",
        "salary_component_id" => "",
        "koefisien" => "&#039;1&#039;::double precision",
        "default_value" => "&#039;0&#039;::double precision",
        "component_value" => "&#039;0&#039;::double precision",
        "description" => "",
        "created_by" => "",
        "updated_by" => "",
        "created_at" => "",
        "updated_at" => "",
    ];
    const FIELD_RELATION = [
        "bpjs_tk_bill_detail_id" => [
            "linkTable" => "bpjs_tk_bill_details",
            "aliasTable" => "B",
            "linkField" => "id",
            "displayName" => "rel_bpjs_tk_bill_detail_id",
            "selectFields" => ["id"],
            "selectValue" => "id AS rel_bpjs_tk_bill_detail_id"
        ],
        "salary_component_id" => [
            "linkTable" => "master_salary_components",
            "aliasTable" => "C",
            "linkField" => "id",
            "displayName" => "rel_salary_component_id",
            "selectFields" => ["name"],
            "selectValue" => "id AS rel_salary_component_id"
        ],
        "created_by" => [
            "linkTable" => "users",
            "aliasTable" => "D",
            "linkField" => "id",
            "displayName" => "rel_created_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_created_by"
        ],
        "updated_by" => [
            "linkTable" => "users",
            "aliasTable" => "E",
            "linkField" => "id",
            "displayName" => "rel_updated_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_updated_by"
        ],
    ];
    const CUSTOM_SELECT = "";
    const FIELD_VALIDATION = [
        "bpjs_tk_bill_detail_id" => "required|integer",
        "salary_component_id" => "required|integer",
        "koefisien" => "nullable",
        "default_value" => "nullable",
        "component_value" => "nullable",
        "description" => "nullable|string",
        "created_by" => "nullable|integer",
        "updated_by" => "nullable|integer",
        "created_at" => "nullable|date",
        "updated_at" => "nullable|date",
    ];
    const PARENT_CHILD = [];
    // start custom
    const CUSTOM_LIST_FILTER = [];
    const FIELD_CASTING = [
        "koefisien" => "float",
        "default_value" => "float",
        "component_value" => "float",
    ];
    const CHILD_TABLE = [
        //"child_table" => [
        //    "foreignField" => "field"
        //]
    ];

    public static function beforeInsert($input)
    {
        return $input;
    }

    public static function afterInsert($object, $input)
    {
        return $input;
    }
    
    public static function beforeUpdate($input)
    {
        return $input;
    }
    
    public static function afterUpdate($object, $input)
    {
        return $input;
    }
    
    public static function beforeDelete($input)
    {
        return $input;
    }

    public static function afterDelete($object, $input)
    {
        return $input;
    }// end custom
}
