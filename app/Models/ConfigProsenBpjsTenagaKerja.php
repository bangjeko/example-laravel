<?php 

namespace App\Models;

use App\CoreService\CallService;
use DateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class ConfigProsenBpjsTenagaKerja extends Model
{
    protected $table = 'config_prosen_bpjs_tenaga_kerja';
    protected $dateFormat = 'c';
    const TABLE = "config_prosen_bpjs_tenaga_kerja";
    const FILEROOT = "/config_prosen_bpjs_tenaga_kerja";
    const IS_LIST = true;
    const IS_ADD = true;
    const IS_EDIT = true;
    const IS_DELETE = true;
    const IS_VIEW = true;
    const FIELD_LIST = ["id", "periode_month", "max_age", "upah_max_jp_value", "prosen_jkk", "prosen_jkm", "prosen_jp_bb_company", "prosen_jp_bb_employee", "prosen_jht_company", "prosen_jht_employee", "description", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_ADD = ["periode_month", "max_age", "upah_max_jp_value", "prosen_jkk", "prosen_jkm", "prosen_jp_bb_company", "prosen_jp_bb_employee", "prosen_jht_company", "prosen_jht_employee", "description", "created_by", "updated_by"];
    const FIELD_EDIT = ["periode_month", "max_age", "upah_max_jp_value", "prosen_jkk", "prosen_jkm", "prosen_jp_bb_company", "prosen_jp_bb_employee", "prosen_jht_company", "prosen_jht_employee", "description", "updated_by"];
    const FIELD_VIEW = ["id", "periode_month", "max_age", "upah_max_jp_value", "prosen_jkk", "prosen_jkm", "prosen_jp_bb_company", "prosen_jp_bb_employee", "prosen_jht_company", "prosen_jht_employee", "description", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_READONLY = [];
    const FIELD_FILTERABLE = [
        "id" => [
            "operator" => "=",
        ],
        "periode_month" => [
            "operator" => "=",
        ],
        "max_age" => [
            "operator" => "=",
        ],
        "upah_max_jp_value" => [
            "operator" => "=",
        ],
        "prosen_jkk" => [
            "operator" => "=",
        ],
        "prosen_jkm" => [
            "operator" => "=",
        ],
        "prosen_jp_bb_company" => [
            "operator" => "=",
        ],
        "prosen_jp_bb_employee" => [
            "operator" => "=",
        ],
        "prosen_jht_company" => [
            "operator" => "=",
        ],
        "prosen_jht_employee" => [
            "operator" => "=",
        ],
        "description" => [
            "operator" => "=",
        ],
        "created_by" => [
            "operator" => "=",
        ],
        "updated_by" => [
            "operator" => "=",
        ],
        "created_at" => [
            "operator" => "=",
        ],
        "updated_at" => [
            "operator" => "=",
        ],
    ];
    const FIELD_SEARCHABLE = [];
    const FIELD_ARRAY = [];
    const FIELD_SORTABLE = ["id", "periode_month", "max_age", "upah_max_jp_value", "prosen_jkk", "prosen_jkm", "prosen_jp_bb_company", "prosen_jp_bb_employee", "prosen_jht_company", "prosen_jht_employee", "description", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_UNIQUE = [["periode_month"]];
    const FIELD_UPLOAD = [];
    const FIELD_TYPE = [
        "id" => "bigint",
        "periode_month" => "date",
        "max_age" => "integer",
        "upah_max_jp_value" => "double_precision",
        "prosen_jkk" => "double_precision",
        "prosen_jkm" => "double_precision",
        "prosen_jp_bb_company" => "double_precision",
        "prosen_jp_bb_employee" => "double_precision",
        "prosen_jht_company" => "double_precision",
        "prosen_jht_employee" => "double_precision",
        "description" => "text",
        "created_by" => "bigint",
        "updated_by" => "bigint",
        "created_at" => "timestamp_with_time_zone",
        "updated_at" => "timestamp_with_time_zone",
    ];

    const FIELD_DEFAULT_VALUE = [
        "periode_month" => "",
        "max_age" => "0",
        "upah_max_jp_value" => "&#039;0&#039;::double precision",
        "prosen_jkk" => "&#039;0&#039;::double precision",
        "prosen_jkm" => "&#039;0&#039;::double precision",
        "prosen_jp_bb_company" => "&#039;0&#039;::double precision",
        "prosen_jp_bb_employee" => "&#039;0&#039;::double precision",
        "prosen_jht_company" => "&#039;0&#039;::double precision",
        "prosen_jht_employee" => "&#039;0&#039;::double precision",
        "description" => "",
        "created_by" => "",
        "updated_by" => "",
        "created_at" => "",
        "updated_at" => "",
    ];
    const FIELD_RELATION = [
        "created_by" => [
            "linkTable" => "users",
            "aliasTable" => "B",
            "linkField" => "id",
            "displayName" => "rel_created_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_created_by"
        ],
        "updated_by" => [
            "linkTable" => "users",
            "aliasTable" => "C",
            "linkField" => "id",
            "displayName" => "rel_updated_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_updated_by"
        ],
    ];
    const CUSTOM_SELECT = "";
    const FIELD_VALIDATION = [
        "periode_month" => "required",
        "max_age" => "required|integer",
        "upah_max_jp_value" => "nullable",
        "prosen_jkk" => "nullable",
        "prosen_jkm" => "nullable",
        "prosen_jp_bb_company" => "nullable",
        "prosen_jp_bb_employee" => "nullable",
        "prosen_jht_company" => "nullable",
        "prosen_jht_employee" => "nullable",
        "description" => "nullable|string",
        "created_by" => "nullable|integer",
        "updated_by" => "nullable|integer",
        "created_at" => "nullable|date",
        "updated_at" => "nullable|date",
    ];
    const PARENT_CHILD = [];
    // start custom
    const CUSTOM_LIST_FILTER = [];
    const FIELD_CASTING = [
        "upah_max_jp_value" => "float",
        "prosen_jkk" => "float",
        "prosen_jkm" => "float",
        "prosen_jp_bb_company" => "float",
        "prosen_jp_bb_employee" => "float",
        "prosen_jht_company" => "float",
        "prosen_jht_employee" => "float",
    ];
    const CHILD_TABLE = [
        //"child_table" => [
        //    "foreignField" => "field"
        //]
    ];

    public static function beforeInsert($input)
    {
        return $input;
    }

    public static function afterInsert($object, $input)
    {
        return $input;
    }
    
    public static function beforeUpdate($input)
    {
        return $input;
    }
    
    public static function afterUpdate($object, $input)
    {
        return $input;
    }
    
    public static function beforeDelete($input)
    {
        return $input;
    }

    public static function afterDelete($object, $input)
    {
        return $input;
    }// end custom
}
