<?php 

namespace App\Models;

use App\CoreService\CallService;
use DateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class KoperasiSimpanPinjamDetails extends Model
{
    protected $table = 'koperasi_simpan_pinjam_details';
    protected $dateFormat = 'c';
    const TABLE = "koperasi_simpan_pinjam_details";
    const FILEROOT = "/koperasi_simpan_pinjam_details";
    const IS_LIST = true;
    const IS_ADD = true;
    const IS_EDIT = true;
    const IS_DELETE = true;
    const IS_VIEW = true;
    const FIELD_LIST = ["id", "koperasi_simpan_pinjam_id", "employee_id", "simpanan_value", "pinjaman_value", "total_value", "description", "status_code", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_ADD = ["koperasi_simpan_pinjam_id", "employee_id", "simpanan_value", "pinjaman_value", "total_value", "description", "status_code", "created_by", "updated_by"];
    const FIELD_EDIT = ["koperasi_simpan_pinjam_id", "employee_id", "simpanan_value", "pinjaman_value", "total_value", "description", "status_code", "updated_by"];
    const FIELD_VIEW = ["id", "koperasi_simpan_pinjam_id", "employee_id", "simpanan_value", "pinjaman_value", "total_value", "description", "status_code", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_READONLY = [];
    const FIELD_FILTERABLE = [
        "id" => [
            "operator" => "=",
        ],
        "koperasi_simpan_pinjam_id" => [
            "operator" => "=",
        ],
        "employee_id" => [
            "operator" => "=",
        ],
        "simpanan_value" => [
            "operator" => "=",
        ],
        "pinjaman_value" => [
            "operator" => "=",
        ],
        "total_value" => [
            "operator" => "=",
        ],
        "description" => [
            "operator" => "=",
        ],
        "status_code" => [
            "operator" => "=",
        ],
        "created_by" => [
            "operator" => "=",
        ],
        "updated_by" => [
            "operator" => "=",
        ],
        "created_at" => [
            "operator" => "=",
        ],
        "updated_at" => [
            "operator" => "=",
        ],
    ];
    const FIELD_SEARCHABLE = [];
    const FIELD_ARRAY = [];
    const FIELD_SORTABLE = ["id", "koperasi_simpan_pinjam_id", "employee_id", "simpanan_value", "pinjaman_value", "total_value", "description", "status_code", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_UNIQUE = [["koperasi_simpan_pinjam_id", "employee_id"]];
    const FIELD_UPLOAD = [];
    const FIELD_TYPE = [
        "id" => "bigint",
        "koperasi_simpan_pinjam_id" => "bigint",
        "employee_id" => "bigint",
        "simpanan_value" => "double_precision",
        "pinjaman_value" => "double_precision",
        "total_value" => "double_precision",
        "description" => "text",
        "status_code" => "character_varying",
        "created_by" => "bigint",
        "updated_by" => "bigint",
        "created_at" => "timestamp_with_time_zone",
        "updated_at" => "timestamp_with_time_zone",
    ];

    const FIELD_DEFAULT_VALUE = [
        "koperasi_simpan_pinjam_id" => "",
        "employee_id" => "",
        "simpanan_value" => "&#039;0&#039;::double precision",
        "pinjaman_value" => "&#039;0&#039;::double precision",
        "total_value" => "&#039;0&#039;::double precision",
        "description" => "",
        "status_code" => "&#039;not_calculated_yet&#039;::character varying",
        "created_by" => "",
        "updated_by" => "",
        "created_at" => "",
        "updated_at" => "",
    ];
    const FIELD_RELATION = [
        "koperasi_simpan_pinjam_id" => [
            "linkTable" => "koperasi_simpan_pinjam",
            "aliasTable" => "B",
            "linkField" => "id",
            "displayName" => "rel_koperasi_simpan_pinjam_id",
            "selectFields" => ["id"],
            "selectValue" => "id AS rel_koperasi_simpan_pinjam_id"
        ],
        "employee_id" => [
            "linkTable" => "employees",
            "aliasTable" => "C",
            "linkField" => "id",
            "displayName" => "rel_employee_id",
            "selectFields" => ["fullname"],
            "selectValue" => "id AS rel_employee_id"
        ],
        "created_by" => [
            "linkTable" => "users",
            "aliasTable" => "D",
            "linkField" => "id",
            "displayName" => "rel_created_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_created_by"
        ],
        "updated_by" => [
            "linkTable" => "users",
            "aliasTable" => "E",
            "linkField" => "id",
            "displayName" => "rel_updated_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_updated_by"
        ],
    ];
    const CUSTOM_SELECT = "";
    const FIELD_VALIDATION = [
        "koperasi_simpan_pinjam_id" => "required|integer",
        "employee_id" => "required|integer",
        "simpanan_value" => "nullable",
        "pinjaman_value" => "nullable",
        "total_value" => "nullable",
        "description" => "nullable|string",
        "status_code" => "nullable|string|max:255",
        "created_by" => "nullable|integer",
        "updated_by" => "nullable|integer",
        "created_at" => "nullable|date",
        "updated_at" => "nullable|date",
    ];
    const PARENT_CHILD = [];
    // start custom
    const CUSTOM_LIST_FILTER = [];
    const FIELD_CASTING = [
        "simpanan_value" => "float",
        "pinjaman_value" => "float",
        "total_value" => "float",
    ];
    const CHILD_TABLE = [
        //"child_table" => [
        //    "foreignField" => "field"
        //]
    ];

    public static function beforeInsert($input)
    {
        $input["status_code"] = "not_calculated_yet";
        return $input;
    }

    public static function afterInsert($object, $input)
    {
        #1 INSERT KOPERASI SIMPAN PINJAM DETAIL (GET MEMBER KOPERAIS AKTIF)
        $sqlMemberActivParams["koperasi_simpan_pinjam_detail_id"] = $object->id;
        $sqlMemberActivParams["created_by"] = $object->created_by;
        $sqlMemberActivParams["created_at"] = $object->created_at;
        $sqlMemberActivParams["updated_by"] = $object->updated_by;
        $sqlMemberActivParams["updated_at"] = $object->updated_at;
        $sqlMemberActive = "WITH insert_pinjaman_details AS (
             INSERT INTO koperasi_pinjaman_details (koperasi_simpan_pinjam_detail_id, pinjaman_value, created_by,created_at,updated_by,updated_at)
             SELECT :koperasi_simpan_pinjam_detail_id, 0, :created_by, :created_at, :updated_by, :updated_at
         ), insert_simpanan_details AS (
             INSERT INTO koperasi_simpanan_details (koperasi_simpan_pinjam_detail_id, comp_simpanan_koperasi_id, created_by,created_at,updated_by,updated_at)
             SELECT :koperasi_simpan_pinjam_detail_id, n.id, :created_by, :created_at, :updated_by, :updated_at
             FROM master_comp_simpanan_koperasi n 
             WHERE n.active = 1
         )
         SELECT * FROM koperasi_simpan_pinjam_details WHERE id = :koperasi_simpan_pinjam_detail_id
         ";

        $insertData =  DB::insert($sqlMemberActive, $sqlMemberActivParams);
        return $input;
    }

    public static function beforeUpdate($input)
    {
        return $input;
    }

    public static function afterUpdate($object, $input)
    {
        return $input;
    }

    public static function beforeDelete($input)
    {
        return $input;
    }

    public static function afterDelete($object, $input)
    {
        return $input;
    } // end custom
}
