<?php 

namespace App\Models;

use App\CoreService\CallService;
use DateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class TunjanganLemburDetails extends Model
{
    protected $table = 'tunjangan_lembur_details';
    protected $dateFormat = 'c';
    const TABLE = "tunjangan_lembur_details";
    const FILEROOT = "/tunjangan_lembur_details";
    const IS_LIST = true;
    const IS_ADD = true;
    const IS_EDIT = true;
    const IS_DELETE = true;
    const IS_VIEW = true;
    const FIELD_LIST = ["id", "tunjangan_lembur_id", "employee_id", "upah_value", "pokok_tunjangan_tetap_value", "three_quarter_upah_value", "upah_lembur_perjam_value", "penerimaan_value", "pembulatan_penerimaan_value", "description", "status_code", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_ADD = ["tunjangan_lembur_id", "employee_id", "upah_value", "pokok_tunjangan_tetap_value", "three_quarter_upah_value", "upah_lembur_perjam_value", "penerimaan_value", "pembulatan_penerimaan_value", "description", "status_code", "created_by", "updated_by"];
    const FIELD_EDIT = ["tunjangan_lembur_id", "employee_id", "upah_value", "pokok_tunjangan_tetap_value", "three_quarter_upah_value", "upah_lembur_perjam_value", "penerimaan_value", "pembulatan_penerimaan_value", "description", "status_code", "updated_by"];
    const FIELD_VIEW = ["id", "tunjangan_lembur_id", "employee_id", "upah_value", "pokok_tunjangan_tetap_value", "three_quarter_upah_value", "upah_lembur_perjam_value", "penerimaan_value", "pembulatan_penerimaan_value", "description", "status_code", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_READONLY = [];
    const FIELD_FILTERABLE = [
        "id" => [
            "operator" => "=",
        ],
        "tunjangan_lembur_id" => [
            "operator" => "=",
        ],
        "employee_id" => [
            "operator" => "=",
        ],
        "upah_value" => [
            "operator" => "=",
        ],
        "pokok_tunjangan_tetap_value" => [
            "operator" => "=",
        ],
        "three_quarter_upah_value" => [
            "operator" => "=",
        ],
        "upah_lembur_perjam_value" => [
            "operator" => "=",
        ],
        "penerimaan_value" => [
            "operator" => "=",
        ],
        "pembulatan_penerimaan_value" => [
            "operator" => "=",
        ],
        "description" => [
            "operator" => "=",
        ],
        "status_code" => [
            "operator" => "=",
        ],
        "created_by" => [
            "operator" => "=",
        ],
        "updated_by" => [
            "operator" => "=",
        ],
        "created_at" => [
            "operator" => "=",
        ],
        "updated_at" => [
            "operator" => "=",
        ],
    ];
    const FIELD_SEARCHABLE = [];
    const FIELD_ARRAY = [];
    const FIELD_SORTABLE = ["id", "tunjangan_lembur_id", "employee_id", "upah_value", "pokok_tunjangan_tetap_value", "three_quarter_upah_value", "upah_lembur_perjam_value", "penerimaan_value", "pembulatan_penerimaan_value", "description", "status_code", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_UNIQUE = [["tunjangan_lembur_id", "employee_id"]];
    const FIELD_UPLOAD = [];
    const FIELD_TYPE = [
        "id" => "bigint",
        "tunjangan_lembur_id" => "bigint",
        "employee_id" => "bigint",
        "upah_value" => "double_precision",
        "pokok_tunjangan_tetap_value" => "double_precision",
        "three_quarter_upah_value" => "double_precision",
        "upah_lembur_perjam_value" => "double_precision",
        "penerimaan_value" => "double_precision",
        "pembulatan_penerimaan_value" => "double_precision",
        "description" => "text",
        "status_code" => "character_varying",
        "created_by" => "bigint",
        "updated_by" => "bigint",
        "created_at" => "timestamp_with_time_zone",
        "updated_at" => "timestamp_with_time_zone",
    ];

    const FIELD_DEFAULT_VALUE = [
        "tunjangan_lembur_id" => "",
        "employee_id" => "",
        "upah_value" => "&#039;0&#039;::double precision",
        "pokok_tunjangan_tetap_value" => "&#039;0&#039;::double precision",
        "three_quarter_upah_value" => "&#039;0&#039;::double precision",
        "upah_lembur_perjam_value" => "&#039;0&#039;::double precision",
        "penerimaan_value" => "&#039;0&#039;::double precision",
        "pembulatan_penerimaan_value" => "&#039;0&#039;::double precision",
        "description" => "",
        "status_code" => "&#039;not_calculated_yet&#039;::character varying",
        "created_by" => "",
        "updated_by" => "",
        "created_at" => "",
        "updated_at" => "",
    ];
    const FIELD_RELATION = [
        "tunjangan_lembur_id" => [
            "linkTable" => "tunjangan_lembur",
            "aliasTable" => "B",
            "linkField" => "id",
            "displayName" => "rel_tunjangan_lembur_id",
            "selectFields" => ["id"],
            "selectValue" => "id AS rel_tunjangan_lembur_id"
        ],
        "employee_id" => [
            "linkTable" => "employees",
            "aliasTable" => "C",
            "linkField" => "id",
            "displayName" => "rel_employee_id",
            "selectFields" => ["fullname"],
            "selectValue" => "id AS rel_employee_id"
        ],
        "created_by" => [
            "linkTable" => "users",
            "aliasTable" => "D",
            "linkField" => "id",
            "displayName" => "rel_created_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_created_by"
        ],
        "updated_by" => [
            "linkTable" => "users",
            "aliasTable" => "E",
            "linkField" => "id",
            "displayName" => "rel_updated_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_updated_by"
        ],
    ];
    const CUSTOM_SELECT = "";
    const FIELD_VALIDATION = [
        "tunjangan_lembur_id" => "required|integer",
        "employee_id" => "required|integer",
        "upah_value" => "nullable",
        "pokok_tunjangan_tetap_value" => "nullable",
        "three_quarter_upah_value" => "nullable",
        "upah_lembur_perjam_value" => "nullable",
        "penerimaan_value" => "nullable",
        "pembulatan_penerimaan_value" => "nullable",
        "description" => "nullable|string",
        "status_code" => "nullable|string|max:255",
        "created_by" => "nullable|integer",
        "updated_by" => "nullable|integer",
        "created_at" => "nullable|date",
        "updated_at" => "nullable|date",
    ];
    const PARENT_CHILD = [];
    // start custom
    const CUSTOM_LIST_FILTER = [];
    const FIELD_CASTING = [
        "upah_value" => "float",
        "upah_lembur_perjam_value" => "float",
        "pokok_tunjangan_tetap_value" => "float",
        "three_quarter_upah_value" => "float",
        "penerimaan_value" => "float",
    ];
    const CHILD_TABLE = [
        //"child_table" => [
        //    "foreignField" => "field"
        //]
    ];

    public static function beforeInsert($input)
    {
        return $input;
    }

    public static function afterInsert($object, $input)
    {
        return $input;
    }
    
    public static function beforeUpdate($input)
    {
        return $input;
    }
    
    public static function afterUpdate($object, $input)
    {
        return $input;
    }
    
    public static function beforeDelete($input)
    {
        return $input;
    }

    public static function afterDelete($object, $input)
    {
        return $input;
    }// end custom
}
