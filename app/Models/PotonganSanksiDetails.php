<?php 

namespace App\Models;

use App\CoreService\CallService;
use DateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class PotonganSanksiDetails extends Model
{
    protected $table = 'potongan_sanksi_details';
    protected $dateFormat = 'c';
    const TABLE = "potongan_sanksi_details";
    const FILEROOT = "/potongan_sanksi_details";
    const IS_LIST = true;
    const IS_ADD = true;
    const IS_EDIT = true;
    const IS_DELETE = true;
    const IS_VIEW = true;
    const FIELD_LIST = ["id", "potongan_sanksi_id", "employee_id", "potongan_value", "description", "status_code", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_ADD = ["potongan_sanksi_id", "employee_id", "potongan_value", "description", "status_code", "created_by", "updated_by"];
    const FIELD_EDIT = ["potongan_sanksi_id", "employee_id", "potongan_value", "description", "status_code", "updated_by"];
    const FIELD_VIEW = ["id", "potongan_sanksi_id", "employee_id", "potongan_value", "description", "status_code", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_READONLY = [];
    const FIELD_FILTERABLE = [
        "id" => [
            "operator" => "=",
        ],
        "potongan_sanksi_id" => [
            "operator" => "=",
        ],
        "employee_id" => [
            "operator" => "=",
        ],
        "potongan_value" => [
            "operator" => "=",
        ],
        "description" => [
            "operator" => "=",
        ],
        "status_code" => [
            "operator" => "=",
        ],
        "created_by" => [
            "operator" => "=",
        ],
        "updated_by" => [
            "operator" => "=",
        ],
        "created_at" => [
            "operator" => "=",
        ],
        "updated_at" => [
            "operator" => "=",
        ],
    ];
    const FIELD_SEARCHABLE = [];
    const FIELD_ARRAY = [];
    const FIELD_SORTABLE = ["id", "potongan_sanksi_id", "employee_id", "potongan_value", "description", "status_code", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_UNIQUE = [["potongan_sanksi_id", "employee_id"]];
    const FIELD_UPLOAD = [];
    const FIELD_TYPE = [
        "id" => "bigint",
        "potongan_sanksi_id" => "bigint",
        "employee_id" => "bigint",
        "potongan_value" => "double_precision",
        "description" => "text",
        "status_code" => "character_varying",
        "created_by" => "bigint",
        "updated_by" => "bigint",
        "created_at" => "timestamp_with_time_zone",
        "updated_at" => "timestamp_with_time_zone",
    ];

    const FIELD_DEFAULT_VALUE = [
        "potongan_sanksi_id" => "",
        "employee_id" => "",
        "potongan_value" => "&#039;0&#039;::double precision",
        "description" => "",
        "status_code" => "&#039;not_calculated_yet&#039;::character varying",
        "created_by" => "",
        "updated_by" => "",
        "created_at" => "",
        "updated_at" => "",
    ];
    const FIELD_RELATION = [
        "potongan_sanksi_id" => [
            "linkTable" => "potongan_sanksi",
            "aliasTable" => "B",
            "linkField" => "id",
            "displayName" => "rel_potongan_sanksi_id",
            "selectFields" => ["id"],
            "selectValue" => "id AS rel_potongan_sanksi_id"
        ],
        "employee_id" => [
            "linkTable" => "employees",
            "aliasTable" => "C",
            "linkField" => "id",
            "displayName" => "rel_employee_id",
            "selectFields" => ["fullname"],
            "selectValue" => "id AS rel_employee_id"
        ],
        "created_by" => [
            "linkTable" => "users",
            "aliasTable" => "D",
            "linkField" => "id",
            "displayName" => "rel_created_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_created_by"
        ],
        "updated_by" => [
            "linkTable" => "users",
            "aliasTable" => "E",
            "linkField" => "id",
            "displayName" => "rel_updated_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_updated_by"
        ],
    ];
    const CUSTOM_SELECT = "";
    const FIELD_VALIDATION = [
        "potongan_sanksi_id" => "required|integer",
        "employee_id" => "required|integer",
        "potongan_value" => "nullable",
        "description" => "nullable|string",
        "status_code" => "nullable|string|max:255",
        "created_by" => "nullable|integer",
        "updated_by" => "nullable|integer",
        "created_at" => "nullable|date",
        "updated_at" => "nullable|date",
    ];
    const PARENT_CHILD = [];
    // start custom
    const CUSTOM_LIST_FILTER = [];
    const FIELD_CASTING = [
        //"nama field" => "float",
    ];
    const CHILD_TABLE = [
        //"child_table" => [
        //    "foreignField" => "field"
        //]
    ];

    public static function beforeInsert($input)
    {
        return $input;
    }

    public static function afterInsert($object, $input)
    {
        return $input;
    }
    
    public static function beforeUpdate($input)
    {
        return $input;
    }
    
    public static function afterUpdate($object, $input)
    {
        return $input;
    }
    
    public static function beforeDelete($input)
    {
        return $input;
    }

    public static function afterDelete($object, $input)
    {
        return $input;
    }// end custom
}
