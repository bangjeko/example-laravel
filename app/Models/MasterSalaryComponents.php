<?php 

namespace App\Models;

use App\CoreService\CallService;
use DateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class MasterSalaryComponents extends Model
{
    protected $table = 'master_salary_components';
    protected $dateFormat = 'c';
    const TABLE = "master_salary_components";
    const FILEROOT = "/master_salary_components";
    const IS_LIST = true;
    const IS_ADD = true;
    const IS_EDIT = true;
    const IS_DELETE = true;
    const IS_VIEW = true;
    const FIELD_LIST = ["id", "salary_component_category_id", "name", "default_value", "description", "charge", "type", "is_salary_cut", "is_mpp_component", "is_pph_twoone_component", "is_custom_component", "custom_salary_module_id", "order_number", "active", "created_by", "updated_by", "created_at", "updated_at", "is_jurnal_component"];
    const FIELD_ADD = ["salary_component_category_id", "name", "default_value", "description", "charge", "type", "is_salary_cut", "is_mpp_component", "is_pph_twoone_component", "is_custom_component", "custom_salary_module_id", "order_number", "active", "created_by", "updated_by", "is_jurnal_component"];
    const FIELD_EDIT = ["salary_component_category_id", "name", "default_value", "description", "charge", "type", "is_salary_cut", "is_mpp_component", "is_pph_twoone_component", "is_custom_component", "custom_salary_module_id", "order_number", "active", "updated_by", "is_jurnal_component"];
    const FIELD_VIEW = ["id", "salary_component_category_id", "name", "default_value", "description", "charge", "type", "is_salary_cut", "is_mpp_component", "is_pph_twoone_component", "is_custom_component", "custom_salary_module_id", "order_number", "active", "created_by", "updated_by", "created_at", "updated_at", "is_jurnal_component"];
    const FIELD_READONLY = [];
    const FIELD_FILTERABLE = [
        "id" => [
            "operator" => "=",
        ],
        "salary_component_category_id" => [
            "operator" => "=",
        ],
        "name" => [
            "operator" => "=",
        ],
        "default_value" => [
            "operator" => "=",
        ],
        "description" => [
            "operator" => "=",
        ],
        "charge" => [
            "operator" => "=",
        ],
        "type" => [
            "operator" => "=",
        ],
        "is_salary_cut" => [
            "operator" => "=",
        ],
        "is_mpp_component" => [
            "operator" => "=",
        ],
        "is_pph_twoone_component" => [
            "operator" => "=",
        ],
        "is_custom_component" => [
            "operator" => "=",
        ],
        "custom_salary_module_id" => [
            "operator" => "=",
        ],
        "order_number" => [
            "operator" => "=",
        ],
        "active" => [
            "operator" => "=",
        ],
        "created_by" => [
            "operator" => "=",
        ],
        "updated_by" => [
            "operator" => "=",
        ],
        "created_at" => [
            "operator" => "=",
        ],
        "updated_at" => [
            "operator" => "=",
        ],
        "is_jurnal_component" => [
            "operator" => "=",
        ],
    ];
    const FIELD_SEARCHABLE = ["name", "charge", "type"];
    const FIELD_ARRAY = [];
    const FIELD_SORTABLE = ["id", "salary_component_category_id", "name", "default_value", "description", "charge", "type", "is_salary_cut", "is_mpp_component", "is_pph_twoone_component", "is_custom_component", "custom_salary_module_id", "order_number", "active", "created_by", "updated_by", "created_at", "updated_at", "is_jurnal_component"];
    const FIELD_UNIQUE = [["name"]];
    const FIELD_UPLOAD = [];
    const FIELD_TYPE = [
        "id" => "bigint",
        "salary_component_category_id" => "bigint",
        "name" => "character_varying",
        "default_value" => "double_precision",
        "description" => "text",
        "charge" => "character_varying",
        "type" => "character_varying",
        "is_salary_cut" => "boolean",
        "is_mpp_component" => "boolean",
        "is_pph_twoone_component" => "boolean",
        "is_custom_component" => "boolean",
        "custom_salary_module_id" => "bigint",
        "order_number" => "integer",
        "active" => "integer",
        "created_by" => "bigint",
        "updated_by" => "bigint",
        "created_at" => "timestamp_with_time_zone",
        "updated_at" => "timestamp_with_time_zone",
        "is_jurnal_component" => "boolean",
    ];

    const FIELD_DEFAULT_VALUE = [
        "salary_component_category_id" => "",
        "name" => "",
        "default_value" => "&#039;0&#039;::double precision",
        "description" => "",
        "charge" => "&#039;employee&#039;::character varying",
        "type" => "",
        "is_salary_cut" => "false",
        "is_mpp_component" => "false",
        "is_pph_twoone_component" => "false",
        "is_custom_component" => "false",
        "custom_salary_module_id" => "",
        "order_number" => "",
        "active" => "1",
        "created_by" => "",
        "updated_by" => "",
        "created_at" => "",
        "updated_at" => "",
        "is_jurnal_component" => "false",
    ];
    const FIELD_RELATION = [
        "salary_component_category_id" => [
            "linkTable" => "master_salary_component_categories",
            "aliasTable" => "B",
            "linkField" => "id",
            "displayName" => "rel_salary_component_category_id",
            "selectFields" => ["name"],
            "selectValue" => "id AS rel_salary_component_category_id"
        ],
        "custom_salary_module_id" => [
            "linkTable" => "custom_salary_modules",
            "aliasTable" => "C",
            "linkField" => "id",
            "displayName" => "rel_custom_salary_module_id",
            "selectFields" => ["name"],
            "selectValue" => "id AS rel_custom_salary_module_id"
        ],
        "created_by" => [
            "linkTable" => "users",
            "aliasTable" => "D",
            "linkField" => "id",
            "displayName" => "rel_created_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_created_by"
        ],
        "updated_by" => [
            "linkTable" => "users",
            "aliasTable" => "E",
            "linkField" => "id",
            "displayName" => "rel_updated_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_updated_by"
        ],
    ];
    const CUSTOM_SELECT = "";
    const FIELD_VALIDATION = [
        "salary_component_category_id" => "required|integer",
        "name" => "required|string|max:255",
        "default_value" => "nullable",
        "description" => "nullable|string",
        "charge" => "required|string|max:255",
        "type" => "nullable|string|max:255",
        "is_salary_cut" => "required",
        "is_mpp_component" => "required",
        "is_pph_twoone_component" => "required",
        "is_custom_component" => "required",
        "custom_salary_module_id" => "nullable|integer",
        "order_number" => "nullable|integer",
        "active" => "nullable|integer",
        "created_by" => "nullable|integer",
        "updated_by" => "nullable|integer",
        "created_at" => "nullable|date",
        "updated_at" => "nullable|date",
        "is_jurnal_component" => "required",
    ];
    const PARENT_CHILD = [];
    // start custom
    const CUSTOM_LIST_FILTER = [];
    const FIELD_CASTING = [
        "default_value" => "float",
    ];
    const CHILD_TABLE = [
        //"child_table" => [
        //    "foreignField" => "field"
        //]
    ];

    public static function beforeInsert($input)
    {
        if (isset($input["is_custom_component"])) {
            if (!$input["is_custom_component"]) {
                $input["custom_salary_module_id"] = null;
            }
        }
        return $input;
    }

    public static function afterInsert($object, $input)
    {
        return $input;
    }

    public static function beforeUpdate($input)
    {
        if (isset($input["is_custom_component"])) {
            if (!$input["is_custom_component"]) {
                $input["custom_salary_module_id"] = null;
            }
        }
        return $input;
    }

    public static function afterUpdate($object, $input)
    {
        return $input;
    }

    public static function beforeDelete($input)
    {
        return $input;
    }

    public static function afterDelete($object, $input)
    {
        return $input;
    } // end custom
}
