<?php 

namespace App\Models;

use App\CoreService\CallService;
use DateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class BpjsTkBills extends Model
{
    protected $table = 'bpjs_tk_bills';
    protected $dateFormat = 'c';
    const TABLE = "bpjs_tk_bills";
    const FILEROOT = "/bpjs_tk_bills";
    const IS_LIST = true;
    const IS_ADD = true;
    const IS_EDIT = true;
    const IS_DELETE = true;
    const IS_VIEW = true;
    const FIELD_LIST = ["id", "periode_month", "total_iuran_jkk_value", "total_iuran_jkm_value", "total_iuran_jp_bb_company_value", "total_iuran_jp_bb_employee_value", "total_iuran_jht_company_value", "total_iuran_jht_employee_value", "total_kekurangan_jp_value", "total_iuran_bpjs_company_value", "total_iuran_bpjs_employee_value", "total_iuran_value", "description", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_ADD = ["periode_month", "total_iuran_jkk_value", "total_iuran_jkm_value", "total_iuran_jp_bb_company_value", "total_iuran_jp_bb_employee_value", "total_iuran_jht_company_value", "total_iuran_jht_employee_value", "total_kekurangan_jp_value", "total_iuran_bpjs_company_value", "total_iuran_bpjs_employee_value", "total_iuran_value", "description", "created_by", "updated_by"];
    const FIELD_EDIT = ["periode_month", "total_iuran_jkk_value", "total_iuran_jkm_value", "total_iuran_jp_bb_company_value", "total_iuran_jp_bb_employee_value", "total_iuran_jht_company_value", "total_iuran_jht_employee_value", "total_kekurangan_jp_value", "total_iuran_bpjs_company_value", "total_iuran_bpjs_employee_value", "total_iuran_value", "description", "updated_by"];
    const FIELD_VIEW = ["id", "periode_month", "total_iuran_jkk_value", "total_iuran_jkm_value", "total_iuran_jp_bb_company_value", "total_iuran_jp_bb_employee_value", "total_iuran_jht_company_value", "total_iuran_jht_employee_value", "total_kekurangan_jp_value", "total_iuran_bpjs_company_value", "total_iuran_bpjs_employee_value", "total_iuran_value", "description", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_READONLY = [];
    const FIELD_FILTERABLE = [
        "id" => [
            "operator" => "=",
        ],
        "periode_month" => [
            "operator" => "=",
        ],
        "total_iuran_jkk_value" => [
            "operator" => "=",
        ],
        "total_iuran_jkm_value" => [
            "operator" => "=",
        ],
        "total_iuran_jp_bb_company_value" => [
            "operator" => "=",
        ],
        "total_iuran_jp_bb_employee_value" => [
            "operator" => "=",
        ],
        "total_iuran_jht_company_value" => [
            "operator" => "=",
        ],
        "total_iuran_jht_employee_value" => [
            "operator" => "=",
        ],
        "total_kekurangan_jp_value" => [
            "operator" => "=",
        ],
        "total_iuran_bpjs_company_value" => [
            "operator" => "=",
        ],
        "total_iuran_bpjs_employee_value" => [
            "operator" => "=",
        ],
        "total_iuran_value" => [
            "operator" => "=",
        ],
        "description" => [
            "operator" => "=",
        ],
        "created_by" => [
            "operator" => "=",
        ],
        "updated_by" => [
            "operator" => "=",
        ],
        "created_at" => [
            "operator" => "=",
        ],
        "updated_at" => [
            "operator" => "=",
        ],
    ];
    const FIELD_SEARCHABLE = [];
    const FIELD_ARRAY = [];
    const FIELD_SORTABLE = ["id", "periode_month", "total_iuran_jkk_value", "total_iuran_jkm_value", "total_iuran_jp_bb_company_value", "total_iuran_jp_bb_employee_value", "total_iuran_jht_company_value", "total_iuran_jht_employee_value", "total_kekurangan_jp_value", "total_iuran_bpjs_company_value", "total_iuran_bpjs_employee_value", "total_iuran_value", "description", "created_by", "updated_by", "created_at", "updated_at"];
    const FIELD_UNIQUE = [["periode_month"]];
    const FIELD_UPLOAD = [];
    const FIELD_TYPE = [
        "id" => "bigint",
        "periode_month" => "date",
        "total_iuran_jkk_value" => "double_precision",
        "total_iuran_jkm_value" => "double_precision",
        "total_iuran_jp_bb_company_value" => "double_precision",
        "total_iuran_jp_bb_employee_value" => "double_precision",
        "total_iuran_jht_company_value" => "double_precision",
        "total_iuran_jht_employee_value" => "double_precision",
        "total_kekurangan_jp_value" => "double_precision",
        "total_iuran_bpjs_company_value" => "double_precision",
        "total_iuran_bpjs_employee_value" => "double_precision",
        "total_iuran_value" => "double_precision",
        "description" => "text",
        "created_by" => "bigint",
        "updated_by" => "bigint",
        "created_at" => "timestamp_with_time_zone",
        "updated_at" => "timestamp_with_time_zone",
    ];

    const FIELD_DEFAULT_VALUE = [
        "periode_month" => "",
        "total_iuran_jkk_value" => "&#039;0&#039;::double precision",
        "total_iuran_jkm_value" => "&#039;0&#039;::double precision",
        "total_iuran_jp_bb_company_value" => "&#039;0&#039;::double precision",
        "total_iuran_jp_bb_employee_value" => "&#039;0&#039;::double precision",
        "total_iuran_jht_company_value" => "&#039;0&#039;::double precision",
        "total_iuran_jht_employee_value" => "&#039;0&#039;::double precision",
        "total_kekurangan_jp_value" => "&#039;0&#039;::double precision",
        "total_iuran_bpjs_company_value" => "&#039;0&#039;::double precision",
        "total_iuran_bpjs_employee_value" => "&#039;0&#039;::double precision",
        "total_iuran_value" => "&#039;0&#039;::double precision",
        "description" => "",
        "created_by" => "",
        "updated_by" => "",
        "created_at" => "",
        "updated_at" => "",
    ];
    const FIELD_RELATION = [
        "created_by" => [
            "linkTable" => "users",
            "aliasTable" => "B",
            "linkField" => "id",
            "displayName" => "rel_created_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_created_by"
        ],
        "updated_by" => [
            "linkTable" => "users",
            "aliasTable" => "C",
            "linkField" => "id",
            "displayName" => "rel_updated_by",
            "selectFields" => ["username"],
            "selectValue" => "id AS rel_updated_by"
        ],
    ];
    const CUSTOM_SELECT = "";
    const FIELD_VALIDATION = [
        "periode_month" => "required",
        "total_iuran_jkk_value" => "nullable",
        "total_iuran_jkm_value" => "nullable",
        "total_iuran_jp_bb_company_value" => "nullable",
        "total_iuran_jp_bb_employee_value" => "nullable",
        "total_iuran_jht_company_value" => "nullable",
        "total_iuran_jht_employee_value" => "nullable",
        "total_kekurangan_jp_value" => "nullable",
        "total_iuran_bpjs_company_value" => "nullable",
        "total_iuran_bpjs_employee_value" => "nullable",
        "total_iuran_value" => "nullable",
        "description" => "nullable|string",
        "created_by" => "nullable|integer",
        "updated_by" => "nullable|integer",
        "created_at" => "nullable|date",
        "updated_at" => "nullable|date",
    ];
    const PARENT_CHILD = [];
    // start custom
    const CUSTOM_LIST_FILTER = [];
    const FIELD_CASTING = [
        "total_iuran_jkk_value" => "float",
        "total_iuran_jkm_value" => "float",
        "total_iuran_jp_bb_company_value" => "float",
        "total_iuran_jp_bb_employee_value" => "float",
        "total_iuran_jht_company_value" => "float",
        "total_iuran_jht_employee_value" => "float",
        "total_kekurangan_jp_value" => "float",
        "total_iuran_bpjs_company_value" => "float",
        "total_iuran_bpjs_employee_value" => "float",
        "total_iuran_value" => "float",
    ];
    const CHILD_TABLE = [
        //"child_table" => [
        //    "foreignField" => "field"
        //]
    ];

    public static function beforeInsert($input)
    {
        return $input;
    }

    public static function afterInsert($object, $input)
    {
        return $input;
    }
    
    public static function beforeUpdate($input)
    {
        return $input;
    }
    
    public static function afterUpdate($object, $input)
    {
        return $input;
    }
    
    public static function beforeDelete($input)
    {
        return $input;
    }

    public static function afterDelete($object, $input)
    {
        return $input;
    }// end custom
}
