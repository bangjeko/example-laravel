<?php

namespace App\Services\NoAuth;

use App\Models\Users;
use App\CoreService\CoreService;
use App\Models\MobileAppVersions;

class LatestMobileAppVersions extends CoreService
{

    public $transaction = false;
    public $task = null;

    public function prepare($input)
    {
        return $input;
    }

    public function process($input, $originalInput)
    {

        $object = MobileAppVersions::select("*")
        ->orderBy('id', 'desc')
        ->first();


        return [
            "data" => $object
        ];
    }

    protected function validation()
    {
        return [];
    }
}
