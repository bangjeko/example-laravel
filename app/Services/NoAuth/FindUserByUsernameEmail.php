<?php

namespace App\Services\NoAuth;

use App\Models\Users;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\CoreService\CoreException;
use App\CoreService\CoreService;


class FindUserByUsernameEmail extends CoreService
{

    public $transaction = false;
    public $task = null;

    public function prepare($input)
    {
        return $input;
    }

    public function process($input, $originalInput)
    {

        $object = Users::select("*");
        if (isset($input["username"])) {
            $object = $object->orWhere('username', '=', $input["username"]);
        }
        if (isset($input["email"])) {
            $object = $object->orWhere('email', '=', $input["email"]);
        }

        $object = $object->first();
        if (is_null($object)) {
            $object = false;
        } else {
            $object = true;
        }

        return $object;
    }

    protected function validation()
    {
        return [
            "email" => "email|nullable",
        ];
    }
}
