<?php

namespace App\Services\FinaIntegration\SyncData;

use App\CoreService\CoreException;
use App\CoreService\CoreService;
use App\Models\LogSyncFinaOperationalAccounts;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class FInaOperationalAccountsSyncData extends CoreService
{

    public $transaction = false;
    public $task = null;

    public function prepare($input)
    {
        $model = "fina-operational-accounts";
        $classModel = "\\App\\Models\\" . Str::ucfirst(Str::camel($model));
        $permission = "sync-" . $model;
        $authRoles = getRoleName(Auth::user()->role_id);
        if (!class_exists($classModel))
            throw new CoreException(__("message.model404", ['model' => $model]), 404);

        if (!$classModel::IS_LIST)
            throw new CoreException("Not found", 404);

        if (!hasPermission($permission))
            throw new CoreException(__("message.forbidden403", ['permission' => $permission, 'roles' => $authRoles]), 403);

        $input["class_model"] = $classModel;
        return $input;
    }

    public function process($input, $originalInput)
    {
        # GET FROM FINA


        # INSERT TO FINA OPERATIONAL


        sleep(5);
        # INSERT LOG SYNC DATA
        $objectLog = new LogSyncFinaOperationalAccounts;
        $objectLog->sync_at = now();
        $objectLog->sync_by = Auth::id();
        $objectLog->created_by = Auth::id();
        $objectLog->updated_by = Auth::id();
        $objectLog->save();

        # GET LAST LOG
        $sql = "SELECT A.*, U.username as rel_sync_by 
        FROM log_sync_fina_operational_accounts A
        LEFT JOIN users U ON A.sync_by = A.id
        LEFT JOIN users CR ON CR.created_by = A.id
        LEFT JOIN users UP ON UP.updated_by = A.id
        WHERE true ORDER BY A.sync_at DESC";
        $object =  DB::selectOne($sql);

        // END FOR IMG PHOTO CREATED BY
        return [
            "data" => $object
        ];
    }

    protected function validation()
    {
        return [];
    }
}
