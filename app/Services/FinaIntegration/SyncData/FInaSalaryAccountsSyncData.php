<?php

namespace App\Services\FinaIntegration\SyncData;

use App\CoreService\CoreException;
use App\CoreService\CoreService;
use App\Models\FinaSalaryAccounts;
use App\Models\LogSyncFinaSalaryAccounts;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class FInaSalaryAccountsSyncData extends CoreService
{

    public $transaction = false;
    public $task = null;

    public function prepare($input)
    {
        $model = "fina-salary-accounts";
        $classModel = "\\App\\Models\\" . Str::ucfirst(Str::camel($model));
        $permission = "sync-" . $model;
        $authRoles = getRoleName(Auth::user()->role_id);
        if (!class_exists($classModel))
            throw new CoreException(__("message.model404", ['model' => $model]), 404);

        if (!$classModel::IS_LIST)
            throw new CoreException("Not found", 404);

        if (!hasPermission($permission))
            throw new CoreException(__("message.forbidden403", ['permission' => $permission, 'roles' => $authRoles]), 403);

        $input["class_model"] = $classModel;
        return $input;
    }

    public function process($input, $originalInput)
    {
        # GET FROM FINA
        $endpoint = finaEndPointUrl();
        $limit = isset($input["limit"]) ? $input["limit"] : 15;
        $page = isset($input["page"]) ? $input["page"] : 1;
        $search = isset($input["search"]) ? $input["search"] : null;
        $offset = ($page - 1) * $limit;
        $params = [
            "act" => "getsdm",
        ];
        // $headers = [
        //     "client-id: " . taptapIntegrationDataDesired('taptap_client_id'),
        //     "client-secret: " . taptapIntegrationDataDesired('taptap_secret_key'),
        // ];
        $url = $endpoint . '?' . http_build_query($params);
        // CURL
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $output = curl_exec($ch);
        if ($output === FALSE) {
            die('Curl error: ' . curl_error($ch));
        }
        curl_close($ch);
        $output = json_decode($output, true);

        if (!isset($output["data"])) {
            throw new CoreException("Can't find data from Fina Application", 500);
        }

        // return $output["data"];


        # INSERT TO FINA OPERATIONAL
        #######################################################
        $dataInserted = [];
        foreach ($output["data"] as $k) {
            $dataAkun = [];
            $dataAkun["id"] = $k["id"];
            $dataAkun["account_code"] = $k["kode"];
            $dataAkun["account_name"] = $k["nama"];
            $dataAkun["debit_kredit"] = "debit";
            array_push($dataInserted, $dataAkun);
        }
        #######################################################
        $finaSalaryAccount = FinaSalaryAccounts::upsert(
            $dataInserted,
            ['id'],
            ['account_code']
        );
        DB::statement("SELECT setval('fina_salary_accounts_id_seq', (SELECT MAX(id) FROM fina_salary_accounts)+1)");

        # INSERT LOG SYNC DATA
        $objectLog = new LogSyncFinaSalaryAccounts;
        $objectLog->sync_at = now();
        $objectLog->sync_by = Auth::id();
        $objectLog->created_by = Auth::id();
        $objectLog->updated_by = Auth::id();
        $objectLog->save();

        # GET LAST LOG
        $sql = "SELECT A.*, U.username as rel_sync_by 
        FROM log_sync_fina_salary_accounts A
        LEFT JOIN users U ON A.sync_by = A.id
        LEFT JOIN users CR ON CR.created_by = A.id
        LEFT JOIN users UP ON UP.updated_by = A.id
        WHERE true ORDER BY A.sync_at DESC";
        $object =  DB::selectOne($sql);

        return [
            "data" => $object
        ];
    }

    protected function validation()
    {
        return [];
    }
}
