<?php

namespace App\Services\Sso;

use App\CoreService\CoreException;
use App\CoreService\CoreService;
use App\Models\Users;
use Illuminate\Support\Facades\Auth;

class SsoUserDataset extends CoreService
{

    public $transaction = false;
    public $permission = null;

    public function prepare($input)
    {
        return $input;
    }

    public function process($input, $originalInput)
    {
        // #1 GET USER SSO EXISTING
        $ssoIds = Users::whereNotNull('sso_user_id')->pluck('sso_user_id');
        $endpoint = 'https://sso.kiw.center/api/sso/users/list';
        $params = [
            "application_code" => "kiw-hrm-v2",
            "ids_not_in" => json_encode($ssoIds)
        ];
        $url = $endpoint . '?' . http_build_query($params);
        // CURL
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $output = curl_exec($ch);
        if ($output === FALSE) {
            die('Curl error: ' . curl_error($ch));
        }
        curl_close($ch);
        $output = json_decode($output, true);
        return $output;
    }

    protected function validation()
    {
        return [];
    }
}
