<?php

namespace App\Services\Sso;

use App\CoreService\CoreException;
use App\CoreService\CoreService;
use App\Models\Employees;
use App\Models\Users;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\URL;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;

class SsoUserAdd extends CoreService
{

    public $transaction = false;
    public $permission = null;

    public function prepare($input)
    {
        $model = 'users';
        $authRoles = getRoleName(Auth::user()->role_id);
        $permission = "create-" . $model;
        if (!hasPermission($permission))
            throw new CoreException(__("message.forbidden403", ['permission' => $permission, 'role' => $authRoles]), 403);

        $classModel = "\\App\\Models\\" . Str::ucfirst(Str::camel($model));
        if (!class_exists($classModel))
            throw new CoreException("Model " . $model . " Not found", 404);

        $input["class_model"] = $classModel;
        return $input;
    }

    public function process($input, $originalInput)
    {
        // GUZZLE
        // $client = new Client();
        // $res = $client->request('GET', 'https://kiw-sso.pttas.xyz/api/sso/users/show', [
        //     "query" => [
        //         "application_code" => "kiw-hrm-v2",
        //         "id" => $input["id"]
        //     ]
        // ]);

        // return $res;
        //GET DATA DETAIL SSO
        $endpoint = 'https://sso.kiw.center/api/sso/users/show';
        $params = [
            "application_code" => "kiw-hrm-v2",
            "id" => $input["sso_user_id"]
        ];
        $url = $endpoint . '?' . http_build_query($params);
        // CURL
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $output = curl_exec($ch);
        if ($output === FALSE) {
            die('Curl error: ' . curl_error($ch));
        }
        curl_close($ch);
        $output = json_decode($output, true);


        if (is_null($output["data"])) {
            throw new CoreException("Data SSO Tidak Ditemukan", 404);
        }

        //


        $dataSso = $output["data"];
        // CHECK USER EXISTS
        $user = Users::select("users.*")
            ->where(function ($query) use ($dataSso) {
                $query->orWhere('username', '=', $dataSso["username"])
                    ->orWhere('sso_user_id', '=', $dataSso["id"]);
            })
            ->first();
        if (!is_null($user)) {
            throw new CoreException("Gagal Menambahkan Data Baru. Data User SSO " . $dataSso["username"] . " Sudah Ada.");
        }
        /// BEGIN ADD USER 
        $inputUser["fullname"] = $dataSso["fullname"];
        $inputUser["username"] = $dataSso["username"];
        $inputUser["password"] = $dataSso["password"];
        $inputUser["email"] = $dataSso["email"];
        $inputUser["telephone"] = $dataSso["telephone"];
        $inputUser["nip"] = $dataSso["nip"];
        $inputUser["sso_user_id"] = $dataSso["id"];

        $inputUser["role_id"] = $input["role_id"];
        $inputUser["employee_id"] = $input["employee_id"];
        $inputUser['status_code'] = 'user_active';

        ///
        $classModel = $input["class_model"];
        //  DEFAULT STATUS
        $object = new $classModel;

        $fieldUpload = ["img_photo_user"];
        if ($fieldUpload) {
            foreach ($fieldUpload as $item) {
                if (isset($inputUser[$item])) {
                    if (is_array($inputUser[$item])) {
                        $inputUser[$item] = isset($inputUser[$item]["path"]) ? $inputUser[$item]["path"] : $inputUser[$item]["field_value"];
                    }
                }
            }
        }
        // MOVE FILE
        foreach ($fieldUpload as $item) {
            $tmpPath = $inputUser[$item] ?? null;
            if (!is_null($tmpPath)) {
                if (!Storage::exists($tmpPath)) {
                    throw new CoreException(__("message.tempFileNotFound", ['field' => $item]));
                }
                $tmpPath = $inputUser[$item];
                $originalname = pathinfo(storage_path($tmpPath), PATHINFO_FILENAME);
                $ext = pathinfo(storage_path($tmpPath), PATHINFO_EXTENSION);

                $newPath = $classModel::FILEROOT . "/" . $originalname . "." . $ext;
                //START MOVE FILE
                if (Storage::exists($newPath)) {

                    $id = 1;
                    $filename = pathinfo(storage_path($newPath), PATHINFO_FILENAME);
                    $ext = pathinfo(storage_path($newPath), PATHINFO_EXTENSION);
                    while (true) {
                        $originalname = $filename . "($id)." . $ext;
                        if (!Storage::exists($classModel::FILEROOT . "/" . $originalname))
                            break;
                        $id++;
                    }
                    $newPath = $classModel::FILEROOT . "/" . $originalname;
                }

                $ext = pathinfo(storage_path($newPath), PATHINFO_EXTENSION);
                $object->{$item} = $newPath;
                Storage::move($tmpPath, $newPath);
                //END MOVE FILE
            }
        }
        foreach ($classModel::FIELD_ADD as $item) {
            if (!in_array($item, $fieldUpload)) {
                $inputUserValue = $inputUser[$item] ?? $classModel::FIELD_DEFAULT_VALUE[$item];
                $object->{$item} = ($inputUserValue != '') ? $inputUserValue : null;
            }
        }

        $object->save();

        $classModel::afterInsert($object, $inputUser);

        // UNTUK FORMAT DATA IMG
        if (!empty($fieldUpload)) {
            foreach ($fieldUpload as $item) {
                if ((preg_match("/file_/i", $item) or preg_match("/img_/i", $item)) and !is_null($object->$item)) {
                    $url = URL::to('api/file/' . $classModel::TABLE . '/' . $item . '/' . $object->id . '/' . time());
                    $ext = pathinfo($object->$item, PATHINFO_EXTENSION);
                    $filename = pathinfo(storage_path($object->$item), PATHINFO_BASENAME);
                    $object->$item = (object) [
                        "ext" => $ext,
                        "url" => $url,
                        "filename" => $filename,
                        "field_value" => $object->$item
                    ];
                }
            }
        }

        ############### SELAJUTNYA UPDATE DATA SSO USER ID DI TABLE EMPLOYEE ##############
        DB::statement("UPDATE employees SET user_id = :user_id WHERE id = :employee_id", [
            "user_id" => $object->id,
            "employee_id" => $object->employee_id
        ]);
        return [
            "data" => $object,
            "message" => __("message.successfullyAdd")
        ];
    }

    protected function validation()
    {
        return [
            "sso_user_id" => "required",
            "role_id" => "required",
            "employee_id" => "required"
        ];
    }
}
