<?php

namespace App\Services\SsoOauth;

use Illuminate\Support\Facades\DB;
use App\CoreService\CoreException;
use App\CoreService\CoreService;
use App\Models\User;
use App\Models\Users;
use Illuminate\Support\Facades\URL;
use Tymon\JWTAuth\Facades\JWTAuth;

class VerifySsoOauthToken extends CoreService
{

    public $transaction = false;
    public $permission = null;

    public function prepare($input)
    {
        return $input;
    }

    public function process($input, $originalInput)
    {
        $endpoint = 'https://sso.kiw.center/api/oauth/verify-oauth-token';
        $params = [
            "token" => $input["token"]
        ];
        $url = $endpoint . '?' . http_build_query($params);
        // CURL
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $output = curl_exec($ch);
        if ($output === FALSE) {
            die('Curl error: ' . curl_error($ch));
        }
        curl_close($ch);
        $output = json_decode($output, true);
        if (!isset($output["data"])) {
            throw new CoreException($output["message"], 401);
        }
        // SELANJUTNYA LOGIN
        $dataSso = $output["data"];
        $ssoUserId = $dataSso["id"];
        $user = Users::select("users.*", "roles.role_name")
            ->leftjoin('roles', 'roles.id', 'users.role_id')
            ->where('sso_user_id', '=', $ssoUserId)
            ->first();

        if (is_null($user)) {
            return [
                "message" => "User SSO Anda Belum Ditambahkan ke dalam sistem HRM",
            ];
        } else {
            $user = User::select("users.*", "roles.role_name")->leftjoin('roles', 'roles.id', 'users.role_id')
                ->where('users.id', '=', $user->id)
                ->first();
            if ($token = JWTAuth::fromUser($user)) {
                /*
                | PERMISSION
                */
                $sql = "SELECT B.permission_code FROM mapping_roles_permissions A
                INNER JOIN permissions B ON B.id = A.permission_id
                INNER JOIN users C ON C.role_id = A.role_id AND C.id = ? WHERE A.active = 1";

                $permissionList =  array_map(function ($item) {
                    return $item->permission_code;
                }, DB::select($sql, [$user->id]));

                // FOR IMG PHOTO CREATED BY
                if ($user->img_photo_user) {
                    $url = URL::to('api/file/users/img_photo_user/' . $user->id . '/' . time());
                    $tumbnailUrl = URL::to('api/tumb-file/users/img_photo_user/' . $user->id . '/' . time());
                    $ext = pathinfo($user->img_photo_user, PATHINFO_EXTENSION);
                    $filename = pathinfo(storage_path($user->img_photo_user), PATHINFO_BASENAME);
                    $user->img_photo_user = (object) [
                        "ext" => (is_null($user->img_photo_user)) ? null : $ext,
                        "url" => $url,
                        "tumbnail_url" => $tumbnailUrl,
                        "filename" => (is_null($user->img_photo_user)) ? null : $filename,
                        "field_value" => $user->img_photo_user
                    ];
                }
                unset($user->password);
                // END REMOVE PROPERTY OF OBJECT
                return [
                    "user" => $user,
                    "token" => $token,
                    "permissions" => $permissionList,
                    "message" => __("message.loginSuccess")
                ];
            } else {
                throw new CoreException(__("message.loginCredentialFalse"), 422);
            }
            // if ($token = Auth::loginUsingId(1)) {
            // } else {
            // }
        }
        // return $token;
    }

    protected function validation()
    {
        return [
            "token" => "required",
        ];
    }
}
