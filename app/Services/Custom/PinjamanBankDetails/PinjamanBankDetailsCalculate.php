<?php

namespace App\Services\Custom\PinjamanBankDetails;

use App\CoreService\CallService;
use App\CoreService\CoreException;
use App\CoreService\CoreService;
use App\Models\KoperasiPinjamanDetails;
use App\Models\KoperasiSimpananDetails;
use App\Models\KoperasiSimpanPinjam;
use App\Models\KoperasiSimpanPinjamDetails;
use App\Models\PinjamanBank;
use App\Models\PinjamanBankDetailComponents;
use App\Models\PinjamanBankDetails;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;

class PinjamanBankDetailsCalculate extends CoreService
{

    public $transaction = true;
    public $task = null;

    public function prepare($input)
    {
        $model = "pinjaman-bank-details";
        $classModel = "\\App\\Models\\" . Str::ucfirst(Str::camel($model));
        $permission = "create-" . $model;
        $authRoles = getRoleName(Auth::user()->role_id);
        if (!class_exists($classModel))
            throw new CoreException(__("message.model404", ['model' => $model]), 404);
        if (!$classModel::IS_ADD)
            throw new CoreException("Not found", 404);
        if (!hasPermission($permission))
            throw new CoreException(__("message.forbidden403", ['permission' => $permission, 'roles' => $authRoles]), 403);
        $input["class_model"] = $classModel;
        return $input;
    }

    public function process($input, $originalInput)
    {
        $response = [];

        #1 UPDATE pinjaman_bank_details

        $dataPinjamanBankDetails = PinjamanBankDetails::find($input["id"]);
        $dataPinjamanBankDetails->description = $input["description"];
        $dataPinjamanBankDetails->status_code = "calculated";

        #2 UPDATE child_pinjaman_bank_detail_components
        foreach ($input["child_pinjaman_bank_detail_components"] as $childPinjamanComponentsItem) {
            if (isset($childPinjamanComponentsItem["id"])) {
                $childPinjamanComponentsItem["model"] = "pinjaman_bank_detail_components";
                $childPinjamanComponentsItem["pinjaman_bank_detail_id"] = $input["id"];
                $childDataPinjamanComponentsUpdated = CallService::run("Edit", $childPinjamanComponentsItem);
            }
        }


        #3 HITUNG JUMLAH PINJAMAN
        $jumlahPinjaman = PinjamanBankDetailComponents::where('pinjaman_bank_detail_id', '=', $input["id"])
            ->sum('comp_value');


        $dataPinjamanBankDetails->pinjaman_value = $jumlahPinjaman;
        $dataPinjamanBankDetails->save();

        #5 UPDATE pinjaman_bank
        $totalPinjaman = PinjamanBankDetails::where('pinjaman_bank_id', '=', $dataPinjamanBankDetails->pinjaman_bank_id)
            ->sum('pinjaman_value');

        PinjamanBank::where('id', '=', $dataPinjamanBankDetails->pinjaman_bank_id)
            ->update([
                'total_pinjaman_value' => $totalPinjaman,
            ]);

        // THEN SHOW
        $classModel = $input["class_model"];
        $selectableList = [];
        $tableJoinList = [];
        $params = ["id" =>  $input["id"]];

        foreach ($classModel::FIELD_VIEW as $list) {
            $selectableList[] = $classModel::TABLE . "." . $list;
        }

        $i = 0;
        foreach ($classModel::FIELD_RELATION as $key => $relation) {
            $alias = toAlpha($i + 1);
            $fieldDisplayed = "CONCAT_WS (' - ',";
            foreach ($relation["selectFields"] as $keyField) {
                $fieldDisplayed .= $alias . '.' . $keyField . ",";
            }
            $fieldDisplayed = substr($fieldDisplayed, 0, strlen($fieldDisplayed) - 1);
            $fieldDisplayed .= ") AS " . $relation["displayName"];
            $selectableList[] = $fieldDisplayed;
            //
            $tableJoinList[] = "LEFT JOIN " . $relation["linkTable"] . " " . $alias . " ON " .
                $classModel::TABLE . "." . $key . " = " .  $alias . "." . $relation["linkField"];
            $i++;
        }

        if (!empty($classModel::CUSTOM_SELECT)) $selectableList[] = $classModel::CUSTOM_SELECT;

        $condition = " WHERE " . $classModel::TABLE . ".id = :id";

        $sql = "SELECT " . implode(", ", $selectableList) . ", B.periode_month FROM " . $classModel::TABLE . " " .
            implode(" ", $tableJoinList) . $condition;

        $object =  DB::selectOne($sql, $params);
        if (is_null($object)) {
            throw new CoreException(__("message.dataNotFound", ['id' => $input["id"]]));
        }

        $fieldCasting = $classModel::FIELD_CASTING;
        if (!empty($fieldCasting)) {
            foreach ($fieldCasting as $item => $k) {
                if (isset($fieldCasting[$item])) {
                    if (array_key_exists($item, $fieldCasting)) {
                        if ($fieldCasting[$item] == 'float') {
                            $object->$item = is_null($object->$item) ? 0 : (float) $object->$item;
                        }
                    }
                }
            }
        }
        // FORMAT IMAGE
        if (!empty($classModel::FIELD_UPLOAD)) {
            foreach ($classModel::FIELD_UPLOAD as $item) {
                if ((preg_match("/file_/i", $item) or preg_match("/img_/i", $item)) and !is_null($object->$item)) {
                    $url = URL::to('api/file/' . $classModel::TABLE . '/' . $item . '/' . $object->id . '/' . time());
                    $tumbnailUrl = URL::to('api/tumb-file/' . $classModel::TABLE . '/' . $item . '/' . $object->id . '/' . time());
                    $ext = pathinfo($object->$item, PATHINFO_EXTENSION);
                    $filename = pathinfo(storage_path($object->$item), PATHINFO_BASENAME);
                    $object->$item = (object) [
                        "ext" => (is_null($object->$item)) ? null : $ext,
                        "url" => $url,
                        "tumbnail_url" => $tumbnailUrl,
                        "filename" => (is_null($object->$item)) ? null : $filename,
                        "field_value" => $object->$item
                    ];
                }
                if (preg_match("/array_/i", $item)) {
                    $key->$item = unserialize($key->$item);
                    if (!$key->$item) {
                        $key->$item = null;
                    }
                }
            }
        }

        // FOR IMG PHOTO CREATED BY
        if (property_exists($object, 'created_by')) {
            $url = URL::to('api/file/users/img_photo_user/' . $object->created_by . '/' . time());
            $tumbnailUrl = URL::to('api/tumb-file/users/img_photo_user/' . $object->created_by . '/' . time());
            $object->img_photo_created_by = (object) [
                "url" => $url,
                "tumbnail_url" => $tumbnailUrl,
            ];
        }

        if (property_exists($object, 'json_data')) {
            $object->json_data =  json_decode($object->json_data);
        }

        // DATA PINJAMAN BANK DETAIL COMPONENTS
        $paramPinjamanBankDetailComponents = [];
        $paramPinjamanBankDetailComponents["pinjaman_bank_detail_id"] = $object->id;

        $dataPinjamanBankDetailComp = DB::select("SELECT A.*, MC.name as rel_comp_pinjaman_bank_id
        FROM pinjaman_bank_detail_components A
        JOIN master_comp_pinjaman_bank MC ON A.comp_pinjaman_bank_id = MC.id
        WHERE A.pinjaman_bank_detail_id =:pinjaman_bank_detail_id", $paramPinjamanBankDetailComponents);
        if ($dataPinjamanBankDetailComp) {
            foreach ($dataPinjamanBankDetailComp as $k) {
                $k->comp_value = (float)  $k->comp_value;
            }
        }
        $object->child_pinjaman_bank_detail_components = $dataPinjamanBankDetailComp;


        $response["data"] = $object;
        $response["message"] = __("message.successfullyAdd");
        return $response;
    }

    protected function validation()
    {
        return [];
    }
}
