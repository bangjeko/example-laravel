<?php

namespace App\Services\Custom\PresenceOnPeriodeRecaps;

use App\CoreService\CallService;
use App\CoreService\CoreException;
use App\CoreService\CoreService;
use App\Models\PayrollPeriodes;
use App\Models\PresenceOnPeriodeRecaps;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Carbon\Carbon;

class PresenceOnPeriodeRecapsSync extends CoreService
{

    public $transaction = true;
    public $task = null;

    public function prepare($input)
    {
        $model = "presence-on-periode-recaps";
        $classModel = "\\App\\Models\\" . Str::ucfirst(Str::camel($model));
        $permission = "create-" . $model;
        $authRoles = getRoleName(Auth::user()->role_id);
        if (!class_exists($classModel))
            throw new CoreException(__("message.model404", ['model' => $model]), 404);
        if (!$classModel::IS_ADD)
            throw new CoreException("Not found", 404);
        if (!hasPermission($permission))
            throw new CoreException(__("message.forbidden403", ['permission' => $permission, 'roles' => $authRoles]), 403);
        $input["class_model"] = $classModel;
        return $input;
    }

    public function process($input, $originalInput)
    {
        $workDayCount = null;
        $workDayLostCount = null;
        $workDayLateCount = null;
        $workDayMinuteLateCount = null;

        // GET START DATE & END DATE FROM PAYROLl PERIODES
        $dataPayrollPeriodes = PayrollPeriodes::where('periode_month', '=', $input["periode_month"])->first();
        $startDate = $dataPayrollPeriodes->start_date;
        $endDate = $dataPayrollPeriodes->end_date;

        // DATA PREVIOUS MONTH
        $firstDayofPreviousMonth = Carbon::parse($input["periode_month"])->subMonthsNoOverflow()->startOfMonth()->toDateString();
        $lastDayofPreviousMonth = Carbon::parse($input["periode_month"])->subMonthsNoOverflow()->endOfMonth()->toDateString();

        $paramSeriesMonthBefore["start_periode"] = $firstDayofPreviousMonth;
        $paramSeriesMonthBefore["end_periode"] = $lastDayofPreviousMonth;
        $sqlDataSeriesPeriodesMonthBefore = "SELECT date_trunc('day', dd):: date as series_periode
            FROM generate_series
            ( :start_periode::timestamp 
             , :end_periode::timestamp
             , '1 day'::interval) dd
        ";
        $dataSeriesPeriodesMonthBefore =  DB::select($sqlDataSeriesPeriodesMonthBefore, $paramSeriesMonthBefore);
        $tapTapPreviousMonthData = [];

        ################################
        // DATA TAP TAP
        $paramSeries["start_periode"] = $startDate;
        $paramSeries["end_periode"] = $endDate;
        $sqlDataSeriesPeriodes = "SELECT date_trunc('day', dd):: date as series_periode
            FROM generate_series
            ( :start_periode::timestamp 
             , :end_periode::timestamp
             , '1 day'::interval) dd
        ";
        $dataSeriesPeriodes =  DB::select($sqlDataSeriesPeriodes, $paramSeries);
        $tapTapCurrentPeriodeData = [];
        #######################################################
        if (isset($input["taptap_user_id"])) {
            $tapTapUserId = $input["taptap_user_id"];
            # DATA TAP TAP MONTH BEFORE
            $endpointPM = taptapEndPointUrl() . "/history/presence/" . $tapTapUserId;
            $paramsPM = [
                "start_date" => $firstDayofPreviousMonth,
                "end_date" => $lastDayofPreviousMonth,
            ];
            $headers = [
                "client-id: " . taptapIntegrationDataDesired('taptap_client_id'),
                "client-secret: " . taptapIntegrationDataDesired('taptap_secret_key'),
            ];
            $urlPM = $endpointPM . '?' . http_build_query($paramsPM);
            // CURL
            $chPM = curl_init();
            curl_setopt($chPM, CURLOPT_URL, $urlPM);
            curl_setopt($chPM, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($chPM, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($chPM, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
            curl_setopt($chPM, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($chPM, CURLOPT_HTTPHEADER, $headers);
            $outputPreviousMonth = curl_exec($chPM);
            if ($outputPreviousMonth === FALSE) {
                die('Curl error: ' . curl_error($chPM));
            }
            curl_close($chPM);
            $outputPreviousMonth = json_decode($outputPreviousMonth, true);
            ####
            if ($outputPreviousMonth["success"]) {
                $workDayLateCount = 0;
                $workDayMinuteLateCount = 0;
                $ttRawDataPM = $outputPreviousMonth["data"]["data"];
                if (!empty($ttRawDataPM)) {
                    foreach ($ttRawDataPM as $kPM) {
                        $tapTapPreviousMonthData[$kPM["date"]] = $kPM;
                        #hitung hari terlambat
                        if ($kPM["late"] > 0) {
                            $workDayLateCount++;
                            $workDayMinuteLateCount = $workDayMinuteLateCount + $kPM["late"];
                        }
                    }
                }
            }

            #####################################################################
            #####################################################################
            # DATA TAP TAP CURRENT PERIODE 
            $endpointDT = taptapEndPointUrl() . "/history/presence/" . $tapTapUserId;
            $paramsDT = [
                "start_date" => $startDate,
                "end_date" => $endDate,
            ];
            $headers = [
                "client-id: " . taptapIntegrationDataDesired('taptap_client_id'),
                "client-secret: " . taptapIntegrationDataDesired('taptap_secret_key'),
            ];
            $urlDT = $endpointDT . '?' . http_build_query($paramsDT);
            // CURL
            $chDT = curl_init();
            curl_setopt($chDT, CURLOPT_URL, $urlDT);
            curl_setopt($chDT, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($chDT, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($chDT, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
            curl_setopt($chDT, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($chDT, CURLOPT_HTTPHEADER, $headers);
            $outputCurrentPeriode = curl_exec($chDT);
            if ($outputCurrentPeriode === FALSE) {
                die('Curl error: ' . curl_error($chDT));
            }
            curl_close($chDT);
            $outputCurrentPeriode = json_decode($outputCurrentPeriode, true);
            ####
            if ($outputCurrentPeriode["success"]) {
                $workDayCount = 0;
                $workDayLostCount = 0;
                $ttRawDataCP = $outputCurrentPeriode["data"]["data"];
                if (!empty($ttRawDataCP))
                    foreach ($ttRawDataCP as $kCP) {
                        $tapTapCurrentPeriodeData[$kCP["date"]] = $kCP;
                        #hitung hari kerja dan ijin
                        if (!$kCP["absent"]) {
                            $workDayCount++;
                        }
                        if ($kCP["permit"]) {
                            $workDayLostCount++;
                        }
                    }
            }
        }
        ####
        array_map(function ($key) use ($tapTapPreviousMonthData) {
            foreach ($key as $field => $value) {
                $key->rel_working_category_id = null;
                $key->checkin = null;
                $key->checkout = null;
                $key->late = null;
                $key->status_presence = null;
                if (isset($tapTapPreviousMonthData[$key->series_periode])) {
                    $key->rel_working_category_id = $tapTapPreviousMonthData[$key->series_periode]["office_hours_name"];
                    $key->checkin = $tapTapPreviousMonthData[$key->series_periode]["checkin"];
                    $key->checkout = $tapTapPreviousMonthData[$key->series_periode]["checkout"];
                    $key->late = $tapTapPreviousMonthData[$key->series_periode]["late"];
                    $statusPresencePM = null;
                    if ($tapTapPreviousMonthData[$key->series_periode]["permit"]) {
                        $statusPresencePM = "Ijin";
                    } else {
                        $statusPresencePM = "Masuk";
                    }
                    $key->status_presence = $statusPresencePM;
                }
            }
            return $key;
        }, $dataSeriesPeriodesMonthBefore);
        $objectDataTaptapMonthBefore = $dataSeriesPeriodesMonthBefore;

        ///////////////

        array_map(function ($key) use ($tapTapCurrentPeriodeData) {
            foreach ($key as $field => $value) {
                $key->rel_working_category_id = null;
                $key->checkin = null;
                $key->checkout = null;
                $key->late = null;
                $key->status_presence = null;
                if (isset($tapTapCurrentPeriodeData[$key->series_periode])) {
                    $key->rel_working_category_id = $tapTapCurrentPeriodeData[$key->series_periode]["office_hours_name"];
                    $key->checkin = $tapTapCurrentPeriodeData[$key->series_periode]["checkin"];
                    $key->checkout = $tapTapCurrentPeriodeData[$key->series_periode]["checkout"];
                    $key->late = $tapTapCurrentPeriodeData[$key->series_periode]["late"];
                    $statusPresencePM = null;
                    if ($tapTapCurrentPeriodeData[$key->series_periode]["permit"]) {
                        $statusPresencePM = "Ijin";
                    } else {
                        $statusPresencePM = "Masuk";
                    }
                    $key->status_presence = $statusPresencePM;
                }
            }
            return $key;
        }, $dataSeriesPeriodes);

        $objectDataTaptap = $dataSeriesPeriodes;

        $objectRecaps = [
            "work_day_count" => $workDayCount,
            "work_day_lost_count" => $workDayLostCount,
            "work_day_late_count" => $workDayLateCount,
            "work_day_minute_late_count" => $workDayMinuteLateCount,
        ];

        $response["data"] = [
            "data_taptap_month_before" => $objectDataTaptapMonthBefore,
            "data_taptap" => $objectDataTaptap,
            "data_recaps" => $objectRecaps
        ];
        $response["message"] = __("message.successfullySynchronized");
        return $response;
    }

    protected function validation()
    {
        return [
            "id" => "required",
            "periode_month" => "required",
            "start_date" => "required",
            "end_date" => "required",
        ];
    }
}
