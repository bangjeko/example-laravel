<?php

namespace App\Services\Custom\PresenceOnPeriodeRecaps;

use App\CoreService\CallService;
use App\CoreService\CoreException;
use App\CoreService\CoreService;
use App\Models\BpjsTkBillDetails;
use App\Models\BpjsTkBillDetailUpahComponents;
use App\Models\BpjsTkBills;
use App\Models\ConfigProsenBpjsTenagaKerja;
use App\Models\PresenceOnPeriodeRecaps;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Carbon\Carbon;

class PresenceOnPeriodeRecapsEdit extends CoreService
{

    public $transaction = true;
    public $task = null;

    public function prepare($input)
    {
        $model = "presence-on-periode-recaps";
        $classModel = "\\App\\Models\\" . Str::ucfirst(Str::camel($model));
        $permission = "create-" . $model;
        $authRoles = getRoleName(Auth::user()->role_id);
        if (!class_exists($classModel))
            throw new CoreException(__("message.model404", ['model' => $model]), 404);
        if (!$classModel::IS_ADD)
            throw new CoreException("Not found", 404);
        if (!hasPermission($permission))
            throw new CoreException(__("message.forbidden403", ['permission' => $permission, 'roles' => $authRoles]), 403);
        $input["class_model"] = $classModel;
        return $input;
    }

    public function process($input, $originalInput)
    {
        // SYNCHRONIZE DATA FROM TAP TAP 
        $dataPresenceOnPeriodeRecaps = PresenceOnPeriodeRecaps::find($input["id"]);
        $dataPresenceOnPeriodeRecaps->description = $input["description"];
        $dataPresenceOnPeriodeRecaps->status_code = "synchronized";
        $dataPresenceOnPeriodeRecaps->work_day_count = isset($input["work_day_count"]) ? $input["work_day_count"] : 0;
        $dataPresenceOnPeriodeRecaps->work_day_lost_count = isset($input["work_day_lost_count"]) ? $input["work_day_lost_count"] : 0;
        $dataPresenceOnPeriodeRecaps->work_day_late_count = isset($input["work_day_late_count"]) ? $input["work_day_late_count"] : 0;
        $dataPresenceOnPeriodeRecaps->work_day_minute_late_count = isset($input["work_day_minute_late_count"]) ? $input["work_day_minute_late_count"] : 0;
        $dataPresenceOnPeriodeRecaps->save();


        // THEN SHOW
        $classModel = $input["class_model"];
        $selectableList = [];
        $tableJoinList = [];
        $params = ["id" => $input["id"]];

        foreach ($classModel::FIELD_VIEW as $list) {
            $selectableList[] = $classModel::TABLE . "." . $list;
        }

        $i = 0;
        $FIELD_RELATION = [
            "presence_on_periode_id" => [
                "linkTable" => "presence_on_periodes",
                "aliasTable" => "B",
                "linkField" => "id",
                "displayName" => "rel_presence_on_periode_id",
                "selectFields" => ["id"],
                "selectValue" => "id AS rel_presence_on_periode_id"
            ],
            "employee_id" => [
                "linkTable" => "employees",
                "aliasTable" => "C",
                "linkField" => "id",
                "displayName" => "rel_employee_id",
                "selectFields" => ["fullname"],
                "selectValue" => "id AS rel_employee_id"
            ],
            "created_by" => [
                "linkTable" => "users",
                "aliasTable" => "D",
                "linkField" => "id",
                "displayName" => "rel_created_by",
                "selectFields" => ["username"],
                "selectValue" => "id AS rel_created_by"
            ],
            "updated_by" => [
                "linkTable" => "users",
                "aliasTable" => "E",
                "linkField" => "id",
                "displayName" => "rel_updated_by",
                "selectFields" => ["username"],
                "selectValue" => "id AS rel_updated_by"
            ],
        ];
        foreach ($FIELD_RELATION as $key => $relation) {
            $alias = toAlpha($i + 1);
            ///
            $fieldDisplayed = "CONCAT_WS (' - ',";
            foreach ($relation["selectFields"] as $keyField) {
                $fieldDisplayed .= $alias . '.' . $keyField . ",";
            }
            $fieldDisplayed = substr($fieldDisplayed, 0, strlen($fieldDisplayed) - 1);
            $fieldDisplayed .= ") AS " . $relation["displayName"];
            $selectableList[] = $fieldDisplayed;
            ///
            $tableJoinList[] = "LEFT JOIN " . $relation["linkTable"] . " " . $alias . " ON " .
                $classModel::TABLE . "." . $key . " = " .  $alias . "." . $relation["linkField"];
            $i++;
        }

        if (!empty($classModel::CUSTOM_SELECT)) $selectableList[] = $classModel::CUSTOM_SELECT;

        $condition = " WHERE " . $classModel::TABLE . ".id = :id";

        $sql = "SELECT " . implode(", ", $selectableList) . ", B.periode_month, EMP.date_of_birth, EMP.nomor_kpj_bpjs_tk, JP.job_position_name as rel_job_position_id, MSE.name as rel_status_employment_id
            FROM " . $classModel::TABLE . " " .
            implode(" ", $tableJoinList) . "
            LEFT JOIN employee_on_periodes EOP on presence_on_periode_recaps.employee_id = EOP.employee_id AND EOP.periode_month = B.periode_month 
            LEFT JOIN employees EMP on presence_on_periode_recaps.employee_id = EMP.id 
            LEFT JOIN job_positions JP ON EOP.job_position_id = JP.id
            LEFT JOIN master_status_employments MSE ON EOP.status_employment_id = MSE.id
            " . $condition;


        $object =  DB::selectOne($sql, $params);
        if (is_null($object)) {
            throw new CoreException(__("message.dataNotFound", ['id' => $input["id"]]));
        }

        // END SHOW

        $response["data"] = $object;
        $response["message"] = "Sukses. Tapi Sek durung bar pak, ngenteni api ne pak hendrik";
        return $response;
    }

    protected function validation()
    {
        return [
            "periode_month" => "required"
        ];
    }
}
