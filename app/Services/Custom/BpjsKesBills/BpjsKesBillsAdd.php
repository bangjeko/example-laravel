<?php

namespace App\Services\Custom\BpjsKesBills;

use App\CoreService\CallService;
use App\CoreService\CoreException;
use App\CoreService\CoreService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;

class BpjsKesBillsAdd extends CoreService
{

    public $transaction = true;
    public $task = null;

    public function prepare($input)
    {
        $model = "bpjs-kes-bills";
        $classModel = "\\App\\Models\\" . Str::ucfirst(Str::camel($model));
        $permission = "create-" . $model;
        $authRoles = getRoleName(Auth::user()->role_id);
        if (!class_exists($classModel))
            throw new CoreException(__("message.model404", ['model' => $model]), 404);
        if (!$classModel::IS_ADD)
            throw new CoreException("Not found", 404);
        if (!hasPermission($permission))
            throw new CoreException(__("message.forbidden403", ['permission' => $permission, 'roles' => $authRoles]), 403);
        $input["class_model"] = $classModel;

        if ($classModel::FIELD_ARRAY) {
            foreach ($classModel::FIELD_ARRAY as $item) {
                $input[$item] = serialize($input[$item]);
            }
        }

        if ($classModel::FIELD_UPLOAD) {
            foreach ($classModel::FIELD_UPLOAD as $item) {
                if (isset($input[$item])) {
                    if (is_array($input[$item])) {
                        $input[$item] = isset($input[$item]["path"]) ? $input[$item]["path"] : $input[$item]["field_value"];
                    }
                }
            }
        }

        $validator = Validator::make($input, $classModel::FIELD_VALIDATION);

        if ($validator->fails()) {
            throw new CoreException($validator->errors()->first());
        }

        if ($classModel::FIELD_UNIQUE) {
            foreach ($classModel::FIELD_UNIQUE as $search) {
                $query = $classModel::whereRaw("true");
                $fieldTrans = [];
                foreach ($search as $key) {
                    $fieldTrans[] = __("field.$key");
                    $query->where($key, $input[$key]);
                };
                $isi = $query->first();
                if (!is_null($isi)) {
                    throw new CoreException(__("message.alreadyExist", ['field' => implode(",", $fieldTrans)]));
                }
            }
        }

        //VALIDASI INPUT PERIODE MONTH TIDAK BOLEH MELEBIHI BULAN INI 

        if (isset($input["periode_month"])) {
            $month = $input["periode_month"];
            $curMonth = date("Y-m-d");
            if ($month > $curMonth) {
                throw new CoreException("Periode Bulan yang Dipilih Melebihi Periode Saat Ini. Silahkan Pilih Periode Bulan Saat ini atau bulan sebelumnya.");
            }
        }

        // CHECK EMPLOYEE ON PERIODES
        $checkEmployeeOnPeriodes = DB::selectOne("SELECT COUNT(id) FROM employee_on_periodes WHERE periode_month = :periode_month", [
            "periode_month" => $input["periode_month"]
        ]);
        if ($checkEmployeeOnPeriodes->count < 1) {
            throw new CoreException("Tidak terdapat data tenaga kerja aktif pada periode penggajian bulan yang dipilih. Silahkan cek kembali tenaga kerja pada bulan ini.");
        }

        // CHECK PREMI BPJS KES ON PERIODE
        $checkPremiOnPeriodes = DB::selectOne("SELECT COUNT(id) FROM config_prosen_bpjs_kesehatan WHERE periode_month = :periode_month", [
            "periode_month" => $input["periode_month"]
        ]);
        if ($checkPremiOnPeriodes->count < 1) {
            throw new CoreException("Tidak terdapat data premi BPJS Kesehatan pada periode bulan yang dipilih. Silahkan cek data premi BPJS Kesehatan pada bulan ini.");
        }
        return $input;
    }

    public function process($input, $originalInput)
    {

        $response = [];
        $classModel = $input["class_model"];

        $input = $classModel::beforeInsert($input);

        $object = new $classModel;
        foreach ($classModel::FIELD_ADD as $item) {
            if ($item == "created_by") {
                $input[$item] = Auth::id();
            }
            if ($item == "updated_by") {
                $input[$item] = Auth::id();
            }
            if (isset($input[$item])) {
                $inputValue = $input[$item] ?? $classModel::FIELD_DEFAULT_VALUE[$item];
                $object->{$item} = ($inputValue !== '') ? $inputValue : null;
            }
        }

        $object->save();

        // MOVE FILE
        foreach ($classModel::FIELD_UPLOAD as $item) {
            $tmpPath = $input[$item] ?? null;

            if (!is_null($tmpPath)) {
                if (!Storage::exists($tmpPath)) {
                    throw new CoreException(__("message.tempFileNotFound", ['field' => $item]));
                }
                $tmpPath = $input[$item];
                $originalname = pathinfo(storage_path($tmpPath), PATHINFO_FILENAME);
                $ext = pathinfo(storage_path($tmpPath), PATHINFO_EXTENSION);

                $newPath = $classModel::FILEROOT . "/" . $originalname . "." . $ext;
                //START MOVE FILE
                if (Storage::exists($newPath)) {

                    $id = 1;
                    $filename = pathinfo(storage_path($newPath), PATHINFO_FILENAME);
                    $ext = pathinfo(storage_path($newPath), PATHINFO_EXTENSION);
                    while (true) {
                        $originalname = $filename . "($id)." . $ext;
                        if (!Storage::exists($classModel::FILEROOT . "/" . $originalname))
                            break;
                        $id++;
                    }
                    $newPath = $classModel::FILEROOT . "/" . $originalname;
                }

                $ext = pathinfo(storage_path($newPath), PATHINFO_EXTENSION);
                $object->{$item} = $newPath;
                Storage::move($tmpPath, $newPath);
            }
        }
        //END MOVE FILE

        $object->save();
        $displayedDataAfterInsert["id"] = $object->id;
        $displayedDataAfterInsert["model"] = $classModel::TABLE;
        $displayedDataAfterInsert = CallService::run("Find", $displayedDataAfterInsert);
        if (isset($displayedDataAfterInsert->original["data"])) {
            $displayedDataAfterInsert = $displayedDataAfterInsert->original["data"];
        } else {
            $displayedDataAfterInsert = $displayedDataAfterInsert->original;
            throw new CoreException($displayedDataAfterInsert, 500);
        }

        //AFTER INSERT
        $afterInsertedRespnese = $classModel::afterInsert($displayedDataAfterInsert, $input);


        #1 INSERT BPJS KES BILL DETAILS : EMPLOYEES BASED ON MAPPING
        # CALCULATE UPAH BASED ON MAPPING UPAH BPJS
        # JOB POSITION, STATUS EMPLOYMENT BASED ON TABLE EMPLOYEE ON PERIODES
        $paramsInsertDetail["bpjs_kes_bill_id"] = $object->id;
        $paramsInsertDetail["periode_month"] = $object->periode_month;
        $paramsInsertDetail["created_by"] = $object->created_by;
        $paramsInsertDetail["created_at"] = $object->created_at;
        $paramsInsertDetail["updated_by"] = $object->updated_by;
        $paramsInsertDetail["updated_at"] = $object->updated_at;
        $sqlDataDetail = "WITH insert_bpjs_kes_bill_details AS (
            INSERT INTO bpjs_kes_bill_details (bpjs_kes_bill_id, employee_id, created_by,created_at,updated_by,updated_at)
            SELECT :bpjs_kes_bill_id, employee_id, :created_by, :created_at, :updated_by, :updated_at
            FROM employee_on_periodes EOP WHERE EOP.periode_month = :periode_month
            ON CONFLICT DO NOTHING
            RETURNING id, employee_id
        ), insert_bpjs_kes_bill_detail_upah_components AS (
            INSERT INTO bpjs_kes_bill_detail_upah_components (bpjs_kes_bill_detail_id, salary_component_id, koefisien, default_value, component_value, created_by,created_at,updated_by,updated_at)
            SELECT DISTINCT DTL.id, MUPBPJS.salary_component_id, MUPBPJS.koefisien, MTSE.default_value, MUPBPJS.koefisien * MTSE.default_value, :created_by, :created_at, :updated_by, :updated_at
            FROM insert_bpjs_kes_bill_details DTL, 
            mapping_template_salary_employees MTSE 
            JOIN employee_on_periodes EOPA ON EOPA.employee_id =  MTSE.employee_id
            JOIN job_positions JP ON EOPA.job_position_id = JP.id
            JOIN master_job_position_categories MJPC ON JP.job_position_category_id = MJPC.id
            JOIN mapping_upah_bpjs MUPBPJS ON MJPC.id = MUPBPJS.job_position_category_id AND MUPBPJS.status_employment_id = EOPA.status_employment_id AND MUPBPJS.salary_component_id = MTSE.salary_component_id AND MTSE.periode_month = :periode_month
            WHERE MUPBPJS.active = 1 AND EOPA.employee_id = DTL.employee_id
            ON CONFLICT DO NOTHING
        ), insert_bpjs_kes_bill_detail_tambahan AS (
            INSERT INTO bpjs_kes_bill_detail_tambahan (bpjs_kes_bill_detail_id, employee_family_id, created_by,created_at,updated_by,updated_at)
            SELECT DISTINCT DTL.id, EF.id, :created_by, :created_at, :updated_by, :updated_at
            FROM insert_bpjs_kes_bill_details DTL, 
            employee_families EF
            JOIN employee_on_periodes EOPA ON EOPA.employee_id =  EF.employee_id
            WHERE EF.is_bpjs_tambahan = 'true' AND EOPA.employee_id = DTL.employee_id
            ON CONFLICT DO NOTHING
        )
        SELECT * FROM insert_bpjs_kes_bill_details
        ";

        $insertDataDetail =  DB::insert($sqlDataDetail, $paramsInsertDetail);

        // THEN UPDATE UPAH
        $paramUpdateUpah["bpjs_kes_bill_id"] = $object->id;
        $sqlUpdateUpah = "UPDATE bpjs_kes_bill_details 
        SET upah_value = (SELECT SUM(component_value) FROM bpjs_kes_bill_detail_upah_components WHERE bpjs_kes_bill_detail_id = bpjs_kes_bill_details.id )
        WHERE bpjs_kes_bill_id = :bpjs_kes_bill_id";
        $updateUpah = DB::statement($sqlUpdateUpah, $paramUpdateUpah);


        # SELANJUTNYA COPY DATA PREMI UNTUK PERIODE BULAN SELANJUTNYA
        $periodeMonthNext = Carbon::parse($object->periode_month)->addMonth()->format('Y-m-d');
        $checkDataPremiNM = DB::selectOne("SELECT * FROM config_prosen_bpjs_kesehatan WHERE periode_month = :periode_month_next", [
            "periode_month_next" => $periodeMonthNext
        ]);
        if (is_null($checkDataPremiNM)) {
            $paramsInsertPNM["periode_month"] = $object->periode_month;
            $paramsInsertPNM["periode_month_next"] = $periodeMonthNext;
            $paramsInsertPNM["created_by"] = $object->created_by;
            $paramsInsertPNM["created_at"] = $object->created_at;
            $paramsInsertPNM["updated_by"] = $object->updated_by;
            $paramsInsertPNM["updated_at"] = $object->updated_at;
            DB::insert("INSERT INTO config_prosen_bpjs_kesehatan (periode_month, prosen_premi_company, prosen_premi_employee, upah_min_value, upah_max_value, description, created_by,created_at,updated_by,updated_at)
            SELECT :periode_month_next, prosen_premi_company, prosen_premi_employee, upah_min_value, upah_max_value, description, :created_by, :created_at, :updated_by, :updated_at
            FROM config_prosen_bpjs_kesehatan WHERE periode_month = :periode_month
            ON CONFLICT DO NOTHING", $paramsInsertPNM);
        }

        $response["data"] = $displayedDataAfterInsert;
        $response["after_inserted_response"] = $afterInsertedRespnese;
        $response["check"] = $insertDataDetail;
        $response["message"] = __("message.successfullyAdd");

        return $response;
    }

    protected function validation()
    {
        return [];
    }
}
