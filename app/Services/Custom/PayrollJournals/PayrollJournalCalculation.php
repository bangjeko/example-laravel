<?php

namespace App\Services\Custom\PayrollJournals;

use App\CoreService\CoreException;
use Illuminate\Support\Facades\DB;
use App\CoreService\CoreService;
use App\Models\EmployeeOnPeriodes;
use App\Models\PayrollPeriodes;
use App\Models\SalaryComponentOnPeriodes;
use Illuminate\Support\Facades\Auth;
use Mavinoo\Batch\BatchFacade;

class PayrollJournalCalculation extends CoreService
{

    public $transaction = false;
    public $permission = null;

    public function prepare($input)
    {
        $permission = "payroll-journal-calculation";
        $authRoles = getRoleName(Auth::user()->role_id);
        if (!hasPermission($permission))
            throw new CoreException(__("message.forbidden403", ['permission' => $permission, 'roles' => $authRoles]), 403);
        return $input;
    }

    public function process($input, $originalInput)
    {
        $response = [];

        $periodeMonth = $input["periode_month"];
        $checkPayrollPeriodeExist = PayrollPeriodes::where('periode_month', '=', $periodeMonth)->first();
        if (is_null($checkPayrollPeriodeExist)) {
            throw new CoreException(__("message.periodePenggajianNotFound", ['periode_month' => $input["periode_month"]]));
        }

        // if($checkPayrollPeriodeExist->is_lock){
        //     throw new CoreException(__("message.periodePenggajianLocked", ['periode_month' => $input["periode_month"]]));
        // }

        #DELETE DATA BASED ON PERIODE
        $sqlDelete = "DELETE 
        FROM 
              salary_component_account_proportions AS a 
        USING 
              salary_component_on_periodes AS b,
              employee_on_periodes AS c
        WHERE 
              a.salary_component_on_periode_id = b.id 
          AND b.employee_on_periode_id = c.id
          AND c.periode_month = :periode_month ";
        DB::statement($sqlDelete, [
            "periode_month" => $input["periode_month"]
        ]);

        # DATA MAPPING PROPORTION
        $dataMappingProportions = DB::select("SELECT B.job_position_category_id, B.status_employment_id, B.salary_component_id,
        A.fina_salary_account_id, A.proportion_value
        FROM mapping_salary_account_proportions A
        JOIN mapping_salary_accounts B ON A.mapping_salary_account_id = B.id
        WHERE B.active = 1 AND A.active = 1");

        // $cek = multiArraySearchReturnArray($dataMappingProportions, [
        //     "job_position_category_id" => 1,
        //     "status_employment_id" => 1,
        //     "salary_component_id" => 1,
        // ]);

        #looping salary_component_on_periodes
        $params["periode_month"] = $periodeMonth;
        $sql = "SELECT A.id as salary_component_on_periode_id, JP.job_position_category_id, B.status_employment_id, 
        A.salary_component_id, A.salary_component_value  
        FROM salary_component_on_periodes A
        JOIN employee_on_periodes B ON A.employee_on_periode_id = B.id
        JOIN master_salary_components MSC ON A.salary_component_id = MSC.id
        JOIN employees EMP ON B.employee_id = EMP.id
        JOIN job_positions JP ON B.job_position_id = JP.id
        LEFT JOIN custom_salary_modules CSC ON MSC.custom_salary_module_id = CSC.id
        WHERE B.periode_month = :periode_month";
        $salaryOnPeriodes = DB::select($sql, $params);


        $salaryComponentShouldUpdated = [];
        foreach ($salaryOnPeriodes as $k) {
            $salaryComponentOnPeriodeId = $k->salary_component_on_periode_id;
            $jobPositionCategoryId = $k->job_position_category_id;
            $statusEmploymentId = $k->status_employment_id;
            $salaryComponentId = $k->salary_component_id;
            $salaryComponentValue = $k->salary_component_value;

            #FIND PROPORTION
            $proportionData = multiArraySearchReturnArray($dataMappingProportions, [
                "job_position_category_id" => $jobPositionCategoryId,
                "status_employment_id" => $statusEmploymentId,
                "salary_component_id" => $salaryComponentId,
            ]);

            if (!is_null($proportionData)) {
                foreach ($proportionData as $kProp) {
                    $dataInserted = [];
                    $dataInserted["salary_component_on_periode_id"] = $salaryComponentOnPeriodeId;
                    $dataInserted["fina_salary_account_id"] = $kProp["fina_salary_account_id"];
                    $dataInserted["proportion_value"] = $kProp["proportion_value"];

                    array_push($salaryComponentShouldUpdated, $dataInserted);
                }
            }
        }
        # INSERT DATA
        $insertedData = DB::table('salary_component_account_proportions')->insert($salaryComponentShouldUpdated);


        $response["check"] = $insertedData;
        $response["success"] = true;
        $response["message"] = __("message.successfullyCalulateAllSalaries");

        return $response;
    }

    protected function validation()
    {
        return [
            "periode_month" => "required"
        ];
    }
}
