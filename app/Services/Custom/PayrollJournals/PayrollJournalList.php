<?php

namespace App\Services\Custom\PayrollJournals;

use App\CoreService\CoreException;
use App\CoreService\CoreService;
use App\Models\EmployeeOnPeriodes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;

class PayrollJournalList extends CoreService
{

    public $transaction = false;
    public $task = null;

    public function prepare($input)
    {
        $permission = "view-payroll-journals";
        $authRoles = getRoleName(Auth::user()->role_id);
        if (!hasPermission($permission))
            throw new CoreException(__("message.forbidden403", ['permission' => $permission, 'role' => $authRoles]), 403);

        return $input;
    }

    public function process($input, $originalInput)
    {
        $selectableList = [];
        $sortBy = "FSA.id";
        $sort = strtoupper($input["sort"] ?? "ASC") == "DESC" ? "DESC" : "ASC";

        if (isset($input["sort_by"])) {
            $sortBy = $input["sort_by"];
        }

        $searchableList = ["FSA.account_name", "FSA.account_code", "CONCAT(FSA.account_name, ' - ', FSA.account_code)"];

        $tableJoinList = [];
        $filterList = [];
        $params = [];

        $FIELD_FILTERABLE = [
            "periode_month" => [
                "operator" => "=",
            ],
            "account_code" => [
                "operator" => "=",
            ],
            "account_name" => [
                "operator" => "=",
            ],
            "debit_kredit" => [
                "operator" => "=",
            ],

        ];
        foreach ($FIELD_FILTERABLE as $filter => $operator) {
            if (!is_blank($input, $filter)) {
                if ($filter == 'periode_month') {
                    $filterList[] = " AND EOP." . $filter .  " " . $operator["operator"] . " :$filter";
                    $params[$filter] = $input[$filter];
                } else {
                    $filterList[] = " AND FSA." . $filter .  " " . $operator["operator"] . " :$filter";
                    $params[$filter] = $input[$filter];
                }
            }
        }

        $condition = " WHERE true";

        if (!is_blank($input, "search")) {
            $searchableList = array_map(function ($item) {
                return $item . " ILIKE :search";
            }, $searchableList);
        } else {
            $searchableList = [];
        }

        if (count($searchableList) > 0 && !is_blank($input, "search"))
            $params["search"] = "%" . strtoupper($input["search"] ?? "") . "%";

        if (isset($input["limit"])) {
            if ($input["limit"] == 'null') {
                $limit = 'null';
            } else {
                $limit = $input["limit"];
            }
        } else {
            $limit = 'null';
        }
        $offset = $input["offset"] ?? 0;
        if (!is_null($input["page"] ?? null)) {
            if ($limit == 'null') {
                $offset = 'null';
            } else {
                $offset = $limit * ($input["page"] - 1);
            }
        }

        $sql = "SELECT FSA.id as fina_salary_account_id, FSA.account_name, FSA.account_code, CONCAT(FSA.account_name, ' - ', FSA.account_code) as concat_account_name_account_code, FSA.debit_kredit, 
        CASE 
			WHEN FSA.debit_kredit = 'kredit' THEN sum(A.proportion_value * B.salary_component_value / 100) * -1
			ELSE sum(A.proportion_value * B.salary_component_value / 100)
		END as sum_value
        FROM salary_component_account_proportions A
        JOIN salary_component_on_periodes B ON A.salary_component_on_periode_id = B.id
        JOIN employee_on_periodes EOP ON B.employee_on_periode_id = EOP.id
        JOIN fina_salary_accounts FSA ON A.fina_salary_account_id = FSA.id"
            . $condition .
            (count($searchableList) > 0 ? " AND (" . implode(" OR ", $searchableList) . ")" : "") .
            implode("\n", $filterList) .
            " GROUP BY FSA.id 
            ORDER BY " . $sortBy . " " . $sort . " LIMIT $limit OFFSET $offset ";

        $sqlForCount = "WITH data_sql AS (
            SELECT FSA.id as fina_salary_account_id, FSA.account_name, FSA.account_code, CONCAT(FSA.account_name, ' - ', FSA.account_code) as concat_account_name_account_code, FSA.debit_kredit, 
        sum(A.proportion_value * B.salary_component_value / 100) as sum_value
        FROM salary_component_account_proportions A
        JOIN salary_component_on_periodes B ON A.salary_component_on_periode_id = B.id
        JOIN employee_on_periodes EOP ON B.employee_on_periode_id = EOP.id
        JOIN fina_salary_accounts FSA ON A.fina_salary_account_id = FSA.id"
            . $condition .
            (count($searchableList) > 0 ? " AND (" . implode(" OR ", $searchableList) . ")" : "") .
            implode("\n", $filterList) .
            " GROUP BY FSA.id
        )
        SELECT sum(sum_value) as total_sum_value ,COUNT(1) AS total FROM data_sql";

        $object =  DB::select($sql, $params);

        array_map(function ($key) {
            foreach ($key as $field => $v) {
                $key->sum_value = (float) $key->sum_value;
            }
            return $key;
        }, $object);

        $total = DB::selectOne($sqlForCount, $params)->total;
        if ($limit == 'null') {
            $totalPage = 1;
        } else {
            $totalPage = ceil($total / $limit);
        }

        ###################################
        $sqlSummaryData = "WITH data_sql AS (
        SELECT FSA.id as fina_salary_account_id, FSA.account_name, FSA.account_code, CONCAT(FSA.account_name, ' - ', FSA.account_code) as concat_account_name_account_code, FSA.debit_kredit, 
            CASE 
			WHEN FSA.debit_kredit = 'kredit' THEN sum(A.proportion_value * B.salary_component_value / 100) * -1
			ELSE sum(A.proportion_value * B.salary_component_value / 100)
		END as sum_value
        FROM salary_component_account_proportions A
        JOIN salary_component_on_periodes B ON A.salary_component_on_periode_id = B.id
        JOIN employee_on_periodes EOP ON B.employee_on_periode_id = EOP.id
        JOIN fina_salary_accounts FSA ON A.fina_salary_account_id = FSA.id"
            . $condition .
            (count($searchableList) > 0 ? " AND (" . implode(" OR ", $searchableList) . ")" : "") .
            implode("\n", $filterList) .
            " GROUP BY FSA.id
        )
        SELECT debit_kredit, sum(sum_value) as total_sum_value FROM data_sql
        GROUP BY debit_kredit";
        $smData = DB::select($sqlSummaryData, $params);


        $totalDebit = 0;
        $totalKredit = 0;
        foreach ($smData as $kSmd) {
            if ($kSmd->debit_kredit == 'debit') {
                $totalDebit = (float) $kSmd->total_sum_value;
            } else if ($kSmd->debit_kredit == 'kredit') {
                $totalKredit = (float) $kSmd->total_sum_value;
            }
        }
        $totalValue = $totalDebit + $totalKredit;

        $summaryData = [
            "periode_month" => $input["periode_month"],
            "total_debit_value" => $totalDebit,
            "total_kredit_value" => $totalKredit,
            "total_value" => $totalValue
        ];

        ###################################

        return [
            "data" => $object,
            "total" => $total,
            "totalPage" => $totalPage,
            "summary_data" => $summaryData,
        ];
    }

    protected function validation()
    {
        return [
            "periode_month" => "required"
        ];
    }
}
