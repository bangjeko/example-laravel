<?php

namespace App\Services\Custom\DashboardHrm;

use App\CoreService\CoreException;
use App\CoreService\CoreService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DashboardEmployeeByAge extends CoreService
{
    public $transaction = false;
    public $task = null;
    public function prepare($input)
    {
        $permission = "view-dashboard-hrm";
        $authRoles = getRoleName(Auth::user()->role_id);
        if (!hasPermission($permission))
            throw new CoreException(__("message.forbidden403", ['permission' => $permission, 'roles' => $authRoles]), 403);

        return $input;
    }

    public function process($input, $originalInput)
    {

        // START GET COUNT EMPLOYEE ACTIVE
        $sqlEmployeeActive = "WITH data AS (
            SELECT fullname, date_of_birth, date_part('year', age(now(), date_of_birth)) as age, 
            CASE 
                WHEN date_part('year', age(now(), date_of_birth)) < 18 THEN 'A' 
                WHEN date_part('year', age(now(), date_of_birth)) >= 18 AND date_part('year', age(now(), date_of_birth)) <= 24 THEN 'B' 
                WHEN date_part('year', age(now(), date_of_birth)) >= 25 AND date_part('year', age(now(), date_of_birth)) <= 34 THEN 'C' 
                WHEN date_part('year', age(now(), date_of_birth)) >= 35 AND date_part('year', age(now(), date_of_birth)) <= 44 THEN 'D'
                WHEN date_part('year', age(now(), date_of_birth)) >= 45 AND date_part('year', age(now(), date_of_birth)) <= 54 THEN 'E'
                WHEN date_part('year', age(now(), date_of_birth)) >= 55 AND date_part('year', age(now(), date_of_birth)) <= 65 THEN 'F' 
                WHEN date_part('year', age(now(), date_of_birth)) > 65 THEN 'G' 
                ELSE 'undefined'
            END as range_code
            FROM employees
            WHERE status_code = 'active'
        ), data_range AS (
            SELECT range_code, range_name 
            FROM (
                    values ('A','<18'), ('B','18 - 24'), ('C','25 - 34'), ('D','35 - 44'), ('E','45 - 54'), ('F','55 - 65'), ('G','>65')
            ) s(range_code,range_name)
        )
        SELECT A.range_code, A.range_name, COUNT(B.*) as value 
        FROM data_range A
        LEFT JOIN data B ON A.range_code = B.range_code
        GROUP BY A.range_code, A.range_name
        ORDER BY A.range_code";



        $objectEmployeeActive = DB::select($sqlEmployeeActive);

        return [
            "data" => $objectEmployeeActive,
        ];
    }

    protected function validation()
    {
        return [];
    }
}
