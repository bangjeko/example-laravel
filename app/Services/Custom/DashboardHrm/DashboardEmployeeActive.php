<?php

namespace App\Services\Custom\DashboardHrm;

use App\CoreService\CoreException;
use App\CoreService\CoreService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DashboardEmployeeActive extends CoreService
{
    public $transaction = false;
    public $task = null;
    public function prepare($input)
    {
        $permission = "view-dashboard-hrm";
        $authRoles = getRoleName(Auth::user()->role_id);
        if (!hasPermission($permission))
            throw new CoreException(__("message.forbidden403", ['permission' => $permission, 'roles' => $authRoles]), 403);

        return $input;
    }

    public function process($input, $originalInput)
    {
        // START EMPLOYEE ACTIVE
        $sqlEmployeeActive = "SELECT COUNT(*) as count_value FROM employees WHERE status_code = :status_code";
        $objectEmployeeActive = DB::selectOne($sqlEmployeeActive, [
            "status_code" => "active"
        ]);

        // START EMPLOYEE MPP
        $sqlEmployeeMppActive = "SELECT COUNT(*) as count_value FROM employees WHERE is_mpp = true AND status_code = :status_code";
        $objectEmployeeMppActive = DB::selectOne($sqlEmployeeMppActive, [
            "status_code" => "active"
        ]);

        // START EMPLOYEE PLT
        $sqlEmployeePltActive = "SELECT COUNT(*) as count_value FROM employees WHERE is_plt = true AND status_code = :status_code";
        $objectEmployeePltActive = DB::selectOne($sqlEmployeePltActive, [
            "status_code" => "active"
        ]);

        return [
            "data" => [
                "count_active_value" => $objectEmployeeActive->count_value,
                "count_mpp_value" => $objectEmployeeMppActive->count_value,
                "count_plt_value" => $objectEmployeePltActive->count_value,
            ],
        ];
    }

    protected function validation()
    {
        return [];
    }
}
