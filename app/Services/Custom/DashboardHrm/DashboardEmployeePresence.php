<?php

namespace App\Services\Custom\DashboardHrm;

use App\CoreService\CoreException;
use App\CoreService\CoreService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DashboardEmployeePresence extends CoreService
{
    public $transaction = false;
    public $task = null;
    public function prepare($input)
    {
        $permission = "view-dashboard-hrm";
        $authRoles = getRoleName(Auth::user()->role_id);
        if (!hasPermission($permission))
            throw new CoreException(__("message.forbidden403", ['permission' => $permission, 'roles' => $authRoles]), 403);

        return $input;
    }

    public function process($input, $originalInput)
    {

        $filterList = [];
        $params = [];
        $condition = " WHERE true ";

        $fieldFilterable = [
            "start_periode" => [
                "operator" => ">=",
            ],
            "end_periode" => [
                "operator" => "<=",
            ]
        ];
        foreach ($fieldFilterable as $filter => $operator) {
            if (!is_blank($input, $filter)) {
                if ($filter == 'start_periode') {
                    $filterList[] = " AND A.periode_month >= :$filter";
                    $params[$filter] = $input[$filter];
                } else if ($filter == 'end_periode') {
                    $filterList[] = " AND A.periode_month <= :$filter";
                    $params[$filter] = $input[$filter];
                } else {
                    $filterList[] = " AND A." . $filter .  " " . $operator["operator"] . " :$filter";
                    $params[$filter] = $input[$filter];
                }
            }
        }

        // SERIES PERIODE
        $paramSeries["start_periode"] = $input["start_periode"];
        $paramSeries["end_periode"] = $input["end_periode"];
        $sqlDataSeriesPeriodesMasuk = "SELECT date_trunc('day', dd):: date as series_periode
            FROM generate_series
            ( :start_periode::timestamp 
             , :end_periode::timestamp
             , '1 month'::interval) dd
        ";

        // START WORK DAY COUNT
        $sqlWorkDayCount = "SELECT A.periode_month, SUM(B.work_day_count) as count_value FROM presence_on_periode_recaps B
        JOIN presence_on_periodes A ON B.presence_on_periode_id = A.id
        " .
            $condition .
            implode("\n", $filterList) . "
        GROUP BY A.periode_month";
        $objectWorkDayCount =  DB::select($sqlWorkDayCount, $params);

        $dataSeriesPeriodesMasuk =  DB::select($sqlDataSeriesPeriodesMasuk, $paramSeries);
        array_map(function ($key) use ($objectWorkDayCount) {
            foreach ($key as $field => $value) {
                $key->value = 0;
                foreach ($objectWorkDayCount as $kOWDC) {
                    if ($key->series_periode === $kOWDC->periode_month) {
                        $key->value = $kOWDC->count_value;
                    }
                }
            }
            return $key;
        }, $dataSeriesPeriodesMasuk);


        // START WORK DAY LOST COUNT
        $sqlWorkDayLostCount = "SELECT A.periode_month, SUM(B.work_day_lost_count) as count_value FROM presence_on_periode_recaps B
        JOIN presence_on_periodes A ON B.presence_on_periode_id = A.id
        " .
            $condition .
            implode("\n", $filterList) . "
        GROUP BY A.periode_month";
        $objectWorkDayLostCount =  DB::select($sqlWorkDayLostCount, $params);
        $dataSeriesPeriodesTidakMasuk =  DB::select($sqlDataSeriesPeriodesMasuk, $paramSeries);
        array_map(function ($key) use ($objectWorkDayLostCount) {
            foreach ($key as $field => $value) {
                $key->value = 0;
                foreach ($objectWorkDayLostCount as $kOWDLC) {
                    if ($key->series_periode === $kOWDLC->periode_month) {
                        $key->value = $kOWDLC->count_value;
                    }
                }
            }
            return $key;
        }, $dataSeriesPeriodesTidakMasuk);
        return [
            "data" => [
                "masuk" => $dataSeriesPeriodesMasuk,
                "tidak_masuk" => $dataSeriesPeriodesTidakMasuk,
            ],
        ];
    }

    protected function validation()
    {
        return [
            "start_periode" => "required",
            "end_periode" => "required"
        ];
    }
}
