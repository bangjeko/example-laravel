<?php

namespace App\Services\Custom\DashboardHrm;

use App\CoreService\CoreException;
use App\CoreService\CoreService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DashboardEmployeeByDivision extends CoreService
{
    public $transaction = false;
    public $task = null;
    public function prepare($input)
    {
        $permission = "view-dashboard-hrm";
        $authRoles = getRoleName(Auth::user()->role_id);
        if (!hasPermission($permission))
            throw new CoreException(__("message.forbidden403", ['permission' => $permission, 'roles' => $authRoles]), 403);

        return $input;
    }

    public function process($input, $originalInput)
    {
        // START DIVISION ACTIVE
        $sqlDivisionActive = "SELECT id, division_name, order_number FROM divisions WHERE active = :active
        ORDER BY order_number ASC";
        $objectDivisionActive = DB::select($sqlDivisionActive, [
            "active" => 1
        ]);

        // START GET COUNT EMPLOYEE ACTIVE
        $sqlEmployeeActive = "SELECT D.id as division_id, D.division_name,  COUNT(EMP.*) as count_value
        FROM employees EMP
        JOIN job_positions JP ON EMP.job_position_id = JP.id
        JOIN divisions D ON JP.division_id = D.id
        WHERE EMP.status_code = 'active'
        GROUP BY D.id";

        $objectEmployeeActive = DB::select($sqlEmployeeActive);
        $arEA = [];

        foreach ($objectEmployeeActive as $kEA) {
            $arEA[$kEA->division_id] = ["division_name" => $kEA->division_name, "count_value" => $kEA->count_value];
        }

        // 
        $objectResult = [];
        foreach ($objectDivisionActive as $kD) {
            $arD = [];
            $arD["id"] = $kD->id;
            $arD["name"] = $kD->division_name;
            $arD["value"] = isset($arEA[$kD->id]) ? $arEA[$kD->id]["count_value"] : 0;

            array_push($objectResult, $arD);
        }
        return [
            "data" => $objectResult,
        ];
    }

    protected function validation()
    {
        return [];
    }
}
