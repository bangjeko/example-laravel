<?php

namespace App\Services\Custom\DashboardHrm;

use App\CoreService\CoreException;
use App\CoreService\CoreService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DashboardEmployeeByJobPositionCategories extends CoreService
{
    public $transaction = false;
    public $task = null;
    public function prepare($input)
    {
        $permission = "view-dashboard-hrm";
        $authRoles = getRoleName(Auth::user()->role_id);
        if (!hasPermission($permission))
            throw new CoreException(__("message.forbidden403", ['permission' => $permission, 'roles' => $authRoles]), 403);

        return $input;
    }

    public function process($input, $originalInput)
    {

        // START GET COUNT EMPLOYEE ACTIVE
        $sqlEmployeeActive = "WITH data_employee AS (
            SELECT EMP.id, EMP.fullname, EMP.job_position_id, EMP.status_employment_id, 
            JP.job_position_category_id
            FROM employees EMP
            JOIN job_positions JP ON EMP.job_position_id = JP.id
            WHERE EMP.status_code = 'active'
        ), data_master_job_position_categories AS (
            SELECT * FROM master_job_position_categories WHERE active = 1 
            ORDER BY order_number ASC
        )
        SELECT A.id, A.name, COUNT(B.*) as value 
        FROM data_master_job_position_categories A
        LEFT JOIN data_employee B ON A.id = B.job_position_category_id
        GROUP BY A.id, A.name, A.order_number
		ORDER BY A.order_number ASC";

        $objectEmployeeActive = DB::select($sqlEmployeeActive);

        return [
            "data" => $objectEmployeeActive,
        ];
    }

    protected function validation()
    {
        return [];
    }
}
