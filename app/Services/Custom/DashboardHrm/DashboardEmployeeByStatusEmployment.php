<?php

namespace App\Services\Custom\DashboardHrm;

use App\CoreService\CoreException;
use App\CoreService\CoreService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DashboardEmployeeByStatusEmployment extends CoreService
{
    public $transaction = false;
    public $task = null;
    public function prepare($input)
    {
        $permission = "view-dashboard-hrm";
        $authRoles = getRoleName(Auth::user()->role_id);
        if (!hasPermission($permission))
            throw new CoreException(__("message.forbidden403", ['permission' => $permission, 'roles' => $authRoles]), 403);

        return $input;
    }

    public function process($input, $originalInput)
    {

        // START GET COUNT EMPLOYEE ACTIVE
        $sqlEmployeeActive = "WITH data_employee AS (
            SELECT id, fullname, status_employment_id
            FROM employees
            WHERE status_code = 'active'
        ), data_status_employment AS (
            SELECT * FROM master_status_employments WHERE active = 1 
            ORDER BY order_number ASC
        )
        SELECT A.id, A.name, COUNT(B.*) as value 
        FROM data_status_employment A
        LEFT JOIN data_employee B ON A.id = B.status_employment_id
        GROUP BY A.id, A.name, A.order_number
		ORDER BY A.order_number ASC";

        $objectEmployeeActive = DB::select($sqlEmployeeActive);

        return [
            "data" => $objectEmployeeActive,
        ];
    }

    protected function validation()
    {
        return [];
    }
}
