<?php

namespace App\Services\Custom\DashboardHrm;

use App\CoreService\CoreException;
use App\CoreService\CoreService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DashboardEmployeeByGender extends CoreService
{
    public $transaction = false;
    public $task = null;
    public function prepare($input)
    {
        $permission = "view-dashboard-hrm";
        $authRoles = getRoleName(Auth::user()->role_id);
        if (!hasPermission($permission))
            throw new CoreException(__("message.forbidden403", ['permission' => $permission, 'roles' => $authRoles]), 403);

        return $input;
    }

    public function process($input, $originalInput)
    {

        // START GET COUNT EMPLOYEE ACTIVE
        $sqlEmployeeActive = "WITH data AS (
            SELECT EMP.id, EMP.gender as gender_code
            FROM employees EMP
            WHERE EMP.status_code = 'active'
        ), data_gender AS (
            SELECT gender_code, gender_name 
            FROM (
                    values ('male','Laki - Laki'), ('female','Perempuan'), ('null','Undefined')
            ) s(gender_code,gender_name)
        )
        SELECT A.gender_code, A.gender_name, COUNT(B.*) as value 
        FROM data_gender A
        LEFT JOIN data B ON A.gender_code = B.gender_code
        GROUP BY A.gender_code, A.gender_name
        ORDER BY A.gender_code";

        $objectEmployeeActive = DB::select($sqlEmployeeActive);

        return [
            "data" => $objectEmployeeActive,
        ];
    }

    protected function validation()
    {
        return [];
    }
}
