<?php

namespace App\Services\Custom\KoperasiSimpanPinjamDetails;

use App\CoreService\CallService;
use App\CoreService\CoreException;
use App\CoreService\CoreService;
use App\Models\KoperasiPinjamanDetails;
use App\Models\KoperasiSimpananDetails;
use App\Models\KoperasiSimpanPinjam;
use App\Models\KoperasiSimpanPinjamDetails;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;

class KoperasiSimpanPinjamDetailsAdd extends CoreService
{

    public $transaction = true;
    public $task = null;

    public function prepare($input)
    {
        $model = "koperasi-simpan-pinjam-details";
        $classModel = "\\App\\Models\\" . Str::ucfirst(Str::camel($model));
        $permission = "create-" . $model;
        $authRoles = getRoleName(Auth::user()->role_id);
        if (!class_exists($classModel))
            throw new CoreException(__("message.model404", ['model' => $model]), 404);
        if (!$classModel::IS_ADD)
            throw new CoreException("Not found", 404);
        if (!hasPermission($permission))
            throw new CoreException(__("message.forbidden403", ['permission' => $permission, 'roles' => $authRoles]), 403);
        $input["class_model"] = $classModel;
        return $input;
    }

    public function process($input, $originalInput)
    {
        $response = [];

        #1 UPDATE koperasi_simpan_pinjam_details

        $dataKoperasiSimpanPinjamDetail = KoperasiSimpanPinjamDetails::find($input["id"]);
        $dataKoperasiSimpanPinjamDetail->description = $input["description"];
        $dataKoperasiSimpanPinjamDetail->status_code = "calculated";

        #2 UPDATE child_koperasi_simpanan_details
        foreach ($input["child_koperasi_simpanan_details"] as $childSimpananItem) {
            if (isset($childSimpananItem["id"])) {
                $childSimpananItem["model"] = "koperasi_simpanan_details";
                $childSimpananItem["koperasi_simpan_pinjam_detail_id"] = $input["id"];
                $childDataSimpananUpdated = CallService::run("Edit", $childSimpananItem);
            }
        }

        #3 UPDATE child_koperasi_pinjaman_details
        foreach ($input["child_koperasi_pinjaman_details"] as $childPinjamanItem) {
            if (isset($childPinjamanItem["id"])) {
                $childPinjamanItem["model"] = "koperasi_pinjaman_details";
                $childPinjamanItem["koperasi_simpan_pinjam_detail_id"] = $input["id"];
                $childDataPinjamanUpdated = CallService::run("Edit", $childPinjamanItem);
            }
        }


        #4 HITUNG JUMLAH SIMPANAN & PINJAMAN
        $jumlahSimpanan = KoperasiSimpananDetails::where('koperasi_simpan_pinjam_detail_id', '=', $input["id"])
            ->sum('comp_value');

        $jumlahPinjaman = KoperasiPinjamanDetails::where('koperasi_simpan_pinjam_detail_id', '=', $input["id"])
            ->sum(DB::raw('pokok_pinjaman_value + bunga_pinjaman_value'));


        $dataKoperasiSimpanPinjamDetail->simpanan_value = $jumlahSimpanan;
        $dataKoperasiSimpanPinjamDetail->pinjaman_value = $jumlahPinjaman;
        $dataKoperasiSimpanPinjamDetail->total_value = $jumlahSimpanan + $jumlahPinjaman;
        $dataKoperasiSimpanPinjamDetail->save();

        #5 UPDATE koperasi_simpan_pinjam

        $totalSimpanan = KoperasiSimpanPinjamDetails::where('koperasi_simpan_pinjam_id', '=', $dataKoperasiSimpanPinjamDetail->koperasi_simpan_pinjam_id)
            ->sum('simpanan_value');
        $totalPinjaman = KoperasiSimpanPinjamDetails::where('koperasi_simpan_pinjam_id', '=', $dataKoperasiSimpanPinjamDetail->koperasi_simpan_pinjam_id)
            ->sum('pinjaman_value');

        KoperasiSimpanPinjam::where('id', '=', $dataKoperasiSimpanPinjamDetail->koperasi_simpan_pinjam_id)
            ->update([
                'total_simpanan_value' => $totalSimpanan,
                'total_pinjaman_value' => $totalPinjaman,
                'total_value' => $totalSimpanan + $totalPinjaman,
            ]);
        $response["data"] = $dataKoperasiSimpanPinjamDetail;
        $response["after_inserted_response"] = $childDataSimpananUpdated;
        $response["message"] = __("message.successfullyAdd");
        return $response;
    }

    protected function validation()
    {
        return [];
    }
}
