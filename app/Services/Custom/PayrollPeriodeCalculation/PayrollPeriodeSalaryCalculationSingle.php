<?php

namespace App\Services\Custom\PayrollPeriodeCalculation;

use App\CoreService\CoreException;
use Illuminate\Support\Facades\DB;
use App\CoreService\CoreService;
use App\Models\EmployeeOnPeriodes;
use App\Models\KoperasiSimpanPinjam;
use App\Models\PayrollPeriodes;
use App\Models\PresenceOnPeriodes;
use App\Models\SalaryComponentOnPeriodes;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Mavinoo\Batch\BatchFacade;

class PayrollPeriodeSalaryCalculationSingle extends CoreService
{

    public $transaction = false;
    public $permission = null;

    public function prepare($input)
    {
        $permission = "salary-calculation";
        $authRoles = getRoleName(Auth::user()->role_id);
        if (!hasPermission($permission))
            throw new CoreException(__("message.forbidden403", ['permission' => $permission, 'roles' => $authRoles]), 403);
        return $input;
    }

    public function process($input, $originalInput)
    {
        $response = [];
        $employeeOnPeriodeId = $input["id"];
        $periodeMonth = $input["periode_month"];
        $checkPayrollPeriodeExist = PayrollPeriodes::where('periode_month', '=', $periodeMonth)->first();
        if (is_null($checkPayrollPeriodeExist)) {
            throw new CoreException(__("message.periodePenggajianNotFound", ['periode_month' => $input["periode_month"]]));
        }

        if($checkPayrollPeriodeExist->is_lock){
            throw new CoreException(__("message.periodePenggajianLocked", ['periode_month' => $input["periode_month"]]));
        }

        $checkEmployeeOnPeriodeExist = EmployeeOnPeriodes::find($employeeOnPeriodeId);
        if (is_null($checkEmployeeOnPeriodeExist)) {
            throw new CoreException(__("message.employeeOnPeriodeNotFound", ['id' => $input["periode_month"]]));
        }

        #1 INSERT BPJS KES BILL DETAILS : EMPLOYEES BASED ON MAPPING
        # CALCULATE UPAH BASED ON MAPPING UPAH BPJS
        # JOB POSITION, STATUS EMPLOYMENT BASED ON TABLE EMPLOYEE ON PERIODES
        $paramsInsertDetail["periode_month"] = $periodeMonth;
        $paramsInsertDetail["employee_on_periode_id"] = $employeeOnPeriodeId;

        $sqlDataDetail = "WITH insert_salary_component_on_periodes AS (
            INSERT INTO salary_component_on_periodes (employee_on_periode_id, salary_component_id, salary_component_value, active)
            SELECT DISTINCT DTL.id, MSC.id as salary_component_id, DM.default_value, DM.active
            FROM employee_on_periodes DTL, 
            master_salary_components MSC 
            LEFT JOIN mapping_template_salary_employees DM ON DM.salary_component_id = MSC.id AND DM.periode_month = :periode_month
            WHERE DTL.periode_month = :periode_month AND DTL.id = :employee_on_periode_id AND DM.employee_id = DTL.employee_id
            ON CONFLICT ON CONSTRAINT salary_component_on_periodes_employee_on_periode_id_salary_comp 
            DO UPDATE SET salary_component_value = excluded.salary_component_value, active = excluded.active
            RETURNING id
        )
        SELECT * FROM insert_salary_component_on_periodes
        ";

        $insertDataDetail =  DB::insert($sqlDataDetail, $paramsInsertDetail);

        # AFTER INSERT SALARY COMPONENT, CALCULATE EMPLOYEES
        # Get Data Employee On Periodes
        $params["periode_month"] = $periodeMonth;
        $params["employee_on_periode_id"] = $employeeOnPeriodeId;
        $sql = "SELECT A.*, B.periode_month, B.employee_id, B.job_position_id, B.status_employment_id, B.is_mpp, B.is_plt, B.status_salary,
        JP.job_position_category_id,
        EMP.fullname, 
        MSC.name, MSC.is_custom_component, MSC.custom_salary_module_id, MSC.charge, MSC.is_salary_cut, MSC.is_mpp_component,
        CSC.function_name 
        FROM salary_component_on_periodes A
        JOIN employee_on_periodes B ON A.employee_on_periode_id = B.id
        JOIN master_salary_components MSC ON A.salary_component_id = MSC.id
        JOIN employees EMP ON B.employee_id = EMP.id
        JOIN job_positions JP ON B.job_position_id = JP.id
        LEFT JOIN custom_salary_modules CSC ON MSC.custom_salary_module_id = CSC.id
        WHERE B.periode_month = :periode_month AND B.id = :employee_on_periode_id";
        $salaryOnPeriodes = DB::select($sql, $params);

        $salaryComponentShouldUpdated = [];

        $paramDataMappingSalaryAccount = [];
        $paramDataMappingSalaryAccount["job_position_category_id"] = $checkEmployeeOnPeriodeExist->job_position_category_id;
        $paramDataMappingSalaryAccount["status_employment_id"] = $checkEmployeeOnPeriodeExist->status_employment_id;

        // $dataMappingSalaryAccounts = DB::select("SELECT * FROM mapping_salary_accounts 
        // WHERE job_position_category_id = :job_position_category_id AND status_employment_id = :status_employment_id 
        // AND active = 1 ", $paramDataMappingSalaryAccount);
        $dataMappingSalaryAccounts = DB::select("SELECT * FROM mapping_salary_accounts");

        foreach ($salaryOnPeriodes as $k) {
            $id = $k->id;
            $salaryComponentId = $k->salary_component_id;
            $employeeId = $k->employee_id;
            $jobPositionCategoryId = $k->job_position_category_id;
            $statusEmploymentId = $k->status_employment_id;
            $dataUpdated = [];
            $dataUpdated["id"] = $k->id;
            $dataUpdated["salary_component_value"] = $k->salary_component_value;
            $dataUpdated["fina_salary_account_id"] = multiArraySerach($dataMappingSalaryAccounts, [
                "job_position_category_id" => $jobPositionCategoryId,
                "status_employment_id" => $statusEmploymentId,
                "salary_component_id" => $salaryComponentId,
                "active" => 1
            ], 'fina_salary_account_id');
            if ($k->is_custom_component) {
                if (!is_null($k->function_name)) {
                    $functionName = $k->function_name;
                    if (function_exists($functionName)) {
                        $dataUpdated["salary_component_value"] = $functionName($periodeMonth, $employeeId, $salaryComponentId);
                    }
                }
            }
            array_push($salaryComponentShouldUpdated, $dataUpdated);
        }

        # UPDATE SALARY COMPONENT
        $salaryComponentInstance = new SalaryComponentOnPeriodes;
        $index = 'id';
        BatchFacade::update($salaryComponentInstance, $salaryComponentShouldUpdated, $index);


        # THEN UPDATE TOTAL GAJI
        $paramsEmployeeOnPeriode["periode_month"] = $periodeMonth;
        $paramsEmployeeOnPeriode["employee_on_periode_id"] = $employeeOnPeriodeId;
        $sqlEmployeeOnPeriode = "SELECT A.*
        FROM employee_on_periodes A 
        WHERE A.periode_month = :periode_month AND A.id = :employee_on_periode_id";
        $employeeOnPeriodes = DB::select($sqlEmployeeOnPeriode, $paramsEmployeeOnPeriode);
        $employeeOnPeriodeSalaryValue = [];
        foreach ($employeeOnPeriodes as $kEmp) {
            $employeeId = $kEmp->employee_id;
            $dEOP = [];
            $dEOP["id"] = $kEmp->id;
            $dEOP["salary_value"] = calculatedSalaryValueOnPeriode($employeeId, $periodeMonth);
            $dEOP["pembulatan_salary"] = round(calculatedSalaryValueOnPeriode($employeeId, $periodeMonth), -2);
            // $dEOP["selisih_pembulatan"] = abs($dEOP["pembulatan_salary"] - $dEOP["salary_value"]);
            $dEOP["selisih_pembulatan"] = $dEOP["pembulatan_salary"] - $dEOP["salary_value"];
            $dEOP["status_salary"] = "calculated";
            $dEOP["salary_calculated_by"] = Auth::user()->id;
            $dEOP["salary_calculated_at"] = Carbon::now();

            array_push($employeeOnPeriodeSalaryValue, $dEOP);
        }

        $employeeOnPeriodeInstance = new EmployeeOnPeriodes;
        $indexEmployeeOnPeriode = 'id';
        BatchFacade::update($employeeOnPeriodeInstance, $employeeOnPeriodeSalaryValue, $indexEmployeeOnPeriode);

        $response["check"] = $insertDataDetail;
        $response["message"] = __("message.successfullyCalulateAllSalaries");

        return $response;
    }

    protected function validation()
    {
        return [
            "id" => "required",
            "periode_month" => "required"
        ];
    }
}
