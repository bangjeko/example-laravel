<?php

namespace App\Services\Custom\PayrollPeriodeCalculation;

use Illuminate\Support\Facades\DB;
use App\CoreService\CoreService;
use App\Models\BpjsKesBills;
use App\Models\BpjsTkBills;
use App\Models\EmployeeOnPeriodes;
use App\Models\KoperasiSimpanPinjam;
use App\Models\PayrollPeriodes;
use App\Models\PinjamanBank;
use App\Models\PotonganSanksi;
use App\Models\PresenceOnPeriodes;
use App\Models\TunjanganBeasiswa;
use App\Models\TunjanganKehadiran;
use App\Models\TunjanganLembur;
use App\Models\TunjanganMakan;
use App\Models\TunjanganTransports;
use Carbon\Carbon;

class PayrollPeriodeChecklist extends CoreService
{

    public $transaction = false;
    public $permission = null;

    public function prepare($input)
    {
        return $input;
    }

    public function process($input, $originalInput)
    {
        setlocale(LC_ALL, 'IND');
        $object = [];
        $periodeMonth = $input["periode_month"];

        ##---------------------------------START-------------------------------------------#
        ## KONDISI ARRAY VALUE
        /*
        | JIKA DATA KONIDISI TERPENUHI MENJADI PRIMARY
        | JIKA DATA KONIDISI TERPENUHI TAPI TIDAK KOMPLIT MENJADI WARNING
        | JIKA DATA KONIDISI TIDAK TERPENUHI MENJADI DANGER
        */
        $arrayValue = ["danger", "warning", "success",];
        $periodeMonthName = Carbon::parse($periodeMonth)->formatLocalized('%B %Y');
        ##----------------------------------END-------------------------------------------#

        ##---------------------------------START-------------------------------------------#
        #1 PAYROLL PERIODE
        /*
        | CHECK SUDAH ADA DATA DI MODUL PAYROLL PERIODES
        */
        $objPayrollPeriodes = [];

        $payrollPeriodeData = !!PayrollPeriodes::where('periode_month', '=', $periodeMonth)->first();
        $objPayrollPeriodeVal = $arrayValue[0];
        if ($payrollPeriodeData) {
            $objPayrollPeriodeVal = $arrayValue[2];
        }
        $objPayrollPeriodes["module_code"] = "payroll_periodes";
        $objPayrollPeriodes["name"] = "Periode Penggajian";
        $objPayrollPeriodes["value"] = $objPayrollPeriodeVal;
        $objPayrollPeriodes["object_data"] = PayrollPeriodes::select('id')->where('periode_month', '=', $periodeMonth)->first();
        // $objPayrollPeriodes["description"] = ($objPayrollPeriodes["value"]) ? __("message.payrollPeriodeExist") : __("message.payrollPeriodeNotExist");
        $objPayrollPeriodes["description"] = "";

        array_push($object, $objPayrollPeriodes);
        ##----------------------------------END-------------------------------------------#

        ##---------------------------------START-------------------------------------------#
        #2 EMPLOYEE ON PERIODES

        $countEmployeeOnPeriodes = EmployeeOnPeriodes::where('periode_month', '=', $periodeMonth)->count();
        $objEmployeeOnPeriodeVal = $arrayValue[0];
        if ($countEmployeeOnPeriodes > 0) {
            $objEmployeeOnPeriodeVal = $arrayValue[2];
        }
        $objEmployeeOnPeriodes = [];
        $objEmployeeOnPeriodes["module_code"] = "employee_on_periodes";
        $objEmployeeOnPeriodes["name"] = "Daftar Tenaga Kerja";
        $objEmployeeOnPeriodes["value"] = $objEmployeeOnPeriodeVal;
        $objEmployeeOnPeriodes["object_data"] = PayrollPeriodes::select('id')->where('periode_month', '=', $periodeMonth)->first();
        // $objEmployeeOnPeriodes["description"] = ($objEmployeeOnPeriodes["value"]) ? __("message.employeeOnPeriodeExist", ['count' => $countEmployeeOnPeriodes]) : __("message.employeeOnPeriodeDoesntExist");
        $objEmployeeOnPeriodes["description"] = "";

        array_push($object, $objEmployeeOnPeriodes);
        ##----------------------------------END-------------------------------------------#

        ##---------------------------------START-------------------------------------------#
        #3 TEMPLATE SALARY ON PERIODE
        $objTemplateSalaryEmployees = [];
        $objTemplateSalaryEmployees["module_code"] = "mapping_template_salary_employees";
        $objTemplateSalaryEmployees["name"] = "Template Gaji Tenaga Kerja";
        $objTemplateSalaryEmployees["value"] = $arrayValue[2];
        $objTemplateSalaryEmployees["object_data"] = (object) ["periode_month" => $periodeMonth];
        $objTemplateSalaryEmployees["description"] = "";

        array_push($object, $objTemplateSalaryEmployees);
        ##----------------------------------END-------------------------------------------#

        ##---------------------------------START-------------------------------------------#
        #3 MAPPING SALARY ACCOUNTS
        ##----------------------------------END-------------------------------------------#

        ##---------------------------------START-------------------------------------------#
        #4 PRESENCE ON PERIODES
        $countPresenceOnPeriodeRecaps = DB::table('presence_on_periode_recaps as A')
            ->join('presence_on_periodes as B', 'B.id', '=', 'A.presence_on_periode_id')
            ->where('B.periode_month', '=', $periodeMonth)
            ->count();
        $countPresenceOnPeriodeRecapSynchronized = DB::table('presence_on_periode_recaps as A')
            ->join('presence_on_periodes as B', 'B.id', '=', 'A.presence_on_periode_id')
            ->where('A.status_code', '=', 'synchronized')
            ->where('B.periode_month', '=', $periodeMonth)
            ->count();

        $objPresenceOnPeriodeVal = $arrayValue[0];
        if ($countPresenceOnPeriodeRecapSynchronized != 0 and $countPresenceOnPeriodeRecaps == $countPresenceOnPeriodeRecapSynchronized) {
            $objPresenceOnPeriodeVal = $arrayValue[2];
        }
        $objPresenceOnPeriodes = [];
        $objPresenceOnPeriodes["module_code"] = "presence_on_periodes";
        $objPresenceOnPeriodes["name"] = "Daftar Presensi Tenaga Kerja";
        $objPresenceOnPeriodes["value"] = $objPresenceOnPeriodeVal;
        $objPresenceOnPeriodes["object_data"] = PresenceOnPeriodes::select('id')->where('periode_month', '=', $periodeMonth)->first();
        $objPresenceOnPeriodes["description"] = "";

        array_push($object, $objPresenceOnPeriodes);
        ##----------------------------------END-------------------------------------------#

        ##---------------------------------START-------------------------------------------#
        // #5 KOPERASI SIMPAN PINJAM
        $countKoperasiSimpanPinjamDetails = DB::table('koperasi_simpan_pinjam_details as A')
            ->join('koperasi_simpan_pinjam as B', 'B.id', '=', 'A.koperasi_simpan_pinjam_id')
            ->where('B.periode_month', '=', $periodeMonth)
            ->count();
        $countKoperasiSimpanPinjamDetailCalculated = DB::table('koperasi_simpan_pinjam_details as A')
            ->join('koperasi_simpan_pinjam as B', 'B.id', '=', 'A.koperasi_simpan_pinjam_id')
            ->where('A.status_code', '=', 'calculated')
            ->where('B.periode_month', '=', $periodeMonth)
            ->count();

        $objKoperasiSimpanPinjamVal = $arrayValue[0];
        if ($countKoperasiSimpanPinjamDetailCalculated != 0 and $countKoperasiSimpanPinjamDetails == $countKoperasiSimpanPinjamDetailCalculated) {
            $objKoperasiSimpanPinjamVal = $arrayValue[2];
        }
        $objKoperasiSimpanPinjam = [];
        $objKoperasiSimpanPinjam["module_code"] = "koperasi_simpan_pinjam";
        $objKoperasiSimpanPinjam["name"] = "Iuran Simpan Pinjaman Koperasi";
        $objKoperasiSimpanPinjam["value"] = $objKoperasiSimpanPinjamVal;
        $objKoperasiSimpanPinjam["object_data"] = KoperasiSimpanPinjam::select('id')->where('periode_month', '=', $periodeMonth)->first();
        $objKoperasiSimpanPinjam["description"] = "";

        array_push($object, $objKoperasiSimpanPinjam);
        ##----------------------------------END-------------------------------------------#

        ##---------------------------------START-------------------------------------------#
        #6 IURAN BPJS KESEHATAN
        $countBpjsKesBillDetails = DB::table('bpjs_kes_bill_details as A')
            ->join('bpjs_kes_bills as B', 'B.id', '=', 'A.bpjs_kes_bill_id')
            ->where('B.periode_month', '=', $periodeMonth)
            ->count();
        $countBpjsKesBillDetailsCalculated = DB::table('bpjs_kes_bill_details as A')
            ->join('bpjs_kes_bills as B', 'B.id', '=', 'A.bpjs_kes_bill_id')
            ->where('A.status_code', '=', 'calculated')
            ->where('B.periode_month', '=', $periodeMonth)
            ->count();

        $objBpjsKesBillsVal = $arrayValue[0];
        if ($countBpjsKesBillDetailsCalculated != 0 and $countBpjsKesBillDetails == $countBpjsKesBillDetailsCalculated) {
            $objBpjsKesBillsVal = $arrayValue[2];
        }
        $objBpjsKesBills = [];
        $objBpjsKesBills["module_code"] = "bpjs_kes_bills";
        $objBpjsKesBills["name"] = "Iuran BPJS Kesehatan";
        $objBpjsKesBills["value"] = $objBpjsKesBillsVal;
        $objBpjsKesBills["object_data"] = BpjsKesBills::select('id')->where('periode_month', '=', $periodeMonth)->first();
        $objBpjsKesBills["description"] = "";

        array_push($object, $objBpjsKesBills);

        ##----------------------------------END-------------------------------------------#

        ##---------------------------------START-------------------------------------------#
        #7 IURAN BPJS TENAGA KERJA
        $countBpjsTkBillDetails = DB::table('bpjs_tk_bill_details as A')
            ->join('bpjs_tk_bills as B', 'B.id', '=', 'A.bpjs_tk_bill_id')
            ->where('B.periode_month', '=', $periodeMonth)
            ->count();
        $countBpjsTkBillDetailsCalculated = DB::table('bpjs_tk_bill_details as A')
            ->join('bpjs_tk_bills as B', 'B.id', '=', 'A.bpjs_tk_bill_id')
            ->where('A.status_code', '=', 'calculated')
            ->where('B.periode_month', '=', $periodeMonth)
            ->count();

        $objBpjsTkBillsVal = $arrayValue[0];
        if ($countBpjsTkBillDetailsCalculated != 0 and $countBpjsTkBillDetails == $countBpjsTkBillDetailsCalculated) {
            $objBpjsTkBillsVal = $arrayValue[2];
        }
        $objBpjsTkBills = [];
        $objBpjsTkBills["module_code"] = "bpjs_tk_bills";
        $objBpjsTkBills["name"] = "Iuran BPJS Tenaga Kerja";
        $objBpjsTkBills["value"] = $objBpjsTkBillsVal;
        $objBpjsTkBills["object_data"] = BpjsTkBills::select('id')->where('periode_month', '=', $periodeMonth)->first();
        $objBpjsTkBills["description"] = "";

        array_push($object, $objBpjsTkBills);
        ##----------------------------------END-------------------------------------------#

        ##---------------------------------START-------------------------------------------#
        #7 TUNJANGAN KEHADIRAN
        $tunjanganKehadiran = DB::table('tunjangan_kehadiran as A')
            ->where('A.periode_month', '=', $periodeMonth)
            ->first();
        $countTunjanganKehadiranDetails = DB::table('tunjangan_kehadiran_details as A')
            ->join('tunjangan_kehadiran as B', 'B.id', '=', 'A.tunjangan_kehadiran_id')
            ->where('B.periode_month', '=', $periodeMonth)
            ->count();
        $countTunjanganKehadiranDetailsCalculated = DB::table('tunjangan_kehadiran_details as A')
            ->join('tunjangan_kehadiran as B', 'B.id', '=', 'A.tunjangan_kehadiran_id')
            ->where('A.status_code', '=', 'calculated')
            ->where('B.periode_month', '=', $periodeMonth)
            ->count();

        $objTunjanganKehadiranVal = $arrayValue[0];
        if (!is_null($tunjanganKehadiran) and $countTunjanganKehadiranDetails == $countTunjanganKehadiranDetailsCalculated) {
            $objTunjanganKehadiranVal = $arrayValue[2];
        }
        $objTunjanganKehadirans = [];
        $objTunjanganKehadirans["module_code"] = "tunjangan_kehadiran";
        $objTunjanganKehadirans["name"] = "Tunjangan Kehadiran";
        $objTunjanganKehadirans["value"] = $objTunjanganKehadiranVal;
        $objTunjanganKehadirans["object_data"] = TunjanganKehadiran::select('id')->where('periode_month', '=', $periodeMonth)->first();
        $objTunjanganKehadirans["description"] = "";

        array_push($object, $objTunjanganKehadirans);
        ##----------------------------------END-------------------------------------------#


        ##---------------------------------START-------------------------------------------#
        #7 TUNJANGAN TRANSPORT
        // $countTunjanganTransportDetails = DB::table('tunjangan_transport_details as A')
        //     ->join('tunjangan_transports as B', 'B.id', '=', 'A.tunjangan_transport_id')
        //     ->where('B.periode_month', '=', $periodeMonth)
        //     ->count();
        // $countTunjanganTransportDetailsCalculated = DB::table('tunjangan_transport_details as A')
        //     ->join('tunjangan_transports as B', 'B.id', '=', 'A.tunjangan_transport_id')
        //     ->where('A.status_code', '=', 'calculated')
        //     ->where('B.periode_month', '=', $periodeMonth)
        //     ->count();

        // $objTunjanganTransportVal = $arrayValue[0];
        // if ($countTunjanganTransportDetailsCalculated != 0 and $countTunjanganTransportDetails == $countTunjanganTransportDetailsCalculated) {
        //     $objTunjanganTransportVal = $arrayValue[2];
        // }
        // $objTunjanganTransports = [];
        // $objTunjanganTransports["module_code"] = "tunjangan_transports";
        // $objTunjanganTransports["name"] = "Tunjangan Transports";
        // $objTunjanganTransports["value"] = $objTunjanganTransportVal;
        // $objTunjanganTransports["object_data"] = TunjanganTransports::select('id')->where('periode_month', '=', $periodeMonth)->first();
        // $objTunjanganTransports["description"] = "";

        // array_push($object, $objTunjanganTransports);
        ##----------------------------------END-------------------------------------------#

        ##---------------------------------START-------------------------------------------#
        #8 TUNJANGAN MAKAN
        // $countTunjanganMakanDetails = DB::table('tunjangan_makan_details as A')
        //     ->join('tunjangan_makan as B', 'B.id', '=', 'A.tunjangan_makan_id')
        //     ->where('B.periode_month', '=', $periodeMonth)
        //     ->count();
        // $countTunjanganMakanDetailsCalculated = DB::table('tunjangan_makan_details as A')
        //     ->join('tunjangan_makan as B', 'B.id', '=', 'A.tunjangan_makan_id')
        //     ->where('A.status_code', '=', 'calculated')
        //     ->where('B.periode_month', '=', $periodeMonth)
        //     ->count();

        // $objTunjanganMakanVal = $arrayValue[0];
        // if ($countTunjanganMakanDetailsCalculated != 0 and $countTunjanganMakanDetails == $countTunjanganMakanDetailsCalculated) {
        //     $objTunjanganMakanVal = $arrayValue[2];
        // }
        // $objTunjanganMakan = [];
        // $objTunjanganMakan["module_code"] = "tunjangan_makan";
        // $objTunjanganMakan["name"] = "Tunjangan Makan";
        // $objTunjanganMakan["value"] = $objTunjanganMakanVal;
        // $objTunjanganMakan["object_data"] = TunjanganMakan::select('id')->where('periode_month', '=', $periodeMonth)->first();
        // $objTunjanganMakan["description"] = "";

        // array_push($object, $objTunjanganMakan);
        ##----------------------------------END-------------------------------------------#

        ##---------------------------------START-------------------------------------------#
        #9 TUNJANGAN BEASISWA ANAK
        $tunjanganBeasiswa = DB::table('tunjangan_beasiswa as A')
            ->where('A.periode_month', '=', $periodeMonth)
            ->first();
        $countTunjanganBeasiswaAnakDetails = DB::table('tunjangan_beasiswa_details as A')
            ->join('tunjangan_beasiswa as B', 'B.id', '=', 'A.tunjangan_beasiswa_id')
            ->where('B.periode_month', '=', $periodeMonth)
            ->count();
        $countTunjanganBeasiswaAnakDetailsCalculated = DB::table('tunjangan_beasiswa_details as A')
            ->join('tunjangan_beasiswa as B', 'B.id', '=', 'A.tunjangan_beasiswa_id')
            ->where('A.status_code', '=', 'calculated')
            ->where('B.periode_month', '=', $periodeMonth)
            ->count();

        $objTunjanganBeasiswaVal = $arrayValue[0];
        if (!is_null($tunjanganBeasiswa) and $countTunjanganBeasiswaAnakDetails == $countTunjanganBeasiswaAnakDetailsCalculated) {
            $objTunjanganBeasiswaVal = $arrayValue[2];
        }
        $objTunjanganBeasiswa = [];
        $objTunjanganBeasiswa["module_code"] = "tunjangan_beasiswa";
        $objTunjanganBeasiswa["name"] = "Tunjangan Beasiswa Anak";
        $objTunjanganBeasiswa["value"] = $objTunjanganBeasiswaVal;
        $objTunjanganBeasiswa["object_data"] = TunjanganBeasiswa::select('id')->where('periode_month', '=', $periodeMonth)->first();
        $objTunjanganBeasiswa["description"] = "";

        array_push($object, $objTunjanganBeasiswa);
        ##----------------------------------END-------------------------------------------#



        ##---------------------------------START-------------------------------------------#
        #10 TUNJANGAN LEMBUR
        $tunjanganLembur = DB::table('tunjangan_lembur as A')
            ->where('A.periode_month', '=', $periodeMonth)
            ->first();
        $countTunjanganLemburDetails = DB::table('tunjangan_lembur_details as A')
            ->join('tunjangan_lembur as B', 'B.id', '=', 'A.tunjangan_lembur_id')
            ->where('B.periode_month', '=', $periodeMonth)
            ->count();
        $countTunjanganLemburDetailsCalculated = DB::table('tunjangan_lembur_details as A')
            ->join('tunjangan_lembur as B', 'B.id', '=', 'A.tunjangan_lembur_id')
            ->where('A.status_code', '=', 'calculated')
            ->where('B.periode_month', '=', $periodeMonth)
            ->count();

        $objTunjanganLemburVal = $arrayValue[0];
        if (!is_null($tunjanganLembur) and $countTunjanganLemburDetails == $countTunjanganLemburDetailsCalculated) {
            $objTunjanganLemburVal = $arrayValue[2];
        }
        $objTunjanganLembur = [];
        $objTunjanganLembur["module_code"] = "tunjangan_lembur";
        $objTunjanganLembur["name"] = "Tunjangan Lembur";
        $objTunjanganLembur["value"] = $objTunjanganLemburVal;
        $objTunjanganLembur["object_data"] = TunjanganLembur::select('id')->where('periode_month', '=', $periodeMonth)->first();
        $objTunjanganLembur["description"] = "";

        array_push($object, $objTunjanganLembur);
        ##----------------------------------END-------------------------------------------#

        ##---------------------------------START-------------------------------------------#
        #11 POTONGAN PINJAMAN BANK
        $pinjamanBank = DB::table('pinjaman_bank as A')
            ->where('A.periode_month', '=', $periodeMonth)
            ->first();
        $countPotonganPinjamanBankDetails = DB::table('pinjaman_bank_details as A')
            ->join('pinjaman_bank as B', 'B.id', '=', 'A.pinjaman_bank_id')
            ->where('B.periode_month', '=', $periodeMonth)
            ->count();
        $countPotonganPinjamanBankDetailsCalculated = DB::table('pinjaman_bank_details as A')
            ->join('pinjaman_bank as B', 'B.id', '=', 'A.pinjaman_bank_id')
            ->where('A.status_code', '=', 'calculated')
            ->where('B.periode_month', '=', $periodeMonth)
            ->count();

        $objPotonganPinjamanBankVal = $arrayValue[0];
        if (!is_null($pinjamanBank) and $countPotonganPinjamanBankDetails == $countPotonganPinjamanBankDetailsCalculated) {
            $objPotonganPinjamanBankVal = $arrayValue[2];
        }
        $objPotonganPinjamanBank = [];
        $objPotonganPinjamanBank["module_code"] = "pinjaman_bank";
        $objPotonganPinjamanBank["name"] = "Pinjaman Bank";
        $objPotonganPinjamanBank["value"] = $objPotonganPinjamanBankVal;
        $objPotonganPinjamanBank["object_data"] = PinjamanBank::select('id')->where('periode_month', '=', $periodeMonth)->first();
        $objPotonganPinjamanBank["description"] = "";

        array_push($object, $objPotonganPinjamanBank);
        ##----------------------------------END-------------------------------------------#

        ##---------------------------------START-------------------------------------------#
        #12 POTONGAN SANKSI
        $potonganSanksi = DB::table('potongan_sanksi as A')
            ->where('A.periode_month', '=', $periodeMonth)
            ->first();
        $countPotonganPotonganSanksiDetails = DB::table('potongan_sanksi_details as A')
            ->join('potongan_sanksi as B', 'B.id', '=', 'A.potongan_sanksi_id')
            ->where('B.periode_month', '=', $periodeMonth)
            ->count();
        $countPotonganPotonganSanksiDetailsCalculated = DB::table('potongan_sanksi_details as A')
            ->join('potongan_sanksi as B', 'B.id', '=', 'A.potongan_sanksi_id')
            ->where('A.status_code', '=', 'calculated')
            ->where('B.periode_month', '=', $periodeMonth)
            ->count();

        $objPotonganPotonganSanksiVal = $arrayValue[0];
        if (!is_null($potonganSanksi) and $countPotonganPotonganSanksiDetails == $countPotonganPotonganSanksiDetailsCalculated) {
            $objPotonganPotonganSanksiVal = $arrayValue[2];
        }
        $objPotonganPotonganSanksi = [];
        $objPotonganPotonganSanksi["module_code"] = "potongan_sanksi";
        $objPotonganPotonganSanksi["name"] = "Potongan Sanksi";
        $objPotonganPotonganSanksi["value"] = $objPotonganPotonganSanksiVal;
        $objPotonganPotonganSanksi["object_data"] = PotonganSanksi::select('id')->where('periode_month', '=', $periodeMonth)->first();
        $objPotonganPotonganSanksi["description"] = "";

        array_push($object, $objPotonganPotonganSanksi);
        ##----------------------------------END-------------------------------------------#



        ##---------------------------------START-------------------------------------------#
        ## STATUS OVERALL
        $overall = array_filter($object, function ($var) {
            return ($var['value'] == 'success');
        });
        $statusOverall = (count($overall) == count($object)) ? true : false;
        ##----------------------------------END-------------------------------------------#
        return [
            "status_overall" => $statusOverall,
            "data" => $object,
        ];
    }

    protected function validation()
    {
        return [
            "periode_month" => "required"
        ];
    }
}
