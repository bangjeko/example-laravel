<?php

namespace App\Services\Custom\PayrollPeriodeCalculation;

use App\CoreService\CoreException;
use App\CoreService\CoreService;
use App\Models\PayrollPeriodes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;
use Illuminate\Support\Carbon;

class PayrollPeriodeCalculationDetailListTree extends CoreService
{

    public $transaction = false;
    public $task = null;

    public function prepare($input)
    {
        $model = "salary-component-on-periodes";
        $classModel = "\\App\\Models\\" . Str::ucfirst(Str::camel($model));
        $permission = "view-" . $model;
        $authRoles = getRoleName(Auth::user()->role_id);
        if (!class_exists($classModel))
            throw new CoreException(__("message.model404", ['model' => $model]), 404);
        if (!$classModel::IS_LIST)
            throw new CoreException(__("message.404"), 404);
        if (!hasPermission($permission))
            throw new CoreException(__("message.forbidden403", ['permission' => $permission, 'role' => $authRoles]), 403);

        $input["class_model_name"] = $model;
        $input["class_model"] = $classModel;
        return $input;
    }

    public function process($input, $originalInput)
    {
        $employeeOnPeriodeId = $input["id"];

        #1 DATA EMPLOYEE ON PERIODE
        $dataEmployeeOnPeriodes = DB::selectOne("SELECT A.id, A.employee_id, A.periode_month, A.job_position_id, 
        A.status_employment_id, A.is_mpp, A.is_plt,A.salary_value, A.status_salary, A.pembulatan_salary, A.selisih_pembulatan, A.salary_calculated_at, A.salary_calculated_by,
        EMP.fullname as rel_employee_id,
        EMP.nik,EMP.nip,
        JP.job_position_name as rel_job_position_id,
        MSE.name as rel_status_employment_id,
        UCAL.username as rel_salary_calculated_by
        FROM employee_on_periodes A
        JOIN job_positions JP ON A.job_position_id = JP.id
        JOIN master_status_employments MSE ON A.status_employment_id = MSE.id
        JOIN employees EMP ON A.employee_id = EMP.id
        LEFT JOIN users UCAL ON A.salary_calculated_by = UCAL.id
        WHERE A.id = :employee_on_periode_id", [
            "employee_on_periode_id" => $employeeOnPeriodeId
        ]);

        ####################################################################
        if (empty($dataEmployeeOnPeriodes)) {
            throw new CoreException(__("message.dataNotFound", ['id' => $employeeOnPeriodeId]));
        }

        $periodeMonth = $dataEmployeeOnPeriodes->periode_month;
        $dataPayrollPeriode = PayrollPeriodes::where('periode_month', '=', $periodeMonth)->first();
        if (is_null($dataPayrollPeriode)) {
            throw new CoreException(__("message.periodePenggajianNotFound", ['periode_month' => $periodeMonth]));
        }

        $dataEmployeeOnPeriodes->is_lock = $dataPayrollPeriode->is_lock;
        $employeeId = $dataEmployeeOnPeriodes->employee_id;
        #2 EMPLOYEE PRESENCES
        $dataEmployeePresenceRecaps = DB::selectOne("SELECT 
        A.work_day_count, A.work_day_lost_count, A.work_day_late_count, A.work_day_minute_late_count
        FROM presence_on_periode_recaps A
        JOIN presence_on_periodes B ON A.presence_on_periode_id = B.id
        WHERE A.employee_id = :employee_id AND B.periode_month = :periode_month", [
            "employee_id" => $employeeId,
            "periode_month" => $periodeMonth
        ]);

        ####################################################################

        $dataCategory = DB::select("SELECT * FROM master_salary_component_categories");
        $paramDetailData = [];

        $paramDetailData["employee_on_periode_id"] = $employeeOnPeriodeId;
        $sqlDetailData = "SELECT A.*, B.name as rel_salary_component_id, 
        B.salary_component_category_id as salary_component_category_id,
        C.name as rel_salary_component_category_id,
        B.is_salary_cut,
        B.charge,
        CASE 
            WHEN B.is_salary_cut = 'true' THEN A.salary_component_value * -1 
            ELSE A.salary_component_value 
        END AS salary_component_value,
        A.active
        FROM salary_component_on_periodes A
        JOIN master_salary_components B ON A.salary_component_id = B.id 
        JOIN master_salary_component_categories C ON B.salary_component_category_id = C.id
        WHERE A.employee_on_periode_id = :employee_on_periode_id AND A.active = 1 AND A.salary_component_value != 0
        ";
        $detailData =  DB::select($sqlDetailData, $paramDetailData);


        $salaryDetails = array_map(function ($key) use ($detailData) {
            foreach ($key as $field => $value) {
                $id = $key->id;
                $children = [];
                $key->children = [];
                foreach ($detailData as $kSc) {
                    $kSc->salary_component_value = (float) $kSc->salary_component_value;
                    if ($kSc->salary_component_category_id == $id) {
                        array_push($key->children, $kSc);
                    }
                }
            }
            return $key;
        }, $dataCategory);
        return [
            "data" => [
                "data_employee" => $dataEmployeeOnPeriodes,
                "data_employee_presence_recaps" => $dataEmployeePresenceRecaps,
                "data_salary_detail" => $salaryDetails,
            ]
        ];
    }

    protected function validation()
    {
        return [
            "id" => "required"
        ];
    }
}
