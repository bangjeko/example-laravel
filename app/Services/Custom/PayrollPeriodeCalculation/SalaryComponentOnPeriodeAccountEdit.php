<?php

namespace App\Services\Custom\PayrollPeriodeCalculation;

use App\CoreService\CoreException;
use Illuminate\Support\Facades\DB;
use App\CoreService\CoreService;
use App\Models\EmployeeOnPeriodes;
use App\Models\KoperasiSimpanPinjam;
use App\Models\PayrollPeriodes;
use App\Models\PresenceOnPeriodes;
use App\Models\SalaryComponentOnPeriodes;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Mavinoo\Batch\BatchFacade;

class SalaryComponentOnPeriodeAccountEdit extends CoreService
{

    public $transaction = false;
    public $permission = null;

    public function prepare($input)
    {
        $permission = "salary-calculation";
        $authRoles = getRoleName(Auth::user()->role_id);
        if (!hasPermission($permission))
            throw new CoreException(__("message.forbidden403", ['permission' => $permission, 'roles' => $authRoles]), 403);
        return $input;
    }

    public function process($input, $originalInput)
    {
        $response = [];

        $data = SalaryComponentOnPeriodes::find($input["id"]);
        if (is_null($data)) {
            throw new CoreException(__("message.dataNotFound", ['id' => $input["id"]]));
        }
        $data->fina_salary_account_id = isset($input["fina_salary_account_id"]) ? $input["fina_salary_account_id"] : null;
        $data->save();

        $response["message"] = __("message.successfullyChangeSalaryAccount");

        return $response;
    }

    protected function validation()
    {
        return [
            "id" => "required",
        ];
    }
}
