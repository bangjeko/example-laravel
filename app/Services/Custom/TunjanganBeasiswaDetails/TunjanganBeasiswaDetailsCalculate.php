<?php

namespace App\Services\Custom\TunjanganBeasiswaDetails;

use App\CoreService\CallService;
use App\CoreService\CoreException;
use App\CoreService\CoreService;
use App\Models\TunjanganBeasiswa;
use App\Models\TunjanganBeasiswaComponents;
use App\Models\TunjanganBeasiswaDetails;
use App\Models\TunjanganMakan;
use App\Models\TunjanganMakanDetails;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Carbon\Carbon;

class TunjanganBeasiswaDetailsCalculate extends CoreService
{

    public $transaction = true;
    public $task = null;

    public function prepare($input)
    {
        $model = "tunjangan-beasiswa-details";
        $classModel = "\\App\\Models\\" . Str::ucfirst(Str::camel($model));
        $permission = "create-" . $model;
        $authRoles = getRoleName(Auth::user()->role_id);
        if (!class_exists($classModel))
            throw new CoreException(__("message.model404", ['model' => $model]), 404);
        if (!$classModel::IS_ADD)
            throw new CoreException("Not found", 404);
        if (!hasPermission($permission))
            throw new CoreException(__("message.forbidden403", ['permission' => $permission, 'roles' => $authRoles]), 403);
        $input["class_model"] = $classModel;
        return $input;
    }

    public function process($input, $originalInput)
    {
        $periodeMonth = $input["periode_month"];
        #1 FIND tunjangan_makan_details

        $dataTunjanganBeasiswaDetails = TunjanganBeasiswaDetails::find($input["id"]);
        $dataTunjanganBeasiswaDetails->description = $input["description"];
        $dataTunjanganBeasiswaDetails->status_code = "calculated";

        // MULAI HITUNG
        $penerimaanValue = 0;

        if (isset($input["data_tunjangan_beasiswa_components"])) {
            foreach ($input["data_tunjangan_beasiswa_components"] as $kDComp => $valComp) {
                if (isset($valComp["id"])) {
                    $valComp["model"] = "tunjangan_beasiswa_components";
                    $valComp["tunjangan_beasiswa_detail_id"] = $dataTunjanganBeasiswaDetails->id;
                    $childDataUpdated = CallService::run("Edit", $valComp);
                } else {
                    if (isset($valComp["employee_family_id"]) and isset($valComp["education_id"]) and isset($valComp["rank"]) and isset($valComp["component_value"]) and $valComp["active"] == 1) {
                        $valComp["model"] = "tunjangan_beasiswa_components";
                        $valComp["tunjangan_beasiswa_detail_id"] = $dataTunjanganBeasiswaDetails->id;
                        $childDataInserted = CallService::run("Add", $valComp);
                    }
                }
            }
        }

        $penerimaanValue = TunjanganBeasiswaComponents::where('tunjangan_beasiswa_detail_id', '=', $dataTunjanganBeasiswaDetails->id)
            ->where('active', '=', 1)
            ->sum('component_value');


        #6 UPDATE DETAIL
        $dataTunjanganBeasiswaDetails->penerimaan_value = $penerimaanValue;
        $dataTunjanganBeasiswaDetails->save();


        #7 UPDATE PARENT tunjangan_makans
        $totalPenerimaan = TunjanganBeasiswaDetails::where('tunjangan_beasiswa_id', '=', $dataTunjanganBeasiswaDetails->tunjangan_beasiswa_id)
            ->sum('penerimaan_value');

        TunjanganBeasiswa::where('id', '=', $dataTunjanganBeasiswaDetails->tunjangan_beasiswa_id)
            ->update([
                'total_penerimaan_value' => $totalPenerimaan,
            ]);

        // THEN SHOW
        $classModel = $input["class_model"];
        $selectableList = [];
        $tableJoinList = [];
        $params = ["id" => $input["id"]];

        foreach ($classModel::FIELD_VIEW as $list) {
            $selectableList[] = $classModel::TABLE . "." . $list;
        }

        $i = 0;
        $FIELD_RELATION = [
            "tunjangan_beasiswa_id" => [
                "linkTable" => "tunjangan_beasiswa",
                "aliasTable" => "B",
                "linkField" => "id",
                "displayName" => "rel_tunjangan_beasiswa_id",
                "selectFields" => ["id"],
                "selectValue" => "id AS rel_tunjangan_beasiswa_id"
            ],
            "employee_id" => [
                "linkTable" => "employees",
                "aliasTable" => "C",
                "linkField" => "id",
                "displayName" => "rel_employee_id",
                "selectFields" => ["fullname"],
                "selectValue" => "id AS rel_employee_id"
            ],
            "created_by" => [
                "linkTable" => "users",
                "aliasTable" => "D",
                "linkField" => "id",
                "displayName" => "rel_created_by",
                "selectFields" => ["username"],
                "selectValue" => "id AS rel_created_by"
            ],
            "updated_by" => [
                "linkTable" => "users",
                "aliasTable" => "E",
                "linkField" => "id",
                "displayName" => "rel_updated_by",
                "selectFields" => ["username"],
                "selectValue" => "id AS rel_updated_by"
            ],
        ];
        foreach ($FIELD_RELATION as $key => $relation) {
            $alias = toAlpha($i + 1);
            ///
            $fieldDisplayed = "CONCAT_WS (' - ',";
            foreach ($relation["selectFields"] as $keyField) {
                $fieldDisplayed .= $alias . '.' . $keyField . ",";
            }
            $fieldDisplayed = substr($fieldDisplayed, 0, strlen($fieldDisplayed) - 1);
            $fieldDisplayed .= ") AS " . $relation["displayName"];
            $selectableList[] = $fieldDisplayed;
            ///
            $tableJoinList[] = "LEFT JOIN " . $relation["linkTable"] . " " . $alias . " ON " .
                $classModel::TABLE . "." . $key . " = " .  $alias . "." . $relation["linkField"];
            $i++;
        }

        if (!empty($classModel::CUSTOM_SELECT)) $selectableList[] = $classModel::CUSTOM_SELECT;

        $condition = " WHERE " . $classModel::TABLE . ".id = :id";

        $sql = "SELECT " . implode(", ", $selectableList) . ", EOP.is_mpp, B.periode_month, EMP.date_of_birth, JP.job_position_name as rel_job_position_id, MSE.name as rel_status_employment_id
            FROM " . $classModel::TABLE . " " .
            implode(" ", $tableJoinList) . "
            LEFT JOIN employee_on_periodes EOP on tunjangan_beasiswa_details.employee_id = EOP.employee_id AND EOP.periode_month = B.periode_month 
            LEFT JOIN employees EMP on tunjangan_beasiswa_details.employee_id = EMP.id 
            LEFT JOIN job_positions JP ON EOP.job_position_id = JP.id
            LEFT JOIN master_status_employments MSE ON EOP.status_employment_id = MSE.id
            " . $condition;


        $object =  DB::selectOne($sql, $params);
        if (is_null($object)) {
            throw new CoreException(__("message.dataNotFound", ['id' => $input["id"]]));
        }

        $fieldCasting = $classModel::FIELD_CASTING;
        if (!empty($fieldCasting)) {
            foreach ($fieldCasting as $item => $k) {
                if (isset($fieldCasting[$item])) {
                    if (array_key_exists($item, $fieldCasting)) {
                        if ($fieldCasting[$item] == 'float') {
                            $object->$item = (float) $object->$item;
                        }
                    }
                }
            }
        }

        // DATA DETAILS
        // STATIC UNTUK ANAK KANDUNG DAN ANAK ANGKAT
        $paramDataComponents = [];
        $paramDataComponents["tunjangan_beasiswa_detail_id"] = $input["id"];
        $paramDataComponents["employee_id"] = $object->employee_id;

        $sqlDataComponents = "WITH tunjangan_components AS (
            SELECT * FROM tunjangan_beasiswa_components WHERE tunjangan_beasiswa_detail_id = :tunjangan_beasiswa_detail_id
        )
        SELECT TBC.id, TBC.tunjangan_beasiswa_detail_id, EF.id as employee_family_id, TBC.education_id, TBC.rank, 
                CASE 
                    WHEN TBC.component_value IS NOT NULL 
                    THEN TBC.component_value ELSE 0 
                END AS component_value, 
                TBC.description, TBC.active,
                EF.fullname as rel_employee_family_id , EF.family_relationship_id, MFR.name as rel_family_relationship_id
                FROM employee_families EF
                LEFT JOIN master_family_relationships MFR ON EF.family_relationship_id = MFR.id
                LEFT JOIN tunjangan_components TBC ON TBC.employee_family_id = EF.id
                WHERE EF.employee_id = :employee_id
                AND EF.family_relationship_id IN (11,12) ";
        $dataComponents = DB::select($sqlDataComponents, $paramDataComponents);

        if (!empty($dataComponents)) {
            array_map(function ($key) {
                foreach ($key as $field => $value) {
                    $key->component_value = (float) $key->component_value;
                }
                return $key;
            }, $dataComponents);
        }


        $object->data_tunjangan_beasiswa_components = $dataComponents;
        // END SHOW

        $response["data"] = $object;
        $response["message"] = __("message.successfullyAdd");
        return $response;
    }

    protected function validation()
    {
        return [
            "periode_month" => "required"
        ];
    }
}
