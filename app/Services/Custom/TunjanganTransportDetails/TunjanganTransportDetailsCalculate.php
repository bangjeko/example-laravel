<?php

namespace App\Services\Custom\TunjanganTransportDetails;

use App\CoreService\CallService;
use App\CoreService\CoreException;
use App\CoreService\CoreService;
use App\Models\BpjsTkBillDetails;
use App\Models\BpjsTkBillDetailUpahComponents;
use App\Models\BpjsTkBills;
use App\Models\ConfigProsenBpjsTenagaKerja;
use App\Models\PresenceOnPeriodeRecaps;
use App\Models\PresenceOnPeriodes;
use App\Models\TunjanganTransportDetails;
use App\Models\TunjanganTransports;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Carbon\Carbon;

class TunjanganTransportDetailsCalculate extends CoreService
{

    public $transaction = true;
    public $task = null;

    public function prepare($input)
    {
        $model = "tunjangan-transport-details";
        $classModel = "\\App\\Models\\" . Str::ucfirst(Str::camel($model));
        $permission = "create-" . $model;
        $authRoles = getRoleName(Auth::user()->role_id);
        if (!class_exists($classModel))
            throw new CoreException(__("message.model404", ['model' => $model]), 404);
        if (!$classModel::IS_ADD)
            throw new CoreException("Not found", 404);
        if (!hasPermission($permission))
            throw new CoreException(__("message.forbidden403", ['permission' => $permission, 'roles' => $authRoles]), 403);
        $input["class_model"] = $classModel;
        return $input;
    }

    public function process($input, $originalInput)
    {
        $periodeMonth = $input["periode_month"];
        #1 FIND tunjangan_transport_details

        $dataTunjanganTransportDetails = TunjanganTransportDetails::find($input["id"]);
        $dataTunjanganTransportDetails->description = $input["description"];
        $dataTunjanganTransportDetails->status_code = "calculated";

        // MULAI HITUNG
        $baseValue = isset($input["base_value"]) ?  $input["base_value"] : 0;
        $isUsePresenceData = isset($input["is_use_presence_data"]) ?  $input["is_use_presence_data"] : false;

        # GET DATA REKAP PRESENCE ON PERIODE BASED ON EMPLOYEE
        $dataPresenceRecaps = DB::selectOne("SELECT A.* FROM presence_on_periode_recaps A
        JOIN presence_on_periodes B ON A.presence_on_periode_id = B.id
        WHERE B.periode_month = :periode_month AND A.employee_id = :employee_id", [
            "periode_month" => $periodeMonth,
            "employee_id" => $dataTunjanganTransportDetails->employee_id
        ]);

        $workDayCount = $dataPresenceRecaps->work_day_count;
        $workDayLostCount = $dataPresenceRecaps->work_day_lost_count;
        $workDayLateCount = $dataPresenceRecaps->work_day_late_count;
        $workDayMinuteLateCount = $dataPresenceRecaps->work_day_minute_late_count;

        # TUNJANGAN VALUE DIDAPAT DARI : (JUMLAH HARI KERJA - JUMLAH TIDAK MASUK ) * BASE VALUE 
        $tunjanganValue = ($workDayCount - $workDayLostCount) * $baseValue;

        # POTONGAN TERLAMBAT : 
        # JIKA TERLAMBAT 5 KALI ATAU TERLAMBAT 60 MENIT DIPOTONG 50%
        # JIKA TERLAMBAT > 5 KALI ATAU TERLAMBAT > 60 MENIT DIPOTONG 100%
        $potonganValue = 0;
        if ($workDayLateCount == 5 or $workDayMinuteLateCount == 60) {
            $potonganValue = round($baseValue * 50 / 100, 0);
        } else if ($workDayLateCount > 5 or $workDayMinuteLateCount > 60) {
            $potonganValue = round($baseValue * 100 / 100, 0);
        }

        # TOTAL IURAN
        if ($isUsePresenceData) {
            $penerimaanValue = round($tunjanganValue - $potonganValue, 0);
        } else {
            $tunjanganValue = $baseValue;
            $penerimaanValue = $baseValue;
        }

        #6 UPDATE DETAIL
        $dataTunjanganTransportDetails->is_use_presence_data = $isUsePresenceData;
        $dataTunjanganTransportDetails->base_value = $baseValue;
        $dataTunjanganTransportDetails->tunjangan_value = $tunjanganValue;
        $dataTunjanganTransportDetails->potongan_value = $potonganValue;
        $dataTunjanganTransportDetails->penerimaan_value = $penerimaanValue;
        $dataTunjanganTransportDetails->save();


        #7 UPDATE PARENT tunjangan_transports
        $totalPenerimaan = TunjanganTransportDetails::where('tunjangan_transport_id', '=', $dataTunjanganTransportDetails->tunjangan_transport_id)
            ->sum('penerimaan_value');

        TunjanganTransports::where('id', '=', $dataTunjanganTransportDetails->tunjangan_transport_id)
            ->update([
                'total_penerimaan_value' => $totalPenerimaan,
            ]);

        // THEN SHOW
        $classModel = $input["class_model"];
        $selectableList = [];
        $tableJoinList = [];
        $params = ["id" => $input["id"]];

        foreach ($classModel::FIELD_VIEW as $list) {
            $selectableList[] = $classModel::TABLE . "." . $list;
        }

        $i = 0;
        $FIELD_RELATION = [
            "tunjangan_transport_id" => [
                "linkTable" => "tunjangan_transports",
                "aliasTable" => "B",
                "linkField" => "id",
                "displayName" => "rel_tunjangan_transport_id",
                "selectFields" => ["id"],
                "selectValue" => "id AS rel_tunjangan_transport_id"
            ],
            "employee_id" => [
                "linkTable" => "employees",
                "aliasTable" => "C",
                "linkField" => "id",
                "displayName" => "rel_employee_id",
                "selectFields" => ["fullname"],
                "selectValue" => "id AS rel_employee_id"
            ],
            "created_by" => [
                "linkTable" => "users",
                "aliasTable" => "D",
                "linkField" => "id",
                "displayName" => "rel_created_by",
                "selectFields" => ["username"],
                "selectValue" => "id AS rel_created_by"
            ],
            "updated_by" => [
                "linkTable" => "users",
                "aliasTable" => "E",
                "linkField" => "id",
                "displayName" => "rel_updated_by",
                "selectFields" => ["username"],
                "selectValue" => "id AS rel_updated_by"
            ],
        ];
        foreach ($FIELD_RELATION as $key => $relation) {
            $alias = toAlpha($i + 1);
            ///
            $fieldDisplayed = "CONCAT_WS (' - ',";
            foreach ($relation["selectFields"] as $keyField) {
                $fieldDisplayed .= $alias . '.' . $keyField . ",";
            }
            $fieldDisplayed = substr($fieldDisplayed, 0, strlen($fieldDisplayed) - 1);
            $fieldDisplayed .= ") AS " . $relation["displayName"];
            $selectableList[] = $fieldDisplayed;
            ///
            $tableJoinList[] = "LEFT JOIN " . $relation["linkTable"] . " " . $alias . " ON " .
                $classModel::TABLE . "." . $key . " = " .  $alias . "." . $relation["linkField"];
            $i++;
        }

        if (!empty($classModel::CUSTOM_SELECT)) $selectableList[] = $classModel::CUSTOM_SELECT;

        $condition = " WHERE " . $classModel::TABLE . ".id = :id";

        $sql = "SELECT " . implode(", ", $selectableList) . ", EOP.is_mpp, B.periode_month, EMP.date_of_birth, JP.job_position_name as rel_job_position_id, MSE.name as rel_status_employment_id
            FROM " . $classModel::TABLE . " " .
            implode(" ", $tableJoinList) . "
            LEFT JOIN employee_on_periodes EOP on tunjangan_transport_details.employee_id = EOP.employee_id AND EOP.periode_month = B.periode_month 
            LEFT JOIN employees EMP on tunjangan_transport_details.employee_id = EMP.id 
            LEFT JOIN job_positions JP ON EOP.job_position_id = JP.id
            LEFT JOIN master_status_employments MSE ON EOP.status_employment_id = MSE.id
            " . $condition;


        $object =  DB::selectOne($sql, $params);
        if (is_null($object)) {
            throw new CoreException(__("message.dataNotFound", ['id' => $input["id"]]));
        }
        $fieldCasting = $classModel::FIELD_CASTING;
        if (!empty($fieldCasting)) {
            foreach ($fieldCasting as $item => $k) {
                if (isset($fieldCasting[$item])) {
                    if (array_key_exists($item, $fieldCasting)) {
                        if ($fieldCasting[$item] == 'float') {
                            $object->$item = (float) $object->$item;
                        }
                    }
                }
            }
        }
        // DATA REKAP PRESENSI
        $dataEmployeePresenceRecaps = DB::selectOne("SELECT 
         A.work_day_count, A.work_day_lost_count, A.work_day_late_count, A.work_day_minute_late_count
         FROM presence_on_periode_recaps A
         JOIN presence_on_periodes B ON A.presence_on_periode_id = B.id
         WHERE A.employee_id = :employee_id AND B.periode_month = :periode_month", [
            "employee_id" => $object->employee_id,
            "periode_month" => $object->periode_month
        ]);

        $object->data_employee_presence_recaps = $dataEmployeePresenceRecaps;
        // END SHOW

        $response["data"] = $object;
        $response["message"] = __("message.successfullyAdd");
        return $response;
    }

    protected function validation()
    {
        return [
            "periode_month" => "required"
        ];
    }
}
