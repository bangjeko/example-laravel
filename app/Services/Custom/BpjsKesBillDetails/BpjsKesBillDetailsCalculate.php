<?php

namespace App\Services\Custom\BpjsKesBillDetails;

use App\CoreService\CallService;
use App\CoreService\CoreException;
use App\CoreService\CoreService;
use App\Models\BpjsKesBillDetails;
use App\Models\BpjsKesBillDetailTambahan;
use App\Models\BpjsKesBillDetailUpahComponents;
use App\Models\BpjsKesBills;
use App\Models\ConfigProsenBpjsKesehatan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class BpjsKesBillDetailsCalculate extends CoreService
{

    public $transaction = true;
    public $task = null;

    public function prepare($input)
    {
        $model = "bpjs-kes-bill-details";
        $classModel = "\\App\\Models\\" . Str::ucfirst(Str::camel($model));
        $permission = "create-" . $model;
        $authRoles = getRoleName(Auth::user()->role_id);
        if (!class_exists($classModel))
            throw new CoreException(__("message.model404", ['model' => $model]), 404);
        if (!$classModel::IS_ADD)
            throw new CoreException("Not found", 404);
        if (!hasPermission($permission))
            throw new CoreException(__("message.forbidden403", ['permission' => $permission, 'roles' => $authRoles]), 403);
        $input["class_model"] = $classModel;
        return $input;
    }

    public function process($input, $originalInput)
    {
        #1 UPDATE bpjs_kes_bill_details

        $dataBpjsKesBillDetail = BpjsKesBillDetails::find($input["id"]);
        $dataBpjsKesBillDetail->description = $input["description"];
        $dataBpjsKesBillDetail->status_code = "calculated";

        #2 UPDATE bpjs_kes_bill_detail_upah_components
        foreach ($input["child_data_bpjs_kes_bill_detail_upah_components"] as $childUpahCompItem) {
            if (isset($childUpahCompItem["id"])) {
                $childUpahCompItem["model"] = "bpjs_kes_bill_detail_upah_components";
                $childUpahCompItem["bpjs_kes_bill_detail_id"] = $input["id"];
                $childUpahCompItem["component_value"] = $childUpahCompItem["koefisien"] * $childUpahCompItem["default_value"];
                $childDataUpahCompUpdated = CallService::run("Edit", $childUpahCompItem);
            }
        }

        #3 HITUNG IURAN BPJS PERUSAHAAN DAN TENAGA KERJA
        // HITUNG UPAH
        $jumlahUpah = BpjsKesBillDetailUpahComponents::where('bpjs_kes_bill_detail_id', '=', $input["id"])
            ->sum('component_value');
        // DATA OBJECT PREMI THIS MONTH
        $objectPremi = ConfigProsenBpjsKesehatan::select('id', 'periode_month', 'prosen_premi_company', 'prosen_premi_employee', 'upah_min_value', 'upah_max_value')->where('periode_month', '=', $input["periode_month"])
            ->first();
        $prosenPremiCompany = $objectPremi->prosen_premi_company;
        $prosenPremiEmployee = $objectPremi->prosen_premi_employee;
        $upahMin = $objectPremi->upah_min_value;
        $upahMax = $objectPremi->upah_max_value;
        // THEN CALCULATE IURAN PERUSAHAAN DAN TENAGA KERJA BASED ON PREMI
        $iuranCompany = 0;
        $iuranEmployee = 0;

        if ($jumlahUpah >= $upahMax) {
            $iuranCompany =  round(($upahMax * $prosenPremiCompany) / 100,0);
            $iuranEmployee =  round(($upahMax * $prosenPremiEmployee) / 100,0);
        } else if ($jumlahUpah >= $upahMin and $jumlahUpah < $upahMax) {
            $iuranCompany =  round(($jumlahUpah * $prosenPremiCompany) / 100,0);
            $iuranEmployee =  round(($jumlahUpah * $prosenPremiEmployee) / 100,0);
        } 
        $dataBpjsKesBillDetail->upah_value = $jumlahUpah;
        $dataBpjsKesBillDetail->iuran_company_value = $iuranCompany;
        $dataBpjsKesBillDetail->iuran_employee_value = $iuranEmployee;

        #4 UPDATE bpjs_kes_bill_detail_tambahan
        foreach ($input["child_data_bpjs_kes_bill_detail_tambahan"] as $childTambahanItem) {
            if (isset($childTambahanItem["id"])) {
                $childTambahanItem["model"] = "bpjs_kes_bill_detail_tambahan";
                $childTambahanItem["bpjs_kes_bill_detail_id"] = $input["id"];
                $childTambahanItem["iuran_value"] = $iuranEmployee;
                $childDataTambahanUpdated = CallService::run("Edit", $childTambahanItem);
            }
        }

        #5 HITUNG JUMLAH BPJS TAMBAHAN
        $jumlahBpjsTambahan = BpjsKesBillDetailTambahan::where('bpjs_kes_bill_detail_id', '=', $input["id"])
            ->sum('iuran_value');
        $dataBpjsKesBillDetail->iuran_tambahan_value = $jumlahBpjsTambahan;

        # HITUNG TOTAL IURAN
        $totalIuran = round($iuranCompany + $iuranEmployee + $jumlahBpjsTambahan,0);
        $dataBpjsKesBillDetail->total_iuran_value = $totalIuran;


        #6 UPDATE DETAIL
        $dataBpjsKesBillDetail->save();

        #7 UPDATE PARENT bpjs_kes_bills
        $totalIuranCompany = BpjsKesBillDetails::where('bpjs_kes_bill_id', '=', $dataBpjsKesBillDetail->bpjs_kes_bill_id)
            ->sum('iuran_company_value');
        $totalIuranEmployee = BpjsKesBillDetails::where('bpjs_kes_bill_id', '=', $dataBpjsKesBillDetail->bpjs_kes_bill_id)
            ->sum('iuran_employee_value');
        $totalIuranTambahan = BpjsKesBillDetails::where('bpjs_kes_bill_id', '=', $dataBpjsKesBillDetail->bpjs_kes_bill_id)
            ->sum('iuran_tambahan_value');
        BpjsKesBills::where('id', '=', $dataBpjsKesBillDetail->bpjs_kes_bill_id)
            ->update([
                'total_iuran_company_value' => $totalIuranCompany,
                'total_iuran_employee_value' => $totalIuranEmployee,
                'total_iuran_tambahan_value' => $totalIuranTambahan,
                'total_iuran_value' => $totalIuranCompany + $totalIuranTambahan + $totalIuranTambahan,
            ]);

        // THEN SHOW

        $classModel = $input["class_model"];
        $selectableList = [];
        $tableJoinList = [];
        $params = ["id" => $input["id"]];

        foreach ($classModel::FIELD_VIEW as $list) {
            $selectableList[] = $classModel::TABLE . "." . $list;
        }

        $i = 0;
        $FIELD_RELATION = [
            "bpjs_kes_bill_id" => [
                "linkTable" => "bpjs_kes_bills",
                "aliasTable" => "B",
                "linkField" => "id",
                "displayName" => "rel_bpjs_kes_bill_id",
                "selectFields" => ["id"],
                "selectValue" => "id AS rel_bpjs_kes_bill_id"
            ],
            "employee_id" => [
                "linkTable" => "employees",
                "aliasTable" => "C",
                "linkField" => "id",
                "displayName" => "rel_employee_id",
                "selectFields" => ["fullname"],
                "selectValue" => "id AS rel_employee_id"
            ],
            "created_by" => [
                "linkTable" => "users",
                "aliasTable" => "D",
                "linkField" => "id",
                "displayName" => "rel_created_by",
                "selectFields" => ["username"],
                "selectValue" => "id AS rel_created_by"
            ],
            "updated_by" => [
                "linkTable" => "users",
                "aliasTable" => "E",
                "linkField" => "id",
                "displayName" => "rel_updated_by",
                "selectFields" => ["username"],
                "selectValue" => "id AS rel_updated_by"
            ],
        ];
        foreach ($FIELD_RELATION as $key => $relation) {
            $alias = toAlpha($i + 1);
            ///
            $fieldDisplayed = "CONCAT_WS (' - ',";
            foreach ($relation["selectFields"] as $keyField) {
                $fieldDisplayed .= $alias . '.' . $keyField . ",";
            }
            $fieldDisplayed = substr($fieldDisplayed, 0, strlen($fieldDisplayed) - 1);
            $fieldDisplayed .= ") AS " . $relation["displayName"];
            $selectableList[] = $fieldDisplayed;
            ///
            $tableJoinList[] = "LEFT JOIN " . $relation["linkTable"] . " " . $alias . " ON " .
                $classModel::TABLE . "." . $key . " = " .  $alias . "." . $relation["linkField"];
            $i++;
        }

        if (!empty($classModel::CUSTOM_SELECT)) $selectableList[] = $classModel::CUSTOM_SELECT;

        $condition = " WHERE " . $classModel::TABLE . ".id = :id";

        $sql = "SELECT " . implode(", ", $selectableList) . ", B.periode_month, JP.job_position_name as rel_job_position_id, MSE.name as rel_status_employment_id
            FROM " . $classModel::TABLE . " " .
            implode(" ", $tableJoinList) . "
            LEFT JOIN employee_on_periodes EOP on bpjs_kes_bill_details.employee_id = EOP.employee_id AND EOP.periode_month = B.periode_month 
            LEFT JOIN job_positions JP ON EOP.job_position_id = JP.id
            LEFT JOIN master_status_employments MSE ON EOP.status_employment_id = MSE.id
            " . $condition;

        $object =  DB::selectOne($sql, $params);
        if (is_null($object)) {
            throw new CoreException(__("message.dataNotFound", ['id' => $input["id"]]));
        }

        // DATA OBJECT PREMI THIS MONTH
        $objectPremi = ConfigProsenBpjsKesehatan::select('id', 'periode_month', 'prosen_premi_company', 'prosen_premi_employee', 'upah_min_value', 'upah_max_value')->where('periode_month', '=', $object->periode_month)
            ->first();
        $object->data_premi = $objectPremi;
        // DATA DETAILS
        $CHILD_TABLE = [
            "bpjs_kes_bill_detail_upah_components" => [
                "foreignField" => "bpjs_kes_bill_detail_id"
            ],
            "bpjs_kes_bill_detail_tambahan" => [
                "foreignField" => "bpjs_kes_bill_detail_id"
            ],
        ];

        foreach ($CHILD_TABLE as $keyItem => $valItem) {
            $childModuleName = "child_data_" . $keyItem;
            $foreignFieldName = $valItem["foreignField"];
            $childItem["model"] = $keyItem;
            $childItem["sort"] = "ASC";
            $childItem[$foreignFieldName] = $object->id;
            $object->$childModuleName = CallService::run("Dataset", $childItem)->original["data"];
        }
        // END SHOW


        $response["data"] = $object;
        $response["message"] = __("message.successfullyAdd");
        return $response;
    }

    protected function validation()
    {
        return [
            "periode_month" => "required"
        ];
    }
}
