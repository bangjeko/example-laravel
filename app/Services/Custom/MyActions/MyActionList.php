<?php

namespace App\Services\Custom\MyActions;

use Illuminate\Support\Facades\DB;
use App\CoreService\CoreService;

class MyActionList extends CoreService
{

    public $transaction = false;
    public $permission = null;

    public function prepare($input)
    {
        return $input;
    }

    public function process($input, $originalInput)
    {
        return [
            "data" => [],
            "total" => 0,
            "totalPage" => 0,
            "totalMyActions" => 0,
        ];
    }

    protected function validation()
    {
        return [];
    }
}
