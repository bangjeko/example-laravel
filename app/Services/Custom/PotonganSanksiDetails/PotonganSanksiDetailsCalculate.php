<?php

namespace App\Services\Custom\PotonganSanksiDetails;

use App\CoreService\CallService;
use App\CoreService\CoreException;
use App\CoreService\CoreService;
use App\Models\PotonganSanksi;
use App\Models\PotonganSanksiComponents;
use App\Models\PotonganSanksiDetails;
use App\Models\TunjanganBeasiswa;
use App\Models\TunjanganBeasiswaComponents;
use App\Models\TunjanganBeasiswaDetails;
use App\Models\TunjanganMakan;
use App\Models\TunjanganMakanDetails;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Carbon\Carbon;

class PotonganSanksiDetailsCalculate extends CoreService
{

    public $transaction = true;
    public $task = null;

    public function prepare($input)
    {
        $model = "potongan-sanksi-details";
        $classModel = "\\App\\Models\\" . Str::ucfirst(Str::camel($model));
        $permission = "create-" . $model;
        $authRoles = getRoleName(Auth::user()->role_id);
        if (!class_exists($classModel))
            throw new CoreException(__("message.model404", ['model' => $model]), 404);
        if (!$classModel::IS_ADD)
            throw new CoreException("Not found", 404);
        if (!hasPermission($permission))
            throw new CoreException(__("message.forbidden403", ['permission' => $permission, 'roles' => $authRoles]), 403);
        $input["class_model"] = $classModel;
        return $input;
    }

    public function process($input, $originalInput)
    {
        $periodeMonth = $input["periode_month"];
        #1 FIND 

        $dataPotonganSanksiDetails = PotonganSanksiDetails::find($input["id"]);
        $dataPotonganSanksiDetails->description = $input["description"];
        $dataPotonganSanksiDetails->status_code = "calculated";

        // MULAI HITUNG
        $potonganValue = 0;

        if (isset($input["data_potongan_sanksi_components"])) {
            foreach ($input["data_potongan_sanksi_components"] as $kDComp => $valComp) {
                if (isset($valComp["id"])) {
                    $valComp["model"] = "potongan_sanksi_components";
                    $valComp["potongan_sanksi_detail_id"] = $dataPotonganSanksiDetails->id;
                    $childDataUpdated = CallService::run("Edit", $valComp);
                } else {
                    if (isset($valComp["employee_sanksi_id"]) and isset($valComp["component_value"]) and $valComp["active"] == 1) {
                        $valComp["model"] = "potongan_sanksi_components";
                        $valComp["potongan_sanksi_detail_id"] = $dataPotonganSanksiDetails->id;
                        $childDataInserted = CallService::run("Add", $valComp);
                    }
                }
            }
        }

        $potonganValue = PotonganSanksiComponents::where('potongan_sanksi_detail_id', '=', $dataPotonganSanksiDetails->id)
            ->where('active', '=', 1)
            ->sum('component_value');


        #6 UPDATE DETAIL
        $dataPotonganSanksiDetails->potongan_value = $potonganValue;
        $dataPotonganSanksiDetails->save();


        #7 UPDATE PARENT tunjangan_makans
        $totalPotongan = PotonganSanksiDetails::where('potongan_sanksi_id', '=', $dataPotonganSanksiDetails->potongan_sanksi_id)
            ->sum('potongan_value');

        PotonganSanksi::where('id', '=', $dataPotonganSanksiDetails->potongan_sanksi_id)
            ->update([
                'total_potongan_value' => $totalPotongan,
            ]);

        # UPDATE EMPLOYEE SANKSI
        

        // THEN SHOW
        $classModel = $input["class_model"];
        $selectableList = [];
        $tableJoinList = [];
        $params = ["id" => $input["id"]];

        foreach ($classModel::FIELD_VIEW as $list) {
            $selectableList[] = $classModel::TABLE . "." . $list;
        }

        $i = 0;
        $FIELD_RELATION = [
            "potongan_sanksi_id" => [
                "linkTable" => "potongan_sanksi",
                "aliasTable" => "B",
                "linkField" => "id",
                "displayName" => "rel_potongan_sanksi_id",
                "selectFields" => ["id"],
                "selectValue" => "id AS rel_potongan_sanksi_id"
            ],
            "employee_id" => [
                "linkTable" => "employees",
                "aliasTable" => "C",
                "linkField" => "id",
                "displayName" => "rel_employee_id",
                "selectFields" => ["fullname"],
                "selectValue" => "id AS rel_employee_id"
            ],
            "created_by" => [
                "linkTable" => "users",
                "aliasTable" => "D",
                "linkField" => "id",
                "displayName" => "rel_created_by",
                "selectFields" => ["username"],
                "selectValue" => "id AS rel_created_by"
            ],
            "updated_by" => [
                "linkTable" => "users",
                "aliasTable" => "E",
                "linkField" => "id",
                "displayName" => "rel_updated_by",
                "selectFields" => ["username"],
                "selectValue" => "id AS rel_updated_by"
            ],
        ];
        foreach ($FIELD_RELATION as $key => $relation) {
            $alias = toAlpha($i + 1);
            ///
            $fieldDisplayed = "CONCAT_WS (' - ',";
            foreach ($relation["selectFields"] as $keyField) {
                $fieldDisplayed .= $alias . '.' . $keyField . ",";
            }
            $fieldDisplayed = substr($fieldDisplayed, 0, strlen($fieldDisplayed) - 1);
            $fieldDisplayed .= ") AS " . $relation["displayName"];
            $selectableList[] = $fieldDisplayed;
            ///
            $tableJoinList[] = "LEFT JOIN " . $relation["linkTable"] . " " . $alias . " ON " .
                $classModel::TABLE . "." . $key . " = " .  $alias . "." . $relation["linkField"];
            $i++;
        }

        if (!empty($classModel::CUSTOM_SELECT)) $selectableList[] = $classModel::CUSTOM_SELECT;

        $condition = " WHERE " . $classModel::TABLE . ".id = :id";

        $sql = "SELECT " . implode(", ", $selectableList) . ", EOP.is_mpp, B.periode_month, EMP.date_of_birth, JP.job_position_name as rel_job_position_id, MSE.name as rel_status_employment_id
            FROM " . $classModel::TABLE . " " .
            implode(" ", $tableJoinList) . "
            LEFT JOIN employee_on_periodes EOP on potongan_sanksi_details.employee_id = EOP.employee_id AND EOP.periode_month = B.periode_month 
            LEFT JOIN employees EMP on potongan_sanksi_details.employee_id = EMP.id 
            LEFT JOIN job_positions JP ON EOP.job_position_id = JP.id
            LEFT JOIN master_status_employments MSE ON EOP.status_employment_id = MSE.id
            " . $condition;


        $object =  DB::selectOne($sql, $params);
        if (is_null($object)) {
            throw new CoreException(__("message.dataNotFound", ['id' => $input["id"]]));
        }

        $fieldCasting = $classModel::FIELD_CASTING;
        if (!empty($fieldCasting)) {
            foreach ($fieldCasting as $item => $k) {
                if (isset($fieldCasting[$item])) {
                    if (array_key_exists($item, $fieldCasting)) {
                        if ($fieldCasting[$item] == 'float') {
                            $object->$item = (float) $object->$item;
                        }
                    }
                }
            }
        }

        // DATA DETAILS
        // STATIC UNTUK ANAK KANDUNG DAN ANAK ANGKAT
        $paramDataComponents = [];
        $paramDataComponents["potongan_sanksi_detail_id"] = $input["id"];
        $paramDataComponents["employee_id"] = $object->employee_id;

        $sqlDataComponents = "WITH data_sanksi_not_finished AS (
            SELECT ES.* FROM employee_sanksi ES WHERE ES.employee_id = :employee_id
                AND ES.is_reduce_salary = true AND ES.status_payment = 'not_finished_yet'
            UNION ALL
            SELECT B.* FROM potongan_sanksi_components A
            JOIN employee_sanksi B ON A.employee_sanksi_id = B.id 
            WHERE A.potongan_sanksi_detail_id = :potongan_sanksi_detail_id
        ),potongan_components AS (
            SELECT * FROM potongan_sanksi_components WHERE potongan_sanksi_detail_id = :potongan_sanksi_detail_id
        )
        SELECT DISTINCT PSC.id, PSC.potongan_sanksi_detail_id, ES.id as employee_sanksi_id, 
                CASE 
                    WHEN PSC.component_value IS NOT NULL 
                    THEN PSC.component_value ELSE 0 
                END AS component_value, 
                PSC.description, PSC.active,
                ES.description as sanksi_description, ES.sanksi_value, ES.base_payment_value, ES.count_payment, ES.remaining_sanksi_value, ES.total_payment_value , MST.id as sanksi_type_id, MST.name as rel_sanksi_type_id
                FROM data_sanksi_not_finished ES
                JOIN master_sanksi_types MST ON ES.sanksi_type_id = MST.id
                LEFT JOIN potongan_components PSC ON PSC.employee_sanksi_id = ES.id
                WHERE ES.employee_id = :employee_id
                AND ES.is_reduce_salary = true";
        $dataComponents = DB::select($sqlDataComponents, $paramDataComponents);

        if (!empty($dataComponents)) {
            array_map(function ($key) {
                foreach ($key as $field => $value) {
                    $key->component_value = (float) $key->component_value;
                    $key->sanksi_value = (float) $key->sanksi_value;
                    $key->base_payment_value = (float) $key->base_payment_value;
                    $key->count_payment = (float) $key->count_payment;
                    $key->remaining_sanksi_value = (float) $key->remaining_sanksi_value;
                    $key->total_payment_value = (float) $key->total_payment_value;
                }
                return $key;
            }, $dataComponents);
        }

        $object->data_potongan_sanksi_components = $dataComponents;
        // END SHOW

        $response["data"] = $object;
        $response["message"] = __("message.successfullyAdd");
        return $response;
    }

    protected function validation()
    {
        return [
            "periode_month" => "required"
        ];
    }
}
