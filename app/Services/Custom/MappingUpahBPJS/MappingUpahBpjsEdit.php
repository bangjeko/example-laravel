<?php

namespace App\Services\Custom\MappingUpahBPJS;

use App\CoreService\CoreException;
use Illuminate\Support\Facades\DB;
use App\CoreService\CoreService;
use App\Models\MappingUpahBpjs;
use Illuminate\Support\Facades\Auth;

class MappingUpahBpjsEdit extends CoreService
{

    public $transaction = true;
    public $permission = null;

    public function prepare($input)
    {
        $authRoles = getRoleName(Auth::user()->role_id);
        $permission = "update-mapping-upah-bpjs";
        if (!hasPermission($permission))
            throw new CoreException(__("message.forbidden403", ['permission' => $permission, 'role' => $authRoles]), 403);

        return $input;
    }

    public function process($input, $originalInput)
    {
        $salaryComponentId = $input["id"];
        $jobPositionCategoryId = $input["job_position_category_id"];
        $statusEmploymentId = $input["status_employment_id"];
        $koefisien = isset($input["koefisien"]) ? $input["koefisien"] : null;
        $description = isset($input["description"]) ? $input["description"] : null;
        $valueMapping = $input["active"];
        $mappingExists = DB::selectOne("SELECT * FROM mapping_upah_bpjs 
        WHERE job_position_category_id = :job_position_category_id AND status_employment_id = :status_employment_id
        AND salary_component_id = :salary_component_id", [
            "job_position_category_id" => $jobPositionCategoryId,
            "status_employment_id" => $statusEmploymentId,
            "salary_component_id" => $salaryComponentId
        ]);
        $value = '';
        if ($valueMapping == 1) {
            $value = 'Mengaktifkan';
        } else {
            $value = 'Menonaktifkan';
        }
        if (!$mappingExists) {
            // DO INSERT
            $objectData = new MappingUpahBpjs;

            $objectData->job_position_category_id = $jobPositionCategoryId;
            $objectData->status_employment_id = $statusEmploymentId;
            $objectData->salary_component_id = $salaryComponentId;
            $objectData->koefisien = $koefisien;
            $objectData->description = $description;
            $objectData->active = $valueMapping;
            $objectData->created_by = Auth::user()->id;
            $objectData->updated_by = Auth::user()->id;

            $objectData->save();
            
        } else {
            // DO UPDATE
            MappingUpahBpjs::where('status_employment_id', '=', $statusEmploymentId)
                ->where('job_position_category_id', '=', $jobPositionCategoryId)
                ->where('salary_component_id', '=', $salaryComponentId)
                ->update([
                    'koefisien' => $koefisien,
                    'description' => $description,
                    'active' => $valueMapping,
                    'updated_by' => Auth::user()->id,
                ]);
        }

        return [
            "message" => __("message.succesfullyUpdate")
        ];
    }

    protected function validation()
    {
        return [
            "id" => "required",
            "job_position_category_id" => "required",
            "status_employment_id" => "required",
            "active" => "required"
        ];
    }
}
