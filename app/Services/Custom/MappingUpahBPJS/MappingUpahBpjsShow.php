<?php

namespace App\Services\Custom\MappingUpahBPJS;

use App\CoreService\CallService;
use App\CoreService\CoreException;
use App\CoreService\CoreService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;

class MappingUpahBpjsShow extends CoreService
{

    public $transaction = false;
    public $task = null;

    public function prepare($input)
    {
        $mainModel = "mapping-upah-bpjs";
        $permission = "show-" . $mainModel;
        $authRoles = getRoleName(Auth::user()->role_id);
        $classModel = "\\App\\Models\\" . Str::ucfirst(Str::camel($mainModel));
        if (!class_exists($classModel))
            throw new CoreException(__("message.model404", ['model' => $mainModel]), 404);

        if (!$classModel::IS_LIST)
            throw new CoreException("Not found", 404);

        if (!hasPermission($permission))
            throw new CoreException(__("message.forbidden403", ['permission' => $permission, 'role' => $authRoles]), 403);

        $input["class_model"] = $classModel;
        return $input;
    }

    public function process($input, $originalInput)
    {

        $modelA = "master_salary_components";
        $classModelA = "\\App\\Models\\" . Str::ucfirst(Str::camel($modelA));

        $selectableListModelA = [];
        $tableJoinListModelA = [];
        $params = ["id" => $input["id"]];


        $FIELD_VIEW_MODELA = ["id", "salary_component_category_id", "name"];

        foreach ($FIELD_VIEW_MODELA as $list) {
            $selectableListModelA[] = $classModelA::TABLE . "." . $list;
        }

        $i = 0;
        $FIELD_RELATION = [
            "salary_component_category_id" => [
                "linkTable" => "master_salary_component_categories",
                "aliasTable" => "B",
                "linkField" => "id",
                "displayName" => "rel_salary_component_category_id",
                "selectFields" => ["name"],
                "selectValue" => "id AS rel_salary_component_category_id"
            ],
        ];
        foreach ($FIELD_RELATION as $key => $relation) {
            $alias = toAlpha($i + 1);
            $fieldDisplayed = "CONCAT_WS (' - ',";
            foreach ($relation["selectFields"] as $keyField) {
                $fieldDisplayed .= $alias . '.' . $keyField . ",";
            }
            $fieldDisplayed = substr($fieldDisplayed, 0, strlen($fieldDisplayed) - 1);
            $fieldDisplayed .= ") AS " . $relation["displayName"];
            $selectableListModelA[] = $fieldDisplayed;
            ///
            $tableJoinListModelA[] = "LEFT JOIN " . $relation["linkTable"] . " " . $alias . " ON " .
                $classModelA::TABLE . "." . $key . " = " .  $alias . "." . $relation["linkField"];
            $i++;
        }

        if (!empty($classModelA::CUSTOM_SELECT)) $selectableList[] = $classModelA::CUSTOM_SELECT;

        $conditionModelA = " WHERE " . $classModelA::TABLE . ".id = :id";

        $sqlModelA = "SELECT " . implode(", ", $selectableListModelA) . "
            FROM " . $classModelA::TABLE . " " .
            implode(" ", $tableJoinListModelA) . $conditionModelA;

        $objectModelA =  DB::selectOne($sqlModelA, $params);
        if (is_null($objectModelA)) {
            throw new CoreException(__("message.dataNotFound", ['id' => $input["id"]]));
        }


        // DATA SAVED MAPPING
        $dataMapping = [];
        $salaryComponentId = $input["id"];
        $jobPositionId = $input["job_position_category_id"];
        $statusEmploymentId = $input["status_employment_id"];
        $objectMaping = DB::select("SELECT A.*, B.name as rel_salary_component_id 
        FROM mapping_upah_bpjs A
        LEFT JOIN master_salary_components B ON A.salary_component_id = B.id
        WHERE A.job_position_category_id = :job_position_category_id AND A.status_employment_id = :status_employment_id 
        AND A.salary_component_id = :salary_component_id
        ", [
            "job_position_category_id" => $jobPositionId,
            "status_employment_id" => $statusEmploymentId,
            "salary_component_id" => $salaryComponentId
        ]);
        foreach ($objectMaping as $mappingValue) {
            $dataMapping[$mappingValue->job_position_category_id][$mappingValue->status_employment_id][$mappingValue->salary_component_id] = [
                "job_position_category_id" => $mappingValue->job_position_category_id,
                "status_employment_id" => $mappingValue->status_employment_id,
                "koefisien" => $mappingValue->koefisien,
                "description" => $mappingValue->description,
                "active" => $mappingValue->active
            ];
        }

        // DEFAULT
        $objectModelA->job_position_category_id = $jobPositionId;
        $objectModelA->status_employment_id = $statusEmploymentId;
        $objectModelA->koefisien = 0;
        $objectModelA->description = null;
        $objectModelA->active = 0 ;
        //


        foreach ($dataMapping as $dValue) {
            $objectModelA->job_position_category_id = isset($dataMapping[$jobPositionId][$statusEmploymentId][$objectModelA->id]) ? $dataMapping[$jobPositionId][$statusEmploymentId][$objectModelA->id]["job_position_category_id"] : null;
            $objectModelA->status_employment_id = isset($dataMapping[$jobPositionId][$statusEmploymentId][$objectModelA->id]) ? $dataMapping[$jobPositionId][$statusEmploymentId][$objectModelA->id]["status_employment_id"] : null;
            $objectModelA->koefisien = isset($dataMapping[$jobPositionId][$statusEmploymentId][$objectModelA->id]) ? $dataMapping[$jobPositionId][$statusEmploymentId][$objectModelA->id]["koefisien"] : null;
            $objectModelA->description = isset($dataMapping[$jobPositionId][$statusEmploymentId][$objectModelA->id]) ? $dataMapping[$jobPositionId][$statusEmploymentId][$objectModelA->id]["description"] : null;
            $objectModelA->active = isset($dataMapping[$jobPositionId][$statusEmploymentId][$objectModelA->id]) ? $dataMapping[$jobPositionId][$statusEmploymentId][$objectModelA->id]["active"] : 0;
        }

        // END FOR IMG PHOTO CREATED BY
        return [
            "data" => $objectModelA
        ];
    }

    protected function validation()
    {
        return [
            "id" => "required",
            "job_position_category_id" => "required",
            "status_employment_id" => "required"
        ];
    }
}
