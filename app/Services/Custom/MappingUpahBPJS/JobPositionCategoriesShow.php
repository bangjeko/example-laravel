<?php

namespace App\Services\Custom\MappingUpahBPJS;

use App\CoreService\CallService;
use App\CoreService\CoreException;
use App\CoreService\CoreService;
use App\Models\MasterStatusEmployments;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;

class JobPositionCategoriesShow extends CoreService
{

    public $transaction = false;
    public $task = null;

    public function prepare($input)
    {
        $model = "master-job-position-categories";
        $permission = "show-" . $model;
        $authRoles = getRoleName(Auth::user()->role_id);
        $classModel = "\\App\\Models\\" . Str::ucfirst(Str::camel($model));
        if (!class_exists($classModel))
            throw new CoreException(__("message.model404", ['model' => $model]), 404);

        if (!$classModel::IS_LIST)
            throw new CoreException("Not found", 404);

        if (!hasPermission($permission))
            throw new CoreException(__("message.forbidden403", ['permission' => $permission, 'role' => $authRoles]), 403);

        $input["class_model"] = $classModel;
        return $input;
    }

    public function process($input, $originalInput)
    {
        $classModel = $input["class_model"];
        $selectableList = [];
        $tableJoinList = [];
        $params = ["id" => $input["id"]];

        $FIELD_VIEW = ["id", "name", "employment_level_id"];

        foreach ($FIELD_VIEW as $list) {
            $selectableList[] = $classModel::TABLE . "." . $list;
        }

        $i = 0;
        $FIELD_RELATION = [
            "employment_level_id" => [
                "linkTable" => "master_employment_levels",
                "aliasTable" => "B",
                "linkField" => "id",
                "displayName" => "rel_employment_level_id",
                "selectFields" => ["name"],
                "selectValue" => "id AS rel_employment_level_id"
            ],
        ];
        foreach ($FIELD_RELATION as $key => $relation) {
            $alias = toAlpha($i + 1);
            ///
            $fieldDisplayed = "CONCAT_WS (' - ',";
            foreach ($relation["selectFields"] as $keyField) {
                $fieldDisplayed .= $alias . '.' . $keyField . ",";
            }
            $fieldDisplayed = substr($fieldDisplayed, 0, strlen($fieldDisplayed) - 1);
            $fieldDisplayed .= ") AS " . $relation["displayName"];
            $selectableList[] = $fieldDisplayed;
            ///
            // $selectableList[] = $alias . "." . $relation["selectValue"];

            $tableJoinList[] = "LEFT JOIN " . $relation["linkTable"] . " " . $alias . " ON " .
                $classModel::TABLE . "." . $key . " = " .  $alias . "." . $relation["linkField"];
            $i++;
        }

        if (!empty($classModel::CUSTOM_SELECT)) $selectableList[] = $classModel::CUSTOM_SELECT;

        $condition = " WHERE " . $classModel::TABLE . ".id = :id";

        $sql = "SELECT " . implode(", ", $selectableList) . " 
            FROM " . $classModel::TABLE . " " .
            implode(" ", $tableJoinList) . $condition;



        $object =  DB::selectOne($sql, $params);
        if (is_null($object)) {
            throw new CoreException(__("message.dataNotFound", ['id' => $input["id"]]));
        }

        // GET DATA STATUS EMPLOYMENTS
        $employmentLevelId = $object->employment_level_id;
        $dataStatusEmployments = MasterStatusEmployments::select('id','name','employment_level_id')
            ->where('employment_level_id', '=', $employmentLevelId)
            ->get();
        $object->data_status_employments = $dataStatusEmployments;
        return [
            "data" => $object
        ];
    }

    protected function validation()
    {
        return [];
    }
}
