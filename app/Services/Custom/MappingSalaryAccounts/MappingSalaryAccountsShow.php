<?php

namespace App\Services\Custom\MappingSalaryAccounts;

use App\CoreService\CallService;
use App\CoreService\CoreException;
use App\CoreService\CoreService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;

class MappingSalaryAccountsShow extends CoreService
{

    public $transaction = false;
    public $task = null;

    public function prepare($input)
    {
        $mainModel = 'mapping-salary-accounts';
        $permission = "show-" . $mainModel;
        $authRoles = getRoleName(Auth::user()->role_id);
        $classModel = "\\App\\Models\\" . Str::ucfirst(Str::camel($mainModel));
        if (!class_exists($classModel))
            throw new CoreException(__("message.model404", ['model' => $mainModel]), 404);

        if (!$classModel::IS_LIST)
            throw new CoreException("Not found", 404);

        if (!hasPermission($permission))
            throw new CoreException(__("message.forbidden403", ['permission' => $permission, 'role' => $authRoles]), 403);

        $input["class_model"] = $classModel;
        return $input;
    }

    public function process($input, $originalInput)
    {

        $modelA = "master_salary_components";
        $classModelA = "\\App\\Models\\" . Str::ucfirst(Str::camel($modelA));

        $selectableListModelA = [];
        $tableJoinListModelA = [];
        $params = ["id" => $input["id"]];


        $FIELD_VIEW_MODELA = ["id", "salary_component_category_id", "name"];

        foreach ($FIELD_VIEW_MODELA as $list) {
            $selectableListModelA[] = $classModelA::TABLE . "." . $list;
        }

        $i = 0;
        $FIELD_RELATION = [
            "salary_component_category_id" => [
                "linkTable" => "master_salary_component_categories",
                "aliasTable" => "B",
                "linkField" => "id",
                "displayName" => "rel_salary_component_category_id",
                "selectFields" => ["name"],
                "selectValue" => "id AS rel_salary_component_category_id"
            ],
        ];
        foreach ($FIELD_RELATION as $key => $relation) {
            $alias = toAlpha($i + 1);
            $fieldDisplayed = "CONCAT_WS (' - ',";
            foreach ($relation["selectFields"] as $keyField) {
                $fieldDisplayed .= $alias . '.' . $keyField . ",";
            }
            $fieldDisplayed = substr($fieldDisplayed, 0, strlen($fieldDisplayed) - 1);
            $fieldDisplayed .= ") AS " . $relation["displayName"];
            $selectableListModelA[] = $fieldDisplayed;
            ///
            $tableJoinListModelA[] = "LEFT JOIN " . $relation["linkTable"] . " " . $alias . " ON " .
                $classModelA::TABLE . "." . $key . " = " .  $alias . "." . $relation["linkField"];
            $i++;
        }

        if (!empty($classModelA::CUSTOM_SELECT)) $selectableList[] = $classModelA::CUSTOM_SELECT;

        $jobPositionCategoryId = $input["job_position_category_id"];
        $statusEmploymentId = $input["status_employment_id"];

        $conditionModelA = " WHERE " . $classModelA::TABLE . ".id = :id";

        $sqlModelA = "WITH data_mapping AS (
                SELECT A.*, B.name as rel_salary_component_id,
                C.name as rel_status_employment_id,
                D.name as rel_job_position_category_id
                FROM mapping_salary_accounts A
                LEFT JOIN master_salary_components B ON A.salary_component_id = B.id
                LEFT JOIN master_status_employments C ON A.status_employment_id = C.id
                LEFT JOIN master_job_position_categories D ON A.job_position_category_id = D.id
                WHERE A.job_position_category_id = " . $jobPositionCategoryId . " AND A.status_employment_id = " . $statusEmploymentId . "
            )
            SELECT " . implode(", ", $selectableListModelA) . ",
            DM.job_position_category_id as job_position_category_id,
            DM.rel_job_position_category_id,
            DM.status_employment_id as status_employment_id,
            DM.rel_status_employment_id,
            DM.total_proportion_value,
            DM.active as active
            FROM " . $classModelA::TABLE . " " .
            implode(" ", $tableJoinListModelA) .
            " LEFT JOIN data_mapping AS DM ON " . $classModelA::TABLE . ".id = DM.salary_component_id " . $conditionModelA;

        $objectModelA =  DB::selectOne($sqlModelA, $params);
        if (is_null($objectModelA)) {
            throw new CoreException(__("message.dataNotFound", ['id' => $input["id"]]));
        }

        // REMARKS
        $objectModelA->job_position_category_id = $jobPositionCategoryId;
        $objectModelA->rel_job_position_category_id = DB::selectOne("SELECT name FROM master_job_position_categories WHERE id = :job_position_category_id", [
            "job_position_category_id" => $jobPositionCategoryId
        ])->name;
        $objectModelA->status_employment_id = $statusEmploymentId;
        $objectModelA->rel_status_employment_id = DB::selectOne("SELECT name FROM master_status_employments WHERE id = :status_employment_id", [
            "status_employment_id" => $statusEmploymentId
        ])->name;
        // END FOR IMG PHOTO CREATED BY
        return [
            "data" => $objectModelA
        ];
    }

    protected function validation()
    {
        return [
            "id" => "required",
            "job_position_category_id" => "required",
            "status_employment_id" => "required"
        ];
    }
}
