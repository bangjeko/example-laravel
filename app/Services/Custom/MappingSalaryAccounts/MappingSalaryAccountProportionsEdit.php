<?php

namespace App\Services\Custom\MappingSalaryAccounts;

use App\CoreService\CoreException;
use Illuminate\Support\Facades\DB;
use App\CoreService\CoreService;
use App\Models\MappingSalaryAccountProportions;
use App\Models\MappingSalaryAccounts;
use Illuminate\Support\Facades\Auth;

class MappingSalaryAccountProportionsEdit extends CoreService
{

    public $transaction = true;
    public $permission = null;

    public function prepare($input)
    {
        $authRoles = getRoleName(Auth::user()->role_id);
        $permission = "update-mapping-salary-account-proportions";
        if (!hasPermission($permission))
            throw new CoreException(__("message.forbidden403", ['permission' => $permission, 'role' => $authRoles]), 403);

        return $input;
    }

    public function process($input, $originalInput)
    {
        $jobPositionCategoryId = $input["job_position_category_id"];
        $statusEmploymentId = $input["status_employment_id"];
        $salaryComponentId = $input["salary_component_id"];
        $proportionValue = isset($input["proportion_value"]) ? $input["proportion_value"] : 0;
        $finaSalaryAccountId = (int) $input["id"];
        $valueMapping = $input["active"];

        $getMappingSalaryAccountId = DB::selectOne("SELECT id FROM mapping_salary_accounts 
        WHERE job_position_category_id = :job_position_category_id AND status_employment_id = :status_employment_id AND salary_component_id = :salary_component_id
        LIMIT 1", [
            "job_position_category_id" => $jobPositionCategoryId,
            "status_employment_id" => $statusEmploymentId,
            "salary_component_id" => $salaryComponentId,
        ]);

        if (is_null($getMappingSalaryAccountId)) {
            $objectData = new MappingSalaryAccounts;

            $objectData->job_position_category_id = $jobPositionCategoryId;
            $objectData->status_employment_id = $statusEmploymentId;
            $objectData->salary_component_id = $salaryComponentId;
            $objectData->active = 1;
            $objectData->created_by = Auth::user()->id;
            $objectData->updated_by = Auth::user()->id;
            $objectData->save();

            $mappingSalaryAccountId = $objectData->id;
        } else {
            $mappingSalaryAccountId = $getMappingSalaryAccountId->id;
        }

        $mappingExists = DB::selectOne("SELECT * FROM mapping_salary_account_proportions 
        WHERE mapping_salary_account_id = :mapping_salary_account_id 
        AND fina_salary_account_id = :fina_salary_account_id", [
            "fina_salary_account_id" => $finaSalaryAccountId,
            "mapping_salary_account_id" => $mappingSalaryAccountId,
        ]);

        $value = '';
        if ($valueMapping == 1) {
            $value = 'Mengaktifkan';
        } else {
            $value = 'Menonaktifkan';
        }
        if (!$mappingExists) {
            // DO INSERT
            $objectData = new MappingSalaryAccountProportions;

            $objectData->mapping_salary_account_id = $mappingSalaryAccountId;
            $objectData->fina_salary_account_id = $finaSalaryAccountId;
            $objectData->proportion_value = $proportionValue;
            $objectData->active = $valueMapping;
            $objectData->created_by = Auth::user()->id;
            $objectData->updated_by = Auth::user()->id;

            $objectData->save();
        } else {
            // DO UPDATE
            MappingSalaryAccountProportions::where('mapping_salary_account_id', '=', $mappingSalaryAccountId)
                ->where('fina_salary_account_id', '=', $finaSalaryAccountId)
                ->update([
                    'proportion_value' => $proportionValue,
                    'active' => $valueMapping,
                    'updated_by' => Auth::user()->id,
                ]);
        }

        // UPDATE TOTAL PROPORTION VALUE
        $totalProportionValue = MappingSalaryAccountProportions::where('mapping_salary_account_id', '=', $mappingSalaryAccountId)
            ->sum('proportion_value');

        MappingSalaryAccounts::where('id', '=', $mappingSalaryAccountId)
            ->update([
                'total_proportion_value' => $totalProportionValue,
            ]);

        $dataParent = DB::selectOne("SELECT D.id, A.job_position_category_id, A.status_employment_id, A.salary_component_id, A.total_proportion_value, A.active,
            B.name as rel_job_position_category_id,
            C.name as rel_status_employment_id,
            E.name as rel_salary_component_category_id,
            D.name as name
            FROM mapping_salary_accounts A 
            JOIN master_job_position_categories B ON A.job_position_category_id = B.id
            JOIN master_status_employments C ON A.status_employment_id = C.id
            JOIN master_salary_components D ON A.salary_component_id = D.id
            JOIN master_salary_component_categories E ON D.salary_component_category_id = E.id
            WHERE A.id = :mapping_salary_account_id", [
            "mapping_salary_account_id" => $mappingSalaryAccountId
        ]);
        $dataParent->total_proportion_value = (float) $dataParent->total_proportion_value;

        return [
            "message" => __("message.succesfullyUpdate"),
            "data" => $dataParent
        ];
    }

    protected function validation()
    {
        return [
            "id" => "required",
            "job_position_category_id" => "required",
            "status_employment_id" => "required",
            "salary_component_id" => "required",
            "active" => "required"
        ];
    }
}
