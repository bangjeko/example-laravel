<?php

namespace App\Services\Custom\MappingSalaryAccounts;

use Illuminate\Support\Facades\DB;
use App\CoreService\CoreException;
use App\CoreService\CoreService;
use Illuminate\Support\Str;

class MappingSalaryAccountsList extends CoreService
{

    public $transaction = false;
    public $permission = null;

    public function prepare($input)
    {
        $mainModel = 'mapping-salary-accounts';
        $classMainModel = "\\App\\Models\\" . Str::ucfirst(Str::camel($mainModel));
        if (!class_exists($classMainModel))
            throw new CoreException(__("message.model404", ['model' => $mainModel]), 404);

        $input["main_model"] = $classMainModel;
        return $input;
    }

    public function process($input, $originalInput)
    {
        // LIST DATA KOMPONEN GAJI
        $modelA = "master_salary_components";
        $classModelA = "\\App\\Models\\" . Str::ucfirst(Str::camel($modelA));
        $classMainModel = $input["main_model"];

        $selectableListModelA = [];
        $sortByModelA = $classModelA::TABLE . ".id";
        $sortModelA = strtoupper($input["sort"] ?? "DESC") == "ASC" ? "DESC" : "ASC";

        if (isset($input["sort_by"])) {
            if ($input["sort_by"] == 'active') {
                $sortByModelA = "DM." . $input["sort_by"];
            } else {
                $sortByModelA = $input["sort_by"];
            }
        }

        $searchableListModelA = $classModelA::FIELD_SEARCHABLE;

        $tableJoinListModelA = [];
        $filterListModelA = [];
        $paramsModelA = [];

        $FIELD_LIST = ["id", "salary_component_category_id", "name"];

        foreach ($FIELD_LIST as $list) {
            $selectableList[] = $classModelA::TABLE . "." . $list;
        }

        foreach ($classModelA::FIELD_FILTERABLE as $filter => $operator) {
            if (!is_blank($input, $filter)) {
                $filterListModelA[] = " AND " . $classModelA::TABLE . "." . $filter .  " " . $operator["operator"] . " :$filter";
                $paramsModelA[$filter] = $input[$filter];
            }
        }

        $i = 0;
        $FIELD_RELATION = [
            "salary_component_category_id" => [
                "linkTable" => "master_salary_component_categories",
                "aliasTable" => "B",
                "linkField" => "id",
                "displayName" => "rel_salary_component_category_id",
                "selectFields" => ["name"],
                "selectValue" => "id AS rel_salary_component_category_id"
            ],
        ];
        foreach ($FIELD_RELATION as $key => $relation) {
            $alias = $relation["aliasTable"];
            $fieldDisplayed = "CONCAT_WS (' - ',";
            $searchableRealtionField = "CONCAT_WS (' - ',";
            foreach ($relation["selectFields"] as $keyField) {
                $fieldDisplayed .= $alias . '.' . $keyField . ",";
                $searchableRealtionField .= $alias . '.' . $keyField . ",";
            }
            $fieldDisplayed = substr($fieldDisplayed, 0, strlen($fieldDisplayed) - 1);
            $fieldDisplayed .= ") AS " . $relation["displayName"];
            $selectableList[] = $fieldDisplayed;

            $searchableRealtionField = substr($searchableRealtionField, 0, strlen($searchableRealtionField) - 1);
            $searchableRealtionField .= ")";
            $tableJoinListModelA[] = "LEFT JOIN " . $relation["linkTable"] . " " . $alias . " ON " .
                $classModelA::TABLE . "." . $key . " = " .  $alias . "." . $relation["linkField"];
            $i++;
            $searchableListModelA[] = $searchableRealtionField;
        }

        if (!empty($classModelA::CUSTOM_SELECT)) $selectableList[] = $classModelA::CUSTOM_SELECT;

        $condition = " WHERE true";

        if (!empty($classModelA::CUSTOM_LIST_FILTER)) {
            foreach ($classModelA::CUSTOM_LIST_FILTER as $customListFilter) {
                $condition .= " AND " . $customListFilter;
            }
        }
        if (!is_blank($input, "search")) {
            $searchableListModelA = array_map(function ($item) use ($classModelA) {
                if (in_array($item, $classModelA::FIELD_SEARCHABLE)) {
                    return $classModelA::TABLE . "." . $item . " ILIKE :search";
                } else {
                    return $item . " ILIKE :search";
                }
            }, $searchableListModelA);
        } else {
            $searchableListModelA = [];
        }

        if (count($searchableListModelA) > 0 && !is_blank($input, "search"))
            $paramsModelA["search"] = "%" . strtoupper($input["search"] ?? "") . "%";

        $limit = $input["limit"] ?? 10;
        $offset = $input["offset"] ?? 0;
        if (!is_null($input["page"] ?? null)) {
            $offset = $limit * ($input["page"] - 1);
        }

        $jobPositionCategoryId = $input["job_position_category_id"];
        $statusEmploymentId = $input["status_employment_id"];

        $sqlModelA = "WITH data_mapping AS (
                SELECT A.*, B.name as rel_salary_component_id
                FROM mapping_salary_accounts A
                LEFT JOIN master_salary_components B ON A.salary_component_id = B.id
                WHERE A.job_position_category_id = " . $jobPositionCategoryId . " AND A.status_employment_id = " . $statusEmploymentId . "
            )
            SELECT " . implode(", ", $selectableList) . ",
            " . $jobPositionCategoryId . " as job_position_category_id,
            " . $statusEmploymentId . " as status_employment_id,
            DM.total_proportion_value,
            DM.active as active
            FROM " . $classModelA::TABLE . " " .
            implode(" ", $tableJoinListModelA) .
            " LEFT JOIN data_mapping AS DM ON " . $classModelA::TABLE . ".id = DM.salary_component_id " . $condition .
            (count($searchableListModelA) > 0 ? " AND (" . implode(" OR ", $searchableListModelA) . ")" : "") .
            implode("\n", $filterListModelA) . " ORDER BY " . $sortByModelA . " " . $sortModelA . " LIMIT $limit OFFSET $offset ";

        $sqlForCountModelA = "SELECT COUNT(1) AS total FROM " . $classModelA::TABLE . " " .
            implode(" ", $tableJoinListModelA) . $condition .
            (count($searchableListModelA) > 0 ? " AND (" . implode(" OR ", $searchableListModelA) . ")" : "") .
            implode("\n", $filterListModelA);

        $objectModelA =  DB::select($sqlModelA, $paramsModelA);
        $totalModelA = DB::selectOne($sqlForCountModelA, $paramsModelA)->total;
        $totalPageModelA = ceil($totalModelA / $limit);

        return [
            "data" => $objectModelA,
            "total" => $totalModelA,
            "totalPage" => $totalPageModelA
        ];
    }

    protected function validation()
    {
        return [
            "job_position_category_id" => "required",
            "status_employment_id" => "required"
        ];
    }
}
