<?php

namespace App\Services\Custom\MappingSalaryAccounts;

use Illuminate\Support\Facades\DB;
use App\CoreService\CoreException;
use App\CoreService\CoreService;
use App\Models\MappingSalaryAccounts;
use Illuminate\Support\Str;

class MappingSalaryAccountProportionsList extends CoreService
{

    public $transaction = false;
    public $permission = null;

    public function prepare($input)
    {
        $mainModel = 'mapping-salary-account-proportions';
        $classMainModel = "\\App\\Models\\" . Str::ucfirst(Str::camel($mainModel));
        if (!class_exists($classMainModel))
            throw new CoreException(__("message.model404", ['model' => $mainModel]), 404);

        $input["main_model"] = $classMainModel;
        return $input;
    }

    public function process($input, $originalInput)
    {
        // LIST DATA KOMPONEN GAJI
        $modelA = "fina_salary_accounts";
        $classModelA = "\\App\\Models\\" . Str::ucfirst(Str::camel($modelA));
        $classMainModel = $input["main_model"];

        $selectableListModelA = [];
        $sortByModelA = "DM.proportion_value";
        $sortModelA = strtoupper($input["sort"] ?? "DESC") == "ASC" ? "DESC" : "ASC";

        if (isset($input["sort_by"])) {
            if ($input["sort_by"] == 'active') {
                $sortByModelA = "DM." . $input["sort_by"];
            } else {
                $sortByModelA = $input["sort_by"];
            }
        }

        $searchableListModelA = $classModelA::FIELD_SEARCHABLE;

        $tableJoinListModelA = [];
        $filterListModelA = [];
        $paramsModelA = [];

        $FIELD_LIST = ["id","account_code", "account_name", "debit_kredit", "active", "created_by", "updated_by", "created_at", "updated_at"];

        foreach ($FIELD_LIST as $list) {
            $selectableList[] = $classModelA::TABLE . "." . $list;
        }

        foreach ($classModelA::FIELD_FILTERABLE as $filter => $operator) {
            if (!is_blank($input, $filter)) {
                $filterListModelA[] = " AND " . $classModelA::TABLE . "." . $filter .  " " . $operator["operator"] . " :$filter";
                $paramsModelA[$filter] = $input[$filter];
            }
        }

        $i = 0;
        $FIELD_RELATION = [];
        foreach ($FIELD_RELATION as $key => $relation) {
            $alias = $relation["aliasTable"];
            $fieldDisplayed = "CONCAT_WS (' - ',";
            $searchableRealtionField = "CONCAT_WS (' - ',";
            foreach ($relation["selectFields"] as $keyField) {
                $fieldDisplayed .= $alias . '.' . $keyField . ",";
                $searchableRealtionField .= $alias . '.' . $keyField . ",";
            }
            $fieldDisplayed = substr($fieldDisplayed, 0, strlen($fieldDisplayed) - 1);
            $fieldDisplayed .= ") AS " . $relation["displayName"];
            $selectableList[] = $fieldDisplayed;

            $searchableRealtionField = substr($searchableRealtionField, 0, strlen($searchableRealtionField) - 1);
            $searchableRealtionField .= ")";
            $tableJoinListModelA[] = "LEFT JOIN " . $relation["linkTable"] . " " . $alias . " ON " .
                $classModelA::TABLE . "." . $key . " = " .  $alias . "." . $relation["linkField"];
            $i++;
            $searchableListModelA[] = $searchableRealtionField;
        }

        if (!empty($classModelA::CUSTOM_SELECT)) $selectableList[] = $classModelA::CUSTOM_SELECT;

        $condition = " WHERE true";

        if (!empty($classModelA::CUSTOM_LIST_FILTER)) {
            foreach ($classModelA::CUSTOM_LIST_FILTER as $customListFilter) {
                $condition .= " AND " . $customListFilter;
            }
        }
        if (!is_blank($input, "search")) {
            $searchableListModelA = array_map(function ($item) use ($classModelA) {
                if (in_array($item, $classModelA::FIELD_SEARCHABLE)) {
                    return $classModelA::TABLE . "." . $item . " ILIKE :search";
                } else {
                    return $item . " ILIKE :search";
                }
            }, $searchableListModelA);
        } else {
            $searchableListModelA = [];
        }

        if (count($searchableListModelA) > 0 && !is_blank($input, "search"))
            $paramsModelA["search"] = "%" . strtoupper($input["search"] ?? "") . "%";

        $limit = $input["limit"] ?? 10;
        $offset = $input["offset"] ?? 0;
        if (!is_null($input["page"] ?? null)) {
            $offset = $limit * ($input["page"] - 1);
        }

        #GET MAPPING SALARY ACCOUNT ID

        $jobPositionCategoryId = $input["job_position_category_id"];
        $statusEmploymentId = $input["status_employment_id"];
        $salaryComponentId = $input["salary_component_id"];
        $mappingSalaryAccountId = DB::selectOne("SELECT id FROM mapping_salary_accounts 
        WHERE job_position_category_id = :job_position_category_id AND status_employment_id = :status_employment_id AND salary_component_id = :salary_component_id
        LIMIT 1", [
            "job_position_category_id" => $jobPositionCategoryId,
            "status_employment_id" => $statusEmploymentId,
            "salary_component_id" => $salaryComponentId,
        ]);

        if (!is_null($mappingSalaryAccountId)) {
            $mappingSalaryAccountId = $mappingSalaryAccountId->id;
        } else {
            $mappingSalaryAccountId = 0;
        }

        $sqlModelA = "WITH data_mapping AS (
                SELECT A.*
                FROM mapping_salary_account_proportions A
                LEFT JOIN fina_salary_accounts B ON A.fina_salary_account_id = B.id
                WHERE A.mapping_salary_account_id = " . $mappingSalaryAccountId . "
            )
            SELECT " . implode(", ", $selectableList) . ",
            ".$jobPositionCategoryId." as job_position_category_id,
            ".$statusEmploymentId." as status_employment_id,
            ".$salaryComponentId." as salary_component_id,
            DM.proportion_value,
            DM.active
            FROM " . $classModelA::TABLE . " " .
            implode(" ", $tableJoinListModelA) .
            " LEFT JOIN data_mapping AS DM ON " . $classModelA::TABLE . ".id = DM.fina_salary_account_id " . $condition .
            (count($searchableListModelA) > 0 ? " AND (" . implode(" OR ", $searchableListModelA) . ")" : "") .
            implode("\n", $filterListModelA) . " ORDER BY " . $sortByModelA . " " . $sortModelA . " LIMIT $limit OFFSET $offset ";

        $sqlForCountModelA = "SELECT COUNT(1) AS total FROM " . $classModelA::TABLE . " " .
            implode(" ", $tableJoinListModelA) . $condition .
            (count($searchableListModelA) > 0 ? " AND (" . implode(" OR ", $searchableListModelA) . ")" : "") .
            implode("\n", $filterListModelA);

        $objectModelA =  DB::select($sqlModelA, $paramsModelA);
        $totalModelA = DB::selectOne($sqlForCountModelA, $paramsModelA)->total;
        $totalPageModelA = ceil($totalModelA / $limit);

        array_map(function ($key) {
            foreach ($key as $field => $value) {
                $key->proportion_value = (float) $key->proportion_value;
            }
            return $key;
        }, $objectModelA);
        return [
            "data" => $objectModelA,
            "total" => $totalModelA,
            "totalPage" => $totalPageModelA
        ];
    }

    protected function validation()
    {
        return [
            "job_position_category_id" => "required",
            "status_employment_id" => "required"
        ];
    }
}
