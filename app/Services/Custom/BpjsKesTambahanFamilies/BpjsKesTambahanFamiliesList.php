<?php

namespace App\Services\Custom\BpjsKesTambahanFamilies;

use App\CoreService\CoreException;
use App\CoreService\CoreService;
use App\Models\EventDocumentations;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;
use Illuminate\Support\Carbon;

class BpjsKesTambahanFamiliesList extends CoreService
{

    public $transaction = false;
    public $task = null;

    public function prepare($input)
    {
        $model = "employee-families";
        $classModel = "\\App\\Models\\" . Str::ucfirst(Str::camel($model));
        $permission = "view-bpjs-kes-tambahan-employee-families";
        $authRoles = getRoleName(Auth::user()->role_id);
        if (!class_exists($classModel))
            throw new CoreException(__("message.model404", ['model' => $model]), 404);
        if (!$classModel::IS_LIST)
            throw new CoreException(__("message.404"), 404);
        if (!hasPermission($permission))
            throw new CoreException(__("message.forbidden403", ['permission' => $permission, 'role' => $authRoles]), 403);

        $input["class_model_name"] = $model;
        $input["class_model"] = $classModel;
        return $input;
    }

    public function process($input, $originalInput)
    {
        $classModelName = $input["class_model_name"];
        $classModel = $input["class_model"];

        $selectableList = [];
        $sortBy = "B.id, employee_families.fullname";
        $sort = strtoupper($input["sort"] ?? "DESC") == "ASC" ? "DESC" : "ASC";

        $sortableList = $classModel::FIELD_SORTABLE;
        if (isset($input["sort_by"])) {
            $sortBy = $input["sort_by"];
        }

        $searchableList = $classModel::FIELD_SEARCHABLE;

        $tableJoinList = [];
        $filterList = [];
        $params = [];

        $FIELD_LIST = ["id", "employee_id", "family_relationship_id", "fullname", "nik", "place_of_birth", "date_of_birth", "gender", "address", "telephone", "education_id", "work_description", "description", "is_bpjs_tambahan"];

        foreach ($FIELD_LIST as $list) {
            $selectableList[] = $classModel::TABLE . "." . $list;
        }

        foreach ($classModel::FIELD_FILTERABLE as $filter => $operator) {
            if (!is_blank($input, $filter)) {
                $cekTypeInput = json_decode($input[$filter], true);
                if (!is_array($cekTypeInput)) {
                    $filterList[] = " AND " . $classModel::TABLE . "." . $filter .  " " . $operator["operator"] . " :$filter";
                    $params[$filter] = $input[$filter];
                } else {
                    $input[$filter] = json_decode($input[$filter], true);
                    if ($input[$filter]["operator"] == 'between') {

                        $filterList[] = " AND " . $classModel::TABLE . "." . $filter .  " " . $input[$filter]["operator"] . " '" . $input[$filter]["value"][0] . "' AND '" . $input[$filter]["value"][1] . "'";
                    } else {

                        $filterList[] = " AND " . $classModel::TABLE . "." . $filter .  " " . $input[$filter]["operator"] . " :$filter";
                        $params[$filter] = $input[$filter];
                    }
                }
            }
        }

        $i = 0;
        $FIELD_RELATION = [
            "employee_id" => [
                "linkTable" => "employees",
                "aliasTable" => "B",
                "linkField" => "id",
                "displayName" => "rel_employee_id",
                "selectFields" => ["fullname"],
                "selectValue" => "id AS rel_employee_id"
            ],
            "family_relationship_id" => [
                "linkTable" => "master_family_relationships",
                "aliasTable" => "C",
                "linkField" => "id",
                "displayName" => "rel_family_relationship_id",
                "selectFields" => ["name"],
                "selectValue" => "id AS rel_family_relationship_id"
            ],
            "education_id" => [
                "linkTable" => "master_educations",
                "aliasTable" => "D",
                "linkField" => "id",
                "displayName" => "rel_education_id",
                "selectFields" => ["name"],
                "selectValue" => "id AS rel_education_id"
            ],
        ];
        foreach ($FIELD_RELATION as $key => $relation) {
            $alias = $relation["aliasTable"];
            $fieldDisplayed = "CONCAT_WS (' - ',";
            $searchableRealtionField = "CONCAT_WS (' - ',";
            foreach ($relation["selectFields"] as $keyField) {
                $fieldDisplayed .= $alias . '.' . $keyField . ",";
                $searchableRealtionField .= $alias . '.' . $keyField . ",";
            }
            $fieldDisplayed = substr($fieldDisplayed, 0, strlen($fieldDisplayed) - 1);
            $fieldDisplayed .= ") AS " . $relation["displayName"];
            $selectableList[] = $fieldDisplayed;

            //
            $searchableRealtionField = substr($searchableRealtionField, 0, strlen($searchableRealtionField) - 1);
            $searchableRealtionField .= ")";


            ///
            // $selectableList[] = $alias . "." . $relation["selectValue"];

            $tableJoinList[] = "LEFT JOIN " . $relation["linkTable"] . " " . $alias . " ON " .
                $classModel::TABLE . "." . $key . " = " .  $alias . "." . $relation["linkField"];
            $i++;

            //
            $searchableList[] = $searchableRealtionField;
        }

        if (!empty($classModel::CUSTOM_SELECT)) $selectableList[] = $classModel::CUSTOM_SELECT;

        $condition = " WHERE true";

        if (!empty($classModel::CUSTOM_LIST_FILTER)) {
            foreach ($classModel::CUSTOM_LIST_FILTER as $customListFilter) {
                $condition .= " AND " . $customListFilter;
            }
        }
        if (!is_blank($input, "search")) {
            $searchableList = array_map(function ($item) use ($classModel) {
                if (in_array($item, $classModel::FIELD_SEARCHABLE)) {
                    return $classModel::TABLE . "." . $item . " ILIKE :search";
                } else {
                    return $item . " ILIKE :search";
                }
            }, $searchableList);
        } else {
            $searchableList = [];
        }

        if (count($searchableList) > 0 && !is_blank($input, "search"))
            $params["search"] = "%" . strtoupper($input["search"] ?? "") . "%";

        $limit = $input["limit"] ?? 10;
        $offset = $input["offset"] ?? 0;
        if (!is_null($input["page"] ?? null)) {
            $offset = $limit * ($input["page"] - 1);
        }

        $sql = "SELECT " . implode(", ", $selectableList) . "
            FROM " . $classModel::TABLE . " " .
            implode(" ", $tableJoinList) . $condition .
            (count($searchableList) > 0 ? " AND (" . implode(" OR ", $searchableList) . ")" : "") .
            implode("\n", $filterList) . " ORDER BY " . $sortBy . " " . $sort . " LIMIT $limit OFFSET $offset ";

        $sqlForCount = "SELECT COUNT(1) AS total FROM " . $classModel::TABLE . " " .
            implode(" ", $tableJoinList) . $condition .
            (count($searchableList) > 0 ? " AND (" . implode(" OR ", $searchableList) . ")" : "") .
            implode("\n", $filterList);

        $object =  DB::select($sql, $params);

        foreach ($classModel::FIELD_ARRAY as $item) {
        }

        $fieldCasting = $classModel::FIELD_CASTING;
        array_map(function ($key) use ($classModel, $classModelName, $fieldCasting) {
            foreach ($key as $field => $value) {
                if (isset($fieldCasting[$field])) {
                    if (array_key_exists($field, $fieldCasting)) {
                        if ($fieldCasting[$field] == 'float') {
                            $key->$field = (float) $key->$field;
                        }
                    }
                }
            }
            return $key;
        }, $object);


        $total = DB::selectOne($sqlForCount, $params)->total;
        $totalPage = ceil($total / $limit);
        return [
            "data" => $object,
            "total" => $total,
            "totalPage" => $totalPage,
        ];
    }

    protected function validation()
    {
        return [];
    }
}
