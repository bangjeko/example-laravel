<?php

namespace App\Services\Custom\MappingTemplateSalaryEmployees;

use App\CoreService\CoreException;
use App\CoreService\CoreService;
use App\Models\EmployeeOnPeriodes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;

class MappingTemplateSalaryEmployeeTableList extends CoreService
{

    public $transaction = false;
    public $task = null;

    public function prepare($input)
    {
        // $permission = "view-control-sheet-payroll";
        // $authRoles = getRoleName(Auth::user()->role_id);
        // if (!hasPermission($permission))
        //     throw new CoreException(__("message.forbidden403", ['permission' => $permission, 'role' => $authRoles]), 403);

        return $input;
    }

    public function process($input, $originalInput)
    {
        $modelEmployee = "employee_on_periodes";
        $classModel = "\\App\\Models\\" . Str::ucfirst(Str::camel($modelEmployee));

        $selectableList = [];
        $sortBy = $classModel::TABLE . ".status_employment_id ASC, MJPC.id ASC, C.id";

        $sort = strtoupper($input["sort"] ?? "DESC") == "ASC" ? "ASC" : "DESC";

        $sortableList = $classModel::FIELD_SORTABLE;

        if (isset($input["sort_by"])) {
            $sortBy = $input["sort_by"];
        }

        $searchableList = $classModel::FIELD_SEARCHABLE;

        $tableJoinList = [];
        $filterList = [];
        $params = [];

        $FIELD_LIST = ["id", "periode_month", "employee_id", "job_position_id", "status_employment_id", "is_mpp", "is_plt"];
        foreach ($FIELD_LIST as $list) {
            $selectableList[] = $classModel::TABLE . "." . $list;
        }

        foreach ($classModel::FIELD_FILTERABLE as $filter => $operator) {
            if (!is_blank($input, $filter)) {
                $cekTypeInput = json_decode($input[$filter], true);
                if (!is_array($cekTypeInput)) {
                    $filterList[] = " AND " . $classModel::TABLE . "." . $filter .  " " . $operator["operator"] . " :$filter";
                    $params[$filter] = $input[$filter];
                } else {
                    $input[$filter] = json_decode($input[$filter], true);
                    if ($input[$filter]["operator"] == 'between') {

                        $filterList[] = " AND " . $classModel::TABLE . "." . $filter .  " " . $input[$filter]["operator"] . " '" . $input[$filter]["value"][0] . "' AND '" . $input[$filter]["value"][1] . "'";
                    } else {

                        $filterList[] = " AND " . $classModel::TABLE . "." . $filter .  " " . $input[$filter]["operator"] . " :$filter";
                        $params[$filter] = $input[$filter];
                    }
                }
            }
        }

        $i = 0;
        $FIELD_RELATION = [
            "employee_id" => [
                "linkTable" => "employees",
                "aliasTable" => "B",
                "linkField" => "id",
                "displayName" => "rel_employee_id",
                "selectFields" => ["fullname"],
                "selectValue" => "id AS rel_employee_id"
            ],
            "job_position_id" => [
                "linkTable" => "job_positions",
                "aliasTable" => "C",
                "linkField" => "id",
                "displayName" => "rel_job_position_id",
                "selectFields" => ["job_position_name"],
                "selectValue" => "id AS rel_job_position_id"
            ],
            "status_employment_id" => [
                "linkTable" => "master_status_employments",
                "aliasTable" => "D",
                "linkField" => "id",
                "displayName" => "rel_status_employment_id",
                "selectFields" => ["name"],
                "selectValue" => "id AS rel_status_employment_id"
            ],
            "salary_calculated_by" => [
                "linkTable" => "users",
                "aliasTable" => "E",
                "linkField" => "id",
                "displayName" => "rel_salary_calculated_by",
                "selectFields" => ["username"],
                "selectValue" => "id AS rel_salary_calculated_by"
            ],
        ];
        foreach ($FIELD_RELATION as $key => $relation) {
            $alias = $relation["aliasTable"];
            $fieldDisplayed = "CONCAT_WS (' - ',";
            $searchableRealtionField = "CONCAT_WS (' - ',";
            foreach ($relation["selectFields"] as $keyField) {
                $fieldDisplayed .= $alias . '.' . $keyField . ",";
                $searchableRealtionField .= $alias . '.' . $keyField . ",";
            }
            $fieldDisplayed = substr($fieldDisplayed, 0, strlen($fieldDisplayed) - 1);
            $fieldDisplayed .= ") AS " . $relation["displayName"];
            $selectableList[] = $fieldDisplayed;

            //
            $searchableRealtionField = substr($searchableRealtionField, 0, strlen($searchableRealtionField) - 1);
            $searchableRealtionField .= ")";

            ///
            $tableJoinList[] = "LEFT JOIN " . $relation["linkTable"] . " " . $alias . " ON " .
                $classModel::TABLE . "." . $key . " = " .  $alias . "." . $relation["linkField"];
            $i++;

            //
            $searchableList[] = $searchableRealtionField;
        }

        if (!empty($classModel::CUSTOM_SELECT)) $selectableList[] = $classModel::CUSTOM_SELECT;

        $condition = " WHERE true";

        if (!empty($classModel::CUSTOM_LIST_FILTER)) {
            foreach ($classModel::CUSTOM_LIST_FILTER as $customListFilter) {
                $condition .= " AND " . $customListFilter;
            }
        }
        if (!is_blank($input, "search")) {
            $searchableList = array_map(function ($item) use ($classModel) {
                if (in_array($item, $classModel::FIELD_SEARCHABLE)) {
                    return $classModel::TABLE . "." . $item . " ILIKE :search";
                } else {
                    return $item . " ILIKE :search";
                }
            }, $searchableList);
        } else {
            $searchableList = [];
        }

        if (count($searchableList) > 0 && !is_blank($input, "search"))
            $params["search"] = "%" . strtoupper($input["search"] ?? "") . "%";

        $limit = $input["limit"] ?? 15;
        $offset = $input["offset"] ?? 0;
        if (!is_null($input["page"] ?? null)) {
            $offset = $limit * ($input["page"] - 1);
        }

        $sql = "SELECT " . implode(", ", $selectableList) . ",
            B.nik,
            B.nip,
            B.date_of_birth,
            C.job_position_category_id,
            MJPC.name as rel_job_position_category_id
            FROM " . $classModel::TABLE . " " .
            implode(" ", $tableJoinList) . "
            LEFT JOIN master_job_position_categories MJPC ON C.job_position_category_id = MJPC.id " . $condition .
            (count($searchableList) > 0 ? " AND (" . implode(" OR ", $searchableList) . ")" : "") .
            implode("\n", $filterList) . " ORDER BY " . $sortBy . " " . $sort . " LIMIT $limit OFFSET $offset ";

        $sqlForCount = "SELECT COUNT(1) AS total FROM " . $classModel::TABLE . " " .
            implode(" ", $tableJoinList) . $condition .
            (count($searchableList) > 0 ? " AND (" . implode(" OR ", $searchableList) . ")" : "") .
            implode("\n", $filterList);

        $employeeOnPeriodes =  DB::select($sql, $params);

        #############################
        $filterListSalCompCategory = [];
        $paramsSalCompCategory = [];
        $FIELD_FILTERABLE_SALCOMP_CATEGORY = [
            "salary_component_category_id" => [
                "operator" => "=",
            ],
        ];
        foreach ($FIELD_FILTERABLE_SALCOMP_CATEGORY as $filter => $operator) {
            if (!is_blank($input, $filter)) {
                if ($filter == 'salary_component_category_id') {
                    $filterListSalCompCategory[] = " AND MSCC.id " . $operator["operator"] . " :$filter";
                    $paramsSalCompCategory[$filter] = $input[$filter];
                }
            }
        }
        $conditionSalaryComponentsCategory = " WHERE true ";
        $sqlSalCompCategory = "SELECT MSCC.id, MSCC.name 
        FROM master_salary_components MSC
        JOIN master_salary_component_categories MSCC ON MSC.salary_component_category_id = MSCC.id
        " . $conditionSalaryComponentsCategory . implode("\n", $filterListSalCompCategory) . "
        GROUP BY MSCC.id, MSCC.name
        ORDER BY MSCC.id ASC";
        $dataSalaryComponentsCategories = DB::select($sqlSalCompCategory, $paramsSalCompCategory);

        $filterListSalComp = [];
        $paramsSalComp = [];
        $FIELD_FILTERABLE_SALCOMP = [
            "salary_component_id" => [
                "operator" => "=",
            ],
            "salary_component_category_id" => [
                "operator" => "=",
            ],
        ];
        foreach ($FIELD_FILTERABLE_SALCOMP as $filter => $operator) {
            if (!is_blank($input, $filter)) {
                if ($filter == 'salary_component_id') {
                    $filterListSalComp[] = " AND MSC.id " . $operator["operator"] . " :$filter";
                    $paramsSalComp[$filter] = $input[$filter];
                } else if ($filter == 'salary_component_category_id') {
                    $filterListSalComp[] = " AND MSC." . $filter .  " " . $operator["operator"] . " :$filter";
                    $paramsSalComp[$filter] = $input[$filter];
                }
            }
        }
        $conditionSalaryComponents = " WHERE true ";
        $sqlSalComp = "SELECT MSC.id, 
        MSC.name, 
        MSC.salary_component_category_id, 
        MSC.is_custom_component,
        MSC.active,
        MSCC.name as rel_salary_component_category_id, 
        0 AS default_value 
        FROM master_salary_components MSC
        JOIN master_salary_component_categories MSCC ON MSC.salary_component_category_id = MSCC.id
        " . $conditionSalaryComponents . implode("\n", $filterListSalComp) . "
        ORDER BY MSC.id ASC";
        $dataSalaryComponents = DB::select($sqlSalComp, $paramsSalComp);


        $salaryDetails = array_map(function ($key) use ($dataSalaryComponents) {
            foreach ($key as $field => $value) {
                $catId = $key->id;
                $key->data_salary_components = [];
                foreach ($dataSalaryComponents as $kSc) {
                    $salCompCatId = $kSc->salary_component_category_id;
                    if ($salCompCatId == $catId) {
                        array_push($key->data_salary_components, $kSc);
                    }
                }
            }
            return $key;
        }, $dataSalaryComponentsCategories);


        # MAPPING DATA EMPLOYEE ON PERIODE DENGAN KOMPONEN GAJI
        array_map(function ($key) use ($dataSalaryComponentsCategories) {
            foreach ($key as $field => $value) {
                $key->data_salary = $dataSalaryComponentsCategories;
            }
            return $key;
        }, $employeeOnPeriodes);

        ######################################################################
        $dataMappingTemplateSalaryEmployees = DB::select("SELECT SCOP.*
        FROM mapping_template_salary_employees SCOP
        JOIN employee_on_periodes EOP ON SCOP.employee_id = EOP.employee_id
        JOIN master_salary_components MSC ON SCOP.salary_component_id = MSC.id
        JOIN master_salary_component_categories MSCC ON MSC.salary_component_category_id = MSCC.id
        WHERE EOP.periode_month = :periode_month
        ORDER BY MSC.id ASC", [
            "periode_month" => $input["periode_month"]
        ]);

        #############################
        # MENGISI VALUE BASED ON salary_component_on_periodes

        $finalObject = [];
        foreach ($employeeOnPeriodes as $kEOP) {
            $finalDataEmployee = [];
            $finalDataEmployee["employee_id"] = $kEOP->employee_id;
            $finalDataEmployee["rel_employee_id"] = $kEOP->rel_employee_id;
            $finalDataEmployee["nik"] = $kEOP->nik;
            $finalDataEmployee["nip"] = $kEOP->nip;
            $finalDataEmployee["date_of_birth"] = $kEOP->date_of_birth;
            $finalDataEmployee["periode_month"] = $kEOP->periode_month;
            $finalDataEmployee["status_employment_id"] = $kEOP->status_employment_id;
            $finalDataEmployee["rel_status_employment_id"] = $kEOP->rel_status_employment_id;
            $finalDataEmployee["job_position_category_id"] = $kEOP->job_position_category_id;
            $finalDataEmployee["rel_job_position_category_id"] = $kEOP->rel_job_position_category_id;
            $finalDataEmployee["job_position_id"] = $kEOP->job_position_id;
            $finalDataEmployee["rel_job_position_id"] = $kEOP->rel_job_position_id;
            $finalDataEmployee["data_salary"] = [];
            $rowEmployeeId = $kEOP->employee_id;
            $rowPeriodeMonth = $kEOP->periode_month;
            foreach ($kEOP->data_salary as $kDatSal) {
                $finalDataSalary = [];
                $finalDataSalary["id"] = $kDatSal->id;
                $finalDataSalary["name"] = $kDatSal->name;
                $finalDataSalary["data_salary_components"] = [];
                foreach ($kDatSal->data_salary_components as $kDatSalComp) {
                    $finalDataSalaryComponents = [];
                    $finalDataSalaryComponents["id"] = $kDatSalComp->id;
                    $finalDataSalaryComponents["name"] = $kDatSalComp->name;
                    $finalDataSalaryComponents["is_custom_component"] = $kDatSalComp->is_custom_component;
                    $finalDataSalaryComponents["salary_component_category_id"] = $kDatSalComp->salary_component_category_id;
                    $finalDataSalaryComponents["rel_salary_component_category_id"] = $kDatSalComp->rel_salary_component_category_id;
                    $finalDataSalaryComponents["default_value"] = $kDatSalComp->default_value;
                    $finalDataSalaryComponents["active"] = $kDatSalComp->active;
                    $salCompId = $kDatSalComp->id;
                    if (isset($dataMappingTemplateSalaryEmployees)) {
                        $newSalaryComponentValue = multiArraySerach($dataMappingTemplateSalaryEmployees, [
                            "periode_month" => $rowPeriodeMonth,
                            "employee_id" => $rowEmployeeId,
                            "salary_component_id" => $salCompId,
                        ], 'default_value');
                        $finalDataSalaryComponents["default_value"] = !is_null($newSalaryComponentValue) ? (float) $newSalaryComponentValue : 0;

                        $newSalaryActiveValue = multiArraySerach($dataMappingTemplateSalaryEmployees, [
                            "periode_month" => $rowPeriodeMonth,
                            "employee_id" => $rowEmployeeId,
                            "salary_component_id" => $salCompId,
                        ], 'active');
                        $finalDataSalaryComponents["active"] = !is_null($newSalaryActiveValue) ? (float) $newSalaryActiveValue : null;
                    }
                    array_push($finalDataSalary["data_salary_components"], $finalDataSalaryComponents);
                }
                array_push($finalDataEmployee["data_salary"], $finalDataSalary);
            }
            array_push($finalObject, $finalDataEmployee);
        }

        $total = DB::selectOne($sqlForCount, $params)->total;
        $totalPage = ceil($total / $limit);
        return [
            "data" => $finalObject,
            "total" => $total,
            "totalPage" => $totalPage
        ];
    }

    protected function validation()
    {
        return [
            "periode_month" => "required",
            // "salary_component_id" => "required"
        ];
    }
}
