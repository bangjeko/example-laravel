<?php

namespace App\Services\Custom\MappingTemplateSalaryEmployees;

use Illuminate\Support\Facades\DB;
use App\CoreService\CoreException;
use App\CoreService\CoreService;
use Illuminate\Support\Str;

class MappingTemplateSalaryEmployeesList extends CoreService
{

    public $transaction = false;
    public $permission = null;

    public function prepare($input)
    {
        $mainModel = 'mapping-template-salary-employees';
        $classMainModel = "\\App\\Models\\" . Str::ucfirst(Str::camel($mainModel));
        if (!class_exists($classMainModel))
            throw new CoreException(__("message.model404", ['model' => $mainModel]), 404);

        $input["main_model"] = $classMainModel;
        return $input;
    }

    public function process($input, $originalInput)
    {
        // LIST DATA KOMPONEN GAJI
        $modelA = "master_salary_components";
        $classModelA = "\\App\\Models\\" . Str::ucfirst(Str::camel($modelA));
        $classMainModel = $input["main_model"];

        $selectableListModelA = [];
        $sortByModelA = "DM.active,".$classModelA::TABLE . ".id";
        $sortModelA = strtoupper($input["sort"] ?? "DESC") == "ASC" ? "DESC" : "ASC";

        if (isset($input["sort_by"])) {
            if($input["sort_by"] == 'active' OR $input["sort_by"] == 'default_value' ){
                $sortByModelA = "DM.".$input["sort_by"];
            }else{
                $sortByModelA = $input["sort_by"];
            }
        }

        $searchableListModelA = $classModelA::FIELD_SEARCHABLE;

        $tableJoinListModelA = [];
        $filterListModelA = [];
        $paramsModelA = [];

        $FIELD_LIST = ["id", "salary_component_category_id", "name", "is_custom_component"];

        foreach ($FIELD_LIST as $list) {
            $selectableList[] = $classModelA::TABLE . "." . $list;
        }

        foreach ($classModelA::FIELD_FILTERABLE as $filter => $operator) {
            if (!is_blank($input, $filter)) {
                $filterListModelA[] = " AND " . $classModelA::TABLE . "." . $filter .  " " . $operator["operator"] . " :$filter";
                $paramsModelA[$filter] = $input[$filter];
            }
        }

        $i = 0;
        $FIELD_RELATION = [
            "salary_component_category_id" => [
                "linkTable" => "master_salary_component_categories",
                "aliasTable" => "B",
                "linkField" => "id",
                "displayName" => "rel_salary_component_category_id",
                "selectFields" => ["name"],
                "selectValue" => "id AS rel_salary_component_category_id"
            ],
        ];
        foreach ($FIELD_RELATION as $key => $relation) {
            $alias = $relation["aliasTable"];
            $fieldDisplayed = "CONCAT_WS (' - ',";
            $searchableRealtionField = "CONCAT_WS (' - ',";
            foreach ($relation["selectFields"] as $keyField) {
                $fieldDisplayed .= $alias . '.' . $keyField . ",";
                $searchableRealtionField .= $alias . '.' . $keyField . ",";
            }
            $fieldDisplayed = substr($fieldDisplayed, 0, strlen($fieldDisplayed) - 1);
            $fieldDisplayed .= ") AS " . $relation["displayName"];
            $selectableList[] = $fieldDisplayed;

            $searchableRealtionField = substr($searchableRealtionField, 0, strlen($searchableRealtionField) - 1);
            $searchableRealtionField .= ")";
            $tableJoinListModelA[] = "LEFT JOIN " . $relation["linkTable"] . " " . $alias . " ON " .
                $classModelA::TABLE . "." . $key . " = " .  $alias . "." . $relation["linkField"];
            $i++;
            $searchableListModelA[] = $searchableRealtionField;
        }

        if (!empty($classModelA::CUSTOM_SELECT)) $selectableList[] = $classModelA::CUSTOM_SELECT;

        $condition = " WHERE true";

        if (!empty($classModelA::CUSTOM_LIST_FILTER)) {
            foreach ($classModelA::CUSTOM_LIST_FILTER as $customListFilter) {
                $condition .= " AND " . $customListFilter;
            }
        }
        if (!is_blank($input, "search")) {
            $searchableListModelA = array_map(function ($item) use ($classModelA) {
                if (in_array($item, $classModelA::FIELD_SEARCHABLE)) {
                    return $classModelA::TABLE . "." . $item . " ILIKE :search";
                } else {
                    return $item . " ILIKE :search";
                }
            }, $searchableListModelA);
        } else {
            $searchableListModelA = [];
        }

        if (count($searchableListModelA) > 0 && !is_blank($input, "search"))
            $paramsModelA["search"] = "%" . strtoupper($input["search"] ?? "") . "%";

        $limit = $input["limit"] ?? 10;
        $offset = $input["offset"] ?? 0;
        if (!is_null($input["page"] ?? null)) {
            $offset = $limit * ($input["page"] - 1);
        }

        $employeeId = $input["employee_id"];

        $sqlModelA = "WITH data_mapping AS (
                SELECT * FROM ".$classMainModel::TABLE." WHERE employee_id = ".$employeeId."
            )
            SELECT " . implode(", ", $selectableList) . ",
            DM.employee_id,
            DM.default_value as default_value,
            DM.active as active
            FROM " . $classModelA::TABLE . " " .
            implode(" ", $tableJoinListModelA) . 
            " LEFT JOIN data_mapping AS DM ON ". $classModelA::TABLE .".id = DM.salary_component_id ".$condition .
            (count($searchableListModelA) > 0 ? " AND (" . implode(" OR ", $searchableListModelA) . ")" : "") .
            implode("\n", $filterListModelA) . " ORDER BY " . $sortByModelA . " " . $sortModelA . " LIMIT $limit OFFSET $offset ";

        $sqlForCountModelA = "SELECT COUNT(1) AS total FROM " . $classModelA::TABLE . " " .
            implode(" ", $tableJoinListModelA) . $condition .
            (count($searchableListModelA) > 0 ? " AND (" . implode(" OR ", $searchableListModelA) . ")" : "") .
            implode("\n", $filterListModelA);

        $objectModelA =  DB::select($sqlModelA, $paramsModelA);
        $totalModelA = DB::selectOne($sqlForCountModelA, $paramsModelA)->total;
        $totalPageModelA = ceil($totalModelA / $limit);


        // // DATA SAVED MAPPING
        // $dataMapping = [];
        // $objectMaping = DB::select("SELECT A.*, B.name as rel_salary_component_id 
        // FROM mapping_template_salary_employees A
        // LEFT JOIN master_salary_components B ON A.salary_component_id = B.id
        // WHERE A.employee_id = :employee_id
        // ", ["employee_id" => $employeeId]);
        // foreach ($objectMaping as $mappingValue) {
        //     $dataMapping[$mappingValue->employee_id][$mappingValue->salary_component_id] = [
        //         "default_value" => (float) $mappingValue->default_value,
        //         "active" => $mappingValue->active
        //     ];
        // }

        // $fieldCasting = $classModelA::FIELD_CASTING;
        // array_map(function ($key) use ($classModelA, $fieldCasting, $dataMapping, $employeeId) {
        //     foreach ($key as $field => $value) {
        //         foreach ($dataMapping as $dValue) {
        //             $key->default_value = isset($dataMapping[$employeeId][$key->id]) ? $dataMapping[$employeeId][$key->id]["default_value"] : 0;
        //             $key->active = isset($dataMapping[$employeeId][$key->id]) ? $dataMapping[$employeeId][$key->id]["active"] : 0;
        //         }
        //     }
        //     return $key;
        // }, $objectModelA);


        return [
            "data" => $objectModelA,
            "total" => $totalModelA,
            "totalPage" => $totalPageModelA
        ];
    }

    protected function validation()
    {
        return [
            "employee_id" => "required"
        ];
    }
}
