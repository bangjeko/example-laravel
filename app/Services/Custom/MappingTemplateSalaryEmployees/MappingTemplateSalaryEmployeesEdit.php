<?php

namespace App\Services\Custom\MappingTemplateSalaryEmployees;

use App\CoreService\CoreException;
use Illuminate\Support\Facades\DB;
use App\CoreService\CoreService;
use App\Models\MappingTemplateSalaryEmployees;
use Illuminate\Support\Facades\Auth;

class MappingTemplateSalaryEmployeesEdit extends CoreService
{

    public $transaction = true;
    public $permission = null;

    public function prepare($input)
    {
        $authRoles = getRoleName(Auth::user()->role_id);
        $permission = "update-mapping-template-salary-employees";
        if (!hasPermission($permission))
            throw new CoreException(__("message.forbidden403", ['permission' => $permission, 'role' => $authRoles]), 403);

        return $input;
    }

    public function process($input, $originalInput)
    {
        $salaryComponentId = $input["id"];
        $employeeId = $input["employee_id"];
        $defaultValue = isset($input["default_value"]) ? $input["default_value"] : null;
        $valueMapping = $input["active"];
        $mappingExists = DB::selectOne("SELECT * FROM mapping_template_salary_employees 
        WHERE salary_component_id = :salary_component_id 
        AND employee_id = :employee_id", [
            "salary_component_id" => $salaryComponentId,
            "employee_id" => $employeeId
        ]);
        $value = '';
        if ($valueMapping == 1) {
            $value = 'Mengaktifkan';
        } else {
            $value = 'Menonaktifkan';
        }
        if (!$mappingExists) {
            // DO INSERT
            $objectData = new MappingTemplateSalaryEmployees;

            $objectData->employee_id = $employeeId;
            $objectData->salary_component_id = $salaryComponentId;
            $objectData->default_value = $defaultValue;
            $objectData->active = $valueMapping;
            $objectData->created_by = Auth::user()->id;
            $objectData->updated_by = Auth::user()->id;

            $objectData->save();
        } else {
            // DO UPDATE
            MappingTemplateSalaryEmployees::where('employee_id', '=', $employeeId)
                ->where('salary_component_id', '=', $salaryComponentId)
                ->update([
                    'default_value' => $defaultValue,
                    'active' => $valueMapping,
                    'updated_by' => Auth::user()->id,
                ]);
        }

        return [
            "message" => __("message.succesfullyUpdate")
        ];
    }

    protected function validation()
    {
        return [
            "id" => "required",
            "employee_id" => "required",
            "active" => "required"
        ];
    }
}
