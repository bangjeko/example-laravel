<?php

namespace App\Services\Custom\MappingUpahLembur;

use Illuminate\Support\Facades\DB;
use App\CoreService\CoreException;
use App\CoreService\CoreService;
use Illuminate\Support\Str;

class MappingUpahLemburList extends CoreService
{

    public $transaction = false;
    public $permission = null;

    public function prepare($input)
    {
        $mainModel = 'mapping-upah-lembur';
        $classMainModel = "\\App\\Models\\" . Str::ucfirst(Str::camel($mainModel));
        if (!class_exists($classMainModel))
            throw new CoreException(__("message.model404", ['model' => $mainModel]), 404);

        $input["main_model"] = $classMainModel;
        return $input;
    }

    public function process($input, $originalInput)
    {
        // LIST DATA KOMPONEN GAJI
        $modelA = "master_salary_components";
        $classModelA = "\\App\\Models\\" . Str::ucfirst(Str::camel($modelA));
        $classMainModel = $input["main_model"];

        $selectableListModelA = [];
        $sortByModelA = $classModelA::TABLE . ".id";
        $sortModelA = strtoupper($input["sort"] ?? "DESC") == "ASC" ? "DESC" : "ASC";

        if (isset($input["sort_by"])) {
            $sortByModelA = $input["sort_by"];
        }

        $searchableListModelA = $classModelA::FIELD_SEARCHABLE;

        $tableJoinListModelA = [];
        $filterListModelA = [];
        $paramsModelA = [];

        $FIELD_LIST = ["id", "salary_component_category_id", "name"];

        foreach ($FIELD_LIST as $list) {
            $selectableList[] = $classModelA::TABLE . "." . $list;
        }

        foreach ($classModelA::FIELD_FILTERABLE as $filter => $operator) {
            if (!is_blank($input, $filter)) {
                $filterListModelA[] = " AND " . $classModelA::TABLE . "." . $filter .  " " . $operator["operator"] . " :$filter";
                $paramsModelA[$filter] = $input[$filter];
            }
        }

        $i = 0;
        $FIELD_RELATION = [
            "salary_component_category_id" => [
                "linkTable" => "master_salary_component_categories",
                "aliasTable" => "B",
                "linkField" => "id",
                "displayName" => "rel_salary_component_category_id",
                "selectFields" => ["name"],
                "selectValue" => "id AS rel_salary_component_category_id"
            ],
        ];
        foreach ($FIELD_RELATION as $key => $relation) {
            $alias = $relation["aliasTable"];
            $fieldDisplayed = "CONCAT_WS (' - ',";
            $searchableRealtionField = "CONCAT_WS (' - ',";
            foreach ($relation["selectFields"] as $keyField) {
                $fieldDisplayed .= $alias . '.' . $keyField . ",";
                $searchableRealtionField .= $alias . '.' . $keyField . ",";
            }
            $fieldDisplayed = substr($fieldDisplayed, 0, strlen($fieldDisplayed) - 1);
            $fieldDisplayed .= ") AS " . $relation["displayName"];
            $selectableList[] = $fieldDisplayed;

            $searchableRealtionField = substr($searchableRealtionField, 0, strlen($searchableRealtionField) - 1);
            $searchableRealtionField .= ")";
            $tableJoinListModelA[] = "LEFT JOIN " . $relation["linkTable"] . " " . $alias . " ON " .
                $classModelA::TABLE . "." . $key . " = " .  $alias . "." . $relation["linkField"];
            $i++;
            $searchableListModelA[] = $searchableRealtionField;
        }

        if (!empty($classModelA::CUSTOM_SELECT)) $selectableList[] = $classModelA::CUSTOM_SELECT;

        $condition = " WHERE true AND " . $classModelA::TABLE . ".is_salary_cut = false ";

        if (!empty($classModelA::CUSTOM_LIST_FILTER)) {
            foreach ($classModelA::CUSTOM_LIST_FILTER as $customListFilter) {
                $condition .= " AND " . $customListFilter;
            }
        }
        if (!is_blank($input, "search")) {
            $searchableListModelA = array_map(function ($item) use ($classModelA) {
                if (in_array($item, $classModelA::FIELD_SEARCHABLE)) {
                    return $classModelA::TABLE . "." . $item . " ILIKE :search";
                } else {
                    return $item . " ILIKE :search";
                }
            }, $searchableListModelA);
        } else {
            $searchableListModelA = [];
        }

        if (count($searchableListModelA) > 0 && !is_blank($input, "search"))
            $paramsModelA["search"] = "%" . strtoupper($input["search"] ?? "") . "%";

        $limit = $input["limit"] ?? 10;
        $offset = $input["offset"] ?? 0;
        if (!is_null($input["page"] ?? null)) {
            $offset = $limit * ($input["page"] - 1);
        }

        $jobPositionCategoryId = $input["job_position_category_id"];
        $statusEmploymentId = $input["status_employment_id"];

        // $sqlModelA = "SELECT " . implode(", ", $selectableList) . ",
        //     ".$jobPositionCategoryId." as job_position_category_id,
        //     ".$statusEmploymentId." as status_employment_id,
        //     0 as koefisien,
        //     ''as description,
        //     0 as active
        //     FROM " . $classModelA::TABLE . " " .
        //     implode(" ", $tableJoinListModelA) . $condition .
        //     (count($searchableListModelA) > 0 ? " AND (" . implode(" OR ", $searchableListModelA) . ")" : "") .
        //     implode("\n", $filterListModelA) . " ORDER BY " . $sortByModelA . " " . $sortModelA . " LIMIT $limit OFFSET $offset ";

        $sqlModelA = "WITH data_mapping AS (
                SELECT * FROM " . $classMainModel::TABLE . " WHERE job_position_category_id = " . $jobPositionCategoryId . " AND status_employment_id = " . $statusEmploymentId . "
            )
            SELECT " . implode(", ", $selectableList) . ",
            DM.job_position_category_id,
            DM.status_employment_id,
            DM.koefisien,
            DM.description,
            DM.active as active
            FROM " . $classModelA::TABLE . " " .
            implode(" ", $tableJoinListModelA) .
            " LEFT JOIN data_mapping AS DM ON " . $classModelA::TABLE . ".id = DM.salary_component_id " . $condition .
            (count($searchableListModelA) > 0 ? " AND (" . implode(" OR ", $searchableListModelA) . ")" : "") .
            implode("\n", $filterListModelA) . " ORDER BY " . $sortByModelA . " " . $sortModelA . " LIMIT $limit OFFSET $offset ";


        $sqlForCountModelA = "SELECT COUNT(1) AS total FROM " . $classModelA::TABLE . " " .
            implode(" ", $tableJoinListModelA) . $condition .
            (count($searchableListModelA) > 0 ? " AND (" . implode(" OR ", $searchableListModelA) . ")" : "") .
            implode("\n", $filterListModelA);

        $objectModelA =  DB::select($sqlModelA, $paramsModelA);
        $totalModelA = DB::selectOne($sqlForCountModelA, $paramsModelA)->total;
        $totalPageModelA = ceil($totalModelA / $limit);


        // DATA SAVED MAPPING
        // $dataMapping = [];

        // $objectMaping = DB::select("SELECT A.*, B.name as rel_salary_component_id 
        // FROM mapping_upah_bpjs A
        // LEFT JOIN master_salary_components B ON A.salary_component_id = B.id
        // WHERE A.job_position_category_id = :job_position_category_id AND A.status_employment_id = :status_employment_id
        // ", [
        //     "job_position_category_id" => $jobPositionCategoryId,
        //     "status_employment_id" => $statusEmploymentId
        // ]);
        // foreach ($objectMaping as $mappingValue) {
        //     $dataMapping[$mappingValue->job_position_category_id][$mappingValue->status_employment_id][$mappingValue->salary_component_id] = [
        //         "koefisien" => $mappingValue->koefisien,
        //         "description" => $mappingValue->description,
        //         "active" => $mappingValue->active
        //     ];
        // }

        // $fieldCasting = $classModelA::FIELD_CASTING;
        // array_map(function ($key) use ($classModelA, $fieldCasting, $dataMapping, $jobPositionCategoryId, $statusEmploymentId) {
        //     foreach ($key as $field => $value) {
        //         foreach ($dataMapping as $dValue) {
        //             $key->koefisien = isset($dataMapping[$jobPositionCategoryId][$statusEmploymentId][$key->id]) ? $dataMapping[$jobPositionCategoryId][$statusEmploymentId][$key->id]["koefisien"] : null;
        //             $key->description = isset($dataMapping[$jobPositionCategoryId][$statusEmploymentId][$key->id]) ? $dataMapping[$jobPositionCategoryId][$statusEmploymentId][$key->id]["description"] : null;
        //             $key->active = isset($dataMapping[$jobPositionCategoryId][$statusEmploymentId][$key->id]) ? $dataMapping[$jobPositionCategoryId][$statusEmploymentId][$key->id]["active"] : 0;
        //         }
        //     }
        //     return $key;
        // }, $objectModelA);


        return [
            "data" => $objectModelA,
            "total" => $totalModelA,
            "totalPage" => $totalPageModelA
        ];
    }

    protected function validation()
    {
        return [
            "job_position_category_id" => "required",
            "status_employment_id" => "required"
        ];
    }
}
