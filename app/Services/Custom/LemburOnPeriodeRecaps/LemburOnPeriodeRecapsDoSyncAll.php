<?php

namespace App\Services\Custom\LemburOnPeriodeRecaps;

use App\CoreService\CallService;
use App\CoreService\CoreException;
use App\CoreService\CoreService;
use App\Models\LemburOnPeriodeRecaps;
use App\Models\LemburOnPeriodes;
use App\Models\PayrollPeriodes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Carbon\Carbon;

class LemburOnPeriodeRecapsDoSyncAll extends CoreService
{

    public $transaction = true;
    public $task = null;

    public function prepare($input)
    {
        $model = "lembur-on-periode-recaps";
        $classModel = "\\App\\Models\\" . Str::ucfirst(Str::camel($model));
        $permission = "create-" . $model;
        $authRoles = getRoleName(Auth::user()->role_id);
        if (!class_exists($classModel))
            throw new CoreException(__("message.model404", ['model' => $model]), 404);
        if (!$classModel::IS_ADD)
            throw new CoreException("Not found", 404);
        if (!hasPermission($permission))
            throw new CoreException(__("message.forbidden403", ['permission' => $permission, 'roles' => $authRoles]), 403);
        $input["class_model"] = $classModel;
        return $input;
    }

    public function process($input, $originalInput)
    {
        // MASIH DUMMY
        // DATA DUMMY TAP TAP
        $dataLemburOnPeriodes = LemburOnPeriodes::find($input["id"]);
        $dataLemburTapTap = [];
        // if ($dataLemburOnPeriodes->start_date == '2020-12-20') {
        //     $dataLemburTapTap = [
        //         [
        //             "user_id" => "T59",
        //             "employee_name" => "Novan Setyawan",
        //             "date" => "2020-12-23",
        //             "type" => "weekday",
        //             "lembur_description" => "Test 1",
        //             "start_time" => "2020-12-23 01:00:00",
        //             "end_time" => "2020-12-23 04:00:00",
        //             "duration" => 4
        //         ],
        //         [
        //             "user_id" => "T59",
        //             "employee_name" => "Novan Setyawan",
        //             "date" => "2020-12-28",
        //             "type" => "weekday",
        //             "lembur_description" => "Test 2",
        //             "start_time" => "2020-12-23 01:00:00",
        //             "end_time" => "2020-12-23 04:30:00",
        //             "duration" => 4.5
        //         ],
        //         [
        //             "user_id" => "T59",
        //             "employee_name" => "Novan Setyawan",
        //             "date" => "2020-12-29",
        //             "type" => "weekday",
        //             "lembur_description" => "Test 2",
        //             "start_time" => "2020-12-23 01:00:00",
        //             "end_time" => "2020-12-23 04:00:00",
        //             "duration" => 4
        //         ],
        //         [
        //             "user_id" => "T59",
        //             "employee_name" => "Novan Setyawan",
        //             "date" => "2020-12-30",
        //             "type" => "weekday",
        //             "lembur_description" => "Test 3",
        //             "start_time" => "2020-12-23 01:00:00",
        //             "end_time" => "2020-12-23 03:30:00",
        //             "duration" => 3.5
        //         ],
        //         [
        //             "user_id" => "T34",
        //             "employee_name" => "Agung Tri Wibowo",
        //             "date" => "2020-12-20",
        //             "type" => "weekday",
        //             "lembur_description" => "Test Agung Lembur 1",
        //             "start_time" => "2020-12-23 01:00:00",
        //             "end_time" => "2020-12-23 03:30:00",
        //             "duration" => 3.5
        //         ],
        //         [
        //             "user_id" => "T34",
        //             "employee_name" => "Agung Tri Wibowo",
        //             "date" => "2020-12-24",
        //             "type" => "holiday",
        //             "lembur_description" => "Test Agung Lembur 2",
        //             "start_time" => "2020-12-23 01:00:00",
        //             "end_time" => "2020-12-23 07:00:00",
        //             "duration" => 7
        //         ],
        //         [
        //             "user_id" => "T34",
        //             "employee_name" => "Agung Tri Wibowo",
        //             "date" => "2020-12-25",
        //             "type" => "holiday",
        //             "lembur_description" => "Test Agung Lembur 2",
        //             "start_time" => "2020-12-23 01:00:00",
        //             "end_time" => "2020-12-23 07:00:00",
        //             "duration" => 7
        //         ],
        //         [
        //             "user_id" => "T34",
        //             "employee_name" => "Agung Tri Wibowo",
        //             "date" => "2020-12-31",
        //             "type" => "holiday",
        //             "lembur_description" => "Test Agung Lembur 2",
        //             "start_time" => "2020-12-23 01:00:00",
        //             "end_time" => "2020-12-23 07:00:00",
        //             "duration" => 7
        //         ],
        //         [
        //             "user_id" => "T34",
        //             "employee_name" => "Agung Tri Wibowo",
        //             "date" => "2021-01-01",
        //             "type" => "holiday",
        //             "lembur_description" => "Test Agung Lembur 2",
        //             "start_time" => "2020-12-23 01:00:00",
        //             "end_time" => "2020-12-23 07:00:00",
        //             "duration" => 7
        //         ],
        //         [
        //             "user_id" => "T20",
        //             "employee_name" => "Ahmad Kusen",
        //             "date" => "2020-12-21",
        //             "type" => "weekday",
        //             "lembur_description" => "Test Kusen Lembur 2",
        //             "start_time" => "2020-12-23 01:00:00",
        //             "end_time" => "2020-12-23 07:00:00",
        //             "duration" => 7
        //         ],
        //         [
        //             "user_id" => "T20",
        //             "employee_name" => "Ahmad Kusen",
        //             "date" => "2020-12-24",
        //             "type" => "holiday",
        //             "lembur_description" => "Test Kusen Lembur 2",
        //             "start_time" => "2020-12-23 01:00:00",
        //             "end_time" => "2020-12-23 07:00:00",
        //             "duration" => 7
        //         ],
        //         [
        //             "user_id" => "T20",
        //             "employee_name" => "Ahmad Kusen",
        //             "date" => "2020-12-25",
        //             "type" => "holiday",
        //             "lembur_description" => "Test Kusen Lembur 2",
        //             "start_time" => "2020-12-23 01:00:00",
        //             "end_time" => "2020-12-23 07:00:00",
        //             "duration" => 7
        //         ],
        //         [
        //             "user_id" => "T20",
        //             "employee_name" => "Ahmad Kusen",
        //             "date" => "2020-12-31",
        //             "type" => "holiday",
        //             "lembur_description" => "Test Kusen Lembur 2",
        //             "start_time" => "2020-12-23 01:00:00",
        //             "end_time" => "2020-12-23 07:00:00",
        //             "duration" => 7
        //         ],
        //         [
        //             "user_id" => "T20",
        //             "employee_name" => "Ahmad Kusen",
        //             "date" => "2021-01-01",
        //             "type" => "holiday",
        //             "lembur_description" => "Test Kusen Lembur 2",
        //             "start_time" => "2020-12-23 01:00:00",
        //             "end_time" => "2020-12-23 07:00:00",
        //             "duration" => 7
        //         ],
        //         [
        //             "user_id" => "T17",
        //             "employee_name" => "Oktarian",
        //             "date" => "2020-12-19",
        //             "type" => "weekday",
        //             "lembur_description" => "Test Okta Lembur 2",
        //             "start_time" => "2020-12-23 01:00:00",
        //             "end_time" => "2020-12-23 07:00:00",
        //             "duration" => 7
        //         ],
        //         [
        //             "user_id" => "T17",
        //             "employee_name" => "Oktarian",
        //             "date" => "2020-12-24",
        //             "type" => "holiday",
        //             "lembur_description" => "Test Okta Lembur 2",
        //             "start_time" => "2020-12-23 01:00:00",
        //             "end_time" => "2020-12-23 07:00:00",
        //             "duration" => 7
        //         ],
        //         [
        //             "user_id" => "T17",
        //             "employee_name" => "Oktarian",
        //             "date" => "2020-12-25",
        //             "type" => "holiday",
        //             "lembur_description" => "Test Okta Lembur 2",
        //             "start_time" => "2020-12-23 01:00:00",
        //             "end_time" => "2020-12-23 07:00:00",
        //             "duration" => 7
        //         ],
        //         [
        //             "user_id" => "T17",
        //             "employee_name" => "Oktarian",
        //             "date" => "2020-12-31",
        //             "type" => "holiday",
        //             "lembur_description" => "Test Okta Lembur 2",
        //             "start_time" => "2020-12-23 01:00:00",
        //             "end_time" => "2020-12-23 07:00:00",
        //             "duration" => 7
        //         ],
        //         [
        //             "user_id" => "T17",
        //             "employee_name" => "Oktarian",
        //             "date" => "2021-01-01",
        //             "type" => "holiday",
        //             "lembur_description" => "Test Okta Lembur 2",
        //             "start_time" => "2020-12-23 01:00:00",
        //             "end_time" => "2020-12-23 07:00:00",
        //             "duration" => 7
        //         ],
        //     ];
        // }

        // END DUMMY

        // DATA TAP TAP
        $endpoint = taptapEndPointUrl() . "/history/overtime";
        $limit = isset($input["limit"]) ? $input["limit"] : 500;
        $page = isset($input["page"]) ? $input["page"] : 1;
        $search = isset($input["search"]) ? $input["search"] : null;
        $offset = ($page - 1) * $limit;
        $params = [];
        $headers = [
            "client-id: " . taptapIntegrationDataDesired('taptap_client_id'),
            "client-secret: " . taptapIntegrationDataDesired('taptap_secret_key'),
        ];
        $url = $endpoint . '?' . http_build_query($params);
        // CURL
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $output = curl_exec($ch);
        if ($output === FALSE) {
            die('Curl error: ' . curl_error($ch));
        }
        curl_close($ch);
        $output = json_decode($output, true);

        if (!$output["success"]) {
            throw new CoreException($output["error_message"], 500);
        }
        $dataLemburTapTap =  $output["data"]["data"];
        // REWRITE EMPLOYEE ID
        $dataLemburTapTap = array_unique($dataLemburTapTap, SORT_REGULAR);
        $lemburOnPeriodeId = $input["id"];
        $dataEmployeeActiveWithTapTapId = DB::select("SELECT id, taptap_user_id FROM employees 
        WHERE taptap_user_id IS NOT NULL AND status_code = 'active'");
        $arDataHRM = [];
        if (!empty($dataEmployeeActiveWithTapTapId)) {
            foreach ($dataEmployeeActiveWithTapTapId as $kDH) {
                $taptapUserId = $kDH->taptap_user_id;
                $arDataHRM[$taptapUserId] = $kDH->id;
            }
        }
        $newDataTapTap = [];
        if (!empty($dataLemburTapTap)) {
            foreach ($dataLemburTapTap as $kDT) {
                if (isset($arDataHRM[$kDT["employee_id"]])) {
                    $dataLembur = [];
                    $dataLembur["lembur_on_periode_id"] = $lemburOnPeriodeId;
                    $dataLembur["employee_id"] = $arDataHRM[$kDT["employee_id"]];
                    $dataLembur["date"] = $kDT["date"];
                    $dataLembur["type"] = ($kDT["onschedule"]) ? "holiday" : "weekday";
                    $dataLembur["lembur_description"] = $kDT["reason"];

                    ######
                    $checkIn = $kDT["date"] . " " . $kDT["checkin"];
                    $checkOut = $kDT["date"] . " " . $kDT["checkout"];
                    $timeA = strtotime($kDT["checkin"]);
                    $timeB = strtotime($kDT["checkout"]);
                    if ($timeA > $timeB) {
                        $checkOut = date('Y-m-d H:i:s', strtotime($checkOut . " +1 days"));
                    }
                    ######
                    $dataLembur["start_time"] = $checkIn;
                    $dataLembur["end_time"] = $checkOut;
                    $dataLembur["duration"] = $kDT["duration"];
                    $dataLembur["data_source"] = "taptap";
                    array_push($newDataTapTap, $dataLembur);
                }
            }
        }

        $insertedData = DB::table('lembur_on_periode_recaps')->upsert($newDataTapTap,  [
            'lembur_on_periode_id', 'employee_id', 'date'
        ]);


        $response["message"] = __("message.successfullySynchronized");
        $response["row_updated"] = $insertedData;
        return $response;
    }

    protected function validation()
    {
        return [
            "id" => "required",
        ];
    }
}
