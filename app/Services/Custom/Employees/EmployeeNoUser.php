<?php

namespace App\Services\Custom\Employees;

use Illuminate\Support\Facades\DB;
use App\CoreService\CoreException;
use App\CoreService\CoreService;
use App\Models\Applications;
use App\Models\UserSsoLogins;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class EmployeeNoUser extends CoreService
{

    public $transaction = false;
    public $permission = null;

    public function prepare($input)
    {
        return $input;
    }

    public function process($input, $originalInput)
    {
        $searchableList = ["fullname", "nik", "nip", "nomor_kk", "place_of_birth", "gender", "marital_status", "email", "telephone", "nationality", "npwp"];
        $filterList = [];
        $params = [];
        $condition = " WHERE true AND E.user_id IS NULL";

        $filterableList = [
            "id" => [
                "operator" => "=",
            ],
            "fullname" => [
                "operator" => "=",
            ],
            "nik" => [
                "operator" => "=",
            ],
            "nip" => [
                "operator" => "=",
            ],
            "nomor_kk" => [
                "operator" => "=",
            ],
            "place_of_birth" => [
                "operator" => "=",
            ],
            "date_of_birth" => [
                "operator" => "=",
            ],
            "gender" => [
                "operator" => "=",
            ],
            "marital_status" => [
                "operator" => "=",
            ],
            "address" => [
                "operator" => "=",
            ],
            "religion_id" => [
                "operator" => "=",
            ],
            "email" => [
                "operator" => "=",
            ],
            "telephone" => [
                "operator" => "=",
            ],
            "education_id" => [
                "operator" => "=",
            ],
            "nationality" => [
                "operator" => "=",
            ],
            "npwp" => [
                "operator" => "=",
            ],
            "user_id" => [
                "operator" => "=",
            ],
            "job_position_id" => [
                "operator" => "=",
            ],
            "entry_date" => [
                "operator" => "=",
            ],
            "out_date" => [
                "operator" => "=",
            ],
            "appointment_date" => [
                "operator" => "=",
            ],
            "status_code" => [
                "operator" => "=",
            ],
            "ids_not_in" => [
                "operator" => "=",
            ]
        ];
        foreach ($filterableList as $filter => $operator) {
            if (!is_blank($input, $filter)) {
                if ($filter == 'ids_not_in') {
                    $filterList[] = " AND E.id != ALL ( ARRAY $input[$filter] ) ";
                } else {
                    $filterList[] = " AND E." . $filter .  " " . $operator["operator"] . " :$filter";
                    $params[$filter] = $input[$filter];
                }
            }
        }

        if (!is_blank($input, "search")) {
            $searchableList = array_map(function ($item) {
                return "UPPER(E." . $item . ") ILIKE :search";
            }, $searchableList);
        } else {
            $searchableList = [];
        }

        if (count($searchableList) > 0 && !is_blank($input, "search"))
            $params["search"] = "%" . strtoupper($input["search"] ?? "") . "%";

        if (isset($input["limit"])) {
            if ($input["limit"] == 'null') {
                $limit = 'null';
            } else {
                $limit = $input["limit"];
            }
        } else {
            $limit = 'null';
        }
        $offset = $input["offset"] ?? 0;
        if (!is_null($input["page"] ?? null)) {
            if ($limit == 'null') {
                $offset = 'null';
            } else {
                $offset = $limit * ($input["page"] - 1);
            }
        }

        $sql = "SELECT E.* FROM  employees E "
            . $condition .
            (count($searchableList) > 0 ? " AND (" . implode(" OR ", $searchableList) . ")" : "") .
            implode("\n", $filterList) . " ORDER BY E.id ASC LIMIT $limit OFFSET $offset ";

        $object =  DB::select($sql, $params);

        $sqlForCount = "SELECT COUNT(1) AS total FROM employees E "
            . $condition .
            (count($searchableList) > 0 ? " AND (" . implode(" OR ", $searchableList) . ")" : "") .
            implode("\n", $filterList);
        $total = DB::selectOne($sqlForCount, $params)->total;

        if ($limit == 'null') {
            $totalPage = 1;
        } else {
            $totalPage = ceil($total / $limit);
        }
        return [
            "data" => $object,
            "total" => $total,
            "totalPage" => $totalPage
        ];
    }

    protected function validation()
    {
        return [];
    }
}
