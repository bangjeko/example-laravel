<?php

namespace App\Services\Custom\TunjanganLemburDetails;

use App\CoreService\CallService;
use App\CoreService\CoreException;
use App\CoreService\CoreService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;

class TunjanganLemburDetailsShow extends CoreService
{

    public $transaction = false;
    public $task = null;

    public function prepare($input)
    {
        $model = "tunjangan-lembur-details";
        $permission = "show-" . $model;
        $authRoles = getRoleName(Auth::user()->role_id);
        $classModel = "\\App\\Models\\" . Str::ucfirst(Str::camel($model));
        if (!class_exists($classModel))
            throw new CoreException(__("message.model404", ['model' => $model]), 404);

        if (!$classModel::IS_LIST)
            throw new CoreException("Not found", 404);

        if (!hasPermission($permission))
            throw new CoreException(__("message.forbidden403", ['permission' => $permission, 'role' => $authRoles]), 403);

        $input["class_model"] = $classModel;
        return $input;
    }

    public function process($input, $originalInput)
    {
        $classModel = $input["class_model"];
        $selectableList = [];
        $tableJoinList = [];
        $params = ["id" => $input["id"]];

        foreach ($classModel::FIELD_VIEW as $list) {
            $selectableList[] = $classModel::TABLE . "." . $list;
        }

        $i = 0;
        $FIELD_RELATION = [
            "tunjangan_lembur_id" => [
                "linkTable" => "tunjangan_lembur",
                "aliasTable" => "B",
                "linkField" => "id",
                "displayName" => "rel_tunjangan_lembur_id",
                "selectFields" => ["id"],
                "selectValue" => "id AS rel_tunjangan_lembur_id"
            ],
            "employee_id" => [
                "linkTable" => "employees",
                "aliasTable" => "C",
                "linkField" => "id",
                "displayName" => "rel_employee_id",
                "selectFields" => ["fullname"],
                "selectValue" => "id AS rel_employee_id"
            ],
            "created_by" => [
                "linkTable" => "users",
                "aliasTable" => "D",
                "linkField" => "id",
                "displayName" => "rel_created_by",
                "selectFields" => ["username"],
                "selectValue" => "id AS rel_created_by"
            ],
            "updated_by" => [
                "linkTable" => "users",
                "aliasTable" => "E",
                "linkField" => "id",
                "displayName" => "rel_updated_by",
                "selectFields" => ["username"],
                "selectValue" => "id AS rel_updated_by"
            ],
        ];
        foreach ($FIELD_RELATION as $key => $relation) {
            $alias = toAlpha($i + 1);
            ///
            $fieldDisplayed = "CONCAT_WS (' - ',";
            foreach ($relation["selectFields"] as $keyField) {
                $fieldDisplayed .= $alias . '.' . $keyField . ",";
            }
            $fieldDisplayed = substr($fieldDisplayed, 0, strlen($fieldDisplayed) - 1);
            $fieldDisplayed .= ") AS " . $relation["displayName"];
            $selectableList[] = $fieldDisplayed;
            ///
            $tableJoinList[] = "LEFT JOIN " . $relation["linkTable"] . " " . $alias . " ON " .
                $classModel::TABLE . "." . $key . " = " .  $alias . "." . $relation["linkField"];
            $i++;
        }

        if (!empty($classModel::CUSTOM_SELECT)) $selectableList[] = $classModel::CUSTOM_SELECT;

        $condition = " WHERE " . $classModel::TABLE . ".id = :id";

        $sql = "SELECT " . implode(", ", $selectableList) . ", B.periode_month, JP.job_position_name as rel_job_position_id, MSE.name as rel_status_employment_id
            FROM " . $classModel::TABLE . " " .
            implode(" ", $tableJoinList) . "
            LEFT JOIN employee_on_periodes EOP on tunjangan_lembur_details.employee_id = EOP.employee_id AND EOP.periode_month = B.periode_month 
            LEFT JOIN employees EMP on tunjangan_lembur_details.employee_id = EMP.id 
            LEFT JOIN job_positions JP ON EOP.job_position_id = JP.id
            LEFT JOIN master_status_employments MSE ON EOP.status_employment_id = MSE.id
            " . $condition;


        $object =  DB::selectOne($sql, $params);
        if (is_null($object)) {
            throw new CoreException(__("message.dataNotFound", ['id' => $input["id"]]));
        }
        $fieldCasting = $classModel::FIELD_CASTING;
        if (!empty($fieldCasting)) {
            foreach ($fieldCasting as $item => $k) {
                if (isset($fieldCasting[$item])) {
                    if (array_key_exists($item, $fieldCasting)) {
                        if ($fieldCasting[$item] == 'float') {
                            $object->$item = (float) $object->$item;
                        }
                    }
                }
            }
        }
        // DATA DETAILS
        /*
        DATA UPAH COMPONENTS
        */
        $object->child_data_tunjangan_lembur_upah_components = DB::select("SELECT A.*, 
        MSC.name as rel_salary_component_id,
        MSC.type
        FROM tunjangan_lembur_upah_components A 
        JOIN master_salary_components MSC ON A.salary_component_id = MSC.id
        WHERE A.tunjangan_lembur_detail_id = :tunjangan_lembur_detail_id", [
            "tunjangan_lembur_detail_id" => $object->id
        ]);

        $fieldCastingUpah = [
            "koefisien" => "float",
            "default_value" => "float",
            "component_value" => "float",
        ];;
        array_map(function ($key) use ($fieldCastingUpah) {
            foreach ($key as $field => $value) {
                if (isset($fieldCastingUpah[$field])) {
                    if (array_key_exists($field, $fieldCastingUpah)) {
                        if ($fieldCastingUpah[$field] == 'float') {
                            $key->$field = (float) $key->$field;
                        }
                    }
                }
            }
            return $key;
        }, $object->child_data_tunjangan_lembur_upah_components);

        $CHILD_TABLE = [
            "tunjangan_lembur_hari_kerja" => [
                "foreignField" => "tunjangan_lembur_detail_id"
            ],
            "tunjangan_lembur_hari_libur" => [
                "foreignField" => "tunjangan_lembur_detail_id"
            ],
            "tunjangan_lembur_subsidiary" => [
                "foreignField" => "tunjangan_lembur_detail_id"
            ],
        ];
        foreach ($CHILD_TABLE as $keyItem => $valItem) {
            $childModuleName = "child_data_" . $keyItem;
            $foreignFieldName = $valItem["foreignField"];
            $childItem["model"] = $keyItem;
            $childItem["sort"] = "ASC";
            $childItem[$foreignFieldName] = $object->id;
            $object->$childModuleName = CallService::run("Dataset", $childItem)->original["data"];
        }

        return [
            "data" => $object
        ];
    }

    protected function validation()
    {
        return [];
    }
}
