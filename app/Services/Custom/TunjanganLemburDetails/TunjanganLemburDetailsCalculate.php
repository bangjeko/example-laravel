<?php

namespace App\Services\Custom\TunjanganLemburDetails;

use App\CoreService\CallService;
use App\CoreService\CoreException;
use App\CoreService\CoreService;
use App\Models\TunjanganLembur;
use App\Models\TunjanganLemburDetails;
use App\Models\TunjanganLemburHariKerja;
use App\Models\TunjanganLemburHariLibur;
use App\Models\TunjanganLemburSubsidiary;
use App\Models\TunjanganLemburUpahComponents;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Mavinoo\Batch\BatchFacade;

class TunjanganLemburDetailsCalculate extends CoreService
{

    public $transaction = true;
    public $task = null;

    public function prepare($input)
    {
        $model = "tunjangan-lembur-details";
        $classModel = "\\App\\Models\\" . Str::ucfirst(Str::camel($model));
        $permission = "create-" . $model;
        $authRoles = getRoleName(Auth::user()->role_id);
        if (!class_exists($classModel))
            throw new CoreException(__("message.model404", ['model' => $model]), 404);
        if (!$classModel::IS_ADD)
            throw new CoreException("Not found", 404);
        if (!hasPermission($permission))
            throw new CoreException(__("message.forbidden403", ['permission' => $permission, 'roles' => $authRoles]), 403);
        $input["class_model"] = $classModel;
        return $input;
    }

    public function process($input, $originalInput)
    {
        #1 UPDATE bpjs_tk_bill_details

        $dataTunjanganLemburDetail = TunjanganLemburDetails::find($input["id"]);
        $dataTunjanganLemburDetail->description = $input["description"];
        $dataTunjanganLemburDetail->status_code = "calculated";

        #2 UPDATE tunjangan_lembur_upah_components
        foreach ($input["child_data_tunjangan_lembur_upah_components"] as $childUpahCompItem) {
            if (isset($childUpahCompItem["id"])) {
                $childUpahCompItem["model"] = "tunjangan_lembur_upah_components";
                $childUpahCompItem["tunjangan_lembur_detail_id"] = $input["id"];
                $childUpahCompItem["component_value"] = $childUpahCompItem["koefisien"] * $childUpahCompItem["default_value"];
                $childDataUpahCompUpdated = CallService::run("Edit", $childUpahCompItem);
            }
        }

        #2 UPDATE tunjangan_lembur_subsidiary
        foreach ($input["child_data_tunjangan_lembur_subsidiary"] as $childSubsidiaryItem) {
            if (isset($childSubsidiaryItem["id"])) {
                $childSubsidiaryItem["model"] = "tunjangan_lembur_subsidiary";
                $childSubsidiaryItem["tunjangan_lembur_detail_id"] = $input["id"];
                $childSubsidiaryItem["component_value"] = $childSubsidiaryItem["jumlah_hari"] * $childSubsidiaryItem["koefisien"]  * $childSubsidiaryItem["default_value"];
                $childDataSubsidiaryUpdated = CallService::run("Edit", $childSubsidiaryItem);
            }
        }

        #3 HITUNG UPAH
        $jumlahUpah = TunjanganLemburUpahComponents::where('tunjangan_lembur_detail_id', '=', $input["id"])
            ->sum('component_value');

        // HITUNG TUNJANGAN TETAP VALUE
        $jumlahPokokTunjanganTetap = DB::selectOne("SELECT sum(A.component_value) as sum_value FROM tunjangan_lembur_upah_components A
        JOIN master_salary_components MSC ON A.salary_component_id = MSC.id
        WHERE A.tunjangan_lembur_detail_id = :tunjangan_lembur_detail_id AND MSC.type IN ('tunjangan_tetap','upah_pokok')", [
            "tunjangan_lembur_detail_id" => $input["id"]
        ])->sum_value;

        // HITUNG 75 % THP
        $threeQuarterUpah = round($jumlahUpah * 75 / 100, 0);

        # HITUNG UANG LEMBUR PERJAM
        /*
        Jika Nilai Tunjangan Tetap + Pokok < Nilai 75% THP :75% * Nilai Upah (THP) * 1/173
        Jika Nilai Tunjangan Tetap + Pokok ≥ Nilai 75% THP :Nilai Upah (THP) x 1/173
        */
        $uangLemburPerjam = 0;
        if ($jumlahPokokTunjanganTetap < $threeQuarterUpah) {
            $uangLemburPerjam = round(75 / 100 * $jumlahUpah * 1 / 173, 0);
        } else {
            $uangLemburPerjam = round($jumlahUpah * 1 / 173, 0);
        }

        $dataTunjanganLemburDetail->upah_value = $jumlahUpah;
        $dataTunjanganLemburDetail->pokok_tunjangan_tetap_value = $jumlahPokokTunjanganTetap;
        $dataTunjanganLemburDetail->three_quarter_upah_value = $threeQuarterUpah;
        $dataTunjanganLemburDetail->upah_lembur_perjam_value = $uangLemburPerjam;
        $dataTunjanganLemburDetail->save();

        # HITUNG UANG LEMBUR HARI KERJA
        $dataLemburWeekDay = TunjanganLemburHariKerja::where('tunjangan_lembur_detail_id', '=', $input["id"])->get();
        $arrLemburWeekDayShouldUpdated = [];
        foreach ($dataLemburWeekDay as $kDLWD) {
            $dataWeekdayUpdated = [];
            $lemburDurationOnTheDay = $kDLWD->jam_lembur_count;
            $uangLemburJamPertama = round($uangLemburPerjam * 1.5, 0);
            $uangLemburJamSelanjutnya = 0;
            if ($lemburDurationOnTheDay > 1) {
                $uangLemburJamSelanjutnya = round(($lemburDurationOnTheDay - 1) * $uangLemburPerjam * 2, 0);
            }

            $penerimaanValue = round($uangLemburJamPertama + $uangLemburJamSelanjutnya, 0);

            $dataWeekdayUpdated["id"] = $kDLWD->id;
            $dataWeekdayUpdated["lembur_jam_pertama_value"] = $uangLemburJamPertama;
            $dataWeekdayUpdated["lembur_jam_selanjutnya_value"] = $uangLemburJamSelanjutnya;
            $dataWeekdayUpdated["penerimaan_value"] = $penerimaanValue;

            array_push($arrLemburWeekDayShouldUpdated, $dataWeekdayUpdated);
        }

        $tunjanganLemburHariKerjaInstance = new TunjanganLemburHariKerja;
        $index = 'id';
        BatchFacade::update($tunjanganLemburHariKerjaInstance, $arrLemburWeekDayShouldUpdated, $index);

        # HITUNG UANG LEMBUR HARI LIBUR
        $dataLemburHoliday = TunjanganLemburHariLibur::where('tunjangan_lembur_detail_id', '=', $input["id"])->get();
        $arrLemburHolidayShouldUpdated = [];
        foreach ($dataLemburHoliday as $kDLHD) {
            $dataWeekdayUpdated = [];
            $lemburDurationOnTheDay = $kDLHD->jam_lembur_count;

            // 8 Jam Pertama
            $uangLemburDelapanJamPertama = 0;
            if ($lemburDurationOnTheDay < 8) {
                $uangLemburDelapanJamPertama = $lemburDurationOnTheDay * $uangLemburPerjam * 2;
            } else {
                $uangLemburDelapanJamPertama = round(8 * $uangLemburPerjam * 2, 0);
            }
            // Jam Ke 9
            $uangLemburJamKesembilan = 0;
            if ($lemburDurationOnTheDay > 8) {
                $uangLemburDelapanJamPertama = round(1 * $uangLemburPerjam * 3, 0);
            }
            // Jam Selanjutnya
            $uangLemburJamSelanjutnya = 0;
            if ($lemburDurationOnTheDay > 9) {
                $uangLemburJamSelanjutnya = round(($lemburDurationOnTheDay - 9) * $uangLemburPerjam * 4, 0);
            }

            $penerimaanValue = round($uangLemburDelapanJamPertama + $uangLemburJamKesembilan + $uangLemburJamSelanjutnya, 0);

            $dataWeekdayUpdated["id"] = $kDLHD->id;
            $dataWeekdayUpdated["lembur_jam_kedelapan_value"] = $uangLemburDelapanJamPertama;
            $dataWeekdayUpdated["lembur_jam_kesembilan_value"] = $uangLemburJamKesembilan;
            $dataWeekdayUpdated["lembur_jam_selanjutnya_value"] = $uangLemburJamSelanjutnya;
            $dataWeekdayUpdated["penerimaan_value"] = $penerimaanValue;

            array_push($arrLemburHolidayShouldUpdated, $dataWeekdayUpdated);
        }

        $tunjanganLemburHariLiburInstance = new TunjanganLemburHariLibur;
        $index = 'id';
        BatchFacade::update($tunjanganLemburHariLiburInstance, $arrLemburHolidayShouldUpdated, $index);

        # UPDATE PENERIMAAN
        $totalPenerimaanLemburHariKerja = TunjanganLemburHariKerja::where('tunjangan_lembur_detail_id', '=', $input["id"])
            ->sum('penerimaan_value');
        $totalPenerimaanLemburHariLibur = TunjanganLemburHariLibur::where('tunjangan_lembur_detail_id', '=', $input["id"])
            ->sum('penerimaan_value');
        $totalPenerimaanSubsidiary = TunjanganLemburSubsidiary::where('tunjangan_lembur_detail_id', '=', $input["id"])
            ->sum('component_value');

        $totalPenerimaanValue = round($totalPenerimaanLemburHariKerja + $totalPenerimaanLemburHariLibur + $totalPenerimaanSubsidiary, 0);
        $pembulatanPenerimaanValue = round($totalPenerimaanValue, -2);

        $dataTunjanganLemburDetail->penerimaan_value = $totalPenerimaanValue;
        $dataTunjanganLemburDetail->pembulatan_penerimaan_value = $pembulatanPenerimaanValue;
        $dataTunjanganLemburDetail->save();

        #7 UPDATE PARENT tunjangan_makans
        $totalPenerimaan = TunjanganLemburDetails::where('tunjangan_lembur_id', '=', $dataTunjanganLemburDetail->tunjangan_lembur_id)
            ->sum('penerimaan_value');

        TunjanganLembur::where('id', '=', $dataTunjanganLemburDetail->tunjangan_lembur_id)
            ->update([
                'total_penerimaan_value' => $totalPenerimaan,
            ]);

        //////////////// THEN SHOW //////////
        $classModel = $input["class_model"];
        $selectableList = [];
        $tableJoinList = [];
        $params = ["id" => $input["id"]];

        foreach ($classModel::FIELD_VIEW as $list) {
            $selectableList[] = $classModel::TABLE . "." . $list;
        }

        $i = 0;
        $FIELD_RELATION = [
            "tunjangan_lembur_id" => [
                "linkTable" => "tunjangan_lembur",
                "aliasTable" => "B",
                "linkField" => "id",
                "displayName" => "rel_tunjangan_lembur_id",
                "selectFields" => ["id"],
                "selectValue" => "id AS rel_tunjangan_lembur_id"
            ],
            "employee_id" => [
                "linkTable" => "employees",
                "aliasTable" => "C",
                "linkField" => "id",
                "displayName" => "rel_employee_id",
                "selectFields" => ["fullname"],
                "selectValue" => "id AS rel_employee_id"
            ],
            "created_by" => [
                "linkTable" => "users",
                "aliasTable" => "D",
                "linkField" => "id",
                "displayName" => "rel_created_by",
                "selectFields" => ["username"],
                "selectValue" => "id AS rel_created_by"
            ],
            "updated_by" => [
                "linkTable" => "users",
                "aliasTable" => "E",
                "linkField" => "id",
                "displayName" => "rel_updated_by",
                "selectFields" => ["username"],
                "selectValue" => "id AS rel_updated_by"
            ],
        ];
        foreach ($FIELD_RELATION as $key => $relation) {
            $alias = toAlpha($i + 1);
            ///
            $fieldDisplayed = "CONCAT_WS (' - ',";
            foreach ($relation["selectFields"] as $keyField) {
                $fieldDisplayed .= $alias . '.' . $keyField . ",";
            }
            $fieldDisplayed = substr($fieldDisplayed, 0, strlen($fieldDisplayed) - 1);
            $fieldDisplayed .= ") AS " . $relation["displayName"];
            $selectableList[] = $fieldDisplayed;
            ///
            $tableJoinList[] = "LEFT JOIN " . $relation["linkTable"] . " " . $alias . " ON " .
                $classModel::TABLE . "." . $key . " = " .  $alias . "." . $relation["linkField"];
            $i++;
        }

        if (!empty($classModel::CUSTOM_SELECT)) $selectableList[] = $classModel::CUSTOM_SELECT;

        $condition = " WHERE " . $classModel::TABLE . ".id = :id";

        $sql = "SELECT " . implode(", ", $selectableList) . ", B.periode_month, JP.job_position_name as rel_job_position_id, MSE.name as rel_status_employment_id
            FROM " . $classModel::TABLE . " " .
            implode(" ", $tableJoinList) . "
            LEFT JOIN employee_on_periodes EOP on tunjangan_lembur_details.employee_id = EOP.employee_id AND EOP.periode_month = B.periode_month 
            LEFT JOIN employees EMP on tunjangan_lembur_details.employee_id = EMP.id 
            LEFT JOIN job_positions JP ON EOP.job_position_id = JP.id
            LEFT JOIN master_status_employments MSE ON EOP.status_employment_id = MSE.id
            " . $condition;


        $object =  DB::selectOne($sql, $params);
        if (is_null($object)) {
            throw new CoreException(__("message.dataNotFound", ['id' => $input["id"]]));
        }
        $fieldCasting = $classModel::FIELD_CASTING;
        if (!empty($fieldCasting)) {
            foreach ($fieldCasting as $item => $k) {
                if (isset($fieldCasting[$item])) {
                    if (array_key_exists($item, $fieldCasting)) {
                        if ($fieldCasting[$item] == 'float') {
                            $object->$item = (float) $object->$item;
                        }
                    }
                }
            }
        }
        // DATA DETAILS
        /*
        DATA UPAH COMPONENTS
        */
        $object->child_data_tunjangan_lembur_upah_components = DB::select("SELECT A.*, 
        MSC.name as rel_salary_component_id,
        MSC.type
        FROM tunjangan_lembur_upah_components A 
        JOIN master_salary_components MSC ON A.salary_component_id = MSC.id
        WHERE A.tunjangan_lembur_detail_id = :tunjangan_lembur_detail_id", [
            "tunjangan_lembur_detail_id" => $object->id
        ]);

        $fieldCastingUpah = [
            "koefisien" => "float",
            "default_value" => "float",
            "component_value" => "float",
        ];;
        array_map(function ($key) use ($fieldCastingUpah) {
            foreach ($key as $field => $value) {
                if (isset($fieldCastingUpah[$field])) {
                    if (array_key_exists($field, $fieldCastingUpah)) {
                        if ($fieldCastingUpah[$field] == 'float') {
                            $key->$field = (float) $key->$field;
                        }
                    }
                }
            }
            return $key;
        }, $object->child_data_tunjangan_lembur_upah_components);

        $CHILD_TABLE = [
            "tunjangan_lembur_hari_kerja" => [
                "foreignField" => "tunjangan_lembur_detail_id"
            ],
            "tunjangan_lembur_hari_libur" => [
                "foreignField" => "tunjangan_lembur_detail_id"
            ],
            "tunjangan_lembur_subsidiary" => [
                "foreignField" => "tunjangan_lembur_detail_id"
            ],
        ];
        foreach ($CHILD_TABLE as $keyItem => $valItem) {
            $childModuleName = "child_data_" . $keyItem;
            $foreignFieldName = $valItem["foreignField"];
            $childItem["model"] = $keyItem;
            $childItem["sort"] = "ASC";
            $childItem[$foreignFieldName] = $object->id;
            $object->$childModuleName = CallService::run("Dataset", $childItem)->original["data"];
        }

        $response["data"] = $object;
        $response["message"] = __("message.successfullyAdd");
        return $response;
    }

    protected function validation()
    {
        return [
            "periode_month" => "required"
        ];
    }
}
