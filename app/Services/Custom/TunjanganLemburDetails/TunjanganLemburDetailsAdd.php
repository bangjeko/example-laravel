<?php

namespace App\Services\Custom\TunjanganLemburDetails;

use App\CoreService\CallService;
use App\CoreService\CoreException;
use App\CoreService\CoreService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;

class TunjanganLemburDetailsAdd extends CoreService
{

    public $transaction = true;
    public $task = null;

    public function prepare($input)
    {
        $model = "tunjangan-lembur-details";
        $classModel = "\\App\\Models\\" . Str::ucfirst(Str::camel($model));
        $permission = "create-" . $model;
        $authRoles = getRoleName(Auth::user()->role_id);
        if (!class_exists($classModel))
            throw new CoreException(__("message.model404", ['model' => $model]), 404);
        if (!$classModel::IS_ADD)
            throw new CoreException("Not found", 404);
        if (!hasPermission($permission))
            throw new CoreException(__("message.forbidden403", ['permission' => $permission, 'roles' => $authRoles]), 403);
        $input["class_model"] = $classModel;

        if ($classModel::FIELD_ARRAY) {
            foreach ($classModel::FIELD_ARRAY as $item) {
                $input[$item] = serialize($input[$item]);
            }
        }

        if ($classModel::FIELD_UPLOAD) {
            foreach ($classModel::FIELD_UPLOAD as $item) {
                if (isset($input[$item])) {
                    if (is_array($input[$item])) {
                        $input[$item] = isset($input[$item]["path"]) ? $input[$item]["path"] : $input[$item]["field_value"];
                    }
                }
            }
        }

        $validator = Validator::make($input, $classModel::FIELD_VALIDATION);

        if ($validator->fails()) {
            throw new CoreException($validator->errors()->first());
        }

        if ($classModel::FIELD_UNIQUE) {
            foreach ($classModel::FIELD_UNIQUE as $search) {
                $query = $classModel::whereRaw("true");
                $fieldTrans = [];
                foreach ($search as $key) {
                    $fieldTrans[] = __("field.$key");
                    $query->where($key, $input[$key]);
                };
                $isi = $query->first();
                if (!is_null($isi)) {
                    throw new CoreException(__("message.alreadyExist", ['field' => implode(",", $fieldTrans)]));
                }
            }
        }

        return $input;
    }

    public function process($input, $originalInput)
    {

        $response = [];
        $classModel = $input["class_model"];

        $input = $classModel::beforeInsert($input);

        $object = new $classModel;
        foreach ($classModel::FIELD_ADD as $item) {
            if ($item == "created_by") {
                $input[$item] = Auth::id();
            }
            if ($item == "updated_by") {
                $input[$item] = Auth::id();
            }
            if (isset($input[$item])) {
                $inputValue = $input[$item] ?? $classModel::FIELD_DEFAULT_VALUE[$item];
                $object->{$item} = ($inputValue !== '') ? $inputValue : null;
            }
        }

        $object->save();

        // MOVE FILE
        foreach ($classModel::FIELD_UPLOAD as $item) {
            $tmpPath = $input[$item] ?? null;

            if (!is_null($tmpPath)) {
                if (!Storage::exists($tmpPath)) {
                    throw new CoreException(__("message.tempFileNotFound", ['field' => $item]));
                }
                $tmpPath = $input[$item];
                $originalname = pathinfo(storage_path($tmpPath), PATHINFO_FILENAME);
                $ext = pathinfo(storage_path($tmpPath), PATHINFO_EXTENSION);

                $newPath = $classModel::FILEROOT . "/" . $originalname . "." . $ext;
                //START MOVE FILE
                if (Storage::exists($newPath)) {

                    $id = 1;
                    $filename = pathinfo(storage_path($newPath), PATHINFO_FILENAME);
                    $ext = pathinfo(storage_path($newPath), PATHINFO_EXTENSION);
                    while (true) {
                        $originalname = $filename . "($id)." . $ext;
                        if (!Storage::exists($classModel::FILEROOT . "/" . $originalname))
                            break;
                        $id++;
                    }
                    $newPath = $classModel::FILEROOT . "/" . $originalname;
                }

                $ext = pathinfo(storage_path($newPath), PATHINFO_EXTENSION);
                $object->{$item} = $newPath;
                Storage::move($tmpPath, $newPath);
            }
        }
        //END MOVE FILE

        $object->save();
        $displayedDataAfterInsert["id"] = $object->id;
        $displayedDataAfterInsert["model"] = $classModel::TABLE;
        $displayedDataAfterInsert = CallService::run("Find", $displayedDataAfterInsert);
        if (isset($displayedDataAfterInsert->original["data"])) {
            $displayedDataAfterInsert = $displayedDataAfterInsert->original["data"];
        } else {
            $displayedDataAfterInsert = $displayedDataAfterInsert->original;
            throw new CoreException($displayedDataAfterInsert, 500);
        }

        //AFTER INSERT
        $afterInsertedRespnese = $classModel::afterInsert($displayedDataAfterInsert, $input);


        #
        // FIND PERIODE MONTH
        $periodeMonth = DB::selectOne("SELECT B.periode_month 
        FROM tunjangan_lembur_details  A 
        JOIN tunjangan_lembur B ON A.tunjangan_lembur_id = B.id
        WHERE A.id = :tunjangan_lembur_detail_id", [
            "tunjangan_lembur_detail_id" => $object->id
        ])->periode_month;

        $paramsInsertDetail["tunjangan_lembur_detail_id"] = $object->id;
        $paramsInsertDetail["periode_month"] = $periodeMonth;
        $paramsInsertDetail["employee_id"] = $object->employee_id;
        $paramsInsertDetail["created_by"] = $object->created_by;
        $paramsInsertDetail["created_at"] = $object->created_at;
        $paramsInsertDetail["updated_by"] = $object->updated_by;
        $paramsInsertDetail["updated_at"] = $object->updated_at;

        $sqlDataDetail = "WITH insert_tunjangan_lembur_upah_components AS (
            INSERT INTO tunjangan_lembur_upah_components (tunjangan_lembur_detail_id, salary_component_id, koefisien, default_value, component_value, created_by,created_at,updated_by,updated_at)
            SELECT DISTINCT ON (MUPLBR.salary_component_id) :tunjangan_lembur_detail_id, MUPLBR.salary_component_id, MUPLBR.koefisien, MTSE.default_value, MUPLBR.koefisien * MTSE.default_value, :created_by, :created_at, :updated_by, :updated_at
            FROM mapping_template_salary_employees MTSE 
            JOIN employee_on_periodes EOPA ON EOPA.employee_id =  MTSE.employee_id
            JOIN job_positions JP ON EOPA.job_position_id = JP.id
            JOIN master_job_position_categories MJPC ON JP.job_position_category_id = MJPC.id
            JOIN mapping_upah_lembur MUPLBR ON MJPC.id = MUPLBR.job_position_category_id AND MUPLBR.status_employment_id = EOPA.status_employment_id AND MUPLBR.salary_component_id = MTSE.salary_component_id
            WHERE MUPLBR.active = 1 AND EOPA.employee_id = :employee_id AND MTSE.periode_month = :periode_month
            ON CONFLICT DO NOTHING
        ), insert_tunjangan_lembur_hari_kerja AS (
            INSERT INTO tunjangan_lembur_hari_kerja (tunjangan_lembur_detail_id, date, jam_lembur_count, created_by,created_at,updated_by,updated_at)
            SELECT DISTINCT ON (LOPR.date) :tunjangan_lembur_detail_id, LOPR.date, LOPR.duration, :created_by, :created_at, :updated_by, :updated_at
            FROM lembur_on_periode_recaps LOPR 
            JOIN lembur_on_periodes LOP ON LOP.id =  LOPR.lembur_on_periode_id
            WHERE LOP.periode_month = :periode_month AND LOPR.employee_id = :employee_id 
            AND LOPR.type = 'weekday'
            ON CONFLICT DO NOTHING
        ), insert_tunjangan_lembur_hari_libur AS (
            INSERT INTO tunjangan_lembur_hari_libur (tunjangan_lembur_detail_id, date, jam_lembur_count, created_by,created_at,updated_by,updated_at)
            SELECT DISTINCT ON (LOPR.date) :tunjangan_lembur_detail_id, LOPR.date, LOPR.duration, :created_by, :created_at, :updated_by, :updated_at
            FROM lembur_on_periode_recaps LOPR 
            JOIN lembur_on_periodes LOP ON LOP.id =  LOPR.lembur_on_periode_id
            WHERE LOP.periode_month = :periode_month AND LOPR.employee_id = :employee_id 
            AND LOPR.type = 'holiday'
            ON CONFLICT DO NOTHING
        ), insert_tunjangan_lembur_subsidiary AS (
            INSERT INTO tunjangan_lembur_subsidiary (tunjangan_lembur_detail_id, salary_component_id, jumlah_hari, koefisien, default_value, created_by,created_at,updated_by,updated_at)
            SELECT DISTINCT ON (CTLCS.salary_component_id) :tunjangan_lembur_detail_id, CTLCS.salary_component_id, 0, CTLCS.koefisien, MTSE.default_value, :created_by, :created_at, :updated_by, :updated_at
            FROM mapping_template_salary_employees MTSE 
            JOIN employee_on_periodes EOPA ON EOPA.employee_id =  MTSE.employee_id
            JOIN job_positions JP ON EOPA.job_position_id = JP.id
            JOIN master_job_position_categories MJPC ON JP.job_position_category_id = MJPC.id
            JOIN config_tunjangan_lembur_comp_subsidiary CTLCS ON CTLCS.salary_component_id = MTSE.salary_component_id
            JOIN master_salary_components MSC ON MSC.id = CTLCS.salary_component_id
            WHERE EOPA.employee_id = :employee_id AND CTLCS.active = 1 AND MTSE.periode_month = :periode_month
            ON CONFLICT DO NOTHING
        )
        SELECT * FROM tunjangan_lembur_details
        ";

        $insertDataDetail =  DB::insert($sqlDataDetail, $paramsInsertDetail);

        // THEN UPDATE UPAH
        $paramUpdateUpah["tunjangan_lembur_detail_id"] = $object->id;
        $sqlUpdateUpah = "UPDATE tunjangan_lembur_details 
        SET upah_value = (SELECT SUM(component_value) FROM tunjangan_lembur_upah_components WHERE tunjangan_lembur_detail_id = :tunjangan_lembur_detail_id )
        WHERE id = :tunjangan_lembur_detail_id";
        $updateUpah = DB::statement($sqlUpdateUpah, $paramUpdateUpah);


        $response["data"] = $displayedDataAfterInsert;
        $response["after_inserted_response"] = $afterInsertedRespnese;
        $response["check"] = $insertDataDetail;
        $response["message"] = __("message.successfullyAdd");

        return $response;
    }

    protected function validation()
    {
        return [
            "employee_id" => "required"
        ];
    }
}
