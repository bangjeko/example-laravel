<?php

namespace App\Services\Custom\BpjsTkBillDetails;

use App\CoreService\CallService;
use App\CoreService\CoreException;
use App\CoreService\CoreService;
use App\Models\BpjsTkBillDetails;
use App\Models\BpjsTkBillDetailUpahComponents;
use App\Models\BpjsTkBills;
use App\Models\ConfigProsenBpjsTenagaKerja;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Carbon\Carbon;

class BpjsTkBillDetailsCalculate extends CoreService
{

    public $transaction = true;
    public $task = null;

    public function prepare($input)
    {
        $model = "bpjs-tk-bill-details";
        $classModel = "\\App\\Models\\" . Str::ucfirst(Str::camel($model));
        $permission = "create-" . $model;
        $authRoles = getRoleName(Auth::user()->role_id);
        if (!class_exists($classModel))
            throw new CoreException(__("message.model404", ['model' => $model]), 404);
        if (!$classModel::IS_ADD)
            throw new CoreException("Not found", 404);
        if (!hasPermission($permission))
            throw new CoreException(__("message.forbidden403", ['permission' => $permission, 'roles' => $authRoles]), 403);
        $input["class_model"] = $classModel;
        return $input;
    }

    public function process($input, $originalInput)
    {
        #1 UPDATE bpjs_tk_bill_details

        $dataBpjsTkBillDetail = BpjsTkBillDetails::find($input["id"]);
        $dataBpjsTkBillDetail->description = $input["description"];
        $dataBpjsTkBillDetail->status_code = "calculated";

        #2 UPDATE bpjs_tk_bill_detail_upah_components
        foreach ($input["child_data_bpjs_tk_bill_detail_upah_components"] as $childUpahCompItem) {
            if (isset($childUpahCompItem["id"])) {
                $childUpahCompItem["model"] = "bpjs_tk_bill_detail_upah_components";
                $childUpahCompItem["bpjs_tk_bill_detail_id"] = $input["id"];
                $childUpahCompItem["component_value"] = $childUpahCompItem["koefisien"] * $childUpahCompItem["default_value"];
                $childDataUpahCompUpdated = CallService::run("Edit", $childUpahCompItem);
            }
        }

        #3 HITUNG UPAH
        $jumlahUpah = BpjsTkBillDetailUpahComponents::where('bpjs_tk_bill_detail_id', '=', $input["id"])
            ->sum('component_value');
        // DATA OBJECT PREMI THIS MONTH
        $objectPremi = ConfigProsenBpjsTenagaKerja::select('*')->where('periode_month', '=', $input["periode_month"])
            ->first();

        // THEN CALCULATE IURAN PERUSAHAAN DAN TENAGA KERJA BASED ON PREMI


        $dataBpjsTkBillDetail->upah_value = $jumlahUpah;

        // HITUNG JHT - IURAN
        $dateOfBirth = $input["date_of_birth"];
        $periodeMonth = $input["periode_month"];
        $employeeAge = (int) Carbon::parse($dateOfBirth)->diff($periodeMonth)->format('%y');

        # CONFIG PREMI BPJS TK
        $maxAge = $objectPremi->max_age;
        $upahMaxJp = $objectPremi->upah_max_jp_value;
        $prosenJkk = $objectPremi->prosen_jkk;
        $prosenJkm = $objectPremi->prosen_jkm;
        $prosenJpBbCompany = $objectPremi->prosen_jp_bb_company;
        $prosenJpBbEmployee = $objectPremi->prosen_jp_bb_employee;
        $prosenJhtCompany = $objectPremi->prosen_jht_company;
        $prosenJhtEmployee = $objectPremi->prosen_jht_employee;
        $isBebanCompany = isset($input["is_beban_company"]) ?  $input["is_beban_company"] : false;

        # MULAI MENGHITUNG JKK & JKM
        $iuranJkkValue = round($prosenJkk * $jumlahUpah / 100,0);
        $iuranJkmValue = round($prosenJkm * $jumlahUpah / 100,0);

        # MULAI MENGHITUNG JP
        $iuranJpBbCompanyValue = 0;
        $iuranJpBbEmployeeValue = 0;
        if ($employeeAge < $maxAge) {
            if ($jumlahUpah > $upahMaxJp) {
                $iuranJpBbCompanyValue = round($prosenJpBbCompany * $upahMaxJp / 100,0);
                $iuranJpBbEmployeeValue = round($prosenJpBbEmployee * $upahMaxJp / 100,0);
            } else {
                $iuranJpBbCompanyValue = round($prosenJpBbCompany * $jumlahUpah / 100,0);
                $iuranJpBbEmployeeValue = round($prosenJpBbEmployee * $jumlahUpah / 100,0);
            }
        }

        # MULAI MENGHITUNG JHT
        $iuranJhtBbCompanyValue = round($prosenJhtCompany * $jumlahUpah / 100,0);
        $iuranJhtBbEmployeeValue = round($prosenJhtEmployee * $jumlahUpah / 100,0);

        # MENGITUNG KEKURANGAN JP
        $kekuranganJpValue = isset($input["kekurangan_jp_value"]) ?  $input["kekurangan_jp_value"] : 0;

        # MENGHITUNG IURAN BPJS NYA
        $iuranBpjsCompanyValue = round($iuranJkkValue + $iuranJkmValue +  $iuranJpBbCompanyValue + $iuranJhtBbCompanyValue,0);
        $iuranBpjsEmployeeValue = round($iuranJpBbEmployeeValue + $iuranJhtBbEmployeeValue + $kekuranganJpValue,0);
        if (!$isBebanCompany) {
            $iuranBpjsEmployeeValue = round($iuranBpjsCompanyValue + $iuranBpjsEmployeeValue,0);
            $iuranBpjsCompanyValue = 0;
        }
        # TOTAL IURAN
        $totaliuran = round($iuranBpjsCompanyValue + $iuranBpjsEmployeeValue,0);

        #6 UPDATE DETAIL
        $dataBpjsTkBillDetail->iuran_jkk_value = $iuranJkkValue;
        $dataBpjsTkBillDetail->iuran_jkm_value = $iuranJkmValue;
        $dataBpjsTkBillDetail->iuran_jp_bb_company_value = $iuranJpBbCompanyValue;
        $dataBpjsTkBillDetail->iuran_jp_bb_employee_value = $iuranJpBbEmployeeValue;
        $dataBpjsTkBillDetail->iuran_jht_company_value = $iuranJhtBbCompanyValue;
        $dataBpjsTkBillDetail->iuran_jht_employee_value = $iuranJhtBbEmployeeValue;
        $dataBpjsTkBillDetail->kekurangan_jp_value = $kekuranganJpValue;
        $dataBpjsTkBillDetail->is_beban_company = $isBebanCompany;
        $dataBpjsTkBillDetail->iuran_bpjs_company_value = $iuranBpjsCompanyValue;
        $dataBpjsTkBillDetail->iuran_bpjs_employee_value = $iuranBpjsEmployeeValue;
        $dataBpjsTkBillDetail->total_iuran_value = $totaliuran;
        $dataBpjsTkBillDetail->save();


        #7 UPDATE PARENT bpjs_tk_bills
        $totalIuranJkk = BpjsTkBillDetails::where('bpjs_tk_bill_id', '=', $dataBpjsTkBillDetail->bpjs_tk_bill_id)
            ->sum('iuran_jkk_value');
        $totalIuranJkm = BpjsTkBillDetails::where('bpjs_tk_bill_id', '=', $dataBpjsTkBillDetail->bpjs_tk_bill_id)
            ->sum('iuran_jkm_value');
        $totalIuranJpBbCompany = BpjsTkBillDetails::where('bpjs_tk_bill_id', '=', $dataBpjsTkBillDetail->bpjs_tk_bill_id)
            ->sum('iuran_jp_bb_company_value');
        $totalIuranJpBbEmployee = BpjsTkBillDetails::where('bpjs_tk_bill_id', '=', $dataBpjsTkBillDetail->bpjs_tk_bill_id)
            ->sum('iuran_jp_bb_employee_value');
        $totalIuranJhtBbCompany = BpjsTkBillDetails::where('bpjs_tk_bill_id', '=', $dataBpjsTkBillDetail->bpjs_tk_bill_id)
            ->sum('iuran_jht_company_value');
        $totalIuranJhtBbEmployee = BpjsTkBillDetails::where('bpjs_tk_bill_id', '=', $dataBpjsTkBillDetail->bpjs_tk_bill_id)
            ->sum('iuran_jht_employee_value');
        $totalKekuranganJp = BpjsTkBillDetails::where('bpjs_tk_bill_id', '=', $dataBpjsTkBillDetail->bpjs_tk_bill_id)
            ->sum('kekurangan_jp_value');
        $totalIuranBpjsCompany = BpjsTkBillDetails::where('bpjs_tk_bill_id', '=', $dataBpjsTkBillDetail->bpjs_tk_bill_id)
            ->sum('iuran_bpjs_company_value');
        $totalIuranBpjsEmployee = BpjsTkBillDetails::where('bpjs_tk_bill_id', '=', $dataBpjsTkBillDetail->bpjs_tk_bill_id)
            ->sum('iuran_bpjs_employee_value');
        $totalIuran = BpjsTkBillDetails::where('bpjs_tk_bill_id', '=', $dataBpjsTkBillDetail->bpjs_tk_bill_id)
            ->sum('total_iuran_value');

        BpjsTkBills::where('id', '=', $dataBpjsTkBillDetail->bpjs_tk_bill_id)
            ->update([
                'total_iuran_jkk_value' => $totalIuranJkk,
                'total_iuran_jkm_value' => $totalIuranJkm,
                'total_iuran_jp_bb_company_value' => $totalIuranJpBbCompany,
                'total_iuran_jp_bb_employee_value' => $totalIuranJpBbEmployee,
                'total_iuran_jht_company_value' => $totalIuranJhtBbCompany,
                'total_iuran_jht_employee_value' => $totalIuranJhtBbEmployee,
                'total_kekurangan_jp_value' => $totalKekuranganJp,
                'total_iuran_bpjs_company_value' => $totalIuranBpjsCompany,
                'total_iuran_bpjs_employee_value' => $totalIuranBpjsEmployee,
                'total_iuran_value' => $totalIuran,
            ]);

        // THEN SHOW
        $classModel = $input["class_model"];
        $selectableList = [];
        $tableJoinList = [];
        $params = ["id" => $input["id"]];

        foreach ($classModel::FIELD_VIEW as $list) {
            $selectableList[] = $classModel::TABLE . "." . $list;
        }

        $i = 0;
        $FIELD_RELATION = [
            "bpjs_tk_bill_id" => [
                "linkTable" => "bpjs_tk_bills",
                "aliasTable" => "B",
                "linkField" => "id",
                "displayName" => "rel_bpjs_tk_bill_id",
                "selectFields" => ["id"],
                "selectValue" => "id AS rel_bpjs_tk_bill_id"
            ],
            "employee_id" => [
                "linkTable" => "employees",
                "aliasTable" => "C",
                "linkField" => "id",
                "displayName" => "rel_employee_id",
                "selectFields" => ["fullname"],
                "selectValue" => "id AS rel_employee_id"
            ],
            "created_by" => [
                "linkTable" => "users",
                "aliasTable" => "D",
                "linkField" => "id",
                "displayName" => "rel_created_by",
                "selectFields" => ["username"],
                "selectValue" => "id AS rel_created_by"
            ],
            "updated_by" => [
                "linkTable" => "users",
                "aliasTable" => "E",
                "linkField" => "id",
                "displayName" => "rel_updated_by",
                "selectFields" => ["username"],
                "selectValue" => "id AS rel_updated_by"
            ],
        ];
        foreach ($FIELD_RELATION as $key => $relation) {
            $alias = toAlpha($i + 1);
            ///
            $fieldDisplayed = "CONCAT_WS (' - ',";
            foreach ($relation["selectFields"] as $keyField) {
                $fieldDisplayed .= $alias . '.' . $keyField . ",";
            }
            $fieldDisplayed = substr($fieldDisplayed, 0, strlen($fieldDisplayed) - 1);
            $fieldDisplayed .= ") AS " . $relation["displayName"];
            $selectableList[] = $fieldDisplayed;
            ///
            $tableJoinList[] = "LEFT JOIN " . $relation["linkTable"] . " " . $alias . " ON " .
                $classModel::TABLE . "." . $key . " = " .  $alias . "." . $relation["linkField"];
            $i++;
        }

        if (!empty($classModel::CUSTOM_SELECT)) $selectableList[] = $classModel::CUSTOM_SELECT;

        $condition = " WHERE " . $classModel::TABLE . ".id = :id";

        $sql = "SELECT " . implode(", ", $selectableList) . ", B.periode_month, EMP.date_of_birth, EMP.nomor_kpj_bpjs_tk, JP.job_position_name as rel_job_position_id, MSE.name as rel_status_employment_id
            FROM " . $classModel::TABLE . " " .
            implode(" ", $tableJoinList) . "
            LEFT JOIN employee_on_periodes EOP on bpjs_tk_bill_details.employee_id = EOP.employee_id AND EOP.periode_month = B.periode_month 
            LEFT JOIN employees EMP on bpjs_tk_bill_details.employee_id = EMP.id 
            LEFT JOIN job_positions JP ON EOP.job_position_id = JP.id
            LEFT JOIN master_status_employments MSE ON EOP.status_employment_id = MSE.id
            " . $condition;


        $object =  DB::selectOne($sql, $params);
        if (is_null($object)) {
            throw new CoreException(__("message.dataNotFound", ['id' => $input["id"]]));
        }

        // DATA OBJECT PREMI THIS MONTH
        $objectPremi = ConfigProsenBpjsTenagaKerja::select('*')->where('periode_month', '=', $object->periode_month)
            ->first();
        $object->data_premi = $objectPremi;
        // DATA DETAILS
        $CHILD_TABLE = [
            "bpjs_tk_bill_detail_upah_components" => [
                "foreignField" => "bpjs_tk_bill_detail_id"
            ],
        ];
        foreach ($CHILD_TABLE as $keyItem => $valItem) {
            $childModuleName = "child_data_" . $keyItem;
            $foreignFieldName = $valItem["foreignField"];
            $childItem["model"] = $keyItem;
            $childItem["sort"] = "ASC";
            $childItem[$foreignFieldName] = $object->id;
            $object->$childModuleName = CallService::run("Dataset", $childItem)->original["data"];
        }

        // END SHOW

        $response["data"] = $object;
        $response["message"] = __("message.successfullyAdd");
        return $response;
    }

    protected function validation()
    {
        return [
            "periode_month" => "required"
        ];
    }
}
