<?php

namespace App\Services\Custom\BpjsTkBillDetails;

use App\CoreService\CallService;
use App\CoreService\CoreException;
use App\CoreService\CoreService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;

class BpjsTkBillDetailsAdd extends CoreService
{

    public $transaction = true;
    public $task = null;

    public function prepare($input)
    {
        $model = "bpjs-tk-bill-details";
        $classModel = "\\App\\Models\\" . Str::ucfirst(Str::camel($model));
        $permission = "create-" . $model;
        $authRoles = getRoleName(Auth::user()->role_id);
        if (!class_exists($classModel))
            throw new CoreException(__("message.model404", ['model' => $model]), 404);
        if (!$classModel::IS_ADD)
            throw new CoreException("Not found", 404);
        if (!hasPermission($permission))
            throw new CoreException(__("message.forbidden403", ['permission' => $permission, 'roles' => $authRoles]), 403);
        $input["class_model"] = $classModel;

        if ($classModel::FIELD_ARRAY) {
            foreach ($classModel::FIELD_ARRAY as $item) {
                $input[$item] = serialize($input[$item]);
            }
        }

        if ($classModel::FIELD_UPLOAD) {
            foreach ($classModel::FIELD_UPLOAD as $item) {
                if (isset($input[$item])) {
                    if (is_array($input[$item])) {
                        $input[$item] = isset($input[$item]["path"]) ? $input[$item]["path"] : $input[$item]["field_value"];
                    }
                }
            }
        }

        $validator = Validator::make($input, $classModel::FIELD_VALIDATION);

        if ($validator->fails()) {
            throw new CoreException($validator->errors()->first());
        }

        if ($classModel::FIELD_UNIQUE) {
            foreach ($classModel::FIELD_UNIQUE as $search) {
                $query = $classModel::whereRaw("true");
                $fieldTrans = [];
                foreach ($search as $key) {
                    $fieldTrans[] = __("field.$key");
                    $query->where($key, $input[$key]);
                };
                $isi = $query->first();
                if (!is_null($isi)) {
                    throw new CoreException(__("message.alreadyExist", ['field' => implode(",", $fieldTrans)]));
                }
            }
        }

        return $input;
    }

    public function process($input, $originalInput)
    {

        $response = [];
        $classModel = $input["class_model"];

        $input = $classModel::beforeInsert($input);

        $object = new $classModel;
        foreach ($classModel::FIELD_ADD as $item) {
            if ($item == "created_by") {
                $input[$item] = Auth::id();
            }
            if ($item == "updated_by") {
                $input[$item] = Auth::id();
            }
            if (isset($input[$item])) {
                $inputValue = $input[$item] ?? $classModel::FIELD_DEFAULT_VALUE[$item];
                $object->{$item} = ($inputValue !== '') ? $inputValue : null;
            }
        }

        $object->save();

        // MOVE FILE
        foreach ($classModel::FIELD_UPLOAD as $item) {
            $tmpPath = $input[$item] ?? null;

            if (!is_null($tmpPath)) {
                if (!Storage::exists($tmpPath)) {
                    throw new CoreException(__("message.tempFileNotFound", ['field' => $item]));
                }
                $tmpPath = $input[$item];
                $originalname = pathinfo(storage_path($tmpPath), PATHINFO_FILENAME);
                $ext = pathinfo(storage_path($tmpPath), PATHINFO_EXTENSION);

                $newPath = $classModel::FILEROOT . "/" . $originalname . "." . $ext;
                //START MOVE FILE
                if (Storage::exists($newPath)) {

                    $id = 1;
                    $filename = pathinfo(storage_path($newPath), PATHINFO_FILENAME);
                    $ext = pathinfo(storage_path($newPath), PATHINFO_EXTENSION);
                    while (true) {
                        $originalname = $filename . "($id)." . $ext;
                        if (!Storage::exists($classModel::FILEROOT . "/" . $originalname))
                            break;
                        $id++;
                    }
                    $newPath = $classModel::FILEROOT . "/" . $originalname;
                }

                $ext = pathinfo(storage_path($newPath), PATHINFO_EXTENSION);
                $object->{$item} = $newPath;
                Storage::move($tmpPath, $newPath);
            }
        }
        //END MOVE FILE

        $object->save();
        $displayedDataAfterInsert["id"] = $object->id;
        $displayedDataAfterInsert["model"] = $classModel::TABLE;
        $displayedDataAfterInsert = CallService::run("Find", $displayedDataAfterInsert);
        if (isset($displayedDataAfterInsert->original["data"])) {
            $displayedDataAfterInsert = $displayedDataAfterInsert->original["data"];
        } else {
            $displayedDataAfterInsert = $displayedDataAfterInsert->original;
            throw new CoreException($displayedDataAfterInsert, 500);
        }

        //AFTER INSERT
        $afterInsertedRespnese = $classModel::afterInsert($displayedDataAfterInsert, $input);


        #1 INSERT BPJS TK BILL DETAILS : EMPLOYEES BASED ON MAPPING
        # CALCULATE UPAH BASED ON MAPPING UPAH BPJS
        # JOB POSITION, STATUS EMPLOYMENT BASED ON TABLE EMPLOYEE ON PERIODES
        $getPeriodeMonth = DB::selectOne("SELECT * FROM bpjs_tk_bills WHERE id = :bpjs_tk_bill_id ", [
            "bpjs_tk_bill_id" => $object->bpjs_tk_bill_id
        ]);

        $paramsInsertDetail["bpjs_tk_bill_detail_id"] = $object->id;
        $paramsInsertDetail["periode_month"] = $getPeriodeMonth->periode_month;
        $paramsInsertDetail["employee_id"] = $object->employee_id;
        $paramsInsertDetail["created_by"] = $object->created_by;
        $paramsInsertDetail["created_at"] = $object->created_at;
        $paramsInsertDetail["updated_by"] = $object->updated_by;
        $paramsInsertDetail["updated_at"] = $object->updated_at;
        $sqlDataDetail = "WITH insert_bpjs_tk_bill_detail_upah_components AS (
            INSERT INTO bpjs_tk_bill_detail_upah_components (bpjs_tk_bill_detail_id, salary_component_id, koefisien, default_value, component_value, created_by,created_at,updated_by,updated_at)
            SELECT DISTINCT ON (MUPBPJS.salary_component_id) :bpjs_tk_bill_detail_id, MUPBPJS.salary_component_id, MUPBPJS.koefisien, MTSE.default_value, MUPBPJS.koefisien * MTSE.default_value, :created_by, :created_at, :updated_by, :updated_at
            FROM mapping_template_salary_employees MTSE 
            JOIN employee_on_periodes EOPA ON EOPA.employee_id =  MTSE.employee_id
            JOIN job_positions JP ON EOPA.job_position_id = JP.id
            JOIN master_job_position_categories MJPC ON JP.job_position_category_id = MJPC.id
            JOIN mapping_upah_bpjs MUPBPJS ON MJPC.id = MUPBPJS.job_position_category_id AND MUPBPJS.status_employment_id = EOPA.status_employment_id AND MUPBPJS.salary_component_id = MTSE.salary_component_id AND MTSE.periode_month = :periode_month
            WHERE MUPBPJS.active = 1 AND EOPA.employee_id = :employee_id
            ON CONFLICT DO NOTHING
        )
        SELECT * FROM bpjs_tk_bill_details
        ";

        $insertDataDetail =  DB::insert($sqlDataDetail, $paramsInsertDetail);

        // THEN UPDATE UPAH
        $paramUpdateUpah["bpjs_tk_bill_detail_id"] = $object->id;
        $sqlUpdateUpah = "UPDATE bpjs_tk_bill_details 
        SET upah_value = (SELECT SUM(component_value) FROM bpjs_tk_bill_detail_upah_components WHERE bpjs_tk_bill_detail_id = :bpjs_tk_bill_detail_id )
        WHERE id = :bpjs_tk_bill_detail_id";
        $updateUpah = DB::statement($sqlUpdateUpah, $paramUpdateUpah);


        $response["data"] = $displayedDataAfterInsert;
        $response["after_inserted_response"] = $afterInsertedRespnese;
        $response["check"] = $insertDataDetail;
        $response["message"] = __("message.successfullyAdd");

        return $response;
    }

    protected function validation()
    {
        return [
            "bpjs_tk_bill_id" => "required",
            "employee_id" => "required"
        ];
    }
}
