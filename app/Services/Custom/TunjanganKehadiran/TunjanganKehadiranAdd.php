<?php

namespace App\Services\Custom\TunjanganKehadiran;

use App\CoreService\CallService;
use App\CoreService\CoreException;
use App\CoreService\CoreService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;

class TunjanganKehadiranAdd extends CoreService
{

    public $transaction = true;
    public $task = null;

    public function prepare($input)
    {
        $model = "tunjangan-kehadiran";
        $classModel = "\\App\\Models\\" . Str::ucfirst(Str::camel($model));
        $permission = "create-" . $model;
        $authRoles = getRoleName(Auth::user()->role_id);
        if (!class_exists($classModel))
            throw new CoreException(__("message.model404", ['model' => $model]), 404);
        if (!$classModel::IS_ADD)
            throw new CoreException("Not found", 404);
        if (!hasPermission($permission))
            throw new CoreException(__("message.forbidden403", ['permission' => $permission, 'roles' => $authRoles]), 403);
        $input["class_model"] = $classModel;

        if ($classModel::FIELD_ARRAY) {
            foreach ($classModel::FIELD_ARRAY as $item) {
                $input[$item] = serialize($input[$item]);
            }
        }

        if ($classModel::FIELD_UPLOAD) {
            foreach ($classModel::FIELD_UPLOAD as $item) {
                if (isset($input[$item])) {
                    if (is_array($input[$item])) {
                        $input[$item] = isset($input[$item]["path"]) ? $input[$item]["path"] : $input[$item]["field_value"];
                    }
                }
            }
        }

        $validator = Validator::make($input, $classModel::FIELD_VALIDATION);

        if ($validator->fails()) {
            throw new CoreException($validator->errors()->first());
        }

        if ($classModel::FIELD_UNIQUE) {
            foreach ($classModel::FIELD_UNIQUE as $search) {
                $query = $classModel::whereRaw("true");
                $fieldTrans = [];
                foreach ($search as $key) {
                    $fieldTrans[] = __("field.$key");
                    $query->where($key, $input[$key]);
                };
                $isi = $query->first();
                if (!is_null($isi)) {
                    throw new CoreException(__("message.alreadyExist", ['field' => implode(",", $fieldTrans)]));
                }
            }
        }

        //VALIDASI INPUT PERIODE MONTH TIDAK BOLEH MELEBIHI BULAN INI 

        if (isset($input["periode_month"])) {
            $month = $input["periode_month"];
            $curMonth = date("Y-m-d");
            if ($month > $curMonth) {
                throw new CoreException("Periode Bulan yang Dipilih Melebihi Periode Saat Ini. Silahkan Pilih Periode Bulan Saat ini atau bulan sebelumnya.");
            }
        }

        // CHECK EMPLOYEE ON PERIODES
        $checkEmployeeOnPeriodes = DB::selectOne("SELECT COUNT(id) FROM employee_on_periodes WHERE periode_month = :periode_month", [
            "periode_month" => $input["periode_month"]
        ]);
        if ($checkEmployeeOnPeriodes->count < 1) {
            throw new CoreException("Tidak terdapat data tenaga kerja aktif pada periode penggajian bulan yang dipilih. Silahkan cek kembali tenaga kerja pada bulan ini.");
        }

        // CHECK PRESENCE ON PERIODE
        $checkPresenceOnPeriodes = DB::selectOne("SELECT COUNT(id) FROM presence_on_periodes WHERE periode_month = :periode_month", [
            "periode_month" => $input["periode_month"]
        ]);
        if ($checkPresenceOnPeriodes->count < 1) {
            throw new CoreException("Tidak terdapat data rekap presensi pada periode bulan yang dipilih. Silahkan cek data rekap presensi pada bulan ini.");
        }

        // CHECK MASTER KOMPONEN GAJI DENGAN FUNGSI : customSalaryModuleTunjanganKehadiran
        $salaryComponentTunjanganKehadiran = DB::selectOne("SELECT A.* FROM master_salary_components A
        JOIN custom_salary_modules B ON A.custom_salary_module_id = B.id
        WHERE A.is_custom_component = true AND B.function_name = 'customSalaryModuleTunjanganKehadiran'");
        if (is_null($salaryComponentTunjanganKehadiran)) {
            throw new CoreException("Tidak terdapat data master komponen gaji dengan fungsi Tunjangan Kehadiran. Silahkan cek data master komponen gaji dengan komponen gaji Tunjangan Kehadiran.");
        }
        $input["salaryComponentTunjanganKehadiran"] = $salaryComponentTunjanganKehadiran->id;
        return $input;
    }

    public function process($input, $originalInput)
    {

        $response = [];
        $classModel = $input["class_model"];

        $input = $classModel::beforeInsert($input);

        $object = new $classModel;
        foreach ($classModel::FIELD_ADD as $item) {
            if ($item == "created_by") {
                $input[$item] = Auth::id();
            }
            if ($item == "updated_by") {
                $input[$item] = Auth::id();
            }
            if (isset($input[$item])) {
                $inputValue = $input[$item] ?? $classModel::FIELD_DEFAULT_VALUE[$item];
                $object->{$item} = ($inputValue !== '') ? $inputValue : null;
            }
        }

        $object->save();

        // MOVE FILE
        foreach ($classModel::FIELD_UPLOAD as $item) {
            $tmpPath = $input[$item] ?? null;

            if (!is_null($tmpPath)) {
                if (!Storage::exists($tmpPath)) {
                    throw new CoreException(__("message.tempFileNotFound", ['field' => $item]));
                }
                $tmpPath = $input[$item];
                $originalname = pathinfo(storage_path($tmpPath), PATHINFO_FILENAME);
                $ext = pathinfo(storage_path($tmpPath), PATHINFO_EXTENSION);

                $newPath = $classModel::FILEROOT . "/" . $originalname . "." . $ext;
                //START MOVE FILE
                if (Storage::exists($newPath)) {

                    $id = 1;
                    $filename = pathinfo(storage_path($newPath), PATHINFO_FILENAME);
                    $ext = pathinfo(storage_path($newPath), PATHINFO_EXTENSION);
                    while (true) {
                        $originalname = $filename . "($id)." . $ext;
                        if (!Storage::exists($classModel::FILEROOT . "/" . $originalname))
                            break;
                        $id++;
                    }
                    $newPath = $classModel::FILEROOT . "/" . $originalname;
                }

                $ext = pathinfo(storage_path($newPath), PATHINFO_EXTENSION);
                $object->{$item} = $newPath;
                Storage::move($tmpPath, $newPath);
            }
        }
        //END MOVE FILE

        $object->save();
        $displayedDataAfterInsert["id"] = $object->id;
        $displayedDataAfterInsert["model"] = $classModel::TABLE;
        $displayedDataAfterInsert = CallService::run("Find", $displayedDataAfterInsert);
        if (isset($displayedDataAfterInsert->original["data"])) {
            $displayedDataAfterInsert = $displayedDataAfterInsert->original["data"];
        } else {
            $displayedDataAfterInsert = $displayedDataAfterInsert->original;
            throw new CoreException($displayedDataAfterInsert, 500);
        }

        //AFTER INSERT
        $afterInsertedRespnese = $classModel::afterInsert($displayedDataAfterInsert, $input);


        #1 INSERT TUNJANGAN TRANSPORT DETAILS : EMPLOYEES BASED ON MAPPING
        $paramsInsertDetail["tunjangan_kehadiran_id"] = $object->id;
        $paramsInsertDetail["salary_component_id"] = $input["salaryComponentTunjanganKehadiran"];
        $paramsInsertDetail["periode_month"] = $object->periode_month;
        $paramsInsertDetail["created_by"] = $object->created_by;
        $paramsInsertDetail["created_at"] = $object->created_at;
        $paramsInsertDetail["updated_by"] = $object->updated_by;
        $paramsInsertDetail["updated_at"] = $object->updated_at;
        $sqlDataDetail = "WITH insert_tunjangan_kehadiran_details AS (
            INSERT INTO tunjangan_kehadiran_details (tunjangan_kehadiran_id, employee_id, base_value, created_by,created_at,updated_by,updated_at)
            SELECT :tunjangan_kehadiran_id, EOP.employee_id, MTSE.default_value, :created_by, :created_at, :updated_by, :updated_at
            FROM employee_on_periodes EOP 
            LEFT JOIN mapping_template_salary_employees MTSE ON EOP.employee_id = MTSE.employee_id AND MTSE.salary_component_id = :salary_component_id AND MTSE.periode_month = :periode_month
            WHERE EOP.periode_month = :periode_month
            ON CONFLICT DO NOTHING
            RETURNING id, employee_id
        )
        SELECT * FROM insert_tunjangan_kehadiran_details
        ";

        $insertDataDetail =  DB::insert($sqlDataDetail, $paramsInsertDetail);

        $response["data"] = $displayedDataAfterInsert;
        $response["after_inserted_response"] = $afterInsertedRespnese;
        $response["check"] = $insertDataDetail;
        $response["message"] = __("message.successfullyAdd");

        return $response;
    }

    protected function validation()
    {
        return [];
    }
}
