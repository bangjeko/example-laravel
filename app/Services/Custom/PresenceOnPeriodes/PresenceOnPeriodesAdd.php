<?php

namespace App\Services\Custom\PresenceOnPeriodes;

use App\CoreService\CallService;
use App\CoreService\CoreException;
use App\CoreService\CoreService;
use App\Models\PayrollPeriodes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;

class PresenceOnPeriodesAdd extends CoreService
{

    public $transaction = true;
    public $task = null;

    public function prepare($input)
    {
        $model = "presence-on-periodes";
        $classModel = "\\App\\Models\\" . Str::ucfirst(Str::camel($model));
        $permission = "create-" . $model;
        $authRoles = getRoleName(Auth::user()->role_id);
        if (!class_exists($classModel))
            throw new CoreException(__("message.model404", ['model' => $model]), 404);
        if (!$classModel::IS_ADD)
            throw new CoreException("Not found", 404);
        if (!hasPermission($permission))
            throw new CoreException(__("message.forbidden403", ['permission' => $permission, 'roles' => $authRoles]), 403);
        $input["class_model"] = $classModel;

        if ($classModel::FIELD_ARRAY) {
            foreach ($classModel::FIELD_ARRAY as $item) {
                $input[$item] = serialize($input[$item]);
            }
        }

        if ($classModel::FIELD_UPLOAD) {
            foreach ($classModel::FIELD_UPLOAD as $item) {
                if (isset($input[$item])) {
                    if (is_array($input[$item])) {
                        $input[$item] = isset($input[$item]["path"]) ? $input[$item]["path"] : $input[$item]["field_value"];
                    }
                }
            }
        }

        $validator = Validator::make($input, $classModel::FIELD_VALIDATION);

        if ($validator->fails()) {
            throw new CoreException($validator->errors()->first());
        }

        if ($classModel::FIELD_UNIQUE) {
            foreach ($classModel::FIELD_UNIQUE as $search) {
                $query = $classModel::whereRaw("true");
                $fieldTrans = [];
                foreach ($search as $key) {
                    $fieldTrans[] = __("field.$key");
                    $query->where($key, $input[$key]);
                };
                $isi = $query->first();
                if (!is_null($isi)) {
                    throw new CoreException(__("message.alreadyExist", ['field' => implode(",", $fieldTrans)]));
                }
            }
        }

        //VALIDASI INPUT PERIODE MONTH TIDAK BOLEH MELEBIHI BULAN INI 

        if (isset($input["periode_month"])) {
            $month = $input["periode_month"];
            $curMonth = date("Y-m-d");
            if ($month > $curMonth) {
                throw new CoreException("Periode Bulan yang Dipilih Melebihi Periode Saat Ini. Silahkan Pilih Periode Bulan Saat ini atau bulan sebelumnya.");
            }
        }

        // CHECK PAYROLL ON PERIODES
        $payrollPeriodesData = PayrollPeriodes::where('periode_month', '=', $input["periode_month"])->first();
        if (is_null($payrollPeriodesData)) {
            throw new CoreException("Periode Penggajian Belum Dibuat.");
        }
        $input["start_date"] = $payrollPeriodesData->start_date;
        $input["end_date"] = $payrollPeriodesData->end_date;
        return $input;
    }

    public function process($input, $originalInput)
    {

        $response = [];
        $classModel = $input["class_model"];

        $input = $classModel::beforeInsert($input);

        $object = new $classModel;
        foreach ($classModel::FIELD_ADD as $item) {
            if ($item == "created_by") {
                $input[$item] = Auth::id();
            }
            if ($item == "updated_by") {
                $input[$item] = Auth::id();
            }
            if (isset($input[$item])) {
                $inputValue = $input[$item] ?? $classModel::FIELD_DEFAULT_VALUE[$item];
                $object->{$item} = ($inputValue !== '') ? $inputValue : null;
            }
        }

        $object->save();

        // MOVE FILE
        foreach ($classModel::FIELD_UPLOAD as $item) {
            $tmpPath = $input[$item] ?? null;

            if (!is_null($tmpPath)) {
                if (!Storage::exists($tmpPath)) {
                    throw new CoreException(__("message.tempFileNotFound", ['field' => $item]));
                }
                $tmpPath = $input[$item];
                $originalname = pathinfo(storage_path($tmpPath), PATHINFO_FILENAME);
                $ext = pathinfo(storage_path($tmpPath), PATHINFO_EXTENSION);

                $newPath = $classModel::FILEROOT . "/" . $originalname . "." . $ext;
                //START MOVE FILE
                if (Storage::exists($newPath)) {

                    $id = 1;
                    $filename = pathinfo(storage_path($newPath), PATHINFO_FILENAME);
                    $ext = pathinfo(storage_path($newPath), PATHINFO_EXTENSION);
                    while (true) {
                        $originalname = $filename . "($id)." . $ext;
                        if (!Storage::exists($classModel::FILEROOT . "/" . $originalname))
                            break;
                        $id++;
                    }
                    $newPath = $classModel::FILEROOT . "/" . $originalname;
                }

                $ext = pathinfo(storage_path($newPath), PATHINFO_EXTENSION);
                $object->{$item} = $newPath;
                Storage::move($tmpPath, $newPath);
            }
        }
        //END MOVE FILE

        $object->save();
        $displayedDataAfterInsert["id"] = $object->id;
        $displayedDataAfterInsert["model"] = $classModel::TABLE;
        $displayedDataAfterInsert = CallService::run("Find", $displayedDataAfterInsert);
        if (isset($displayedDataAfterInsert->original["data"])) {
            $displayedDataAfterInsert = $displayedDataAfterInsert->original["data"];
        } else {
            $displayedDataAfterInsert = $displayedDataAfterInsert->original;
            throw new CoreException($displayedDataAfterInsert, 500);
        }

        //AFTER INSERT
        $afterInsertedRespnese = $classModel::afterInsert($displayedDataAfterInsert, $input);


        #1 INSERT PRESENCE On PERIODE RECAPS DATA : EMPLOYEES BASED ON MAPPING
        # JOB POSITION, STATUS EMPLOYMENT BASED ON TABLE EMPLOYEE ON PERIODES
        $paramsInsertDetail["presence_on_periode_id"] = $object->id;
        $paramsInsertDetail["periode_month"] = $object->periode_month;
        $paramsInsertDetail["created_by"] = $object->created_by;
        $paramsInsertDetail["created_at"] = $object->created_at;
        $paramsInsertDetail["updated_by"] = $object->updated_by;
        $paramsInsertDetail["updated_at"] = $object->updated_at;
        $sqlDataDetail = "WITH insert_presence_on_periode_recaps AS (
            INSERT INTO presence_on_periode_recaps (presence_on_periode_id, employee_id, created_by, created_at, updated_by, updated_at)
            SELECT :presence_on_periode_id, employee_id, :created_by, :created_at, :updated_by, :updated_at
            FROM employee_on_periodes EOP WHERE EOP.periode_month = :periode_month
            ON CONFLICT DO NOTHING
            RETURNING id, employee_id
        )
        SELECT * FROM insert_presence_on_periode_recaps
        ";

        $insertDataDetail =  DB::insert($sqlDataDetail, $paramsInsertDetail);

        $response["data"] = $displayedDataAfterInsert;
        $response["after_inserted_response"] = $afterInsertedRespnese;
        $response["check"] = $insertDataDetail;
        $response["message"] = __("message.successfullyAdd");

        return $response;
    }

    protected function validation()
    {
        return [];
    }
}
