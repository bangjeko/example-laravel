<?php

namespace App\Services\Custom\PinjamanBank;

use App\CoreService\CallService;
use App\CoreService\CoreException;
use App\CoreService\CoreService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;

class CardSummaryPinjaman extends CoreService
{

    public $transaction = false;
    public $task = null;

    public function prepare($input)
    {
        $model = "pinjaman-bank";
        $permission = "show-" . $model;
        $authRoles = getRoleName(Auth::user()->role_id);
        $classModel = "\\App\\Models\\" . Str::ucfirst(Str::camel($model));
        if (!class_exists($classModel))
            throw new CoreException(__("message.model404", ['model' => $model]), 404);

        if (!$classModel::IS_LIST)
            throw new CoreException("Not found", 404);

        if (!hasPermission($permission))
            throw new CoreException(__("message.forbidden403", ['permission' => $permission, 'role' => $authRoles]), 403);

        $input["class_model"] = $classModel;
        return $input;
    }

    public function process($input, $originalInput)
    {

        # Data Component Bank On Periode
        $object = DB::select("SELECT MCPB.id, MCPB.name, sum(A.comp_value) as value FROM pinjaman_bank_detail_components A
        JOIN pinjaman_bank_details B ON A.pinjaman_bank_detail_id = B.id
        JOIN pinjaman_bank C ON B.pinjaman_bank_id = C.id
        JOIN master_comp_pinjaman_bank MCPB ON A.comp_pinjaman_bank_id = MCPB.id
        WHERE C.id = :id
        GROUP BY MCPB.id",[
            "id" => $input["id"]
        ]);
        array_map(function ($key) {
            foreach ($key as $field => $value) {
                $key->value = (float) $key->value;
            }
            return $key;
        }, $object);
        return [
            "data" => $object
        ];
    }

    protected function validation()
    {
        return [];
    }
}
