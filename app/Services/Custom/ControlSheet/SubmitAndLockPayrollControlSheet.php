<?php

namespace App\Services\Custom\ControlSheet;

use App\CoreService\CoreException;
use App\CoreService\CoreService;
use App\Models\ControlSheetLogs;
use App\Models\EmployeeOnPeriodes;
use App\Models\PayrollPeriodes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;

class SubmitAndLockPayrollControlSheet extends CoreService
{

    public $transaction = false;
    public $task = null;

    public function prepare($input)
    {
        $permission = "submit-control-sheet-payroll";
        $authRoles = getRoleName(Auth::user()->role_id);
        if (!hasPermission($permission))
            throw new CoreException(__("message.forbidden403", ['permission' => $permission, 'role' => $authRoles]), 403);

        return $input;
    }

    public function process($input, $originalInput)
    {
        $periodeMonth = $input["periode_month"];
        $dataPayrollPeriode = PayrollPeriodes::where('periode_month', '=', $periodeMonth)->first();
        if (is_null($dataPayrollPeriode)) {
            throw new CoreException(__("message.periodePenggajianNotFound", ['periode_month' => $input["periode_month"]]));
        }

        $dataPayrollPeriode->is_lock = true;
        $dataPayrollPeriode->status_code = 'verification_request';
        $dataPayrollPeriode->submitted_by = Auth::id();
        $dataPayrollPeriode->submitted_at = now();
        $dataPayrollPeriode->unlocked_by = null;
        $dataPayrollPeriode->unlocked_at = null;
        $dataPayrollPeriode->save();
        # INSERT DATA VERIFIKASI
        // DELETE DATA VERIFIKASI DULU
        DB::statement("DELETE FROM control_sheet_verifications WHERE payroll_periode_id = :payroll_periode_id", [
            "payroll_periode_id" => $dataPayrollPeriode->id
        ]);

        $sqlParamsVerificationOrders["payroll_periode_id"] = $dataPayrollPeriode->id;
        $sqlParamsVerificationOrders["created_by"] = Auth::id();
        $sqlParamsVerificationOrders["created_at"] = now();
        $sqlParamsVerificationOrders["updated_by"] = Auth::id();
        $sqlParamsVerificationOrders["updated_at"] = now();
        $sqlVerificationOrders = "INSERT INTO control_sheet_verifications (payroll_periode_id, privillege_role_id, verification_order, created_by,created_at,updated_by,updated_at)
        SELECT :payroll_periode_id, role_id, verification_order, :created_by, :created_at, :updated_by, :updated_at
        FROM config_control_sheet_verification_orders WHERE active = 1
        ORDER BY verification_order ASC";
        $insertDataVerificationOrders =  DB::insert($sqlVerificationOrders, $sqlParamsVerificationOrders);


        ######
        # START INSERT LOG CONTROl SHEET
        ######
        $objectLog = new ControlSheetLogs;
        $objectLog->payroll_periode_id = $dataPayrollPeriode->id;
        $objectLog->status_code = 'verification_request';
        $objectLog->description = isset($input["description"]) ? $input["description"] : null;
        $objectLog->save();
        ######
        # END INSERT LOG CONTROl SHEET
        ######

        return [
            "message" => __("message.successfullySubmitAndLockPayroll")
        ];
    }

    protected function validation()
    {
        return [
            "periode_month" => "required"
        ];
    }
}
