<?php

namespace App\Services\Custom\ControlSheet;

use App\CoreService\CoreException;
use App\CoreService\CoreService;
use App\Models\EmployeeOnPeriodes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;

class ControlSheetJurnalList extends CoreService
{

    public $transaction = false;
    public $task = null;

    public function prepare($input)
    {
        $permission = "view-control-sheet-jurnal";
        $authRoles = getRoleName(Auth::user()->role_id);
        if (!hasPermission($permission))
            throw new CoreException(__("message.forbidden403", ['permission' => $permission, 'role' => $authRoles]), 403);

        return $input;
    }

    public function process($input, $originalInput)
    {
        $total = 0;
        $totalPage = 0;

        # GET DATA KARYAWAN On PERIODE
        $employeeOnPeriodes = DB::select("SELECT 
        EOP.periode_month,
        MJPC.id as job_position_category_id,
        MJPC.name as rel_job_position_category_id,
        MSE.id as status_employment_id ,
        MSE.name as rel_status_employment_id,
        sum(EOP.salary_value) as salary_value,
        sum(EOP.pembulatan_salary) as pembulatan_salary,
        sum(EOP.selisih_pembulatan) as selisih_pembulatan
        FROM employee_on_periodes EOP 
        JOIN employees E ON EOP.employee_id = E.id
        LEFT JOIN job_positions JP ON EOP.job_position_id = JP.id
        LEFT JOIN master_job_position_categories MJPC ON JP.job_position_category_id = MJPC.id
        LEFT JOIN master_status_employments MSE ON EOP.status_employment_id = MSE.id
        WHERE EOP.periode_month = :periode_month 
        GROUP BY EOP.periode_month, MJPC.id, MSE.id
        ORDER BY MSE.id ASC, MJPC.id ASC
        ", [
            "periode_month" => $input['periode_month']
        ]);

        # KOMPONEN GAJI ON PERIODE
        $dataSalaryComponentsCategories = DB::select("SELECT MSCC.id, MSCC.name 
        FROM salary_component_on_periodes SCOP
        JOIN employee_on_periodes EOP ON SCOP.employee_on_periode_id = EOP.id
        JOIN master_salary_components MSC ON SCOP.salary_component_id = MSC.id
        JOIN master_salary_component_categories MSCC ON MSC.salary_component_category_id = MSCC.id
        WHERE EOP.periode_month = :periode_month
        GROUP BY MSCC.id, MSCC.name
        ORDER BY MSCC.id ASC", [
            "periode_month" => $input["periode_month"]
        ]);

        $dataSalaryComponents = DB::select("SELECT MSC.id, MSC.name, MSC.salary_component_category_id, MSCC.name as rel_salary_component_category_id, 0 AS value FROM salary_component_on_periodes SCOP
        JOIN employee_on_periodes EOP ON SCOP.employee_on_periode_id = EOP.id
        JOIN master_salary_components MSC ON SCOP.salary_component_id = MSC.id
        JOIN master_salary_component_categories MSCC ON MSC.salary_component_category_id = MSCC.id
        WHERE EOP.periode_month = :periode_month AND MSC.is_jurnal_component = 'true'
        GROUP BY MSC.id, MSCC.name
        ORDER BY MSC.id ASC", [
            "periode_month" => $input["periode_month"]
        ]);

        $salaryDetails = array_map(function ($key) use ($dataSalaryComponents) {
            foreach ($key as $field => $value) {
                $catId = $key->id;
                $key->data_salary_components = [];
                foreach ($dataSalaryComponents as $kSc) {
                    $salCompCatId = $kSc->salary_component_category_id;
                    if ($salCompCatId == $catId) {
                        array_push($key->data_salary_components, $kSc);
                    }
                }
            }
            return $key;
        }, $dataSalaryComponentsCategories);

        # MAPPING DATA EMPLOYEE ON PERIODE DENGAN KOMPONEN GAJI
        array_map(function ($key) use ($dataSalaryComponentsCategories) {
            foreach ($key as $field => $value) {
                $key->data_salary = $dataSalaryComponentsCategories;
            }
            return $key;
        }, $employeeOnPeriodes);

        ######################################################################
        $dataSalaryComponentOnPeriodes = DB::select("SELECT EOP.status_employment_id, 
        JP.job_position_category_id,
		MSC.id as salary_component_id,
		sum(SCOP.salary_component_value) as  salary_component_value
        FROM salary_component_on_periodes SCOP
        JOIN employee_on_periodes EOP ON SCOP.employee_on_periode_id = EOP.id
        JOIN job_positions JP ON EOP.job_position_id = JP.id
        JOIN master_salary_components MSC ON SCOP.salary_component_id = MSC.id
        JOIN master_salary_component_categories MSCC ON MSC.salary_component_category_id = MSCC.id
        WHERE EOP.periode_month = :periode_month
		GROUP BY EOP.status_employment_id, JP.job_position_category_id, MSC.id
        ORDER BY MSC.id ASC", [
            "periode_month" => $input["periode_month"]
        ]);

        ######################################################################
        # MENGISI VALUE BASED ON salary_component_on_periodes

        $finalObject = [];
        foreach ($employeeOnPeriodes as $kEOP) {
            $finalDataEmployee = [];
            $finalDataEmployee["periode_month"] = $kEOP->periode_month;
            $finalDataEmployee["status_employment_id"] = $kEOP->status_employment_id;
            $finalDataEmployee["rel_status_employment_id"] = $kEOP->rel_status_employment_id;
            $finalDataEmployee["job_position_category_id"] = $kEOP->job_position_category_id;
            $finalDataEmployee["rel_job_position_category_id"] = $kEOP->rel_job_position_category_id;
            // $finalDataEmployee["salary_value"] = (float) $kEOP->salary_value;
            // $finalDataEmployee["pembulatan_salary"] = (float) $kEOP->pembulatan_salary;
            // $finalDataEmployee["selisih_pembulatan"] = (float) $kEOP->selisih_pembulatan;
            $finalDataEmployee["data_salary"] = [];
            $rowEmployeOnPeriodeId = $kEOP->job_position_category_id;
            $rowStatusEmploymentId = $kEOP->status_employment_id;
            foreach ($kEOP->data_salary as $kDatSal) {
                $finalDataSalary = [];
                $finalDataSalary["id"] = $kDatSal->id;
                $finalDataSalary["name"] = $kDatSal->name;
                $finalDataSalary["data_salary_components"] = [];
                foreach ($kDatSal->data_salary_components as $kDatSalComp) {
                    $finalDataSalaryComponents = [];
                    $finalDataSalaryComponents["id"] = $kDatSalComp->id;
                    $finalDataSalaryComponents["name"] = $kDatSalComp->name;
                    $finalDataSalaryComponents["salary_component_category_id"] = $kDatSalComp->salary_component_category_id;
                    $finalDataSalaryComponents["rel_salary_component_category_id"] = $kDatSalComp->rel_salary_component_category_id;
                    $finalDataSalaryComponents["value"] = $kDatSalComp->value;
                    $salCompId = $kDatSalComp->id;
                    if (isset($dataSalaryComponentOnPeriodes)) {
                        $newSalaryComponentValue = multiArraySerach($dataSalaryComponentOnPeriodes, [
                            "job_position_category_id" => $rowEmployeOnPeriodeId,
                            "status_employment_id" => $rowStatusEmploymentId,
                            "salary_component_id" => $salCompId,
                        ], 'salary_component_value');
                        $finalDataSalaryComponents["value"] = !is_null($newSalaryComponentValue) ? (float) $newSalaryComponentValue : null;
                    }
                    array_push($finalDataSalary["data_salary_components"], $finalDataSalaryComponents);
                }
                array_push($finalDataEmployee["data_salary"], $finalDataSalary);
            }
            array_push($finalObject, $finalDataEmployee);
        }

        return [
            "data" => $finalObject,
        ];
    }

    protected function validation()
    {
        return [
            "periode_month" => "required"
        ];
    }
}
