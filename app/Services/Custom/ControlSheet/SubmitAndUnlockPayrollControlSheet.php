<?php

namespace App\Services\Custom\ControlSheet;

use App\CoreService\CoreException;
use App\CoreService\CoreService;
use App\Models\ControlSheetLogs;
use App\Models\EmployeeOnPeriodes;
use App\Models\PayrollPeriodes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;

class SubmitAndUnlockPayrollControlSheet extends CoreService
{

    public $transaction = false;
    public $task = null;

    public function prepare($input)
    {
        $permission = "unlock-control-sheet-payroll";
        $authRoles = getRoleName(Auth::user()->role_id);
        if (!hasPermission($permission))
            throw new CoreException(__("message.forbidden403", ['permission' => $permission, 'role' => $authRoles]), 403);

        return $input;
    }

    public function process($input, $originalInput)
    {
        $periodeMonth = $input["periode_month"];
        $dataPayrollPeriode = PayrollPeriodes::where('periode_month', '=', $periodeMonth)->first();
        if (is_null($dataPayrollPeriode)) {
            throw new CoreException(__("message.periodePenggajianNotFound", ['periode_month' => $input["periode_month"]]));
        }

        $dataPayrollPeriode->is_lock = false;
        $dataPayrollPeriode->status_code = 'unlock';
        $dataPayrollPeriode->submitted_by = null;
        $dataPayrollPeriode->submitted_at = null;
        $dataPayrollPeriode->unlocked_by = Auth::id();
        $dataPayrollPeriode->unlocked_at = now();
        $dataPayrollPeriode->save();
        # INSERT DATA VERIFIKASI
        // DELETE DATA VERIFIKASI DULU
        DB::statement("DELETE FROM control_sheet_verifications WHERE payroll_periode_id = :payroll_periode_id", [
            "payroll_periode_id" => $dataPayrollPeriode->id
        ]);


        ######
        # START INSERT LOG CONTROl SHEET
        ######
        $objectLog = new ControlSheetLogs;
        $objectLog->payroll_periode_id = $dataPayrollPeriode->id;
        $objectLog->status_code = 'unlock_verification';
        $objectLog->description = isset($input["description"]) ? $input["description"] : null;
        $objectLog->save();
        ######
        # END INSERT LOG CONTROl SHEET
        ######

        return [
            "message" => __("message.successfullySubmitAndUnlockPayroll")
        ];
    }

    protected function validation()
    {
        return [
            "periode_month" => "required"
        ];
    }
}
