<?php

namespace App\Services\Custom\ControlSheet;

use App\CoreService\CoreException;
use App\CoreService\CoreService;
use App\Models\ControlSheetLogs;
use App\Models\ControlSheetVerifications;
use App\Models\EmployeeOnPeriodes;
use App\Models\PayrollPeriodes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;

class ControlSheetDoVerifications extends CoreService
{

    public $transaction = false;
    public $task = null;

    public function prepare($input)
    {
        $permission = "verify-control-sheet-payroll";
        $authRoles = getRoleName(Auth::user()->role_id);
        if (!hasPermission($permission))
            throw new CoreException(__("message.forbidden403", ['permission' => $permission, 'role' => $authRoles]), 403);

        return $input;
    }

    public function process($input, $originalInput)
    {

        $roleId = Auth::user()->role_id;
        $description = isset($input["description"]) ?  $input["description"] : null;
        if ($roleId > 1) {
            if ($roleId != $input["privillege_role_id"]) {
                throw new CoreException(__("message.invalidRoleCantDoVerification"));
            }
        }
        $dataControlSheetVerification = ControlSheetVerifications::find($input["id"]);

        $dataControlSheetVerification->verified_by = Auth::user()->id;
        $dataControlSheetVerification->verified_at = now();
        $dataControlSheetVerification->status_code = $input["status_code"];
        $dataControlSheetVerification->description = $description;
        $dataControlSheetVerification->save();


        // CHECK OTHER VERIFICATIONS
        $checkOtherVerification = DB::select("SELECT status_code, count(*) FROM control_sheet_verifications
        WHERE payroll_periode_id = :payroll_periode_id 
        GROUP BY status_code", [
            "payroll_periode_id" => $input["payroll_periode_id"]
        ]);

        $countVerification = array_sum(array_column($checkOtherVerification, 'count'));;
        $countRevision = 0;
        $countNull = 0;
        $countApproved = 0;
        $summaryStatusCode = '';
        $isLock = true;
        foreach ($checkOtherVerification as $k) {
            if ($k->status_code == 'revision_request') {
                $countRevision =  $k->count;
            } else if (is_null($k->status_code)) {
                $countNull = $k->count;
            } else if ($k->status_code == 'approved') {
                $countApproved =  $k->count;
            }
        }

        ################################
        if ($countRevision > 0) {
            $summaryStatusCode = 'revision_request';
            $isLock = false;
        } else if ($countNull) {
            $summaryStatusCode = 'verification_request';
        } else if ($countVerification == $countApproved) {
            $summaryStatusCode = 'approved';
        }



        $arrayUpdate = [
            'status_code' => $summaryStatusCode,
            'is_lock' => $isLock,
            'description' => $description
        ];

        if (is_null($description)) {
            unset($arrayUpdate['description']);
        }
        PayrollPeriodes::where('id', '=', $input['payroll_periode_id'])
            ->update($arrayUpdate);

        ######
        # START INSERT LOG CONTROl SHEET
        ######
        $objectLog = new ControlSheetLogs;
        $objectLog->payroll_periode_id = $input['payroll_periode_id'];
        $objectLog->status_code = $input["status_code"];
        $objectLog->description = isset($input["description"]) ? $input["description"] : null;
        $objectLog->save();
        ######
        # END INSERT LOG CONTROl SHEET
        ######

        return [
            "message" => __("message.successfullyDoVerificationControlSheetPayroll")
        ];
    }

    protected function validation()
    {
        return [
            "id" => "required",
            "payroll_periode_id" => "required",
            "privillege_role_id" => "required",
            "verification_order" => "required",
            "status_code" => "required"
        ];
    }
}
