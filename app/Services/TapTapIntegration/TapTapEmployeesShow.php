<?php

namespace App\Services\TapTapIntegration;

use App\CoreService\CoreException;
use App\CoreService\CoreService;
use App\Models\AnnouncementFiles;
use App\Models\EventDocumentations;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;
use Illuminate\Support\Carbon;

class TapTapEmployeesShow extends CoreService
{

    public $transaction = false;
    public $task = null;

    public function prepare($input)
    {
        $permission = "taptap-lookup-employees";
        $authRoles = getRoleName(Auth::user()->role_id);
        if (!hasPermission($permission))
            throw new CoreException(__("message.forbidden403", ['permission' => $permission, 'role' => $authRoles]), 403);

        return $input;
    }

    public function process($input, $originalInput)
    {
        if (!isset($input["id"])) {
            throw new CoreException(__("message.idIsNull"));
        }
        $endpoint = taptapEndPointUrl() . "/data/employee/" . $input["id"];
        $limit = isset($input["limit"]) ? $input["limit"] : 1;
        $page = isset($input["page"]) ? $input["page"] : 1;
        $search = isset($input["search"]) ? $input["search"] : null;
        $offset = ($page - 1) * $limit;
        $params = [
            "limit" => $limit,
            "offset" => $offset,
            "search" => $search
        ];
        $headers = [
            "client-id: " . taptapIntegrationDataDesired('taptap_client_id'),
            "client-secret: " . taptapIntegrationDataDesired('taptap_secret_key'),
        ];
        $url = $endpoint . '?' . http_build_query($params);
        // CURL
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $output = curl_exec($ch);

        if ($output === FALSE) {
            die('Curl error: ' . curl_error($ch));
        }
        curl_close($ch);
        $output = json_decode($output, true);
        if (!$output["success"]) {
            throw new CoreException($output["error_message"], 500);
        }

        $object = $output["data"];
        return [
            "data" => $object,
        ];
    }

    protected function validation()
    {
        return [];
    }
}
