<?php

namespace App\Services\Notification;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\CoreService\CoreException;
use App\CoreService\CoreService;
use App\Models\UserFirebaseTokens;

class SendNotificationSingleUser extends CoreService
{

    public $transaction = false;
    public $task = null;

    public function prepare($input)
    {
        if (!hasPermission("create-notifications"))
            throw new CoreException(__("message.403"), 403);
        return $input;
    }

    public function process($input, $originalInput)
    {
        $url = 'https://fcm.googleapis.com/fcm/send';
        $userId = $input['user_id'];

        // GET DEVICE ID, TOKEN IN user_firebase_token
        $deviceToken = UserFirebaseTokens::whereNotNull('token')
            ->where('user_id', '=', $userId)
            ->pluck('token')->all();

        $FcmKey = 'AAAAVR-8iBU:APA91bFX6I21tr2bX-_NAYnrXoqOXMaWs_kqODaNMCRmUIqDKJ5xrDFem3IJWYMEqpPNjx002UC-_VN9NYQJSMTVD-laPQNL8rwlY15PcaMQkoIrIuyWDntpsKbD5H11MhndlI9rk80m';

        $data = [
            "registration_ids" => $deviceToken,
            "notification" => [
                "title" => $input['title'],
                "body" => $input['body'],
            ],
            "data" => $input['data']
        ];

        $RESPONSE = json_encode($data);

        $headers = [
            'Authorization:key=' . $FcmKey,
            'Content-Type: application/json',
        ];

        // CURL
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $RESPONSE);

        $output = curl_exec($ch);
        if ($output === FALSE) {
            die('Curl error: ' . curl_error($ch));
        }
        curl_close($ch);

        $response = [
            "to" => $deviceToken,
            "output" => json_decode($output)
        ];
        return $response;
    }

    protected function validation()
    {
        return [
            "user_id" => "required",
        ];
    }
}
