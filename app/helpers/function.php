<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Models\Roles;
use App\Models\Users;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;

#### TAP TAP INTEGRATION ####

if (!function_exists('finaEndPointUrl')) {
    function finaEndPointUrl()
    {
        $endPointUrl =  "https://fina.kiw.center/custom/api.php";
        return $endPointUrl;
    }
}

if (!function_exists('taptapEndPointUrl')) {
    function taptapEndPointUrl()
    {
        $endPointUrl =  "https://gate.taptap-presensi.com/v1";
        return $endPointUrl;
    }
}

if (!function_exists('taptapIntegrationDataDesired')) {
    function taptapIntegrationDataDesired($desiredValue)
    {
        $data = DB::selectOne("SELECT taptap_secret_key, taptap_client_id FROM companies WHERE id = 1");
        $response = $data->$desiredValue;
        return $response;
    }
}
#### TAP TAP INTEGRATION ####

if (!function_exists('hasPermission')) {
    function hasPermission($permission)
    {
        $user = Auth::user();
        if ($user) {
            $hasPermission = DB::selectOne("SELECT B.role_id FROM users A
            INNER JOIN mapping_roles_permissions B ON B.role_id = A.role_id
            INNER JOIN permissions C ON B.permission_id = C.id AND C.permission_code = ?
            WHERE B.active = 1 AND A.id = ?", [$permission, $user->id]);
            return !is_null($hasPermission) ? true : ($user->role_id == -1);
        } else {
            return false;
        }
    }
}

if (!function_exists('isProduction')) {
    function isProduction()
    {
        return env("APP_ENV") == "production" || env("APP_ENV") == "staging";
    }
}

if (!function_exists('is_blank')) {

    function is_blank($array, $key)
    {
        return isset($array[$key]) ? (is_null($array[$key]) || $array[$key] === "") : true;
    }
}

if (!function_exists('toAlpha')) {

    function toAlpha($data)
    {
        $alphabet =   array(
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L',
            'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
        );
        return $alphabet[$data];
    }
}

if (!function_exists('arrayToString')) {

    function arrayToString($array)
    {
        $list = [];
        foreach ($array as $value) {
            if (is_array($value))
                $list[] = arrayToString($value);
            else
                $list[] = '"' . $value . '"';
        }
        return "[" . implode(", ", $list) . "]";
    }
}

if (!function_exists('get_string_between')) {

    function get_string_between($string, $start, $end)
    {
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0) return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }
}

if (!function_exists('custom_get_origin_field_upload')) {

    function custom_get_origin_field_upload($string)
    {
        $a = substr($string, 0, 4);

        $newString = preg_replace('/' . $a . '/', '', $string, 1);
        $b = substr($newString, 0, 1);
        if ($b == '_') {
            $newString = preg_replace('/[_]/', '', $newString, 1);
        }
        return $newString;
    }
}


if (!function_exists('getRoleName')) {
    function getRoleName($roleId)
    {
        $roles = Roles::find($roleId);
        return $roles->role_name;
    }
}


if (!function_exists('buildTreeSalaryComponentOnPeriodes')) {
    function buildTreeSalaryComponentOnPeriodes(array $elements, $parentId = null)
    {
        $branch = array();
        foreach ($elements as $element) {
            if ($element['salary_component_category_id'] == $parentId) {
                $children = buildTreeSalaryComponentOnPeriodes($elements, $element['id']);
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[] = $element;
            }
        }
        return $branch;
    }
}


if (!function_exists('multiArraySerach')) {
    /*
    USED ON FILE : 
    - ControlSheetList.php
    - PayrollPeriodeSalaryCalculationAll.php
    - PayrollPeriodeSalaryCalculationSingle.php
    */
    function multiArraySerach(array $array, $search = null, $desiredValue)
    {
        $result = array();
        // Iterate over each array element
        foreach ($array as $key => $value) {
            $value = (array) $value;
            // Iterate over each search condition
            foreach ($search as $k => $v) {
                // If the array element does not meet the search condition then continue to the next element
                if (!isset($value[$k]) || $value[$k] != $v) {
                    continue 2;
                }
            }
            // Add the array element's key to the result array
            $result[] = $value;
        }
        // Return the result array
        $desiredValue = (isset($result[0][$desiredValue])) ? $result[0][$desiredValue] : null;
        return $desiredValue;
    }
}

if (!function_exists('multiArraySearchReturnArray')) {
    /*
    USED ON FILE : 
    - ControlSheetList.php
    - PayrollPeriodeSalaryCalculationAll.php
    - PayrollPeriodeSalaryCalculationSingle.php
    */
    function multiArraySearchReturnArray(array $array, $search = null)
    {
        $result = array();
        // Iterate over each array element
        foreach ($array as $key => $value) {
            $value = (array) $value;
            // Iterate over each search condition
            foreach ($search as $k => $v) {
                // If the array element does not meet the search condition then continue to the next element
                if (!isset($value[$k]) || $value[$k] != $v) {
                    continue 2;
                }
            }
            // Add the array element's key to the result array
            $result[] = $value;
        }
        // Return the result array
        return $result;
    }
}

#########################################################################
######                FUNCTION CUSTOM SALARY MODULES                 ####
#########################################################################
if (!function_exists('calculatedSalaryValueOnPeriode')) {
    function calculatedSalaryValueOnPeriode($employeeId, $periodeMonth)
    {
        $salaryValue = 0;
        #1 
        $paramData = [];
        $paramData["periode_month"] = $periodeMonth;
        $paramData["employee_id"] = $employeeId;
        $sql = "WITH data_gaji_tunjangan AS 
        (
            SELECT B.periode_month, B.employee_id, SUM(A.salary_component_value) as value
                FROM salary_component_on_periodes A
                JOIN employee_on_periodes B ON A.employee_on_periode_id = B.id
                JOIN master_salary_components MSC ON A.salary_component_id = MSC.id
                JOIN employees EMP ON B.employee_id = EMP.id
                LEFT JOIN custom_salary_modules CSC ON MSC.custom_salary_module_id = CSC.id
                WHERE B.periode_month = :periode_month AND B.employee_id = :employee_id
                AND MSC.is_salary_cut = false AND A.active = 1 AND MSC.charge = 'employee'
                GROUP BY B.periode_month, B.employee_id
        ), data_potongan AS (
            SELECT B.periode_month, B.employee_id, SUM(A.salary_component_value) as value
                FROM salary_component_on_periodes A
                JOIN employee_on_periodes B ON A.employee_on_periode_id = B.id
                JOIN master_salary_components MSC ON A.salary_component_id = MSC.id
                JOIN employees EMP ON B.employee_id = EMP.id
                LEFT JOIN custom_salary_modules CSC ON MSC.custom_salary_module_id = CSC.id
                WHERE B.periode_month = :periode_month AND B.employee_id = :employee_id
                AND MSC.is_salary_cut = true AND A.active = 1 AND MSC.charge = 'employee'
                GROUP BY B.periode_month, B.employee_id
        )
        SELECT A.periode_month, A.employee_id, (COALESCE(DGT.value,0) - COALESCE(DP.value,0)) as salary_value FROM employee_on_periodes A 
        LEFT JOIN data_gaji_tunjangan DGT ON A.employee_id = DGT.employee_id AND A.periode_month = DGT.periode_month
        LEFT JOIN data_potongan DP ON A.employee_id = DP.employee_id AND A.periode_month = DP.periode_month
        WHERE A.periode_month = :periode_month AND A.employee_id = :employee_id ";
        $salaryData = DB::select($sql, $paramData);
        if (!empty($salaryData)) {
            $salaryValue = $salaryData[0]->salary_value;
        }
        return $salaryValue;
    }
}

if (!function_exists('customSalaryModuleSimpananKoperasi')) {
    function customSalaryModuleSimpananKoperasi($periodeMonth, $employeeId, $salaryComponentId)
    {
        $salaryComponentValue = 0;

        #1 CARI DATA DI PERIODE TERSEBUT
        $paramData = [];
        $paramData["periode_month"] = $periodeMonth;
        $paramData["employee_id"] = $employeeId;
        $paramData["salary_component_id"] = $salaryComponentId;
        $sql = "SELECT A.comp_value as salary_component_value FROM koperasi_simpanan_details A
        JOIN koperasi_simpan_pinjam_details B ON A.koperasi_simpan_pinjam_detail_id = B.id
        JOIN koperasi_simpan_pinjam C ON B.koperasi_simpan_pinjam_id = C.id
        JOIN master_comp_simpanan_koperasi MCSK ON A.comp_simpanan_koperasi_id = MCSK.id
        WHERE C.periode_month = :periode_month AND B.employee_id = :employee_id
        AND MCSK.salary_component_id = :salary_component_id LIMIT 1";
        $salData = DB::select($sql, $paramData);
        if (!empty($salData)) {
            $salaryComponentValue = $salData[0]->salary_component_value;
        }
        return $salaryComponentValue;
    }
}

if (!function_exists('customSalaryModuleAngsuranPinjamanKoperasi')) {
    function customSalaryModuleAngsuranPinjamanKoperasi($periodeMonth, $employeeId, $salaryComponentId)
    {
        $salaryComponentValue = 0;

        #1 CARI DATA DI PERIODE TERSEBUT
        $paramData = [];
        $paramData["periode_month"] = $periodeMonth;
        $paramData["employee_id"] = $employeeId;
        $sql = "SELECT (A.pokok_pinjaman_value + A.bunga_pinjaman_value) as salary_component_value FROM koperasi_pinjaman_details A
        JOIN koperasi_simpan_pinjam_details B ON A.koperasi_simpan_pinjam_detail_id = B.id
        JOIN koperasi_simpan_pinjam C ON B.koperasi_simpan_pinjam_id = C.id
        WHERE C.periode_month = :periode_month AND B.employee_id = :employee_id LIMIT 1";
        $salData = DB::select($sql, $paramData);
        if (!empty($salData)) {
            $salaryComponentValue = $salData[0]->salary_component_value;
        }
        return $salaryComponentValue;
    }
}

if (!function_exists('customSalaryModuleTunjanganBpjsKes')) {
    function customSalaryModuleTunjanganBpjsKes($periodeMonth, $employeeId, $salaryComponentId)
    {
        $salaryComponentValue = 0;

        #1 CARI DATA DI PERIODE TERSEBUT
        $paramData = [];
        $paramData["periode_month"] = $periodeMonth;
        $paramData["employee_id"] = $employeeId;
        $sql = "SELECT A.iuran_company_value as salary_component_value 
        FROM bpjs_kes_bill_details A
        JOIN bpjs_kes_bills B ON A.bpjs_kes_bill_id = B.id
        WHERE B.periode_month = :periode_month AND A.employee_id = :employee_id LIMIT 1";
        $salData = DB::select($sql, $paramData);
        if (!empty($salData)) {
            $salaryComponentValue = $salData[0]->salary_component_value;
        }
        return $salaryComponentValue;
    }
}

if (!function_exists('customSalaryModulePotonganBpjsKes')) {
    function customSalaryModulePotonganBpjsKes($periodeMonth, $employeeId, $salaryComponentId)
    {
        $salaryComponentValue = 0;

        #1 CARI DATA DI PERIODE TERSEBUT
        $paramData = [];
        $paramData["periode_month"] = $periodeMonth;
        $paramData["employee_id"] = $employeeId;
        $sql = "SELECT A.iuran_employee_value as salary_component_value 
        FROM bpjs_kes_bill_details A
        JOIN bpjs_kes_bills B ON A.bpjs_kes_bill_id = B.id
        WHERE B.periode_month = :periode_month AND A.employee_id = :employee_id LIMIT 1";
        $salData = DB::select($sql, $paramData);
        if (!empty($salData)) {
            $salaryComponentValue = $salData[0]->salary_component_value;
        }
        return $salaryComponentValue;
    }
}

if (!function_exists('customSalaryModulePotonganBpjsKesTambahan')) {
    function customSalaryModulePotonganBpjsKesTambahan($periodeMonth, $employeeId, $salaryComponentId)
    {
        $salaryComponentValue = 0;

        #1 CARI DATA DI PERIODE TERSEBUT
        $paramData = [];
        $paramData["periode_month"] = $periodeMonth;
        $paramData["employee_id"] = $employeeId;
        $sql = "SELECT A.iuran_tambahan_value as salary_component_value 
        FROM bpjs_kes_bill_details A
        JOIN bpjs_kes_bills B ON A.bpjs_kes_bill_id = B.id
        WHERE B.periode_month = :periode_month AND A.employee_id = :employee_id LIMIT 1";
        $salData = DB::select($sql, $paramData);
        if (!empty($salData)) {
            $salaryComponentValue = $salData[0]->salary_component_value;
        }
        return $salaryComponentValue;
    }
}

if (!function_exists('customSalaryModuleTunjanganBpjsTk')) {
    function customSalaryModuleTunjanganBpjsTk($periodeMonth, $employeeId, $salaryComponentId)
    {
        $salaryComponentValue = 0;

        #1 CARI DATA DI PERIODE TERSEBUT
        $paramData = [];
        $paramData["periode_month"] = $periodeMonth;
        $paramData["employee_id"] = $employeeId;
        $sql = "SELECT A.iuran_bpjs_company_value as salary_component_value 
        FROM bpjs_tk_bill_details A
        JOIN bpjs_tk_bills B ON A.bpjs_tk_bill_id = B.id
        WHERE B.periode_month = :periode_month AND A.employee_id = :employee_id LIMIT 1";
        $salData = DB::select($sql, $paramData);
        if (!empty($salData)) {
            $salaryComponentValue = $salData[0]->salary_component_value;
        }
        return $salaryComponentValue;
    }
}

if (!function_exists('customSalaryModulePotonganBpjsTk')) {
    function customSalaryModulePotonganBpjsTk($periodeMonth, $employeeId, $salaryComponentId)
    {
        $salaryComponentValue = 0;

        #1 CARI DATA DI PERIODE TERSEBUT
        $paramData = [];
        $paramData["periode_month"] = $periodeMonth;
        $paramData["employee_id"] = $employeeId;
        $sql = "SELECT A.iuran_bpjs_employee_value as salary_component_value 
        FROM bpjs_tk_bill_details A
        JOIN bpjs_tk_bills B ON A.bpjs_tk_bill_id = B.id
        WHERE B.periode_month = :periode_month AND A.employee_id = :employee_id LIMIT 1";
        $salData = DB::select($sql, $paramData);
        if (!empty($salData)) {
            $salaryComponentValue = $salData[0]->salary_component_value;
        }
        return $salaryComponentValue;
    }
}


if (!function_exists('customSalaryModuleTunjanganTransports')) {
    function customSalaryModuleTunjanganTransports($periodeMonth, $employeeId, $salaryComponentId)
    {
        $salaryComponentValue = 0;

        #1 CARI DATA DI PERIODE TERSEBUT
        $paramData = [];
        $paramData["periode_month"] = $periodeMonth;
        $paramData["employee_id"] = $employeeId;
        $sql = "SELECT A.penerimaan_value as salary_component_value 
        FROM tunjangan_transport_details A
        JOIN tunjangan_transports B ON A.tunjangan_transport_id = B.id
        WHERE B.periode_month = :periode_month AND A.employee_id = :employee_id LIMIT 1";
        $salData = DB::selectOne($sql, $paramData);
        if (!empty($salData)) {
            $salaryComponentValue = $salData->salary_component_value;
        }
        return $salaryComponentValue;
    }
}

if (!function_exists('customSalaryModuleTunjanganMakan')) {
    function customSalaryModuleTunjanganMakan($periodeMonth, $employeeId, $salaryComponentId)
    {
        $salaryComponentValue = 0;

        #1 CARI DATA DI PERIODE TERSEBUT
        $paramData = [];
        $paramData["periode_month"] = $periodeMonth;
        $paramData["employee_id"] = $employeeId;
        $sql = "SELECT A.penerimaan_value as salary_component_value 
        FROM tunjangan_makan_details A
        JOIN tunjangan_makan B ON A.tunjangan_makan_id = B.id
        WHERE B.periode_month = :periode_month AND A.employee_id = :employee_id LIMIT 1";
        $salData = DB::selectOne($sql, $paramData);
        if (!empty($salData)) {
            $salaryComponentValue = $salData->salary_component_value;
        }
        return $salaryComponentValue;
    }
}

if (!function_exists('customSalaryModuleTunjanganBeasiswa')) {
    function customSalaryModuleTunjanganBeasiswa($periodeMonth, $employeeId, $salaryComponentId)
    {
        $salaryComponentValue = 0;

        #1 CARI DATA DI PERIODE TERSEBUT
        $paramData = [];
        $paramData["periode_month"] = $periodeMonth;
        $paramData["employee_id"] = $employeeId;
        $sql = "SELECT A.penerimaan_value as salary_component_value 
        FROM tunjangan_beasiswa_details A
        JOIN tunjangan_beasiswa B ON A.tunjangan_beasiswa_id = B.id
        WHERE B.periode_month = :periode_month AND A.employee_id = :employee_id LIMIT 1";
        $salData = DB::selectOne($sql, $paramData);
        if (!empty($salData)) {
            $salaryComponentValue = $salData->salary_component_value;
        }
        return $salaryComponentValue;
    }
}

if (!function_exists('customSalaryModuleTunjanganLembur')) {
    function customSalaryModuleTunjanganLembur($periodeMonth, $employeeId, $salaryComponentId)
    {
        $salaryComponentValue = 0;

        #1 CARI DATA DI PERIODE TERSEBUT
        $paramData = [];
        $paramData["periode_month"] = $periodeMonth;
        $paramData["employee_id"] = $employeeId;
        $sql = "SELECT A.pembulatan_penerimaan_value as salary_component_value 
        FROM tunjangan_lembur_details A
        JOIN tunjangan_lembur B ON A.tunjangan_lembur_id = B.id
        WHERE B.periode_month = :periode_month AND A.employee_id = :employee_id LIMIT 1";
        $salData = DB::selectOne($sql, $paramData);
        if (!empty($salData)) {
            $salaryComponentValue = $salData->salary_component_value;
        }
        return $salaryComponentValue;
    }
}

if (!function_exists('customSalaryModulePotonganPinjamanBank')) {
    function customSalaryModulePotonganPinjamanBank($periodeMonth, $employeeId, $salaryComponentId)
    {
        $salaryComponentValue = 0;

        #1 CARI DATA DI PERIODE TERSEBUT
        $paramData = [];
        $paramData["periode_month"] = $periodeMonth;
        $paramData["employee_id"] = $employeeId;
        $paramData["salary_component_id"] = $salaryComponentId;
        $sql = "SELECT A.comp_value as salary_component_value FROM pinjaman_bank_detail_components A
        JOIN pinjaman_bank_details B ON A.pinjaman_bank_detail_id = B.id
        JOIN pinjaman_bank C ON B.pinjaman_bank_id = C.id
        JOIN master_comp_pinjaman_bank MCSK ON A.comp_pinjaman_bank_id = MCSK.id
        WHERE C.periode_month = :periode_month AND B.employee_id = :employee_id
        AND MCSK.salary_component_id = :salary_component_id LIMIT 1";
        $salData = DB::selectOne($sql, $paramData);
        if (!empty($salData)) {
            $salaryComponentValue = $salData->salary_component_value;
        }
        return $salaryComponentValue;
    }
}

if (!function_exists('customSalaryModulePotonganSanksi')) {
    function customSalaryModulePotonganSanksi($periodeMonth, $employeeId, $salaryComponentId)
    {
        $salaryComponentValue = 0;

        #1 CARI DATA DI PERIODE TERSEBUT
        $paramData = [];
        $paramData["periode_month"] = $periodeMonth;
        $paramData["employee_id"] = $employeeId;
        $paramData["salary_component_id"] = $salaryComponentId;
        $sql = "SELECT A.component_value as salary_component_value FROM potongan_sanksi_components A
        JOIN potongan_sanksi_details B ON A.potongan_sanksi_detail_id = B.id
        JOIN potongan_sanksi C ON B.potongan_sanksi_id = C.id
        JOIN employee_sanksi EMSANK ON A.employee_sanksi_id = EMSANK.id
        JOIN master_sanksi_types MST ON EMSANK.sanksi_type_id = MST.id
        WHERE C.periode_month = :periode_month AND B.employee_id = :employee_id
        AND MST.salary_component_id = :salary_component_id LIMIT 1";
        $salData = DB::selectOne($sql, $paramData);
        if (!empty($salData)) {
            $salaryComponentValue = $salData->salary_component_value;
        }
        return $salaryComponentValue;
    }
}


if (!function_exists('customSalaryModuleTunjanganKehadiran')) {
    function customSalaryModuleTunjanganKehadiran($periodeMonth, $employeeId, $salaryComponentId)
    {
        $salaryComponentValue = 0;

        #1 CARI DATA DI PERIODE TERSEBUT
        $paramData = [];
        $paramData["periode_month"] = $periodeMonth;
        $paramData["employee_id"] = $employeeId;
        $sql = "SELECT A.penerimaan_value as salary_component_value 
        FROM tunjangan_kehadiran_details A
        JOIN tunjangan_kehadiran B ON A.tunjangan_kehadiran_id = B.id
        WHERE B.periode_month = :periode_month AND A.employee_id = :employee_id LIMIT 1";
        $salData = DB::selectOne($sql, $paramData);
        if (!empty($salData)) {
            $salaryComponentValue = $salData->salary_component_value;
        }
        return $salaryComponentValue;
    }
}
