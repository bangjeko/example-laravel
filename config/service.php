<?php

return [
    [
        "type" => "GET",
        "end_point" => "/get",
        "class" => "App\Services\Crud\Get"
    ],
    [
        "type" => "GET",
        "end_point" => "/dataset",
        "class" => "App\Services\Crud\Dataset"
    ],
    [
        "type" => "POST",
        "end_point" => "/create",
        "class" => "App\Services\Crud\Add"
    ],
    [
        "type" => "POST",
        "end_point" => "/update",
        "class" => "App\Services\Crud\Edit"
    ],
    [
        "type" => "POST",
        "end_point" => "/delete",
        "class" => "App\Services\Crud\Delete"
    ],
    [
        "type" => "GET",
        "end_point" => "/show",
        "class" => "App\Services\Crud\Find"
    ],
    [
        "type" => "GET",
        "end_point" => "/sample",
        "class" => "App\Services\Sample\SampleService"
    ],
    [
        "type" => "GET",
        "end_point" => "/sample",
        "class" => "App\Services\Sample\SampleService"
    ],
    [
        "type" => "GET",
        "end_point" => "/me",
        "class" => "App\Services\Auth\Me"
    ],
    [
        "type" => "GET",
        "end_point" => "/logout",
        "class" => "App\Services\Auth\DoLogout"
    ],
    [
        "type" => "POST",
        "end_point" => "/login",
        "class" => "App\Services\Auth\DoLogin"
    ],
    // NO ATUH
    [
        "type" => "GET",
        "end_point" => "/no-auth/roles/dataset",
        "class" => "App\Services\NoAuth\RolesDataset"
    ],
    [
        "type" => "GET",
        "end_point" => "/no-auth/latest-mobile-app-versions",
        "class" => "App\Services\NoAuth\LatestMobileAppVersions"
    ],
    // [
    //     "type" => "GET",
    //     "end_point" => "/users/show",
    //     "class" => "App\Services\User\FindUserById"
    // ],
    [
        "type" => "GET",
        "end_point" => "/custom/users/findusername",
        "class" => "App\Services\User\FindUserByUsername"
    ],
    [
        "type" => "GET",
        "end_point" => "/users/list",
        "class" => "App\Services\User\GetUser"
    ],
    [
        "type" => "POST",
        "end_point" => "/users/create",
        "class" => "App\Services\User\AddUser"
    ],
    [
        "type" => "PUT",
        "end_point" => "/users/update",
        "class" => "App\Services\User\EditUser"
    ],
    [
        "type" => "POST",
        "end_point" => "/users/active-deactive",
        "class" => "App\Services\User\ActiveDeactiveUser"
    ],

    [
        "type" => "GET",
        "end_point" => "/permission",
        "class" => "App\Services\User\ViewPermission"
    ],
    [
        "type" => "POST",
        "end_point" => "/permission/save",
        "class" => "App\Services\User\SavePermission"
    ],
    [
        "type" => "GET",
        "end_point" => "/roles",
        "class" => "App\Services\User\GetRole"
    ],
    [
        "type" => "POST",
        "end_point" => "/roles/create",
        "class" => "App\Services\User\AddRole"
    ],
    [
        "type" => "PUT",
        "end_point" => "/roles/update",
        "class" => "App\Services\User\EditRole"
    ],
    [
        "type" => "DELETE",
        "end_point" => "/roles/delete",
        "class" => "App\Services\User\DeleteRole"
    ],
    [
        "type" => "GET",
        "end_point" => "/role/find",
        "class" => "App\Services\User\FindRoleById"
    ],


    /*
    |--------------------------------------------------------------------------
    | CUSTOM API
    |--------------------------------------------------------------------------
    |
    | This option controls the default hash driver that will be used to hash
    | passwords for your application. By default, the bcrypt algorithm is
    | used; however, you remain free to modify this option if you wish.
    |
    | Supported: "bcrypt", "argon", "argon2id"
    

    /*
    | MAPPING ROLES PERMISSION
    */
    [
        "type" => "GET",
        "end_point" => "/custom/mapping-roles-permissions/list",
        "class" => "App\Services\Custom\MappingRolesPermissions\MappingRolesPermissionsList"
    ],
    [
        "type" => "PUT",
        "end_point" => "/custom/mapping-roles-permissions/update",
        "class" => "App\Services\Custom\MappingRolesPermissions\MappingRolesPermissionsUpdate"
    ],

    /*
    | SSO OAUTH
    */
    [
        "type" => "POST",
        "end_point" => "/oauth/verify-oauth-sso-token",
        "class" => "App\Services\SsoOauth\VerifySsoOauthToken"
    ],
    /*
    | SSO USER ADD
    */
    [
        "type" => "GET",
        "end_point" => "/sso/user/dataset",
        "class" => "App\Services\Sso\SsoUserDataset"
    ],
    [
        "type" => "POST",
        "end_point" => "/sso/user/create",
        "class" => "App\Services\Sso\SsoUserAdd"
    ],




    /*
    | MASTER STATUS EMPLOYMENTS
    */
    [
        "type" => "GET",
        "end_point" => "/custom/master-status-employments/dataset",
        "class" => "App\Services\Custom\MasterStatusEmployments\MasterStatusEmploymentsDataset"
    ],
    [
        "type" => "GET",
        "end_point" => "/custom/master-status-employments/{id}/show",
        "class" => "App\Services\Custom\MasterStatusEmployments\MasterStatusEmploymentsShow"
    ],
    /*
    | EMPLOYEES
    */
    [
        "type" => "GET",
        "end_point" => "/custom/employees-no-user/dataset",
        "class" => "App\Services\Custom\Employees\EmployeeNoUser"
    ],
    [
        "type" => "GET",
        "end_point" => "/custom/employee-print-cv/{id}/show",
        "class" => "App\Services\Custom\Employees\EmployeePrintCv"
    ],
    
    /*
    | KOPERASI
    */
    [
        "type" => "POST",
        "end_point" => "/custom/koperasi-simpan-pinjam/create",
        "class" => "App\Services\Custom\KoperasiSimpanPinjam\KoperasiSimpanPinjamAdd"
    ],
    [
        "type" => "GET",
        "end_point" => "/custom/koperasi-simpan-pinjam-details/show-month-before",
        "class" => "App\Services\Custom\KoperasiSimpanPinjamDetails\KoperasiSimpanPinjamDetailsShowMonthBefore"
    ],
    [
        "type" => "GET",
        "end_point" => "/custom/koperasi-simpan-pinjam-details/show-with-detail",
        "class" => "App\Services\Custom\KoperasiSimpanPinjamDetails\KoperasiSimpanPinjamDetailsShow"
    ],
    [
        "type" => "POST",
        "end_point" => "/custom/koperasi-simpan-pinjam-details/create-with-detail",
        "class" => "App\Services\Custom\KoperasiSimpanPinjamDetails\KoperasiSimpanPinjamDetailsAdd"
    ],

    /*
    | EVENTS
    */
    [
        "type" => "GET",
        "end_point" => "/custom/events/list",
        "class" => "App\Services\Custom\Events\EventsList"
    ],

    /*
    | ANOUNCEMENTS
    */
    [
        "type" => "GET",
        "end_point" => "/custom/announcements/list",
        "class" => "App\Services\Custom\Announcements\AnnouncementsList"
    ],


    /*
    | TAP TAP INTEGRATIONS
    */
    [
        "type" => "GET",
        "end_point" => "/taptap/hrm/employees-hrm/list",
        "class" => "App\Services\TapTapIntegration\HrmEmployeesList"
    ],
    [
        "type" => "GET",
        "end_point" => "/taptap/external/employees/dataset",
        "class" => "App\Services\TapTapIntegration\TapTapEmployeesDataset"
    ],
    [
        "type" => "GET",
        "end_point" => "/taptap/external/employees/{id}/show",
        "class" => "App\Services\TapTapIntegration\TapTapEmployeesShow"
    ],

    /*
    | FINA INTEGRATIONS
    */
    [
        "type" => "GET",
        "end_point" => "/custom/log-sync-fina-salary-accounts/last-sync-data",
        "class" => "App\Services\FinaIntegration\LogSyncFina\LogSyncFinaSalayAccountsLastSync"
    ],
    [
        "type" => "GET",
        "end_point" => "/custom/log-sync-fina-operational-accounts/last-sync-data",
        "class" => "App\Services\FinaIntegration\LogSyncFina\LogSyncFinaOperationalAccountsLastSync"
    ],
    [
        "type" => "POST",
        "end_point" => "/custom/fina-salary-accounts/sync-data",
        "class" => "App\Services\FinaIntegration\SyncData\FInaSalaryAccountsSyncData"
    ],
    [
        "type" => "POST",
        "end_point" => "/custom/fina-operational-accounts/sync-data",
        "class" => "App\Services\FinaIntegration\SyncData\FInaSalaryAccountsSyncData"
    ],
    /*
    | PAYROLLS MAPPING SALARY ACCOUNTS
    */
    [
        "type" => "GET",
        "end_point" => "/custom/mapping-salary-accounts/list-job-position-categories",
        "class" => "App\Services\Custom\MappingSalaryAccounts\JobPositionCategoriesList"
    ],
    [
        "type" => "GET",
        "end_point" => "/custom/mapping-salary-accounts/show-job-position-categories",
        "class" => "App\Services\Custom\MappingSalaryAccounts\JobPositionCategoriesShow"
    ],
    [
        "type" => "GET",
        "end_point" => "/custom/mapping-salary-accounts/list",
        "class" => "App\Services\Custom\MappingSalaryAccounts\MappingSalaryAccountsList"
    ],
    [
        "type" => "GET",
        "end_point" => "/custom/mapping-salary-accounts/show",
        "class" => "App\Services\Custom\MappingSalaryAccounts\MappingSalaryAccountsShow"
    ],

    [
        "type" => "PUT",
        "end_point" => "/custom/mapping-salary-accounts/update",
        "class" => "App\Services\Custom\MappingSalaryAccounts\MappingSalaryAccountsEdit"
    ],
    [
        "type" => "GET",
        "end_point" => "/custom/mapping-salary-account-proportions/list",
        "class" => "App\Services\Custom\MappingSalaryAccounts\MappingSalaryAccountProportionsList"
    ],
    [
        "type" => "PUT",
        "end_point" => "/custom/mapping-salary-account-proportions/update",
        "class" => "App\Services\Custom\MappingSalaryAccounts\MappingSalaryAccountProportionsEdit"
    ],

    /*
    | PAYROLLS MAPPING TEMPLATE SALARY EMPLOYEES
    */
    [
        "type" => "GET",
        "end_point" => "/custom/mapping-template-salary-employees/list-employees",
        "class" => "App\Services\Custom\MappingTemplateSalaryEmployees\EmployeesList"
    ],

    [
        "type" => "GET",
        "end_point" => "/custom/mapping-template-salary-employees/show-employees",
        "class" => "App\Services\Custom\MappingTemplateSalaryEmployees\EmployeeShow"
    ],
    [
        "type" => "GET",
        "end_point" => "/custom/mapping-template-salary-employees/list",
        "class" => "App\Services\Custom\MappingTemplateSalaryEmployees\MappingTemplateSalaryEmployeesList"
    ],
    [
        "type" => "GET",
        "end_point" => "/custom/mapping-template-salary-employees/show",
        "class" => "App\Services\Custom\MappingTemplateSalaryEmployees\MappingTemplateSalaryEmployeesShow"
    ],
    [
        "type" => "PUT",
        "end_point" => "/custom/mapping-template-salary-employees/update",
        "class" => "App\Services\Custom\MappingTemplateSalaryEmployees\MappingTemplateSalaryEmployeesEdit"
    ],

    [
        "type" => "GET",
        "end_point" => "/custom/mapping-template-salary-employees-table/list",
        "class" => "App\Services\Custom\MappingTemplateSalaryEmployees\MappingTemplateSalaryEmployeeTableList"
    ],
    [
        "type" => "PUT",
        "end_point" => "/custom/mapping-template-salary-employees-table/update",
        "class" => "App\Services\Custom\MappingTemplateSalaryEmployees\MappingTemplateSalaryEmployeeTableEdit"
    ],



    /*
    | BPJS MAPPING UPAH BPJS
    */
    [
        "type" => "GET",
        "end_point" => "/custom/mapping-upah-bpjs/list-job-position-categories",
        "class" => "App\Services\Custom\MappingUpahBPJS\JobPositionCategoriesList"
    ],
    [
        "type" => "GET",
        "end_point" => "/custom/mapping-upah-bpjs/show-job-position-categories",
        "class" => "App\Services\Custom\MappingUpahBPJS\JobPositionCategoriesShow"
    ],
    [
        "type" => "GET",
        "end_point" => "/custom/mapping-upah-bpjs/list",
        "class" => "App\Services\Custom\MappingUpahBPJS\MappingUpahBpjsList"
    ],
    [
        "type" => "GET",
        "end_point" => "/custom/mapping-upah-bpjs/show",
        "class" => "App\Services\Custom\MappingUpahBPJS\MappingUpahBpjsShow"
    ],
    [
        "type" => "PUT",
        "end_point" => "/custom/mapping-upah-bpjs/update",
        "class" => "App\Services\Custom\MappingUpahBPJS\MappingUpahBpjsEdit"
    ],

    /*
    | BPJS KES TAMBAHAN
    */
    [
        "type" => "GET",
        "end_point" => "/custom/bpjs-kes-tambahan-employee-families/list",
        "class" => "App\Services\Custom\BpjsKesTambahanFamilies\BpjsKesTambahanFamiliesList"
    ],
    [
        "type" => "PUT",
        "end_point" => "/custom/bpjs-kes-tambahan-employee-families/update",
        "class" => "App\Services\Custom\BpjsKesTambahanFamilies\BpjsKesTambahanFamiliesEdit"
    ],


    /*
    | BPJS KES BILLS
    */
    [
        "type" => "GET",
        "end_point" => "/custom/bpjs-kes-bills/show",
        "class" => "App\Services\Custom\BpjsKesBills\BpjsKesBillsShow"
    ],

    [
        "type" => "POST",
        "end_point" => "/custom/bpjs-kes-bills/create",
        "class" => "App\Services\Custom\BpjsKesBills\BpjsKesBillsAdd"
    ],

    /*
    | BPJS KES BILL DETAILS
    */

    [
        "type" => "GET",
        "end_point" => "/custom/bpjs-kes-bill-details/list",
        "class" => "App\Services\Custom\BpjsKesBillDetails\BpjsKesBillDetailsList"
    ],
    [
        "type" => "GET",
        "end_point" => "/custom/bpjs-kes-bill-details/show",
        "class" => "App\Services\Custom\BpjsKesBillDetails\BpjsKesBillDetailsShow"
    ],
    [
        "type" => "POST",
        "end_point" => "/custom/bpjs-kes-bill-details/create",
        "class" => "App\Services\Custom\BpjsKesBillDetails\BpjsKesBIllDetailAdd"
    ],
    [
        "type" => "POST",
        "end_point" => "/custom/bpjs-kes-bill-details/calculate",
        "class" => "App\Services\Custom\BpjsKesBillDetails\BpjsKesBillDetailsCalculate"
    ],


    /*
    | BPJS TK BILLS
    */
    [
        "type" => "GET",
        "end_point" => "/custom/bpjs-tk-bills/show",
        "class" => "App\Services\Custom\BpjsTkBills\BpjsTkBillsShow"
    ],

    [
        "type" => "POST",
        "end_point" => "/custom/bpjs-tk-bills/create",
        "class" => "App\Services\Custom\BpjsTkBills\BpjsTkBillsAdd"
    ],

    /*
    | BPJS TK BILL DETAILS
    */

    [
        "type" => "GET",
        "end_point" => "/custom/bpjs-tk-bill-details/list",
        "class" => "App\Services\Custom\BpjsTkBillDetails\BpjsTkBillDetailsList"
    ],
    [
        "type" => "GET",
        "end_point" => "/custom/bpjs-tk-bill-details/show",
        "class" => "App\Services\Custom\BpjsTkBillDetails\BpjsTkBillDetailsShow"
    ],
    [
        "type" => "POST",
        "end_point" => "/custom/bpjs-tk-bill-details/create",
        "class" => "App\Services\Custom\BpjsTkBillDetails\BpjsTkBillDetailsAdd"
    ],
    [
        "type" => "POST",
        "end_point" => "/custom/bpjs-tk-bill-details/calculate",
        "class" => "App\Services\Custom\BpjsTkBillDetails\BpjsTkBillDetailsCalculate"
    ],

    /*
    | TUNJANGAN TRANSPORTS
    */

    [
        "type" => "GET",
        "end_point" => "/custom/tunjangan-transports/show",
        "class" => "App\Services\Custom\TunjanganTransports\TunjanganTransportsShow"
    ],

    [
        "type" => "POST",
        "end_point" => "/custom/tunjangan-transports/create",
        "class" => "App\Services\Custom\TunjanganTransports\TunjanganTransportsAdd"
    ],

    /*
    | TUNJANGAN TRANSPORTS DETAILS
    */
    [
        "type" => "GET",
        "end_point" => "/custom/tunjangan-transport-details/list",
        "class" => "App\Services\Custom\TunjanganTransportDetails\TunjanganTransportDetailsList"
    ],
    [
        "type" => "GET",
        "end_point" => "/custom/tunjangan-transport-details/show",
        "class" => "App\Services\Custom\TunjanganTransportDetails\TunjanganTransportDetailsShow"
    ],
    [
        "type" => "POST",
        "end_point" => "/custom/tunjangan-transport-details/create",
        "class" => "App\Services\Custom\TunjanganTransportDetails\TunjanganTransportDetailsAdd"
    ],
    [
        "type" => "POST",
        "end_point" => "/custom/tunjangan-transport-details/calculate",
        "class" => "App\Services\Custom\TunjanganTransportDetails\TunjanganTransportDetailsCalculate"
    ],

    /*
    | TUNJANGAN MAKAN
    */

    [
        "type" => "GET",
        "end_point" => "/custom/tunjangan-makan/show",
        "class" => "App\Services\Custom\TunjanganMakan\TunjanganMakanShow"
    ],

    [
        "type" => "POST",
        "end_point" => "/custom/tunjangan-makan/create",
        "class" => "App\Services\Custom\TunjanganMakan\TunjanganMakanAdd"
    ],


    /*
    | TUNJANGAN MAKAN DETAILS
    */
    [
        "type" => "GET",
        "end_point" => "/custom/tunjangan-makan-details/list",
        "class" => "App\Services\Custom\TunjanganMakanDetails\TunjanganMakanDetailsList"
    ],
    [
        "type" => "GET",
        "end_point" => "/custom/tunjangan-makan-details/show",
        "class" => "App\Services\Custom\TunjanganMakanDetails\TunjanganMakanDetailsShow"
    ],
    [
        "type" => "POST",
        "end_point" => "/custom/tunjangan-makan-details/create",
        "class" => "App\Services\Custom\TunjanganMakanDetails\TunjanganMakanDetailsAdd"
    ],
    [
        "type" => "POST",
        "end_point" => "/custom/tunjangan-makan-details/calculate",
        "class" => "App\Services\Custom\TunjanganMakanDetails\TunjanganMakanDetailsCalculate"
    ],

    /*
    | TUNJANGAN BEASISWA
    */

    [
        "type" => "GET",
        "end_point" => "/custom/tunjangan-beasiswa/show",
        "class" => "App\Services\Custom\TunjanganBeasiswa\TunjanganBeasiswaShow"
    ],

    [
        "type" => "POST",
        "end_point" => "/custom/tunjangan-beasiswa/create",
        "class" => "App\Services\Custom\TunjanganBeasiswa\TunjanganBeasiswaAdd"
    ],


    /*
    | TUNJANGAN BEASISWA DETAILS
    */
    [
        "type" => "GET",
        "end_point" => "/custom/tunjangan-beasiswa-details/list",
        "class" => "App\Services\Custom\TunjanganBeasiswaDetails\TunjanganBeasiswaDetailsList"
    ],
    [
        "type" => "GET",
        "end_point" => "/custom/tunjangan-beasiswa-details/show",
        "class" => "App\Services\Custom\TunjanganBeasiswaDetails\TunjanganBeasiswaDetailsShow"
    ],
    [
        "type" => "POST",
        "end_point" => "/custom/tunjangan-beasiswa-details/create",
        "class" => "App\Services\Custom\TunjanganBeasiswaDetails\TunjanganBeasiswaDetailsAdd"
    ],
    [
        "type" => "POST",
        "end_point" => "/custom/tunjangan-beasiswa-details/calculate",
        "class" => "App\Services\Custom\TunjanganBeasiswaDetails\TunjanganBeasiswaDetailsCalculate"
    ],


    /*
    | TUNJANGAN KEHADIRAN
    */

    [
        "type" => "GET",
        "end_point" => "/custom/tunjangan-kehadiran/show",
        "class" => "App\Services\Custom\TunjanganKehadiran\TunjanganKehadiranShow"
    ],

    [
        "type" => "POST",
        "end_point" => "/custom/tunjangan-kehadiran/create",
        "class" => "App\Services\Custom\TunjanganKehadiran\TunjanganKehadiranAdd"
    ],

    /*
    | TUNJANGAN KEHADIRAN DETAILS
    */
    [
        "type" => "GET",
        "end_point" => "/custom/tunjangan-kehadiran-details/list",
        "class" => "App\Services\Custom\TunjanganKehadiranDetails\TunjanganKehadiranDetailsList"
    ],
    [
        "type" => "GET",
        "end_point" => "/custom/tunjangan-kehadiran-details/show",
        "class" => "App\Services\Custom\TunjanganKehadiranDetails\TunjanganKehadiranDetailsShow"
    ],
    [
        "type" => "POST",
        "end_point" => "/custom/tunjangan-kehadiran-details/create",
        "class" => "App\Services\Custom\TunjanganKehadiranDetails\TunjanganKehadiranDetailsAdd"
    ],
    [
        "type" => "POST",
        "end_point" => "/custom/tunjangan-kehadiran-details/calculate",
        "class" => "App\Services\Custom\TunjanganKehadiranDetails\TunjanganKehadiranDetailsCalculate"
    ],

    /*
    | PAYROLL PERIODES
    */
    [
        "type" => "POST",
        "end_point" => "/custom/payroll-periodes/create-with-employee-on-periodes",
        "class" => "App\Services\Custom\PayrollPeriodes\PayrollPeriodesAdd"
    ],


    /*
    | PRESENCE ON PERIODES
    */

    [
        "type" => "GET",
        "end_point" => "/custom/presence-on-periodes/list",
        "class" => "App\Services\Custom\PresenceOnPeriodes\PresenceOnPeriodesList"
    ],
    [
        "type" => "GET",
        "end_point" => "/custom/presence-on-periodes/show",
        "class" => "App\Services\Custom\PresenceOnPeriodes\PresenceOnPeriodesShow"
    ],
    [
        "type" => "POST",
        "end_point" => "/custom/presence-on-periodes/create",
        "class" => "App\Services\Custom\PresenceOnPeriodes\PresenceOnPeriodesAdd"
    ],

    /*
    | PRESENCE ON PERIODE RECAPS
    */
    [
        "type" => "GET",
        "end_point" => "/custom/presence-on-periode-recaps/list",
        "class" => "App\Services\Custom\PresenceOnPeriodeRecaps\PresenceOnPeriodeRecapsList"
    ],
    [
        "type" => "GET",
        "end_point" => "/custom/presence-on-periode-recaps/show",
        "class" => "App\Services\Custom\PresenceOnPeriodeRecaps\PresenceOnPeriodeRecapsShow"
    ],
    [
        "type" => "PUT",
        "end_point" => "/custom/presence-on-periode-recaps/update",
        "class" => "App\Services\Custom\PresenceOnPeriodeRecaps\PresenceOnPeriodeRecapsEdit"
    ],
    [
        "type" => "POST",
        "end_point" => "/custom/presence-on-periode-recaps/sync",
        "class" => "App\Services\Custom\PresenceOnPeriodeRecaps\PresenceOnPeriodeRecapsSync"
    ],

    /*
    | PAYROLL PERIODE CALCULATION
    */
    [
        "type" => "GET",
        "end_point" => "/custom/payroll-periode-calculation/checklist",
        "class" => "App\Services\Custom\PayrollPeriodeCalculation\PayrollPeriodeChecklist"
    ],
    [
        "type" => "GET",
        "end_point" => "/custom/payroll-periode-calculation/payroll-periode-show",
        "class" => "App\Services\Custom\PayrollPeriodeCalculation\PayrollPeriodeShow"
    ],
    [
        "type" => "GET",
        "end_point" => "/custom/payroll-periode-calculation/employee-on-periode-list",
        "class" => "App\Services\Custom\PayrollPeriodeCalculation\EmployeeOnPeriodeList"
    ],
    [
        "type" => "GET",
        "end_point" => "/custom/payroll-periode-calculation/employee-on-periode-show",
        "class" => "App\Services\Custom\PayrollPeriodeCalculation\EmployeeOnPeriodeShow"
    ],
    [
        "type" => "POST",
        "end_point" => "/custom/payroll-periode-calculation/salary-calculation-all",
        "class" => "App\Services\Custom\PayrollPeriodeCalculation\PayrollPeriodeSalaryCalculationAll"
    ],
    [
        "type" => "POST",
        "end_point" => "/custom/payroll-periode-calculation/salary-calculation-single",
        "class" => "App\Services\Custom\PayrollPeriodeCalculation\PayrollPeriodeSalaryCalculationSingle"
    ],

    [
        "type" => "GET",
        "end_point" => "/custom/payroll-periode-calculation/salary-component-on-periodes-list",
        "class" => "App\Services\Custom\PayrollPeriodeCalculation\PayrollPeriodeCalculationDetailList"
    ],
    [
        "type" => "GET",
        "end_point" => "/custom/payroll-periode-calculation/salary-component-on-periodes-show",
        "class" => "App\Services\Custom\PayrollPeriodeCalculation\PayrollPeriodeCalculationDetailShow"
    ],

    [
        "type" => "GET",
        "end_point" => "/custom/payroll-periode-calculation/salary-component-on-periodes-list-tree",
        "class" => "App\Services\Custom\PayrollPeriodeCalculation\PayrollPeriodeCalculationDetailListTree"
    ],
    [
        "type" => "PUT",
        "end_point" => "/custom/payroll-periode-calculation/salary-component-on-periodes-account-update",
        "class" => "App\Services\Custom\PayrollPeriodeCalculation\SalaryComponentOnPeriodeAccountEdit"
    ],

    /*
    | LEMBUR ON PERIODES
    */

    [
        "type" => "GET",
        "end_point" => "/custom/lembur-on-periodes/list",
        "class" => "App\Services\Custom\LemburOnPeriodes\LemburOnPeriodesList"
    ],
    [
        "type" => "GET",
        "end_point" => "/custom/lembur-on-periodes/show",
        "class" => "App\Services\Custom\LemburOnPeriodes\LemburOnPeriodesShow"
    ],
    [
        "type" => "POST",
        "end_point" => "/custom/lembur-on-periodes/create",
        "class" => "App\Services\Custom\LemburOnPeriodes\LemburOnPeriodesAdd"
    ],

    /*
    | LEMBUR ON PERIODE RECAPS
    */
    [
        "type" => "GET",
        "end_point" => "/custom/lembur-on-periode-recaps/list",
        "class" => "App\Services\Custom\LemburOnPeriodeRecaps\LemburOnPeriodeRecapsList"
    ],
    [
        "type" => "GET",
        "end_point" => "/custom/lembur-on-periode-recaps/show",
        "class" => "App\Services\Custom\LemburOnPeriodeRecaps\LemburOnPeriodeRecapsShow"
    ],
    [
        "type" => "PUT",
        "end_point" => "/custom/lembur-on-periode-recaps/update",
        "class" => "App\Services\Custom\LemburOnPeriodeRecaps\LemburOnPeriodeRecapsEdit"
    ],
    [
        "type" => "POST",
        "end_point" => "/custom/lembur-on-periode-recaps/create",
        "class" => "App\Services\Custom\LemburOnPeriodeRecaps\LemburOnPeriodeRecapsAdd"
    ],
    [
        "type" => "POST",
        "end_point" => "/custom/lembur-on-periode-recaps/do-sync-all",
        "class" => "App\Services\Custom\LemburOnPeriodeRecaps\LemburOnPeriodeRecapsDoSyncAll"
    ],
    /*
    | TUNJANGAN LEMBUR MAPPING UPAH LEMBUR
    */
    [
        "type" => "GET",
        "end_point" => "/custom/mapping-upah-lembur/list-job-position-categories",
        "class" => "App\Services\Custom\MappingUpahLembur\JobPositionCategoriesList"
    ],
    [
        "type" => "GET",
        "end_point" => "/custom/mapping-upah-lembur/show-job-position-categories",
        "class" => "App\Services\Custom\MappingUpahLembur\JobPositionCategoriesShow"
    ],
    [
        "type" => "GET",
        "end_point" => "/custom/mapping-upah-lembur/list",
        "class" => "App\Services\Custom\MappingUpahLembur\MappingUpahLemburList"
    ],
    [
        "type" => "GET",
        "end_point" => "/custom/mapping-upah-lembur/show",
        "class" => "App\Services\Custom\MappingUpahLembur\MappingUpahLemburShow"
    ],

    [
        "type" => "PUT",
        "end_point" => "/custom/mapping-upah-lembur/update",
        "class" => "App\Services\Custom\MappingUpahLembur\MappingUpahLemburEdit"
    ],

    /*
    | TUNJANGAN LEMBUR
    */

    [
        "type" => "GET",
        "end_point" => "/custom/tunjangan-lembur/show",
        "class" => "App\Services\Custom\TunjanganLembur\TunjanganLemburShow"
    ],

    [
        "type" => "POST",
        "end_point" => "/custom/tunjangan-lembur/create",
        "class" => "App\Services\Custom\TunjanganLembur\TunjanganLemburAdd"
    ],

    /*
    | TUNJANGAN LEMBUR DETAILS
    */

    [
        "type" => "GET",
        "end_point" => "/custom/tunjangan-lembur-details/list",
        "class" => "App\Services\Custom\TunjanganLemburDetails\TunjanganLemburDetailsList"
    ],
    [
        "type" => "GET",
        "end_point" => "/custom/tunjangan-lembur-details/show",
        "class" => "App\Services\Custom\TunjanganLemburDetails\TunjanganLemburDetailsShow"
    ],
    [
        "type" => "POST",
        "end_point" => "/custom/tunjangan-lembur-details/create",
        "class" => "App\Services\Custom\TunjanganLemburDetails\TunjanganLemburDetailsAdd"
    ],
    [
        "type" => "POST",
        "end_point" => "/custom/tunjangan-lembur-details/calculate",
        "class" => "App\Services\Custom\TunjanganLemburDetails\TunjanganLemburDetailsCalculate"
    ],


    /*
    | PINJAMAN BANK
    */

    [
        "type" => "GET",
        "end_point" => "/custom/pinjaman-bank/show",
        "class" => "App\Services\Custom\PinjamanBank\PinjamanBankShow"
    ],
    [
        "type" => "POST",
        "end_point" => "/custom/pinjaman-bank/create",
        "class" => "App\Services\Custom\PinjamanBank\PinjamanBankAdd"
    ],

    [
        "type" => "GET",
        "end_point" => "/custom/pinjaman-bank/card-summary-pinjaman",
        "class" => "App\Services\Custom\PinjamanBank\CardSummaryPinjaman"
    ],

    /*
    | PINJAMAN BANK DETAILS
    */
    [
        "type" => "GET",
        "end_point" => "/custom/pinjaman-bank-details/list",
        "class" => "App\Services\Custom\PinjamanBankDetails\PinjamanBankDetailsList"
    ],
    [
        "type" => "GET",
        "end_point" => "/custom/pinjaman-bank-details/show",
        "class" => "App\Services\Custom\PinjamanBankDetails\PinjamanBankDetailsShow"
    ],
    [
        "type" => "GET",
        "end_point" => "/custom/pinjaman-bank-details/show-month-before",
        "class" => "App\Services\Custom\PinjamanBankDetails\PinjamanBankDetailsShowMonthBefore"
    ],
    [
        "type" => "POST",
        "end_point" => "/custom/pinjaman-bank-details/create",
        "class" => "App\Services\Custom\PinjamanBankDetails\PinjamanBankDetailsAdd"
    ],
    [
        "type" => "POST",
        "end_point" => "/custom/pinjaman-bank-details/calculate",
        "class" => "App\Services\Custom\PinjamanBankDetails\PinjamanBankDetailsCalculate"
    ],

    /*
    | POTONGAN SANKSI
    */
    [
        "type" => "GET",
        "end_point" => "/custom/potongan-sanksi/show",
        "class" => "App\Services\Custom\PotonganSanksi\PotonganSanksiShow"
    ],
    [
        "type" => "POST",
        "end_point" => "/custom/potongan-sanksi/create",
        "class" => "App\Services\Custom\PotonganSanksi\PotonganSanksiAdd"
    ],


    /*
    | TUNJANGAN BEASISWA DETAILS
    */
    [
        "type" => "GET",
        "end_point" => "/custom/potongan-sanksi-details/list",
        "class" => "App\Services\Custom\PotonganSanksiDetails\PotonganSanksiDetailsList"
    ],
    [
        "type" => "GET",
        "end_point" => "/custom/potongan-sanksi-details/show",
        "class" => "App\Services\Custom\PotonganSanksiDetails\PotonganSanksiDetailsShow"
    ],
    [
        "type" => "POST",
        "end_point" => "/custom/potongan-sanksi-details/create",
        "class" => "App\Services\Custom\PotonganSanksiDetails\PotonganSanksiDetailsAdd"
    ],
    [
        "type" => "POST",
        "end_point" => "/custom/potongan-sanksi-details/calculate",
        "class" => "App\Services\Custom\PotonganSanksiDetails\PotonganSanksiDetailsCalculate"
    ],

    /*
    | CONTROL SHEET
    */
    [
        "type" => "GET",
        "end_point" => "/custom/control-sheet-payroll/list",
        "class" => "App\Services\Custom\ControlSheet\ControlSheetPayrollList"
    ],
    [
        "type" => "GET",
        "end_point" => "/custom/control-sheet-jurnal/list",
        "class" => "App\Services\Custom\ControlSheet\ControlSheetJurnalList"
    ],
    [
        "type" => "POST",
        "end_point" => "/custom/control-sheet-payroll/submit-lock",
        "class" => "App\Services\Custom\ControlSheet\SubmitAndLockPayrollControlSheet"
    ],
    [
        "type" => "POST",
        "end_point" => "/custom/control-sheet-payroll/submit-unlock",
        "class" => "App\Services\Custom\ControlSheet\SubmitAndUnlockPayrollControlSheet"
    ],

    [
        "type" => "GET",
        "end_point" => "/custom/control-sheet-payroll/summary-verifications",
        "class" => "App\Services\Custom\ControlSheet\ControlSheetVerificationsSummary"
    ],

    [
        "type" => "POST",
        "end_point" => "/custom/control-sheet-payroll/do-verifications",
        "class" => "App\Services\Custom\ControlSheet\ControlSheetDoVerifications"
    ],


    /*
    | PAYROLL JOURNALS CALCULATIONS
    */
    [
        "type" => "POST",
        "end_point" => "/custom/payroll-journals/do-calculation",
        "class" => "App\Services\Custom\PayrollJournals\PayrollJournalCalculation"
    ],

    [
        "type" => "GET",
        "end_point" => "/custom/payroll-journals/list",
        "class" => "App\Services\Custom\PayrollJournals\PayrollJournalList"
    ],

    /*
    | CONTROl SHEET LOGS
    */
    [
        "type" => "GET",
        "end_point" => "/custom/control-sheet-logs/list",
        "class" => "App\Services\Custom\ControlSheetLogs\ControlSheetLogsList"
    ],
    
    /*
    | DASHBOARD HRM
    */
    [
        "type" => "GET",
        "end_point" => "/custom/dashboard-hrm/employee-active",
        "class" => "App\Services\Custom\DashboardHrm\DashboardEmployeeActive"
    ],
    [
        "type" => "GET",
        "end_point" => "/custom/dashboard-hrm/employee-by-division",
        "class" => "App\Services\Custom\DashboardHrm\DashboardEmployeeByDivision"
    ],
    [
        "type" => "GET",
        "end_point" => "/custom/dashboard-hrm/employee-by-age",
        "class" => "App\Services\Custom\DashboardHrm\DashboardEmployeeByAge"
    ],
    [
        "type" => "GET",
        "end_point" => "/custom/dashboard-hrm/employee-by-status-employment",
        "class" => "App\Services\Custom\DashboardHrm\DashboardEmployeeByStatusEmployment"
    ],
    [
        "type" => "GET",
        "end_point" => "/custom/dashboard-hrm/employee-by-job-position-categories",
        "class" => "App\Services\Custom\DashboardHrm\DashboardEmployeeByJobPositionCategories"
    ],
    [
        "type" => "GET",
        "end_point" => "/custom/dashboard-hrm/employee-by-gender",
        "class" => "App\Services\Custom\DashboardHrm\DashboardEmployeeByGender"
    ],
    [
        "type" => "GET",
        "end_point" => "/custom/dashboard-hrm/employee-presence",
        "class" => "App\Services\Custom\DashboardHrm\DashboardEmployeePresence"
    ],


    /*
    | MY ACTIONS
    */
    [
        "type" => "GET",
        "end_point" => "/custom/my-actions/list",
        "class" => "App\Services\Custom\MyActions\MyActionList"
    ],

];
